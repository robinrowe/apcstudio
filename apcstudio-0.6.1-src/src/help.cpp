// $Id: help.cpp,v 1.7 2002/02/24 16:56:36 martinhenne Exp $

// (C) 2001 Martin Henne
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

/*
 * this class is aimed to provide online-help for several functions
 * by popping up text-windows with a SHORT helptext AND a suggestion
 * where to find deep information in the manual (chapter, side...)
 *
 * beside, this class should handle the commandline help also
 *
 * all should be static
 *
 */

#include <iostream>
#include <globals.h>
#include <help.h>
#include <manual.h>
#include <guiheaders.h>

namespace APC
{
using namespace std;
  /// This dumps commandline parameters and syntax to stdout.
  void Help::usage()
  {
    cout << "\n  usage: apcstudio <options> <file>\n\n";
    cout <<   "  Options must be specified before the filename/s. If no file\n";
    cout <<   "  is specified, aPcStudio checks the 'USE_LAST_FILE' option in\n";
    cout <<   "  the configfile. If this is 'no', the file-requester comes up.\n";
    cout <<   "  The files has to be RIFF-Wavefiles (*.wav).\n\n";
    cout <<   "  Options are:\n";
    cout <<   "  --version                 print version of aPcStudio and exit\n";
    cout <<   "  --no-bother               no bothering popup windows (ATTENTION: ALSO NO WARNINGS!!!)\n";
    cout <<   "  --help                    print this help text and exit\n";
    cout <<   "  --debug-level VALUE       set debug level to VALUE. (see README)\n";
    cout <<   "  --dont-show-tip           don't show tip, ignore configfile\n";
    cout <<   "  --draw-raster             draw raster behind wave, ignore configfile\n";
    cout <<   "  --dont-draw-raster        do not draw raster, ignore configfile\n";
    cout <<   "  --follow-play-cursor      view follows cursor while playing anyway\n";
    cout <<   "  --dont-follow-play-cursor view doesn't follow cursor while playing\n";
    cout <<   "  --use-last                use the last opened file, ignore configfile\n";
    cout <<   "  --dont-use-last           don't use the last opened file, ignore configfile\n";
//    cout <<   "  --newfile                 create an empty, new file";
    cout <<   "  -v                        short for '--version'\n";
    cout <<   "  -h                        short for '--help'\n";
    cout <<   "  -d VALUE                  short for '--debug-level'\n";
//    cout <<   "  -n                        short for '--newfile'\n";
    cout << endl;
  }

  void Help::manual()
  {
    Manual man;
    man.show();
    while(man.visible())
    {
      Fl::wait(0.1);
    }
  }

  void Help::chapter(const char* url)
  {
    Manual man;
    man.show();
    man.load(url);
    while(man.visible())
    {
      Fl::wait(0.1);
    }
  }


} // end of namespace

// end of file

