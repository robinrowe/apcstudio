// $Id: tips.h,v 1.1 2001/12/20 00:43:35 martinhenne Exp $

// (C) 2001 Martin Henne
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

// This class combines a Fl_Double_Window and a Apc_fl_waveedit widget
// with a Wavefile and provides several editing options. It is the actual
// main window of aPcStudio. Maybe there will never be another main
// windows, wich was planned first, but just another mainframe-class.

#ifndef _tips_h
#define _tips_h

#include <globals.h>
#include <string>
#include <vector>

namespace APC
{
  using namespace std;

  /** This class containes all 'tips of the day'. It
    * is aimed for use with 'TipOfTheDay' Class as
    * a component. See 'tipoftheday.h/fl'.
    */
  class Tips
  {
    vector<string> tips;
    static ulong count;
  public:
    Tips();
    ulong size();
    string get();                   // get next tip
    string get(const ulong& index); // get specific tip
  };

}

#endif

// end of file

