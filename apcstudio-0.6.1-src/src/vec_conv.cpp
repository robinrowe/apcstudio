// $Id: vec_conv.cpp,v 1.1 2002/02/12 02:47:08 martinhenne Exp $

// (C) 2002 Martin Henne
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

#include <globals.h>
#include <debug.h>
#include <popups.h>
#include <vec_conv.h>
#include <vector>
#include <iostream>

  std::vector<short>* Vec_conv::uchar2short (const std::vector<uchar>& source,
                                            const ulong& start,
                                            ulong& tlength)
  {
    using namespace std;
    cerr << "Vec_conv::uchar2short(): sorry, not implemented!\n";
    exit(1);

    const ulong slength = source.length(); // source length
    if(tlength>slength) tlength = slength; // trim target length, if vecessary
    vector<short>* return_vec = new vector<ushort>;
    vector<short>& target = *return_vec;
    for(ulong i=0; i<tlength; i+=2) {
      // TODO
    }
  }

// end of file

