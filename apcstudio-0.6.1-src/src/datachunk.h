// $Id: datachunk.h,v 1.16 2002/02/19 22:53:50 martinhenne Exp $

// (C) 2001 Martin Henne
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

/*
 * This class should be used inside a class,
 * that represents a wavefile in accompany
 * with a 'fmt ' chunk.
 */

#ifndef _datachunk_h
#define _datachunk_h

#include <globals.h>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <busywin.h>
#include <exception>

class no_fileaccess : public exception
{
public:
  no_fileaccess() {}
  ~no_fileaccess() {}
  const char* what() {return "no_fileaccess";}
};

namespace APC
{
using namespace std;
// forward:
class Wavefile;

  /// Containes the wavedata itself.
  /** The datachunk is that part of a wavefile, that containes
    * the wavedata itself. It has 8 leading bytes, followed
    * by a huge field of bytes, that are the wavedata.
    */
  class Data_chunk
  {
  // friend
  friend class Wavefile;

  private:
    /// first 4 bytes of the chunk: char values 'd''a''t''a' -> data
    string        ID;

    /// byt 5 to 8 in the chunk, containing a unsigned long wich holds the wavesize
    unsigned long chunkSize;

    /// This is the wavedata. It can be really big (up to 4 GB).
    vector<unsigned char> wave;

    /// This fstream is used for reading files.
    ifstream infile;

    /// This indicates, if the thread is reading a file at the moment.
    bool reading;

  public:
    Data_chunk() {/*cerr << "Data_chunk(): standard constructor\n";*/}
    explicit Data_chunk(string filename);      // call '::read()'
    ~Data_chunk() {/*cerr << "Data_chunk: destroyed\n";*/}

   /// How big is the wave (in bytes)?
    unsigned long     getchunkSize  () {return chunkSize;}

    /// Returns the vector of unsigned chars (the wave) by reference
    vector<unsigned char>& getwave  () {return wave;}

    /// Set the waveSize in byte. It's for creating new wavefiles.
    void              setchunkSize  (unsigned long   arg);

    /// Read the wavedata from a file. This usually takes time.
    /** A status bar poppes up while this function reads
      * the file to the memory. ::read() throws 'no_fileaccess'
      * if it can't read the file!
      */
    int               read          (string filename);
    static void*      read_thread   (void*);

    /// Read all bytes from a file and consider them as sampledata.
    void              read_raw      (const string filename);
    static void*      read_raw_thread (void*);

    /// Write the data chunk.
    int               write         (const string filename, Busywin* = NULL);


    /// This returnes the position of the bytes 'data' TODO: should be private
    int               findIDpos     (const string& filename) const;

    int               getSample     (const unsigned long& frame,
                                     const int& frameSize,
                                     const int& channels,
                                     const int& channel = 1);

    /// This returnes the amplitude between -1.0 and 1.0 of max. possible value
    double           getAmplitude   (const unsigned long& frame,
                                     const int& bitRate,
                                     const int& channels,
                                     const int& channel = 1);
    /// This sets the amplitude between -1.0 and 1.0 of the maximum possible value
    void             setAmplitude   (const double& amplitude,
                                     const unsigned long& frame,
                                     const int& bitRate,
                                     const int& channels,
                                     const int& channel = 1);

    void clear(); // clear data
  };
}

#endif


