// $Id: settings.cpp,v 1.16 2002/02/15 23:53:41 martinhenne Exp $

// (C) 2001 Martin Henne
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

#include <settings.h>
#include <string>
#include <iostream>
#include <configfile.h>
#include <commandline.h>
#include <debug.h>
#include <windowwrapper.h>

namespace APC
{
using std::string;
  int    Settings::MAIN_COLOR                  = 29;
  int    Settings::MAIN_DARKER_COLOR           = 7;
  int    Settings::FONT_COLOR                  = 0;
  int    Settings::NULL_LINE_COLOR             = 1;
  int    Settings::RASTER_COLOR                = 39;
  int    Settings::SELECTED_BACKGROUND_COLOR   = 15;
  int    Settings::SELECTED_WAVEDATA_COLOR     = 63;
  int    Settings::UNSELECTED_BACKGROUND_COLOR = 0;
  int    Settings::UNSELECTED_WAVEDATA_COLOR   = 41;
  int    Settings::CURSOR_COLOR                = 7;
  int    Settings::BUTTON_COLOR                = 29;
  int    Settings::INFOBOX_COLOR               = 7;
  int    Settings::INFOTEXT_COLOR              = 0;
  int    Settings::DIGIT_COLOR                 = 2;
  int    Settings::MARKS_COLOR                 = 2;
  int    Settings::MAX_UNDOS                   = 2;
  string Settings::AUDIO_DEVICE                = string("/dev/dsp");
  string Settings::REC_DEVICE                  = string("/dev/dsp");
  bool   Settings::FOLLOW_PLAY_CURSOR          = true;
  string Settings::DEFAULT_DIRECTORY           = "~/";
  string Settings::LAST_OPENED                 = "your_filename_here";
  bool   Settings::USE_LAST_OPENED             = false;
  bool   Settings::DISC_UNDO                   = false;
  string Settings::TEMP_DIR                    = string("/tmp");
  string Settings::HELP_DIR                    = string("/usr/share/doc/apcstudio");
  string Settings::MIXER_DIR                   = string("/usr/X11R6/bin/kmix");
  bool   Settings::DRAW_RASTER                 = true;
  bool   Settings::DRAW_DIGITS                 = false;
  void*  Settings::winwrapper                  = NULL;
  bool   Settings::SHOW_TIPOTD                 = true;
  bool   Settings::TIPOTD_SHOWN                = false;
  bool   Settings::SEL_PASTED                  = false;

  bool Settings::initialized = false;

  void Settings::read()
  {
    Debug::msg<string>("Settings::read(): called\n",Globals::DBG_FUNCTION_TRACE);
    Configfile::get("MAIN_COLOR"                 ,MAIN_COLOR);
    Configfile::get("MAIN_DARKER_COLOR"          ,MAIN_DARKER_COLOR);
    Configfile::get("FONT_COLOR"                 ,FONT_COLOR);
    Configfile::get("NULL_LINE_COLOR"            ,NULL_LINE_COLOR);
    Configfile::get("RASTER_COLOR"               ,RASTER_COLOR);
    Configfile::get("SELECTED_BACKGROUND_COLOR"  ,SELECTED_BACKGROUND_COLOR);
    Configfile::get("SELECTED_WAVEDATA_COLOR"    ,SELECTED_WAVEDATA_COLOR);
    Configfile::get("UNSELECTED_BACKGROUND_COLOR",UNSELECTED_BACKGROUND_COLOR);
    Configfile::get("UNSELECTED_WAVEDATA_COLOR"  ,UNSELECTED_WAVEDATA_COLOR);
    Configfile::get("CURSOR_COLOR"               ,CURSOR_COLOR);
    Configfile::get("BUTTON_COLOR"               ,BUTTON_COLOR);
    Configfile::get("INFOBOX_COLOR"              ,INFOBOX_COLOR);
    Configfile::get("INFOTEXT_COLOR"             ,INFOTEXT_COLOR);
    Configfile::get("DIGIT_COLOR"                ,DIGIT_COLOR);
    Configfile::get("MARKS_COLOR"                ,MARKS_COLOR);
    Configfile::get("MAX_UNDOS"                  ,MAX_UNDOS);
    Configfile::get("AUDIO_DEVICE"               ,AUDIO_DEVICE);
    Configfile::get("REC_DEVICE"                 ,REC_DEVICE);
    Configfile::get("FOLLOW_PLAY_CURSOR"         ,FOLLOW_PLAY_CURSOR);
    Configfile::get("DEFAULT_DIRECTORY"          ,DEFAULT_DIRECTORY);
    Configfile::get("LAST_OPENED"                ,LAST_OPENED);
    Configfile::get("USE_LAST_OPENED"            ,USE_LAST_OPENED);
    Configfile::get("DISC_UNDO"                  ,DISC_UNDO);
    Configfile::get("TEMP_DIR"                   ,TEMP_DIR);
    Configfile::get("DRAW_RASTER"                ,DRAW_RASTER);
    Configfile::get("DRAW_DIGITS"                ,DRAW_DIGITS);
    Configfile::get("SHOW_TIPOTD"                ,SHOW_TIPOTD);
    Configfile::get("HELP_DIR"                   ,HELP_DIR);
    Configfile::get("MIXER_DIR"                  ,MIXER_DIR);
    Configfile::get("SEL_PASTED"                 ,SEL_PASTED);

    if(Commandline::check_for("0,follow-play-cursor"))
      FOLLOW_PLAY_CURSOR = true;
    if(Commandline::check_for("0,dont-follow-play-cursor"))
      FOLLOW_PLAY_CURSOR = false;
    if(Commandline::check_for("0,draw-raster"))
      DRAW_RASTER = true;
    if(Commandline::check_for("0,dont-draw-raster"))
      DRAW_RASTER = false;
    if(Commandline::check_for("0,show-tip"))
      SHOW_TIPOTD = true;
    if(Commandline::check_for("0,dont-show-tip"))
      SHOW_TIPOTD = false;
    if(Commandline::check_for("0,use-last"))
      USE_LAST_OPENED = true;
    if(Commandline::check_for("0,dont-use-last"))
      USE_LAST_OPENED = false;

    Debug::msg<string>("Settings::read(): leaving\n",Globals::DBG_FUNCTION_TRACE);
  }

  void Settings::write()
  {
    Configfile::set("MAIN_COLOR"                 ,MAIN_COLOR);
    Configfile::set("MAIN_DARKER_COLOR"          ,MAIN_DARKER_COLOR);
    Configfile::set("FONT_COLOR"                 ,FONT_COLOR);
    Configfile::set("NULL_LINE_COLOR"            ,NULL_LINE_COLOR);
    Configfile::set("RASTER_COLOR"               ,RASTER_COLOR);
    Configfile::set("SELECTED_BACKGROUND_COLOR"  ,SELECTED_BACKGROUND_COLOR);
    Configfile::set("SELECTED_WAVEDATA_COLOR"    ,SELECTED_WAVEDATA_COLOR);
    Configfile::set("UNSELECTED_BACKGROUND_COLOR",UNSELECTED_BACKGROUND_COLOR);
    Configfile::set("UNSELECTED_WAVEDATA_COLOR"  ,UNSELECTED_WAVEDATA_COLOR);
    Configfile::set("CURSOR_COLOR"               ,CURSOR_COLOR);
    Configfile::set("BUTTON_COLOR"               ,BUTTON_COLOR);
    Configfile::set("INFOBOX_COLOR"              ,INFOBOX_COLOR);
    Configfile::set("INFOTEXT_COLOR"             ,INFOTEXT_COLOR);
    Configfile::set("DIGIT_COLOR"                ,DIGIT_COLOR);
    Configfile::set("MARKS_COLOR"                ,MARKS_COLOR);
    Configfile::set("MAX_UNDOS"                  ,MAX_UNDOS);
    Configfile::set("AUDIO_DEVICE"               ,AUDIO_DEVICE);
    Configfile::set("REC_DEVICE"                 ,REC_DEVICE);
    Configfile::set("FOLLOW_PLAY_CURSOR"         ,FOLLOW_PLAY_CURSOR);
    Configfile::set("DEFAULT_DIRECTORY"          ,DEFAULT_DIRECTORY);
    Configfile::set("LAST_OPENED"                ,LAST_OPENED);
    Configfile::set("USE_LAST_OPENED"            ,USE_LAST_OPENED);
    Configfile::set("DISC_UNDO"                  ,DISC_UNDO);
    Configfile::set("TEMP_DIR"                   ,TEMP_DIR);
    Configfile::set("DRAW_RASTER"                ,DRAW_RASTER);
    Configfile::set("DRAW_DIGITS"                ,DRAW_DIGITS);
    Configfile::set("SHOW_TIPOTD"                ,SHOW_TIPOTD);
    Configfile::set("HELP_DIR"                   ,HELP_DIR);
    Configfile::set("MIXER_DIR"                  ,MIXER_DIR);
    Configfile::set("SEL_PASTED"                 ,SEL_PASTED);
  }

  void Settings::reset()
  {
    MAIN_COLOR                  = 22;
    MAIN_DARKER_COLOR           = 30;
    FONT_COLOR                  = 0;
    NULL_LINE_COLOR             = 1;
    RASTER_COLOR                = 39;
    SELECTED_BACKGROUND_COLOR   = 15;
    SELECTED_WAVEDATA_COLOR     = 2;
    UNSELECTED_BACKGROUND_COLOR = 0;
    UNSELECTED_WAVEDATA_COLOR   = 41;
    CURSOR_COLOR                = 7;
    BUTTON_COLOR                = 29;
    INFOBOX_COLOR               = 21;
    INFOTEXT_COLOR              = 7;
    DIGIT_COLOR                 = 2;
    MARKS_COLOR                 = 2;
    MAX_UNDOS                   = 2;
    AUDIO_DEVICE                = string("/dev/dsp");
    REC_DEVICE                  = string("/dev/dsp");
    FOLLOW_PLAY_CURSOR          = true;
    DEFAULT_DIRECTORY           = "~/";
    USE_LAST_OPENED             = false;
    DISC_UNDO                   = false;
    TEMP_DIR                    = string("/tmp");
    DRAW_RASTER                 = true;
    DRAW_DIGITS                 = false;
    SHOW_TIPOTD                 = true;
    HELP_DIR                    = string("/usr/share/doc/apcstudio");
    MIXER_DIR                   = string("/usr/X11R6/bin/kmix");
    SEL_PASTED                  = false;
  }

  void Settings::init_wrapper(void* ww)
  {
    winwrapper = ww; // needs cast, if used!
  }

  void Settings::apply_colors()
  {
    if(winwrapper!=NULL)
    {
      Windowwrapper* ww = static_cast<Windowwrapper*>(winwrapper);
      ww->apply_colors();
    }
    else
      return;
  }


  void Settings::clear_undo_buf()
  {
    if(winwrapper!=NULL)
    {
      Windowwrapper* ww = static_cast<Windowwrapper*>(winwrapper);
      ww->clear_undo_buf();
    }
    else
      return;
  }


} // end of namespace

// end of file
