// $Id: windowwrapper.h,v 1.5 2002/02/15 23:53:41 martinhenne Exp $

// Copyright 2001 Martin Henne 2001
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

#ifndef _windowwrapper_h
#define _windowwrapper_h

#include <vector>
#include <editwindow.h>

/// This class handles pointers to all Editwindows.
class Windowwrapper
{
private:
  /// This vector has pointers to all Editwindows.
  std::vector<APC::Editwindow*> vec;

  /// This vector marks, which window is active and visible.
  std::vector<bool> visible;

  /// See, if there's a 'hole' in 'vec', before resizing it.
  /**
    *  Holes can occure, when a window is closed. When user
    *  openes a new window, we can use this pointer again,
    *  instead of resizing the vector.
    *
    *  This returnes 'true' if a hole was found. 'index'
    *  holds the index.
    */
  bool find_unused_index(ulong& index) const;

public:
  Windowwrapper(){}
  explicit Windowwrapper(vector<APC::Editwindow*>);
  ~Windowwrapper(){}

  /// Registrate a new window (add a pointer), return index in 'vec'.
  ulong registrate(APC::Editwindow*);

  /// Delete a pointer at vec[index] (=unregistrate window).
  void unregistrate(ulong index);

  /// Apply colorsettings to all windows.
  void apply_colors();

  /// clear all undo buffers
  void clear_undo_buf();

};

#endif
