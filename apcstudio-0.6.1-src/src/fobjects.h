// $Id: fobjects.h,v 1.3 2001/12/07 17:03:58 martinhenne Exp $

// (C) 2001 Martin Henne
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

/*
 * This file containes function objects. overloaded
 * application operators are implemented inline. All
 * fobjects should be used in STL algorithms or in
 * modified algorithms.
 *
 */

#ifndef _fobjects_h
#define _fobjects_h

#include <string>
#include <iostream>
#include <filehandle.h>

namespace APC
{
using std::string;
using std::cerr;
/*
  // this one is just to test the syntax
  template<class T>
  struct FOTest {
    // count function calls
    static unsigned long counter;
    FOTest& operator()(const T& arg1, const T& arg2) {
      ++counter;
      return *this;
    }
  };
  unsigned long FOTest<unsigned long>::counter = 0;
  */


  // Test, if file exists!
  struct FOisFile
  {
    FOisFile():counter(0UL){}
    unsigned long counter;
    FOisFile& operator()(string& arg)
    {
      std::cerr << "FOisFile: called\n";
      if(!FileHandle::exists(arg))
      {
        std::cerr << "FOisFile: file " << arg << "doesn't exist!\n";
        arg.erase();
      }
      return *this;
    }
  };
}

#endif














