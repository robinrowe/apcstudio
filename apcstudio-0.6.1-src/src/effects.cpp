// $Id: effects.cpp,v 1.19 2002/02/18 17:15:23 martinhenne Exp $

// (C) 2001 Martin Henne
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

/*
 * Effects class for the various effects such as reversing a wavefile,
 * fading, noise gate, etc....
 */


#include <globals.h>
#include <debug.h>
#include <convert.h>
#include <datachunk.h>
#include <wavefile.h>
#include <effects.h>
#include <statusbar.h>
#include <editwindow.h>
#include <lendian.h>
#include <popups.h>
#include <askecho.h>
#include <copy.h>
#include <mixpaste.h>
#include <busywin.h>
#include <fader.h>
#include <noisegate.h>

namespace APC
{ 
  
  // reverse effect by Arne Dueselder
  void Effects::reverse()
  {
    //cerr << "Effects::reverse(): called" << endl;
    Data_chunk& datac             = ew->wfile->getdata();     // get the wave data from data chunk
    vector<unsigned char>&   wd   = datac.getwave();

    short         fs              = ew->frameSize;
    unsigned long e               = fs*ew->we->getmarkEnd();  // where does the selection start
    unsigned long s               = fs*ew->we->getmarkStart();// where does the selection end
    unsigned long r               = (e-s);
    //r                            -= r%fs;

    vector<unsigned char> tmpwd(fs);                          // temporary vector to hold 
                                                              // the wavedate during the reverse 
                                                              // shifting, size according to framesize
//    cerr << "start, end, range, framesize: " << s << ", " << e << ", " << r << ", " << fs << endl;

    for (unsigned long i=s; i<=(s+(r/2)); i+= fs)
    {
      for (short ii=0; ii<fs; ii++)
      {
        tmpwd[ii]                 = wd[i+ii];
        wd[i+ii]                  = wd[s+r-i+ii+s];
        wd[s+r-i+ii+s]            = tmpwd[ii];
      }
    }

    return;
  }


// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  // this uses the volume_ramp function
  void Effects::fade(double startFactor, double endFactor, const int& channel)
  {
    startFactor /= 100.0;
    endFactor /= 100.0;
    Data_chunk& datac      = ew->wfile->getdata();
    vector<unsigned char>&   wave = datac.getwave();
    const int& frameSize = ew->frameSize;
    ulong startFrame = ew->we->getmarkStart();
    ulong endFrame = ew->we->getmarkEnd();

    fader()(wave,
            startFrame,
            endFrame,
            startFactor,
            endFactor,
            ew->bitRate,
            ew->channels,
            frameSize,
            channel);
    return;
  }

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  void Effects::noiseGate(double min_level, const int& attack_time, const int& channel)
  {
    Debug::msg<string>("Effects::noiseGate(): called to mute below ",Globals::DBG_FUNCTION_TRACE);
    Debug::val_cr(min_level),Globals::DBG_FUNCTION_TRACE;
    min_level /= 100.0; // from percent to 0.0-1.0
    Data_chunk& datac      = ew->wfile->getdata();    // get the wave data from data chunk
    vector<unsigned char>&   wave = datac.getwave();
    const int& frameSize = ew->frameSize;

    noisegate()(wave,
                ew->we->getmarkStart(),
                ew->we->getmarkEnd(),
                min_level,
                attack_time,
                ew->bitRate,
                ew->channels,
                frameSize,
                ew->frequency,
                channel);
    return;
  }





  void Effects::tremolo      (const unsigned long& markStart,
                              const unsigned long& markEnd,
                              Wavefile* wfile,
                              const int& channels,
                              const int& bitRate,
                              const int& frequency)
  {
    // TODO
  }





  void Effects::volume_ramp (vector<unsigned char>& data,
                          const ulong& startframe,
                          const ulong& endframe,
                          const double& startfactor,
                          const double& endfactor,
                          const int& bitRate,
                          const int& channels,
                          const int& frameSize,
                          const int& channel)
  {
    fader()(data,
            startframe,
            endframe,
            startfactor,
            endfactor,
            bitRate,
            channels,
            frameSize,
            channel);
    return;
  }




  void Effects::echo        (Editwindow* ew,
                             int amount,
                             int delay,
                             int falloff,
                             int volume,
                             bool wet)
  {
    Debug::msg<string>("Effects::echo(): called\n",Globals::DBG_FUNCTION_TRACE);

    // get information from user, if it is not passed:
    if(amount==0) {
      Askecho ae;
      amount = 2;
      delay  = 180;
      falloff  = 65;
      volume = 45;
      ae.ask(amount,delay,falloff,volume,wet);
    }

    if(amount==0) {
      return;
    }

    // tell the user, that we are busy:
    Busywin busy("Echoing...");
    busy.show(); Fl::check(); Fl::wait(0.1); Sleeper::dosleep(10);

    // save undo-data
    busy.tell("saving undo data..."); Fl::wait(0.2); Sleeper::dosleep(10); Fl::check();
    if(!ew->push_undo()) {
      busy.hide();
      return;
    }

    busy.tell("ok, now echoing..."); Fl::wait(0.2); Sleeper::dosleep(10); Fl::check();
    // get information about the data:
    Data_chunk& datac      = ew->wfile->getdata();
    vector<unsigned char>&   data = datac.getwave();
    const int& frameSize = ew->frameSize;
    const int& bitRate   = ew->bitRate;
    const int& channels  = ew->channels;
    const int& frequency = ew->frequency;
    unsigned long startframe = ew->we->getmarkStart();
    unsigned long endframe   = ew->we->getmarkEnd();
    const int delay_frames  = static_cast<int>(frequency/1000.0*delay);

    // reserve temporary buffer for echoes
    busy.tell("reserving echo buffer..."); Fl::check();
    vector<uchar> echobuf;
    // copy selection to echobuffer
    busy.tell("copying echo data..."); Fl::check();
    Copy::copy(data,echobuf,startframe,endframe,frameSize);
    // retune echobuffer to initial volume
    busy.tell("retuning echo buffer..."); Fl::check();
    fader()(echobuf,
            0,
            echobuf.size()-frameSize,
            volume/100.0,
            volume/100.0,
            bitRate,channels,frameSize);

    // now apply 'amount' echoes:
    ulong echopos = startframe;
    for(int i=0; i<amount; ++i) {
      echopos += delay_frames;
      busy.tell("mixing echo buffer into wave..."); Fl::check();
      Mixpaste::overlay(data,
                        echobuf,
                        echopos,
                        bitRate,channels,frameSize,
                        1.0); // insert echo

      if((i+1)<amount) { // retune only, if echobuffer is needed one more time!
        busy.tell("retuning echo buffer..."); Fl::check();
        fader()(echobuf,
                0,
                echobuf.size()-frameSize,
                falloff/100.0,
                falloff/100.0,
                bitRate,channels,frameSize); // retune echobuffer
      }
    }
    echobuf.clear();

    ew->relabel("echo applied","statusBox_status");
    ew->data_changed();
    ew->redraw_needed |= (ew->REDR_WAVE);
    ew->doRedraw();
    ew->adjustScrollbar();

    busy.hide();

    Debug::msg<string>("Effects::echo(): leaving\n",Globals::DBG_FUNCTION_TRACE);
  }


}

// end of file
