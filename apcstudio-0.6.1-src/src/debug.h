// $Id: debug.h,v 1.8 2002/01/24 00:27:05 martinhenne Exp $

// (C) 2001 Martin Henne
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.


#ifndef _debug_h
#define _debug_h

/* if we want no debugging in the release
 * version, we can set this flag. The most
 * often called function Debug::msg<T> is
 * inline and no code will be inserted or
 * compiled, when calling it, if this flag
 * is set. */

//#define NODEBUG

#include <globals.h>
#include <string>
#include <ctime>

namespace APC
{
using namespace std;
  /// This dumps debugmessages and takes care of the debuglevel.
  /**
    *  Use it like this:
    *
    *  Debug::msg<string>("The answer is 42",DEBUGLEVEL_1);
    *
    *  If you want a message in case of any debuglevel != 0, use
    *
    *  Debug::msg<string>("debugmessage");
    *
    *  To use timer, call ::start_timer() and ::stop_timer()
    *
    *  when calling ::stop_timer(), the meassured time is
    *  written to stderr at any debug-level
    */
  class Debug
  {
    static clock_t start;
    static clock_t end;
    static unsigned long debug_level;
    static unsigned long msg_counter;
  public:
    inline static void msg(const int&, const int& level=1);
    template<class T> inline static void msg(const T& text, const int& level=1);
    static void val(const int& val, const int& level=1);
    static void val(const double& val, const int& level=1);
    static void val_cr(const int& val, const int& level=1);
    static void val_cr(const double& val, const int& level=1);
    static void init(const unsigned long& level);
    static void newline();
    static void start_timer();
    static void stop_timer();
  };


  template<class T> inline void Debug::msg(const T& text, const int& level)
  {
    #ifndef NODEBUG
    if(debug_level&level != 0)
    {
      cerr << "<" << ++msg_counter << ">";
      cerr << static_cast<string>(text);
    }
    #endif
  }


  inline void Debug::msg(const int& value, const int& level)
  {
    #ifndef NODEBUG
    if(debug_level&level != 0)
    {
      cerr << "<" << ++msg_counter << ">";
      cerr << value;
    }
    #endif
  }



}

#endif


