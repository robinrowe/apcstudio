// $Id: record.h,v 1.5 2002/02/01 17:42:49 martinhenne Exp $

// (C) 2001 Martin Henne
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

// This class is to record sound from the sound device.
// It is a part of the aPcStudio Wavefile editing software
// for Linux and Windows.

#ifndef _record_h
#define _record_h

#include <globals.h>
#include <vector>
#include <iostream>

//#include <editwindow.h>
//using namespace APC;

namespace APC
{
using namespace std;
  /// forward declaration
  class Editwindow;

  /// This class records a soundsample from various sources.
  class Record
  {
  private:
    int frequency;
    int bitRate;
    int frameSize;
    int channels;
    unsigned long waveSize;

    /// selection start is rec_start
    unsigned long rec_start;

    /// record from rec_start to selection end (=rec_end)
    unsigned long rec_end;

    /// This is a handle to the window.
    Editwindow* ew;

    /// 'mem' containes the sampled data.

    /// file descriptor
    int audio_fd;

    /// audio device (e.g. '/dev/dsp') <- from configfile
    string device;

  public:
    Record();

    void init(Editwindow*);

    /// record to selected area
    void record_selection      ();
    static void* record_selection_thread(void*);

    /// record a new sound sample
    void record_new            ();

    /// are we recording at the moment (or paused while recording)?
    bool recording;

    /// are we paused while recording?
    bool paused;

    /// record
    void record                ();
  };

}


#endif



