// $Id: lendian.h,v 1.7 2002/02/17 16:45:23 martinhenne Exp $

// Copyright 2001 Martin Henne 2001
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

/*
 * This functions are useful on little endian
 * machines. They convert a set of 8bit vals
 * in little endian order (from the lsb to the
 * msb) to a single int value.
 *
 * note: a 'int' is 4 byte long and signed (= signed long)
 *       a 'short' has 2 bytes and is signed
 *       a 'char' has 1 byte and is signed
 *
 */



#ifndef _lendian_h
#define _lendian_h

#include <globals.h>

namespace APC
{
  /// Convert two unsigned chars to a signed short. (16bit samples)
  void lendian (const unsigned char& lowbyte,
                const unsigned char& highbyte,
                short& result);

  /// Convert three unsigned chars to a signed int. (24bit samples)
  void lendian (const unsigned char& lsb,
                const unsigned char& middle,
                const unsigned char& msb,
                int& result);

  /// Convert four unsigned chars to a signed int. (32bit samples)
  void lendian (const unsigned char& lsb,
                const unsigned char& low,
                const unsigned char& high,
                const unsigned char& msb,
                int& result);

  /// Get Lowbyte of Short.
  void getshortsLow  (short, unsigned char&);
  unsigned char getshortsLow (short);

  /// Get Highbyte of Short.
  void getshortsHigh (short, unsigned char&);
  unsigned char getshortsHigh(short);
}

#endif

