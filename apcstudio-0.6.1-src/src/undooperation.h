// $Id: undooperation.h,v 1.3 2002/02/12 17:29:16 martinhenne Exp $

// Copyright 2001 Martin Henne 2001
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

#ifndef _undooperation_h
#define _undooperation_h

#include <globals.h>
#include <vector>
#include <string>

namespace APC
{
using std::vector;
using std::string;
class Editwindow;

  class Undo_operation
  {
  public:
    Undo_operation(const int& _frequency,
                   const int& _channels,
                   const int& _bitRate,
                   const ulong& _markStart,
                   const ulong& _markEnd,
                   const ulong& _drawStart,
                   const ulong& _cursorPosition,
                   const double& _zoomFactor,
                   const vector<uchar>& _old_data);
    ~Undo_operation()
    {
      delete undo_data; undo_data = NULL;
    }
    /// Perform undo-operation.
    bool perform(Editwindow* ew);
  private:
    /// Kind of Operation to perform.
    /**
      *  "cut"     -> undo's 'paste'
      *  "paste"   -> undo's 'cut'
      *  "replace" -> undo's transforming operations (default)
      *  "reverse" -> undo's 'reverse'
      *
      *  Everyone, who writes a method, that changes the
      *  wavedata, should write a apropriate undo-operation.
      *
      */
    int frequency;
    int channels;
    int bitRate;
    ulong markStart;
    ulong markEnd;
    double zoomFactor;
    ulong drawStart;
    ulong cursorPosition;
    vector<uchar>* undo_data;
  };

} // end of namespace

#endif

// end of file


