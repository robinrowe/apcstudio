// $Id: popups.h,v 1.11 2002/02/24 16:56:36 martinhenne Exp $

// (C) 2001 Martin Henne
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

/*
 *  This is a class for popup windows that ask a question
 *  or inform about an error or warn about something.
 *  The popups look much like M$-Stuff (sorry)
 */


/*#include <FL/Fl_Window.H>
#include <FL/Fl_Input.H>
*/
#include <guiheaders.h>

#include <globals.h>
#include <iostream>

#ifndef _popups_h
#define _popups_h


namespace APC
{
using namespace std;
  /// This class has a few popup windows for messages, questions...
  class Popups
  {
  private:
    // don't create objects of this class
    // it is aimed for static usage
    Popups();
    static bool ask_bool;

  public:
    virtual ~Popups();
    
    // popup-windows

    /// Ask for something. Answer (yes, no) is returned as a bool.
    static bool ask              (string question, string wintitle="I have a question!");

    /// Inform about an error. 
    static void error            (string errormsg, string wintitle="Error!");

    /// Tell user about a normal action.
    static void tell             (string msg, string wintitle="You should know:");
    static void warn             (string msg, string wintitle="Warning!");

    /// Ask for an input. Value is returned as a string.
    static string input          (string question, string wintitle);

    // TODO: remove this, as it is a single class now
    /// Unused. This has been replaced by the class 'Statusbar'
    static void statusBar        (string message, 
                                  unsigned short percent, // 0-100%
                                  const char* wish);      // "NEW", "UPDATE", "CLOSE"
    
    /** @name callbacks
     * Callback functions, to handle buttons in popup windows.
     */
    //@{
    static void cb_ask_true      (Fl_Widget*, void*);
    static void cb_ask_false     (Fl_Widget*, void*);
    static void cb_error         (Fl_Widget*, void*);
    static void cb_input_ok      (Fl_Widget*, void*);
    static void cb_input_cancel  (Fl_Widget*, void*);
    static void cb_input         (Fl_Widget*, void*);
    //@}
  }; // end of class

} // end of namespace

#endif

// end of file

