// $Id: noisegate.cpp,v 1.4 2002/02/18 17:15:23 martinhenne Exp $

// (C) 2002 Martin Henne
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

#include <noisegate.h>
#include <debug.h>
#include <globals.h>
#include <fader.h>
#include <lendian.h>
#include <mute.h>
#include <mstoframe.h>
#include <getamplitude.h>
#include <setamplitude.h>
#include <vector>
#include <algorithm>
#include <iostream>

typedef std::vector<double>::iterator iterator;

// unfinished!
void noisegate::operator()(std::vector<uchar>& data,
                  const ulong& startframe,
                  const ulong& endframe,
                  const double& min_level,
                  const int& attack_time,
                  const int& bitRate,
                  const int& channels,
                  const int& frameSize,
                  const int& frequency,
                  const int& channel)
  {
    using namespace APC;
    Debug::msg<std::string>("noisegate()(): called\n",Globals::DBG_FUNCTION_TRACE);
    const ulong waveSize      = data.size();
    const ulong attack_frames = mstoframe()(attack_time,frequency);
    const ulong allFrames = waveSize/frameSize;
    mute::counter = 0; // reset function counter for 'mute()()'

    // do nothing, if wave is shorter than 2*attack_time
    if(waveSize <= (2*attack_frames*frameSize)) {
      Debug::msg<string>("noisegate()(): wave is too small for that attack time. returning.\n");
      return;
    }

    // define possible fragment states
    enum fragment_state {
      FADEIN = 0, // this is the fragment after a muted section
      FADEOUT,    // this is the fragment before a muted section
      MUTE,       // all values in this fragment will be muted
      NORMAL      // values in this fragment stay untouched
    };

    fragment_state state        = NORMAL;
    fragment_state former_state = NORMAL;
    std::vector<double> fragment;
    iterator min_max_iter;
    double minimum = 0.0;
    double maximum = 0.0;

    if(channel==1 || channel==0) { // left channel
      for(ulong i=startframe; i<endframe; i+=attack_frames) {
        fragment.clear();
        for(ulong ii=i; ii<(i+attack_frames) && ii<allFrames; ++ii) {
          fragment.push_back(getamplitude()(data,ii,bitRate,channels,1));
        } // we have a filled fragment

        min_max_iter = min_element(fragment.begin(), fragment.end());
        if(min_max_iter!=fragment.end()) minimum = *(min_max_iter);
        min_max_iter = max_element(fragment.begin(), fragment.end());
        if(min_max_iter!=fragment.end()) maximum = *(min_max_iter);

        if(minimum>(-1*min_level) && maximum<min_level) {
          switch(former_state) {
            case MUTE    : state = MUTE;    break;
            case FADEIN  : state = FADEOUT; break;
            case FADEOUT : state = MUTE;    break;
            case NORMAL  : state = FADEOUT; break;
          }
        }
        else {
          switch(former_state) {
            case MUTE    : state = FADEIN; break;
            case FADEIN  : state = NORMAL; break;
            case FADEOUT : state = FADEIN; break;
            case NORMAL  : state = NORMAL; break;
          }
        }
        if(state==MUTE) {
          mute()(data,
                 i,
                 i+attack_frames,
                 bitRate,channels,frameSize,
                 1);
        }
        if(state==FADEIN) {
          fader()(data,
                  i,
                  i+attack_frames,
                  0,1.0,
                  bitRate, channels, frameSize,
                  1);
        }
        if(state==FADEOUT) {
          fader()(data,
                  i,
                  i+attack_frames,
                  1.0,0,
                  bitRate, channels, frameSize,
                  1);
        }
        former_state = state;
      } // all fragments processed
    } // end of left or mono processing

    if((channel==2 || channel==0) && channels==2) { // right channel, if stereo
      for(ulong i=startframe; i<endframe; i+=attack_frames) {
        fragment.clear();
        for(ulong ii=i; ii<(i+attack_frames) && ii<allFrames; ++ii) {
          fragment.push_back(getamplitude()(data,ii,bitRate,channels,2));
        } // we have a filled fragment

        min_max_iter = min_element(fragment.begin(), fragment.end());
        if(min_max_iter!=fragment.end()) minimum = *(min_max_iter);
        min_max_iter = max_element(fragment.begin(), fragment.end());
        if(min_max_iter!=fragment.end()) maximum = *(min_max_iter);

        if(minimum>(-1*min_level) && maximum<min_level) {
          switch(former_state) {
            case MUTE    : state = MUTE;    break;
            case FADEIN  : state = FADEOUT; break;
            case FADEOUT : state = MUTE;    break;
            case NORMAL  : state = FADEOUT; break;
          }
        }
        else {
          switch(former_state) {
            case MUTE    : state = FADEIN; break;
            case FADEIN  : state = NORMAL; break;
            case FADEOUT : state = FADEIN; break;
            case NORMAL  : state = NORMAL; break;
          }
        }
        if(state==MUTE) {
          mute()(data,
                 i,
                 i+attack_frames,
                 bitRate,channels,frameSize,
                 2);
        }
        if(state==FADEIN) {
          fader()(data,
                  i,
                  i+attack_frames,
                  0,1.0,
                  bitRate, channels, frameSize,
                  2);
        }
        if(state==FADEOUT) {
          fader()(data,
                  i,
                  i+attack_frames,
                  1.0,0,
                  bitRate, channels, frameSize,
                  2);
        }
        former_state = state;
      } // all fragments processed
    } // end of right channel processing





    Debug::msg<std::string>("noisegate()(): leaving\n",Globals::DBG_FUNCTION_TRACE);
  } // end of ::operator()

// end of file
