// $Id: overlay.h,v 1.5 2001/12/16 14:22:37 martinhenne Exp $

// (C) 2001 Martin Henne
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.


#ifndef _overlay_h
#define _overlay_h

#include <globals.h>

/*
#include <FL/Fl_Overlay_Window.H>
#include <FL/Fl_Gl_Window.H>
*/
#include <guiheaders.h>

namespace APC
{
  class Editwindow;
  class Apc_fl_waveedit;

  class Overlay : public Fl_Overlay_Window
  {
  public:
    Overlay(int x, int y, int w, int h, const char* label);
    Overlay(int w, int h, const char* label);
    void draw_overlay ();
    int cursorcolor;
    int X;
    Editwindow* t_ew;
    Apc_fl_waveedit* t_we;
  };


}

#endif


