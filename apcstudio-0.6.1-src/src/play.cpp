// $Id: play.cpp,v 1.33 2002/02/21 21:53:34 martinhenne Exp $

// (C) 2001 Martin Henne
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

/*
 *  look 'play.h' for a description
 *
 */

//#define WIN32

#include <play.h>

#ifdef WIN32
typedef int pid_t;
#endif

#include <string>
#include <iostream>
#include <vector>
#include <wavefile.h>
#include <fmtchunk.h>
#include <datachunk.h>
#include <configfile.h>
#include <apc_fl_waveedit.h>
#include <sleeper.h>
#include <settings.h>
#include <popups.h>
#include <debug.h>
#include <pthread.h>

//#include <sys/socket.h>

#ifndef WIN32
  // OSS:
  #include <sys/ioctl.h>
  #include <fcntl.h>
  #include <sys/soundcard.h>
#endif

// this is the internal buffersize, NOT the audio
// buffersize!!! I'm unsure about the effect, that
// THIS buffersize has to the playing quality.
// TODO: check out, wether this value effects anything or not
//#define BUF_SIZE 64
#define BUF_SIZE 16

namespace APC
{
using namespace std;
#ifndef WIN32

bool Play::playing;
bool Play::paused;

//----------------------------------------------------------------------------//

  // after all settings are done, now loop playing in a single thread:
  void* Play::playOSSThread (void* arg)
  {
    Debug::msg<string>("Play::playOSSThread(): called\n",Globals::DBG_FUNCTION_TRACE);
    bool skipped_selected = false; // is true, when cursor skipped selection to play unsel. only
    Play* o = (Play*)(arg);
    Apc_fl_waveedit* we = static_cast<Apc_fl_waveedit*>(o->myHandle);

    Data_chunk& datac = o->myWave->getdata();       // handle to the datachunk
    vector<unsigned char>& wdata = datac.getwave(); // get the wavedata
    const unsigned long wavesize = wdata.size();


    //------ now play ------//
    int len = 0;

    unsigned char audio_buffer[BUF_SIZE];

    // copy audiodata from wavefile to buffer and play buffer
    int buffers_needed = (((o->playend)-(o->playstart))/BUF_SIZE)+1;
    unsigned long   end    = 0;
    unsigned long   start  = 0;
    int             markEndBuffer = (we->getmarkEnd()*o->frameSize-o->playstart)/BUF_SIZE;
    unsigned long   ii     = 0; // inner index, containes played byte!
    // playing loop
    for (int i=0; i<buffers_needed; i++)
    {
      end   = (i+1) * BUF_SIZE + o->playstart;
      start = i     * BUF_SIZE + o->playstart;

      if(o->play_unselected==true  &&
        skipped_selected == false &&
        start>=(we->getmarkStart()*o->frameSize))
      {
        i = markEndBuffer;
        skipped_selected = true;
      }

      // copy next buffer segment:
      for (ii=start; ii<end && ii<wavesize && ii<(o->playend); ii++)
      {
        we->setcursorPosition(ii/o->frameSize);
        audio_buffer[ii-start] = wdata[ii];
      }

      // write buffer segment to audio device
      int count = BUF_SIZE;
      len = write(o->audio_fd, audio_buffer, count);

      // pause-loop
      while (o->playing==true && o->paused==true)
      {
        // stop button pressed during pause
        Sleeper::dosleep(20); // reduce CPU usage
        if(o->playing == false) {
          close(o->audio_fd);
          return arg;
        }
      }

      // stop button pressed
      if (o->playing == false || i==buffers_needed)
      {
        close(o->audio_fd);
        return arg;
      }

      if (len == -1) // something weird happened
      {
        cerr << "Play::playOSS(...): audio write failed - cannot play sound\n";
        Popups::error("failed to write audio data","Error!");
        close(o->audio_fd);
        exit(1); // write to file no. 'audio_fd' failed
      }

      if (ii >= o->playend)
      {
        close(o->audio_fd);
        o->playing = false;
        return arg;
      }
    } // end of for

    // everything went fine
    o->playing=false;
    close(o->audio_fd);
    Debug::msg<string>("Play::playOSSThread(): leaving\n",Globals::DBG_FUNCTION_TRACE);
    return arg;
  }

#endif






//----------------------------------------------------------------------------//

  // constructor
  Play::Play()
  {
    playing = false;
    paused  = false;
  }






//----------------------------------------------------------------------------//

  void      Play::play         (Wavefile &wave, void* arg, unsigned int start, unsigned int end)
  {
    APC::Debug::msg<std::string>("Play::play(): called (no leaving message)");
    if(playing && paused)
    {
      paused=false;
      return;
    }
    if(playing && !paused)
      return;
    this->playstart = start;
    this->playend   = end;
    frame = 0;
    if (chooseLib == "USE_SDL")
      return /*playSDL(wave, arg)*/;
#ifndef WIN32
    if (chooseLib == "USE_OSS")
      return playOSS(wave, arg);
#endif
  }







//----------------------------------------------------------------------------//

#ifndef WIN32
  // Open Sound System (4FrontTechnologies) compatible routines
  void Play::playOSS (Wavefile& wave, void* arg)
  {
    Debug::msg<string>("Play::playOSS(): called\n",Globals::DBG_FUNCTION_TRACE);
    if (this->playing)
    {
      this->paused = false;
      Debug::msg<string>("Play::playOSS(): leaving\n",Globals::DBG_FUNCTION_TRACE);
      return;
    }

    this->playing  = true;
    this->paused   = false;
    this->audio_fd = 0;
    this->myWave   = &wave;                           // handle to whole Wavefile

    // ------ open device write only (playing) ------ //
    const char* DEVICE_NAME = (Settings::AUDIO_DEVICE).c_str();
    const int open_mode   = O_WRONLY;
    audio_fd = open(DEVICE_NAME, open_mode, 0);
      //std::cerr << "audio_fd: " << audio_fd << endl;
    if (audio_fd == -1) // open AND check if opened!
    {
      cerr << "Play::playOSS(...):  failed to open sound device "
           << DEVICE_NAME << "\n";
      Popups::error("failed to open audio device","Error!");
      Debug::msg<string>("Play::playOSS(): leaving\n",Globals::DBG_FUNCTION_TRACE);
      return; // return from play()
    }
    else
    {
      //cerr << "succeded to open sounddevice\n";
      //cerr << "file_descriptor: " << audio_fd << endl;
    }

    // ------ set audio format ------ //
    Fmt_chunk fmt   = wave.getfmt();
    this->channels    = fmt.channels();
    this->frequency   = fmt.frequency();
    this->bitrate     = fmt.bitrate();
    this->frameSize   = (channels*bitrate)/8;
    this->playstart  *= frameSize;
    this->playend    *= frameSize;
    int status = 0;

    // temporary vals;
    int format, tmpchannels;
    int fragments = 2, fragment_size = 4;

    // I think I found good values, so I removed this from configfile
    //Configfile::get("AUDIO_BUFFERS",fragments);
    //Configfile::get("AUDIO_BUFFER_SIZE", fragment_size);

    fragment_size *= frameSize;

    int tmpBuffer =  fragments*0x10000 + fragment_size;

    status = ioctl(audio_fd, SNDCTL_DSP_SETFRAGMENT, &tmpBuffer);
    if (status == -1)
    {
      std::cerr << "Play::playOSS(...): error setting fragment size\n";
      Popups::error("could not set fragment size","Error!");
      Debug::msg<string>("Play::playOSS(): leaving\n",Globals::DBG_FUNCTION_TRACE);
      exit(1);
    }

    switch (bitrate)
    {
      case 8 :  format = AFMT_U8;
                status = ioctl(audio_fd, SNDCTL_DSP_SETFMT, &format);
                if (status==-1 || format!=AFMT_U8)
                {
                  std::cerr << "Play::playOSS(...): couldn't set bitrate 8 for " << DEVICE_NAME << endl;
                  std::cerr << "                    status, format: " << status << ", " << format << endl;
                  Popups::error("soundcard doesn't support 8 bit","Error!");
                  exit(1);
                }
                break;

      case 16 : format = AFMT_S16_LE;
                status = ioctl(audio_fd, SNDCTL_DSP_SETFMT, &format);
                if (status==-1 || format!=AFMT_S16_LE)
                {
                  std::cerr << "Play::playOSS(...): couldn't set bitrate 16 for " << DEVICE_NAME << endl;
                  std::cerr << "                    status, format: " << status << ", " << format << endl;
                  Popups::error("soundcard doesn't support 16 bit","Error!");
                  exit(1);
                }
                break;

      default : std::cerr << "Play::playOSS(...): only 8 and 16 Bit Sound supportet by now...\n";
                std::cerr << "                    You tried " << bitrate << " bit." << endl;
                close(audio_fd);
                return;
                break;
    }

    switch (channels)
    {
      case 1 : tmpchannels = channels;
               status = ioctl(audio_fd, SNDCTL_DSP_CHANNELS, &tmpchannels);
               if (status==-1 || tmpchannels!=1)
               {
                 cerr << "Play::playOSS(...): could not set mono for " << DEVICE_NAME << endl;
                 cerr << "                    status, tmpchannels: " << status << ", " << tmpchannels << endl;
                 Popups::error("soundcard doesn't support mono","Error!");
                 exit(1);
               }
               break;

      case 2 : tmpchannels = channels;
               status = ioctl(audio_fd, SNDCTL_DSP_CHANNELS, &tmpchannels);
               if (status==-1 || tmpchannels!=2)
               {
                 cerr << "Play::playOSS(...): could not set stereo for " << DEVICE_NAME << endl;
                 cerr << "                    status, tmpchannels: " << status << ", " << tmpchannels << endl;
                 Popups::error("soundcard doesn't support stereo","Error!");
                 exit(1);
               }
               break;

      default: cerr << "Play::playOSS(...): only mono/stereo supported by now...\n";
               close(audio_fd);
               Debug::msg<string>("Play::playOSS(): leaving\n",Globals::DBG_FUNCTION_TRACE);
               return; break;
    }


    int speed = frequency;
    status = ioctl(audio_fd, SNDCTL_DSP_SPEED, &speed);
    if (status==-1)
    {
      cerr << "Play::playOSS(...): failed to set samplerate (frequency): " << frequency << endl;
      Popups::error("samplerate not supported by soundcard","Error!");
      close(audio_fd);
      Debug::msg<string>("Play::playOSS(): leaving\n",Globals::DBG_FUNCTION_TRACE);
      return;
    }
    if (speed != frequency)
    {
      cerr << "Play::playOSS(...): setting " << DEVICE_NAME << " to frequency " << speed << endl;
      cerr << "                    you requested " << frequency << endl;
    }

    void* argument = (void*)(this);
    pthread_t       myThread;
    pthread_create  (&myThread, NULL, playOSSThread, argument);

    Debug::msg<string>("Play::playOSS(): leaving\n",Globals::DBG_FUNCTION_TRACE);
    return;
  }
#endif


//----------------------------------------------------------------------------//

} // end of namespace

// end of file


