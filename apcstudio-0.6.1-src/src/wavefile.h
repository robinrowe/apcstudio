// $Id: wavefile.h,v 1.15 2002/02/17 16:45:23 martinhenne Exp $

// Copyright 2001 Martin Henne 2001
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

/*
 * This class containes the two chunks of
 * a wave file, the 'fmt '- and the 'data'
 * chunk. The fmt-chunk containes the format
 * information of the wave, as samplerate,
 * channels, bitrate and so on (see the header
 * file 'fmtchunk.h' for more). The datachunk
 * containes the wavedata itself (see the
 * header file 'datachunk.h' for more).
 *
 */

#ifndef _wavefile_h
#define _wavefile_h

#include "globals.h"
#include "fmtchunk.h"
#include "datachunk.h"
#include <string>
#include <vector>

namespace APC
{
using namespace std;
 
  class Wavefile
  {
  private:
    Fmt_chunk*           fmt;                       // see 'fmtchunk.h'
    //static Clipboard  clip;                       // for copy/past data between Wavefiles

    /// This writes a blank wavefile called 'filename' without wavedata.
    void                   write_dummy    (string filename);
    string                 filename;

  public:
    Data_chunk*       data;                       // see 'datachunk.h'
    Wavefile  ();
    Wavefile  (const string     arg);             // arg = any wavefile
    ~Wavefile ();

    void setfilename       (string fname);        // change also vals in chunks
    string getfilename     () const;


    // get specific wavedata
    unsigned short         getSamplerate  () const; // outdated! use getfrequency!
    unsigned short         getChannels    () const; // outdated! use getchannels!
    unsigned short         getBitrate     () const; // outdated! use getbitRate!
    int                    getfrequency   () const;
    int                    getchannels    () const;
    int                    getbitRate     () const;
    vector<unsigned char>& getWave        ();     // the wavedata itself

    /// this returns 0.0 for silence and +/- 1.0 for maximum amplitude
    /**
      *  this function is slow but fairly secure and easy to handle.
      */
    double                 getAmplitude   (const unsigned long& frame,
                                           const int& channel=1);
    /// set the amplitude to a new value (-1.0 <= amplitude <= 1.0)
    /**
      *  this function is slow but fairly secure and easy to handle.
      */
    void                   setAmplitude   (const double& amplitude,
                                           const unsigned long& frame,
                                           const int& channel=1);

    // get the whole chunks
    Fmt_chunk&             getfmt         ();
    Data_chunk&            getdata        ();

    // set specific wavedata
    void setSamplerate     (const unsigned short arg); // use setfrequency instead!
    void setChannels       (const unsigned short arg); // use setchannels instead!
    void setBitrate        (const unsigned short arg); // use setbitRate instead!
    void setbitRate        (const int& arg);
    void setchannels       (const int& arg);
    void setfrequency      (const int& arg);
    void setWave           (unsigned char* arg);

    // set whole chunks
    void setfmt            (Fmt_chunk      &arg);
    void setdata           (Data_chunk     &arg);

    // file operations
    void read              (const string   arg);   // any wavefile
    void write             (const string   arg);
    void read              ();                     // uses 'this->filename'
    void write             ();                     //  -"-
    void clear             ();
  };
}

#endif
