// $Id: mute.cpp,v 1.2 2002/02/18 17:15:23 martinhenne Exp $

// (C) 2002 Martin Henne
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

#include <mute.h>
#include <globals.h>
#include <debug.h>
#include <vector>

ulong mute::counter = 0;

void mute::operator()(std::vector<unsigned char>& data, // the wave data itself
                  const ulong& startframe,
                  const ulong& endframe,
                  const int& bitRate,
                  const int& channels,
                  const int& frameSize,
                  const int& channel)
{
  ++counter; // function counter for debugging
  const ulong start    = startframe*frameSize;
  const ulong end      = endframe*frameSize;
  const ulong waveSize = data.size();
  if(bitRate==8) { // 8 bit
    if(channels==1) { // 8 bit mono
      for(ulong i=start; i<end && i<waveSize; ++i)
        data[i] = 0x80;
      return;
    } // 8m
    if(channels==2) { // 8 bit stereo
      if(channel==1 || channel==0) { // 8 bit stereo left
        for(ulong i=start; i<end && i<waveSize; i+=2)
          data[i] = 0x80;
      } // 8sl
      if(channel==2 || channel==0) { // 8 bit stereo right
        for(ulong i=start; i<end && (i+1)<waveSize; i+=2)
          data[i+1] = 0x80;
        return;
      } // 8sr
    } // 8s
  } // 8
  if(bitRate==16) { // 16 bit
    if(channels==1) { // 16 bit mono
      for(ulong i=start; i<end && i<waveSize; ++i)
        data[i] = 0;
      return;
    } // 16m
    if(channels==2) { // 16 bit stereo
      if(channel==1 || channel==0) { // 16 bit stereo left
        for(ulong i=start; i<end && (i+1)<waveSize; i+=frameSize) {
          data[i]   = 0;
          data[i+1] = 0;
        }
      } // 16sl
      if(channel==2 || channel==0) { // 16 bit stereo right
        for(ulong i=start; i<end && (i+3)<waveSize; i+=frameSize) {
          data[i+2] = 0;
          data[i+3] = 0;
        }
        return;
      } // 16sr
    } // 16s
  } // 16

  APC::Debug::msg<string>("mute()(): If you see this, please FIX ME!\n");
  return;
}



// end of file


