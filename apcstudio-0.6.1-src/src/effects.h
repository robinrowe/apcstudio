// $Id: effects.h,v 1.13 2002/02/18 17:15:23 martinhenne Exp $

// (C) 2001 Martin Henne 
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.


#ifndef _effects_h
#define _effects_h

#include <globals.h>
#include <convert.h>
#include <datachunk.h>
#include <wavefile.h>
#include <statusbar.h>
#include <editwindow.h>

namespace APC
{
 /// The Effect class contains functions to manipulate the waveform.  
 /**
   * Effects class for the various effects such as reversing a wavefile,
   * fading, noise gate, etc....
   */
  class Effects
  {
    public:
        /// pointer to the Editwindow
        Editwindow* ew;

        /// reversing the wavedata in the selected area
        void reverse     ();

        /// fading in or out within the selection using factors (in percent)
        void fade        (double startFadePercentage, // 0% - 200%
                          double endFadePercentage,   // 0% - 200%
                          const int& channel = 0);    // 0-both 1-left 2-right
 
        /// see "noisegate.h"
        void noiseGate   (double noiseGateLevel,
                          const int& attack_time,
                          const int& channel=0); // 0-both 1-left 2-rigth

        /// tremolo effect (fast periodical amplitude alteration)
        /**
          * This uses the 'Tremolo' class.
          */
        void tremolo     (const unsigned long& markStart,
                          const unsigned long& markEnd,
                          Wavefile* handleToWave,
                          const int& channels,
                          const int& bitRate,
                          const int& frequency);

        /// ramps a marked section of 'data' from startfactor to endfactor
        /**
          *  This is needed by the echo functions and can (and will) be
          *  used by the fade functions in the future. It takes a vector
          *  (data) with a marked section (in frames!) and ramps the volume
          *  from startfactor (1.0 means 'no change', 0.0 means 'silence')
          *  to endfactor.
          */
        static void volume_ramp (vector<unsigned char>& data,
                          const ulong& startframe,
                          const ulong& endframe,
                          const double& startfactor,
                          const double& endfactor,
                          const int& bitRate,
                          const int& channels,
                          const int& frameSize,
                          const int& channel=0);

        /// Echo effect
        static void echo        (Editwindow* ew,
                                 int amount  = 0,
                                 int delay   = 0,
                                 int falloff = 0,
                                 int volume  = 0,
                                 bool wet = false);

        /// Reverb effect
        static void reverb      (Editwindow* ew,
                                 int delay   = 0,
                                 int volume  = 0);

  };

}

#endif

