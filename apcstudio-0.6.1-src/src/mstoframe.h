// $Id: mstoframe.h,v 1.1 2002/02/17 16:45:23 martinhenne Exp $

// (C) 2002 Martin Henne
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

#ifndef _mstoframe_h
#define _mstoframe_h

#include <globals.h>

class mstoframe
{
public:
  ulong operator()(const ulong& ms,
                   const int& frequency);
};

#endif

