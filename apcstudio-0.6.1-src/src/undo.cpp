// $Id: undo.cpp,v 1.1 2002/02/09 02:55:44 martinhenne Exp $

// Copyright 2001 Martin Henne 2001
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

#include <deque>
#include <undo.h>
#include <settings.h>
#include <undooperation.h>
#include <editwindow.h>

namespace APC
{
  Undo::Undo()
  {
    counter = 0;
  }

  Undo::~Undo()
  {
    if(lifo_stack.size() > 0) {
      for(ulong i=0; i<lifo_stack.size(); ++i) {
        delete lifo_stack[i];
      }
      lifo_stack.clear();
      counter = 0;
    }
  }


  bool Undo::perform(Editwindow* ew)
  {
    if(lifo_stack.size() > 0) {
      Undo_operation* uo = lifo_stack[counter-1];
      lifo_stack.pop_back();
      uo->perform(ew);
      --counter;
      return true;
    }
    return false;
  }

  bool Undo::put(Undo_operation* uo)
  {
    if(Settings::MAX_UNDOS > static_cast<int>(lifo_stack.size())) {
      lifo_stack.push_back(uo);
      ++counter;
    }
    else {
      lifo_stack.pop_front();
      lifo_stack.push_back(uo);
    }
    return true;
  }

  void Undo::clear()
  {
    if(lifo_stack.size() > 0) {
      for(ulong i=0; i<lifo_stack.size(); ++i) {
        delete lifo_stack[i];
      }
      lifo_stack.clear();
      counter = 0;
    }
  }

  //deque<Undo_operation*> lifo_stack;

} // end of namespace

// end of file


