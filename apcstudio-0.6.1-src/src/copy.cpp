// $Id: copy.cpp,v 1.4 2002/02/21 21:53:34 martinhenne Exp $

// (C) 2001 Martin Henne
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

#include <copy.h>
#include <debug.h>
#include <string>
#include <globals.h>
#include <vector>
#include <datachunk.h>
#include <editwindow.h>
#include <wavefile.h>
#include <busywin.h>
#include <sleeper.h>

namespace APC
{
using namespace std;
typedef vector<uchar>::iterator iterator;

  void Copy::perform(Editwindow* ew)
  {
    Debug::msg<std::string>("Copy::perform(): called\n",Globals::DBG_FUNCTION_TRACE);

    Busywin busy("Copying...");
    busy.show(); Fl::check(); Fl::wait(0.1); Sleeper::dosleep(10); Fl::check();
    busy.tell("getting necessary information..."); Sleeper::dosleep(10);

    Data_chunk& dc = ew->wfile->getdata();
    vector<unsigned char>& data = dc.getwave();

    ew->theClipboard.setchannels  (ew->channels);
    ew->theClipboard.setfrequency (ew->frequency);
    ew->theClipboard.setframeSize (ew->frameSize);
    ew->theClipboard.setbitRate   (ew->bitRate);

    unsigned long start = ew->we->getmarkStart()*ew->frameSize;
    unsigned long end   = ew->we->getmarkEnd()  *ew->frameSize;
    unsigned long vec_size = end-start;

    busy.tell("copying bytes to clipboard..."); Sleeper::dosleep(10);

    Copy::copy(data,
               ew->theClipboard.wave,
               ew->we->getmarkStart(),
               ew->we->getmarkEnd(),
               ew->frameSize);

    busy.tell("doing the rest...");

    string t_status = stream_cast<string>(vec_size) + string(" byte clipped");
    ew->relabel(t_status,"statusBox_status");

    busy.hide();

    Debug::msg<string>("Copy::perform(): leaving\n",Globals::DBG_FUNCTION_TRACE);
  }

  void Copy::copy(const vector<uchar>& source,
                  vector<uchar>& target,
                  const ulong& startframe,
                  const ulong& endframe,
                  const int& frameSize)
  {
    Debug::msg<string>("Copy::copy(): called\n",Globals::DBG_FUNCTION_TRACE);
    ulong startdiff = startframe*frameSize;
    ulong enddiff   = endframe*frameSize;
    iterator istart = const_cast<iterator>(source.begin() + startdiff);
    iterator iend   = const_cast<iterator>(source.begin() + enddiff);
    target.clear();
    target.assign(istart,iend);
    Debug::msg<string>("Copy::copy(): called\n",Globals::DBG_FUNCTION_TRACE);
  }
}

// end of file


