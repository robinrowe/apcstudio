// $Id: rand.cpp,v 1.1 2001/12/20 00:43:34 martinhenne Exp $

// (C) 2001 Martin Henne
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

// This class combines a Fl_Double_Window and a Apc_fl_waveedit widget
// with a Wavefile and provides several editing options. It is the actual
// main window of aPcStudio. Maybe there will never be another main
// windows, wich was planned first, but just another mainframe-class.

#include <cstdlib>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <rand.h>
#include <debug.h>
#include <globals.h>

int Rand::gen(const int& lower, const int& upper)
{
using namespace std;
using namespace APC;
  double rand_val;
  int return_val;
  rand_val = lower + (rand() % (upper-lower+1) );
  return_val = static_cast<int>(rand_val);
  Debug::msg<string>("Rand::gen(): returning ",Globals::DBG_FUNCTION_TRACE);
  Debug::msg(return_val,Globals::DBG_FUNCTION_TRACE);
  Debug::msg<string>("\n",Globals::DBG_FUNCTION_TRACE);
  if (return_val <= upper && return_val >= lower)
      return return_val;
  else
      return upper;
}

void Rand::init()
{
using namespace std;
using namespace APC;
  Debug::msg<string>("Rand::init(): initializing randomizer\n",Globals::DBG_FUNCTION_TRACE);
  time_t aTime;
  aTime = time((time_t *)NULL);
  srand(aTime);
  return;
}

