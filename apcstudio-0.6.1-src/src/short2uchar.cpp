// $Id: short2uchar.cpp,v 1.1 2002/02/12 02:47:08 martinhenne Exp $

// (C) 2002 Martin Henne
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

#include <short2uchar.h>
//#include <globals.h>
//#include <popups.h>
#include <iostream>

// TODO: make this work!

void short2uchar::operator()(const short& source, unsigned char& target)
{
  std::cerr << "** ATTENTION! short2uchar() is unfinished!\n"; exit(1);
  // get just lowbyte
  short s = source;
  s &=0xff00;
  target = static_cast<unsigned char>(s);
}

void short2uchar::operator()(const short& source,
                             unsigned char& lowbyte,
                             unsigned char& highbyte)
{
  std::cerr << "** ATTENTION! short2uchar() is unfinished!\n"; exit(1);
  // lowbyte
    short s = source;
    s &=0xff00;
    lowbyte = static_cast<unsigned char>(s);

  // highbyte
    s = source;
    s &= 0xff00;
    s >>=(8);
    highbyte = static_cast<unsigned char>(s);
}

// end of file

