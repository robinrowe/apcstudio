// $Id: wavefile.cpp,v 1.16 2002/02/17 16:45:23 martinhenne Exp $

// Copyright 2001 Martin Henne 2001
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

/*
 * This class containes the two chunks of
 * a wave file, the 'fmt '- and the 'data'
 * chunk. The fmt-chunk containes the format
 * information of the wave, as samplerate,
 * channels, bitrate and so on (see the header
 * file 'fmtchunk.h' for more). The datachunk
 * containes the wavedata itself (see the
 * header file 'datachunk.h' for more).
 *
 */

#include "globals.h"
#include "fmtchunk.h"
#include "datachunk.h"
#include <string>
#include <iostream>
#include <vector>
#include "wavefile.h"
#include <popups.h>
#include <globals.h>
#include <debug.h>
#include <popups.h>
#include <settings.h>
#include <filehandle.h>
#include <commandline.h>

namespace APC
{
using namespace std;
  // construct and destruct
  Wavefile::Wavefile  ()
  {
    Debug::msg<string>("Wavefile: destroyed\n",Globals::DBG_FUNCTION_TRACE);
    fmt      = new Fmt_chunk;
    data     = new Data_chunk;
    filename = "(unset)";
    Debug::msg<string>("Wavefile: destroyed\n",Globals::DBG_FUNCTION_TRACE);
  }

  Wavefile::Wavefile  (const string     arg)              // arg = any wavefile
  {
    this->read(arg);
  }

  Wavefile::~Wavefile ()
  {
    Debug::msg<string>("Wavefile: destroying\n",Globals::DBG_FUNCTION_TRACE);
    delete fmt; fmt = NULL;
    delete data; data = NULL;
    Debug::msg<string>("Wavefile: destroyed\n",Globals::DBG_FUNCTION_TRACE);
  }


  string Wavefile::getfilename() const
  {
    return this->filename;
  }


  void Wavefile::setfilename(string fname)
  {
    this->filename = fname;
    fmt->setfile(fname);
  }


  void Wavefile::write_dummy                      (string fname)
  {
    ofstream ofile; // create a file stream object
    ofile.open(fname.c_str(), ios::out|ios::binary); // binary output
    if(!ofile.is_open())
    {
      cerr << "Wavefile::write_dummy(): Cannot write file " << fname << endl;
      return;
    }
    char c_dummy[] =
    {
      'R','I','F','F',0,0,0,0,'W','A','V','E','f','m','t',' ',
      0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
      0,0,0,0,'d','a','t','a',0,0,0,0    
    };

    for(unsigned int i=0; i<sizeof(c_dummy); i++)
    {
      ofile.put(c_dummy[i]);
    }
    ofile.close();
  }


  // get specific wavedata
  unsigned short         Wavefile::getSamplerate  () const
  {
    return (fmt->getdwSamplesPerSec());
  }

  unsigned short         Wavefile::getChannels    () const
  {
    return (fmt->getwChannels());
  }

  unsigned short         Wavefile::getBitrate     () const
  {
    return (fmt->getwBitsPerSample());
  }

  vector<unsigned char>& Wavefile::getWave        ()
  {
    return data->getwave();
  }   
                                      

  Fmt_chunk&             Wavefile::getfmt         ()
  {
    return *fmt;
  }  
 
  Data_chunk&            Wavefile::getdata        () 
  {
    return *data;
  }


  void Wavefile::setSamplerate     (const unsigned short arg)
  {
    fmt->setdwSamplesPerSec(arg);
    return;
  }

  void Wavefile::setChannels       (const unsigned short arg)
  {
    fmt->setwChannels(arg);
    return;
  }

  void Wavefile::setBitrate        (const unsigned short arg)
  {
    fmt->setwBitsPerSample(arg);
    return; 
  }

  // set whole chunks
  void Wavefile::setfmt            (Fmt_chunk      &arg)
  {
    fmt = &arg;
  }

  void Wavefile::setdata           (Data_chunk     &arg)
  {
    data = &arg; // TODO: test, if this is used, cause it doesn't work.
  }

  // file operations
  void Wavefile::read              (const string   arg)   // any wavefile
  {
    Debug::msg<string>("Wavefile::read(string): entered\n",Globals::DBG_FUNCTION_TRACE);
    this->fmt->read(arg);
    this->data->read(arg);
    this->filename = arg;
    Debug::msg<string>("Wavefile::read(string): leaving\n",Globals::DBG_FUNCTION_TRACE);
    return;
  }

  //##
  void Wavefile::write             (const string   arg)
  {
    // truncate or create the file
    fstream truncate_file;
    truncate_file.open(arg.c_str(), ios::trunc);
    if(!truncate_file.is_open())
    {
      Popups::error("Couldn't write to file "+arg,"Fileerror!");
      cerr << "Wavefile::write(): couldn't write to file " << arg << endl;
      return;
    }
    truncate_file.close();


    // write the wavefile header dummy!
    this->write_dummy(arg);

    // write the correct fmtchunk
    fmt->write(arg);

    // append all the wavedata
    data->write(arg);

    // now correct datachunksize and filesize-8.
    unsigned long wave_size = (data->wave).size();
    unsigned long file_minus_8 = fmt->getchunkSize() + 20 + wave_size;
    unsigned long t_ulong; // temporary
    unsigned char t_char; // temporary
    int dc_pos = data->findIDpos(arg); // position of data chunk

    fstream myFile;
    myFile.open(arg.c_str(), ios::in|ios::out|ios::binary); // no test if opened. test done while truncating.

    // correct byte 4-8 (which is the filesize minus eigth)
    myFile.seekp(4); // pointer to 5th byte (filesize-8)
    t_ulong = file_minus_8;
    t_ulong &=0x000000ff; // lowest byte
    t_char  = static_cast<unsigned char>(t_ulong);
    myFile.put(t_char);
    t_ulong = file_minus_8;
    t_ulong &=0x0000ff00; // almost lowest byte
    t_ulong >>= (8);
    t_char  = static_cast<unsigned char>(t_ulong);
    myFile.put(t_char);
    t_ulong = file_minus_8;
    t_ulong &=0x00ff0000; // higher byte
    t_ulong >>=(16);
    t_char  = static_cast<unsigned char>(t_ulong);
    myFile.put(t_char);
    t_ulong = file_minus_8;
    t_ulong &=0xff000000; // most significant byte
    t_ulong >>=(24);
    t_char  = static_cast<unsigned char>(t_ulong);
    myFile.put(t_char);

    // correct data chunk size (which is size of wave vector)
    myFile.seekp(dc_pos+4); // pointer to wavesize
    t_ulong = wave_size;
    t_ulong &=0x000000ff; // lowest byte
    t_char  = static_cast<unsigned char>(t_ulong);
    myFile.put(t_char);
    t_ulong = wave_size;
    t_ulong &=0x0000ff00; // almost lowest byte
    t_ulong >>= (8);
    t_char  = static_cast<unsigned char>(t_ulong);
    myFile.put(t_char);
    t_ulong = wave_size;
    t_ulong &=0x00ff0000; // higher byte
    t_ulong >>=(16);
    t_char  = static_cast<unsigned char>(t_ulong);
    myFile.put(t_char);
    t_ulong = wave_size;
    t_ulong &=0xff000000; // most significant byte
    t_ulong >>=(24);
    t_char  = static_cast<unsigned char>(t_ulong);
    myFile.put(t_char);

    // done
    myFile.close();
    return;
  }

  void Wavefile::read              ()                     // uses 'this->filename'
  {
    if(filename=="(unset)")
    {
      cerr << "Wavefile::read(): No filename specified yet\n";
    }
    else
    {
      this->read(this->filename);
    }
    return;
  }

  void Wavefile::write             ()
  {
    if(filename=="(unset)")
    {
      cerr << "Wavefile::write(): No filename specified yet\n";
    }
    else
    {
      this->write(this->filename);
    }
    return;
   
  }


  void Wavefile::clear()
  {
    data->clear();
  }


  void Wavefile::setbitRate        (const int& arg)
  {
    return setBitrate(arg);
  }



  void Wavefile::setchannels       (const int& arg)
  {
    return setChannels(arg);
  }



  void Wavefile::setfrequency      (const int& arg)
  {
    return setSamplerate(arg);
  }


  int Wavefile::getbitRate        () const
  {
    return getBitrate();
  }



  int Wavefile::getchannels       () const
  {
    return getChannels();
  }



  int Wavefile::getfrequency      () const
  {
    return getSamplerate();
  }

  double Wavefile::getAmplitude   (const unsigned long& frame,
                                   const int& channel)
  {
    return data->getAmplitude(frame,this->getBitrate(), this->getchannels(), channel);
  }

  void Wavefile::setAmplitude   (const double& amplitude,
                                 const unsigned long& frame,
                                 const int& channel)
  {
    return data->setAmplitude(amplitude, frame,this->getBitrate(), this->getchannels(), channel);
  }


}

// end of namespace
