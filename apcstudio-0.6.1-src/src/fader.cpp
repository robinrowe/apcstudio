// $Id: fader.cpp,v 1.3 2002/02/21 21:53:34 martinhenne Exp $

// (C) 2002 Martin Henne
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.


#include <globals.h>
#include <fader.h>
#include <lendian.h>
#include <debug.h>

void fader::operator()(std::vector<unsigned char>& data,
                        const ulong& startframe,
                        const ulong& endframe,
                        const double& startfactor,
                        const double& endfactor,
                        const int& bitRate,
                        const int& channels,
                        const int& frameSize,
                        const int& channel)
{
  APC::Debug::msg<std::string>("fader()(): called\n",Globals::DBG_FUNCTION_TRACE);
  using namespace std;
  using namespace APC;
  signed short int tmpi = 0;
  const ulong waveSize = data.size();
  const ulong markSize = (endframe-startframe)*frameSize;
  const double vol_diff = endfactor - startfactor;
  double vol_factor = 1.0;

  for(ulong i=startframe*frameSize;
      i<endframe*frameSize && (i+frameSize-1)<waveSize;
      i+=frameSize)
  {
    vol_factor = startfactor + static_cast<double>(((i-startframe*frameSize*1.0)/(markSize*1.0))*vol_diff); // o.k. 0.0-2.0
    if(bitRate==8)
    {
      if(channel==1 || channel==0) {
        tmpi = data[i]-0x80;
        tmpi = static_cast<short>(tmpi*vol_factor);
        data[i] = tmpi+0x80;
      }
      if(channels==2 && (channel==0 || channel==2)) {
        tmpi = data[i+1]-0x80;
        tmpi = static_cast<short>(tmpi*vol_factor);
        data[i+1] = tmpi+0x80;
      }
    }
    if(bitRate==16)
    {
      if(channel==1 || channel==0) {
        lendian(data[i],data[i+1],tmpi);
        tmpi = static_cast<short>(tmpi*vol_factor);
        getshortsLow (tmpi, data[i]);
        getshortsHigh(tmpi, data[i+1]);
      }
      if(channels==2 && (channel==2 || channel==0)) {
        lendian(data[i+2],data[i+3],tmpi);
        tmpi = static_cast<short>(tmpi*vol_factor);
        getshortsLow (tmpi, data[i+2]);
        getshortsHigh(tmpi, data[i+3]);
      }
    }
  }
  APC::Debug::msg<std::string>("fader()(): leaving\n",Globals::DBG_FUNCTION_TRACE);
  return;
}


