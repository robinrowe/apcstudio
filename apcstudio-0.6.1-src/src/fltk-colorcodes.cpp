// colors.cpp


#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Box.H>
#include <string>
#include <strstream>
#include <iostream>

template <typename T, typename S>
T stream_cast(S const& val)
{
  strstream stream;
  stream << val;
  T rc;
  stream >> rc;
  return rc;
}

int main()
{
  Fl_Window win(970,460,"The FLTK-Colorcodes");
  unsigned short cl = 0; // color
  Fl_Box* box[256];
  string label[256];
  for (unsigned short i=0; i< 11; i++)
  {
    //std::cerr << ".";
    for (unsigned short ii=0; ii<24; ii++)
    {
      if (cl < 256)
      {
        //std::cerr << "-";
        label[cl] = stream_cast<string>(cl);
        box[cl] = new Fl_Box(ii*40+10, i*40+10, 35, 35, label[cl].c_str());
        box[cl]->box(FL_DOWN_BOX);
        box[cl]->color(cl);
        cl++;
      }
    }
  }
  win.end();
  win.show(); 
  return Fl::run();
}


