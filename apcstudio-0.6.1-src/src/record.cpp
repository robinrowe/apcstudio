// $Id: record.cpp,v 1.14 2002/02/11 20:09:55 martinhenne Exp $

// (C) 2001 Martin Henne
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

// This class is to record sound from the sound device.
// It is a part of the aPcStudio Wavefile editing software
// for Linux and Windows.

//#define WIN32

#include <record.h>

#ifndef WIN32
  #include <fcntl.h>
  #include <sys/ioctl.h>
  #include <linux/soundcard.h>
#endif

#include <vector>
#include <iostream>
#include <editwindow.h>
#include <configfile.h>
#include <popups.h>
#include <settings.h>
#include <debug.h>
#include <sleeper.h>

#define BUF_SIZE 64

namespace APC
{
using namespace std;
  Record::Record(){recording=false;paused=false;}

  void Record::init(Editwindow* arg)
  {
    ew = arg;
    frequency       =     (ew->frequency);
    bitRate         =     (ew->bitRate);
    frameSize       =     (ew->frameSize);
    channels        =     (ew->channels);
    waveSize        =     (ew->waveSize);
    recording       =     false;
    paused          =     false;
    rec_start       =     ew->we->getmarkStart() * (frameSize);
    rec_end         =     ew->we->getmarkEnd()   * (frameSize);
    audio_fd        =     0;
    device          =     Settings::REC_DEVICE;
  }


  void Record::record_new            ()
  {}




  void Record::record_selection      ()
  {
    Debug::msg<string>("Record::record_selection(): called\n",Globals::DBG_FUNCTION_TRACE);
#ifdef WIN32
  cerr << "Recording is not yet supported on Win32 Systems\n";
#else
    frequency       =     (ew->frequency);
    bitRate         =     (ew->bitRate);
    frameSize       =     (ew->frameSize);
    channels        =     (ew->channels);
    waveSize        =     (ew->waveSize);
    recording       =     false;
    paused          =     false;
    rec_start       =     ew->we->getmarkStart() * (frameSize);
    rec_end         =     ew->we->getmarkEnd()   * (frameSize);
    audio_fd        =     0;
    device          =     Settings::REC_DEVICE;

    int status; // temp stat info

    // open the device
    const int open_mode = O_RDONLY;
    audio_fd = open(device.c_str(),open_mode,0);
    if(audio_fd == -1)
    {
      cerr << "Record::record_selection(): could not open device " << device << endl;
      string msg = "Could not open device " + device;
      Popups::error(msg,"Error");
      Debug::msg<string>("Record::record_selection(): leaving\n",Globals::DBG_FUNCTION_TRACE);
      return;
    }

    // set audio buffer
    int fragments = 2, fragment_size = 4;
    //fragment_size *= frameSize; // TODO: use this
    int tmpBuffer =  fragments*0x10000 + fragment_size;
    status = ioctl(audio_fd, SNDCTL_DSP_SETFRAGMENT, &tmpBuffer);

    // set bitRate
    int format = 0;
    switch (bitRate)
    {
      case 8 :  format = AFMT_U8;
                status = ioctl(audio_fd, SNDCTL_DSP_SETFMT, &format);
                if (status==-1 || format!=AFMT_U8)
                {
                  std::cerr << "couldn't set bitrate 8 for " << device << endl;
                  std::cerr << "           status, format: " << status << ", " << format << endl;
                  exit(1);
                }
                break;

      case 16 : format = AFMT_S16_LE;
                status = ioctl(audio_fd, SNDCTL_DSP_SETFMT, &format);
                if (status==-1 || format!=AFMT_S16_LE)
                {
                  std::cerr << "couldn't set bitrate 16 for " << device << endl;
                  std::cerr << "            status, format: " << status << ", " << format << endl;
                  exit(1);
                }
                break;

      default : std::cerr << "only 8 and 16 Bit Sound supportet by now...\n";
                std::cerr << "You tried " << bitRate << " bit." << endl;
                close(audio_fd);
                Debug::msg<string>("Record::record_selection(): leaving\n",Globals::DBG_FUNCTION_TRACE);
                return;
                break;
    }

    // set channels
    int tmpchannels = 0;
    switch (channels)
    {
      case 1 : tmpchannels = channels;
               status = ioctl(audio_fd, SNDCTL_DSP_CHANNELS, &tmpchannels);
               if (status==-1 || tmpchannels!=1)
               {
                 cerr << "could not set mono for " << device << endl;
                 cerr << "status, tmpchannels: " << status << ", " << tmpchannels << endl;
                 exit(1);
               }
               break;

      case 2 : tmpchannels = channels;
               status = ioctl(audio_fd, SNDCTL_DSP_CHANNELS, &tmpchannels);
               if (status==-1 || tmpchannels!=2)
               {
                 cerr << "could not set stereo for " << device << endl;
                 cerr << "status, tmpchannels: " << status << ", " << tmpchannels << endl;
                 exit(1);
               }
               break;

      default: cerr << "Only mono/stereo supported by now...\n";
               close(audio_fd);
               Debug::msg<string>("Record::record_selection(): leaving\n",Globals::DBG_FUNCTION_TRACE);
               return; break;
    }

    // set frequency
    int speed = frequency;
    status = ioctl(audio_fd, SNDCTL_DSP_SPEED, &speed);
    if (status==-1)
    {
      cerr << "failed to set samplerate (frequency): " << frequency << endl;
      close(audio_fd);
      Debug::msg<string>("Record::record_selection(): leaving\n",Globals::DBG_FUNCTION_TRACE);
      return;
    }
    if (speed != frequency)
    {
      cerr << "setting " << device << " to frequency " << speed << endl;
      cerr << "you requested " << frequency << endl;
    }


    // now do recording

    pthread_t myThread;
    pthread_create(&myThread,NULL,Record::record_selection_thread,(void*)(this));

    while(recording)
    {
      Fl::wait(0.1);
      Sleeper::dosleep(10);
      Fl::check();

      ew->redraw_needed |= ew->REDR_WAVE;
      ew->doRedraw();
    }

    // done with recording
 	  recording=false;
    close(audio_fd);
    Debug::msg<string>("Record::record_selection(): leaving\n",Globals::DBG_FUNCTION_TRACE);
#endif
  }




  void* Record::record_selection_thread(void* arg)
  {
    Debug::msg<string>("Record::record_selection_thread(): called\n",Globals::DBG_FUNCTION_TRACE);
#ifndef WIN32
    Record* o = static_cast<Record*>(arg);
    vector<unsigned char>& mem = o->ew->wfile->getWave();
    int len = 0;

    unsigned char audio_buffer[BUF_SIZE];

    // get data from device and write to buffer
    int buffers_needed = ((o->rec_end - o->rec_start)/BUF_SIZE)+1;
    unsigned long   end    = 0;
    unsigned long   start  = 0;
    unsigned long   ii     = 0; // inner index
    int count = 0;

    // recording loop
    o->recording = true;
    for (int i=0; i<buffers_needed; i++)
    {
      //cerr << ".";
      end   = (i+1) * BUF_SIZE + o->rec_start;
      start = i     * BUF_SIZE + o->rec_start;

      // read buffer segment from audio device
      count = BUF_SIZE;
      len = read(o->audio_fd, audio_buffer, count);

      // copy next buffer segment:
      for (ii=start; ii<end && ii<(o->waveSize) && ii<(o->rec_end); ii++)
      {
        o->ew->we->setcursorPosition(ii/o->frameSize);
        mem[ii] = audio_buffer[ii-start];
      }

      while(o->paused && o->recording)
      {
        Sleeper::dosleep(10);
      }

      if(o->recording == false)
      {
        cerr << "canceling recording for-loop\n";
        break;
      }

      if (ii >= o->rec_end)
      {
        cerr << "record_sel_thread(): selection end reached!\n";
        close(o->audio_fd);
        o->recording = false;
        Debug::msg<string>("Record::record_selection_thread(): leaving\n",Globals::DBG_FUNCTION_TRACE);
        return arg;
      }
    } // end of for
    o->recording=false;

#endif
    Debug::msg<string>("Record::record_selection_thread(): leaving\n",Globals::DBG_FUNCTION_TRACE);
    return arg;
  }


  void Record::record                ()
  {}

} // end of namespace



