// $Id: rand.h,v 1.1 2001/12/20 00:43:34 martinhenne Exp $

// (C) 2001 Martin Henne
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

// This class combines a Fl_Double_Window and a Apc_fl_waveedit widget
// with a Wavefile and provides several editing options. It is the actual
// main window of aPcStudio. Maybe there will never be another main
// windows, wich was planned first, but just another mainframe-class.

#ifndef _rand_h
#define _rand_h

class Initializer;

/// This class generates random numbers.
class Rand
{
private:
  Rand();
  static Initializer I;

public:
  /// generate number between given bounds
  static int gen(const int& lower, const int& upper);

  /// initialize
  static void init();
};

struct Initializer
{
  Initializer() {Rand::init();}
};


#endif
