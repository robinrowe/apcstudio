// $Id: drawcalc.cpp,v 1.23 2002/01/24 00:27:05 martinhenne Exp $

// (C) 2001 Martin Henne
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

/*
 * This class does all calculations that are needed to
 * display a waveform. The results are used by Wavedraw::draw_wave();
 */


#include <apc_fl_waveedit.h>
#include <statusbar.h>
#include <lendian.h>
#include <drawcalc.h>
#include <vector>
#include <commandline.h>
#include <debug.h>
#include <streamcast.h>
#include <sleeper.h>
#include <busywin.h>

namespace APC
{
using namespace std;
  Drawcalc::Drawcalc() : FIXSTEP(80) // should be 100
  {}



//----------------------------------------------------------------------------//



  //------  fit2screen() ------//
  void Drawcalc::fit2screen()
  {
    Debug::msg<string>("Drawcalc::fit2screen(): entered\n",Globals::DBG_FUNCTION_TRACE);
    // the samples must fit the screen dimensions now:
    // VMUTLI means 'vertical multiplicator'
    double VMULTI = 0;
    switch (*bitRate)
    {
      case 8:  VMULTI = ((*heigth)/((*channels)*2.0))/(0x7f); break;
      case 16: VMULTI = ((*heigth)/((*channels)*2.0))/(0x7fff); break;
    }

    short minL = 0;
    short maxL = 0;
    short minR = 0;
    short maxR = 0;

    unsigned long limit = drawdata.size()-(2*(*channels));
    unsigned long increment = 2*(*channels);

    for(unsigned long i=0; i<limit; i+=increment)
    {
      minL = drawdata[i+0];
      maxL = drawdata[i+1];
      if(*channels == 2)
      {
        minR = drawdata[i+2];
        maxR = drawdata[i+3];
      }

      // now convert to vertical screen dimensions
      if(*channels == 1)
      {
        switch(*bitRate)
        {
          case 8 :  maxL = -1*(static_cast<int> (maxL*(VMULTI)))+static_cast<int>(*heigth*0.5) ;
                    minL = -1*(static_cast<int> (minL*(VMULTI)))+static_cast<int>(*heigth*0.5) ;
                    break;
          case 16 : maxL = -1*(static_cast<int> (maxL*(VMULTI)))+static_cast<int>(*heigth*0.5) ;
                    minL = -1*(static_cast<int> (minL*(VMULTI)))+static_cast<int>(*heigth*0.5) ;
                    break;
        }
        if(i<(drawdata.size()-2)) // prevent segfault
        {
          drawdata[i+0] = minL;
          drawdata[i+1] = maxL;
        }
        else
        {
          Debug::msg<string>("segfault caught in fit2screen(2)\n",Globals::DBG_SEGFAULT_CAUGHT);
        }
      }
      if(*channels == 2) // stereo
      {
        switch(*bitRate)
        {
          case 8 :  maxL = -1*(static_cast<int>(maxL*(VMULTI)))+static_cast<int>(*heigth*0.25);
                    minL = -1*(static_cast<int>(minL*(VMULTI)))+static_cast<int>(*heigth*0.25);
                    maxR = -1*(static_cast<int>(maxR*(VMULTI)))+static_cast<int>(*heigth*0.75);
                    minR = -1*(static_cast<int>(minR*(VMULTI)))+static_cast<int>(*heigth*0.75);
                    break;
          case 16 : maxL = -1*(static_cast<int>(maxL*(VMULTI)))+static_cast<int>(*heigth*0.25);
                    minL = -1*(static_cast<int>(minL*(VMULTI)))+static_cast<int>(*heigth*0.25);
                    maxR = -1*(static_cast<int>(maxR*(VMULTI)))+static_cast<int>(*heigth*0.75);
                    minR = -1*(static_cast<int>(minR*(VMULTI)))+static_cast<int>(*heigth*0.75);
                    break;
        }
        if(i<(drawdata.size()-4)) // prevent segfault
        {
          drawdata[i+0] = minL;
          drawdata[i+1] = maxL;
          drawdata[i+2] = minR;
          drawdata[i+3] = maxR;
        }
        else
        {
          Debug::msg<string>("segfault caught in fit2screen(3)\n",Globals::DBG_SEGFAULT_CAUGHT);
        }

      } // end of if(stereo)
    } // end of for
    Debug::msg<string>("Drawcalc::fit2screen(): leaving\n",Globals::DBG_FUNCTION_TRACE);
  } // end of ::fit2screen()





//----------------------------------------------------------------------------//




  //------ getdrawdata() ------//
  vector<short>&           Drawcalc::getdrawdata        ()
  {
    Debug::msg<string>("Drawcalc::getdrawdata(): entered\n",Globals::DBG_FUNCTION_TRACE);
    // wait, untill the virtual wave is available (in case it's calculation is threaded)
    while(this->calculating == true)
    {
      Fl::wait(0.2);
      Debug::msg<string>("Drawcalc::getdrawdata(): waiting for calculation\n",Globals::DBG_DRAWCALC_LOW);
    }

    // resize 'drawdata' to fit the screenwidth
    drawdata.resize((*width)*2*(*channels));

/*
    // clear drawdata for debug reasons
    // TODO: remove this
    ushort debug_cleardd = 0;
    while(debug_cleardd < drawdata.size())
      drawdata[debug_cleardd++]=0;
*/

    // now calculate 'drawdata' using the 'virtualWave' (if XSTEP>=FIXSTEP)
    // TODO: code for XSTEP<FIXSTEP, if old drawcode shouldn't be used anymore

    // temporary values for min/max
    short newMinL =  32767;
    short newMaxL = -32767;
    short newMinR =  32767;
    short newMaxR = -32767;
    short oldMinL =  32767;
    short oldMaxL = -32767;
    short oldMinR =  32767;
    short oldMaxR = -32767;

    short tmp = 0;

    // start/end/range within 'virtualWave' to be processed (depending on we::drawStart/drawEnd)
    unsigned long vwstart = 2*(*channels)*(*drawStart/ FIXSTEP);
    unsigned long vwend   = 2*(*channels)*(*drawEnd  / FIXSTEP);
    vwstart -= (vwstart%(2*(*channels)));
    vwend   -= (vwend  %(2*(*channels)));
    unsigned long vwrange = vwend - vwstart;
    // index of the 'drawdata' vector
    unsigned long ddindex = 0;

    // vXSTEP = virtual XSTEP
    int vXSTEP      = static_cast<int>(0.5+((vwrange*1.0) / (*width)));
    //vXSTEP -= vXSTEP%(*channels);

    ulong  ii            = 0;        // inner loop index
    ulong  i             = vwstart;  // outer loop index
    ushort width_counter = 0;        // point to width (in pixels) in loop
    ushort leveler       = 0;        // every x-th step we need to adapt vXSTEP; x = leveler
    ushort needed_width  = static_cast<ushort>(0.5+(vwrange/vXSTEP));
    short   wdiff         = needed_width-(*width); // differenz of needed_with and width
    ulong  lev_vXSTEP    = vXSTEP;   // leveled version of vXSTEP, calculated below
    ulong  vframes_left  = 0;        // how many frames not covered when using vXSTEP? calc'd below.

    if(wdiff > 0) // If more pixels than visible are needed, to draw all
    {
      leveler       = static_cast<ushort>(0.5+(*width)/wdiff);
      vframes_left  = vwrange-(vXSTEP*(*width));
      lev_vXSTEP    = static_cast<ulong>(0.5+(vframes_left/wdiff));
    }
    if(wdiff < 0) // a few pixels are left, but the whole wave is drawn
    {
      leveler       = static_cast<ushort>(0.5+(*width)/(-1*wdiff));
      vframes_left  = (vXSTEP*(*width))-vwrange;
      lev_vXSTEP    = static_cast<ulong>(0.5+(vframes_left/(-1*wdiff)));
    }
    if(wdiff == 0) // all fits the window width perfectly
    {
      leveler = (*width);
      vframes_left = 0;
      lev_vXSTEP    = 0;
    }



    // calc vector of XSTEP values, each of them belongs to a pixel of the window width
    vector<short> vec_vXSTEP((*width));

    while(width_counter < (*width))
    {
      vec_vXSTEP[width_counter++] = vXSTEP;
    }
    width_counter = 0;

    while(width_counter < (*width))
    {
      if(width_counter % leveler == 0)
      {
        if(wdiff>0)
          vec_vXSTEP[width_counter]+=lev_vXSTEP;
        if(wdiff<0 && width_counter>0)
          vec_vXSTEP[width_counter]-=lev_vXSTEP;
      }
      width_counter++;
    }

    // outer loop: build the 'drawdata' vector for intervall [vwstart, vwend]
    width_counter = 0;
    unsigned long limit = drawdata.size();
    while((ii<vwend) && (ddindex < limit))
    {
      newMinL =  32767;
      newMaxL = -32767;
      newMinR =  32767;
      newMaxR = -32767;
      ii = i;

      // inner loop: find min/max vals of 'virtualWave' within range vXSTEP
      while(ii<(i+vec_vXSTEP[width_counter]))
      {
        if(ii<(2*(virtualWave.size()))) // prevent segfault
        {
          ii -= (ii%(2*(*channels)));
          tmp = virtualWave[ii+0];
            newMinL=(tmp < newMinL ? tmp : newMinL);
          tmp = virtualWave[ii+1];
            newMaxL=(tmp > newMaxL ? tmp : newMaxL);
          if(*channels == 2)
          {
            tmp = virtualWave[ii+2];
              newMinR=(tmp < newMinR ? tmp : newMinR);
            tmp = virtualWave[ii+3];
              newMaxR=(tmp > newMaxR ? tmp : newMaxR);
          }
        }
        else
        {
          Debug::msg<string>("drawdata: index out of range (1): ",Globals::DBG_SEGFAULT_CAUGHT);
          Debug::msg<string>(stream_cast<string>(ii)+string("\n"),Globals::DBG_SEGFAULT_CAUGHT);
        }
        ii+=2*(*channels);
      }

      // write min/max pairs to 'drawdata'

      if(vec_vXSTEP[width_counter]!=vXSTEP && wdiff<0)
      {
        newMinL = oldMinL;
        newMaxL = oldMaxL;
        if(*channels==2)
        {
          newMinL = oldMinL;
          newMaxL = oldMaxL;
        }
      }

      if(ddindex < limit) // prevent segfault
      {
        ddindex -= ddindex%(2*(*channels));
        drawdata[ddindex+0] = newMinL;
        drawdata[ddindex+1] = newMaxL;
        oldMinL = newMinL;
        oldMaxL = newMaxL;
        if(*channels==2)
        {
          drawdata[ddindex+2] = newMinR;
          drawdata[ddindex+3] = newMaxR;
          oldMinR = newMinR;
          oldMaxR = newMaxR;
          ddindex += 2;
        }
        ddindex += 2;
      }
      else
      {
        Debug::msg<string>("drawdata: index out of range(2)\n",Globals::DBG_SEGFAULT_CAUGHT);
      }
     // now get the next stepsize for the inner loop (vXSTEP) and loop once again
     i += vXSTEP;
     if(width_counter < vec_vXSTEP.size())
     vXSTEP = vec_vXSTEP[width_counter++];
   }

    fit2screen();
    Debug::msg<string>("Drawcalc::getdrawdata(): leaving\n",Globals::DBG_FUNCTION_TRACE);
    return drawdata;
  }




//----------------------------------------------------------------------------//




  //------ init() ------//
  void                     Drawcalc::init               (Apc_fl_waveedit* arg)
  {
    Debug::msg<string>("Drawcalc::init(): entered\n",Globals::DBG_FUNCTION_TRACE);
    we             = arg;
    waveSize       = &(we->waveSize);
    drawStart      = &(we->drawStart);
    drawEnd        = &(we->drawEnd);
    channels       = &(we->channels);
    bitRate        = &(we->bitRate);
    XSTEP          = &(we->XSTEP);
    frameSize      = &(we->frameSize);
    pix_per_sample = &(we->pix_per_sample);
    width          = &(we->width);
    heigth         = &(we->heigth);
    unsigned long virtualWaveSize = (*waveSize)/(FIXSTEP*(*frameSize)/((*channels)*2));
    virtualWaveSize -= (virtualWaveSize%(2*(*channels)));
    (this->virtualWave).resize(virtualWaveSize);
    (this->drawdata).resize(700*2*2);
    Debug::msg<string>("Drawcalc::init(): leaving\n",Globals::DBG_FUNCTION_TRACE);
  }







//----------------------------------------------------------------------------//


  void                     Drawcalc::calcvirtualWave    ()
  {
    Debug::msg<string>("Drawcalc::calcvirtualWave(): called\n",Globals::DBG_FUNCTION_TRACE);

    percent = 0; // reset percent-counter to 0

    Busywin busy("scanning wave...");
    busy.show();

    // now start thread
    this->calculating = true;
    pthread_t calcThread;
    pthread_create(&calcThread,
                   NULL,
                   Drawcalc::calcvirtualWave_thread,
                   static_cast<void*>(this));
    while(calculating)
    {
      Fl::wait(0.1);
      Sleeper::dosleep(50); // saving CPU-usage
      Fl::check(); // keeping GUI alive
    }
    Sleeper::dosleep(20);
    busy.hide();
    Debug::msg<string>("Drawcalc::calcvirtualWave(): leaving\n",Globals::DBG_FUNCTION_TRACE);
    return;
  }



//----------------------------------------------------------------------------//



  void*                    Drawcalc::calcvirtualWave_thread(void* arg)
  {
    Debug::start_timer();
    Drawcalc* o = static_cast<Drawcalc*>(arg);

    // check, if there is already wavedata to be scanned
    while(!((o->we)->dataAssigned))
    {
      Debug::msg<string>("::calcvirtualWave_thread(): waiting for data\n",Globals::DBG_DRAWCALC_LOW);
      Fl::wait(0.2);
    }

    Debug::msg<string>("::calcvirtualWave_thread(): entered\n",Globals::DBG_FUNCTION_TRACE);
    o->calculating = true;
    Debug::msg<string>("::calcvirtualWave_thread(): getting reference to wave\n",Globals::DBG_DRAWCALC_HIGH);
    vector<unsigned char> &wave        = (o->we->getdata())->getWave();
    Debug::msg<string>("::calcvirtualWave_thread(): accessing wave.size()\n",Globals::DBG_DRAWCALC_HIGH);
    ulong t_vwsize = wave.size() / o->FIXSTEP;
    Debug::msg<string>("::calcvirtualWave_thread(): calculating stuff, using *channels\n",Globals::DBG_DRAWCALC_HIGH);
    t_vwsize += ((2*(*(o->channels)) - (t_vwsize%(2*(*(o->channels))))));
    Debug::msg<string>("::calcvirtualWave_thread(): resizing!\n",Globals::DBG_DRAWCALC_HIGH);
    (o->virtualWave).resize(t_vwsize);
    Debug::msg<string>(string("::calcvirtualWave_thread(): new size of virtualWave: ")+
               stream_cast<string>((o->virtualWave).size())+
               string("\n"),Globals::DBG_DRAWCALC_LOW);


    // initialize start- and endpoints for lines:
    ulong    x1      = 0;    // position in virtualWave
    int       newMaxL = 0;    // y position
    int       newMinL = 0;
    int       oldMinL = 0;
    int       oldMaxL = 0;
    int       newMaxR = 0;    // y position
    int       newMinR = 0;
    int       oldMinR = 0;
    int       oldMaxR = 0;
    short     tmpL    = 0;    // for temporary sample values
    short     tmpR    = 0;

    // initialize pitchlevel to zeroline
    switch (*(o->bitRate))
    {
      case 8:  newMinL = newMaxL = newMinR = newMaxR = tmpL = tmpR = 0x0;
      case 16: newMinL = newMaxL = newMinR = newMaxR = tmpL = tmpR = 0x0;
    }

    Debug::msg<string>("::calcvirtualWave_thread(): calcing statusbar data\n",
               Globals::DBG_DRAWCALC_HIGH);
    Debug::msg<string>("::calcvirtualWave_thread(): entering loop\n",
               Globals::DBG_DRAWCALC_HIGH);
    Debug::msg<string>("::calcvirtualWave_thread(): waveSize (addr.): ",
               Globals::DBG_DRAWCALC_HIGH);
    Debug::msg<string>(stream_cast<string>(*(o->waveSize))+string("\n"),
               Globals::DBG_DRAWCALC_HIGH);

    int statusbar_divider = (*(o->waveSize))/100;

    // outer loop: whole wave file in stepwidth 'FIXSTEP'
    (*(o->waveSize)) -= (*(o->waveSize))%(*(o->frameSize)); // TODO: check, if this causes trouble
    for(unsigned long i=0;
        i<(*(o->waveSize));
        i+=static_cast<int>(o->FIXSTEP*(*(o->frameSize))))
    {
      //Debug::msg<string>("-",Globals::DBG_FOR_LOOPS);

      o->percent = i/statusbar_divider;
      switch (*(o->bitRate))
      {
        case 8:  newMinL = newMinR = 127;  newMaxL = newMaxR = -127; break;
        case 16: newMinL = newMinR = 32767; newMaxL = newMaxR = -32767; break;
      }
        // calculate extrema in intervall [i,i+XSTEP]
        if ((i+o->FIXSTEP) <= (*(o->waveSize)))
        {
          // in case it's a mono wave or left channel:
          for(unsigned long ii=  (i-i%(*(o->frameSize)));
                            ii<  (i+o->FIXSTEP*(*(o->frameSize)));
                            ii+= (*(o->frameSize)))
          {
            /*
            if(ii%statusbar_divider == 0)
              o->percent_counter++;
            */
            switch (*(o->bitRate))
            {
              case 8:  tmpL = wave[ii]; tmpL -= 128; break;
              case 16: lendian(wave[ii], wave[ii+1], tmpL); break;
            }
            newMaxL = (tmpL > newMaxL ? tmpL : newMaxL);
            newMinL = (tmpL < newMinL ? tmpL : newMinL);
        }

        // and this is the right channel (in case of stereo):
        if(*(o->channels) == 2)
        {
          for(unsigned long ii=  (i-i%(*(o->frameSize)));
                            ii<  (i+(o->FIXSTEP)*(*(o->frameSize)));
                            ii+= (*(o->frameSize)))
          {
            switch (*(o->bitRate))
            {
              case 8:  tmpR = wave[ii+1];
                       tmpR -= 128;
                       break;
              case 16: lendian(wave[ii+2], wave[ii+3], tmpR);
                       break;
            }
            newMaxR = (tmpR > newMaxR ? tmpR : newMaxR);
            newMinR = (tmpR < newMinR ? tmpR : newMinR);
          }
        }
      }

      if(x1 < (2*(o->virtualWave).size())-(2*(*(o->channels)))) // prevent segfault
      {
        o->virtualWave[x1+0] = newMinL;
        o->virtualWave[x1+1] = newMaxL;
        if(*(o->channels)==2)
        {
          o->virtualWave[x1+2] = newMinR;
          o->virtualWave[x1+3] = newMaxR;
          x1+=2;
        }
        x1+=2;
      }
      else
      {
        Debug::msg<string>("calcvirtualWave_thread: index out of range(1)\n",Globals::DBG_SEGFAULT_CAUGHT);
      }
    oldMinL = newMinL; oldMinR = newMinR;
    oldMaxL = newMaxL; oldMaxR = newMaxR;
    } // end of main for-loop
    Debug::msg<string>("::calcvirtualWave_thread(): loop done\n",Globals::DBG_FUNCTION_TRACE);
    o->calculating = false;
    Debug::msg<string>("::calcvirtualWave_thread(): leaving\n",Globals::DBG_FUNCTION_TRACE);
    Debug::stop_timer();

    return arg;
  } // end of calcvirtualWave()







//----------------------------------------------------------------------------//





  // debugging
  void Drawcalc::dump()
  {
    cerr << "dumping 'drawdata'\n";
    for(unsigned long i=0; i<drawdata.size(); i++)
      std::cout << i << " / " << drawdata[i] << endl;
    cerr << "'drawdata' dumped\n";
  }


//----------------------------------------------------------------------------//


  // debugging
  void Drawcalc::resize_vec()
  {
    cerr << "debug: getting virtualWave.size()\n";
    ulong vws = virtualWave.size();
    cerr << "debug: resizing virtualWave + 100\n";
    virtualWave.resize(vws + 100);
    cerr << "debug: resizing virtualWave - 100\n";
    virtualWave.resize(vws);
    cerr << "debug: leaving resize stuff\n";
  }



//----------------------------------------------------------------------------//



  // debugging
  void Drawcalc::dump_virtual()
  {
    cerr << "debug: dumping virtual wave to stdout... ";
    for(ulong i=0; i<virtualWave.size(); i++)
      cout << "vw[" << i << "]: " << virtualWave[i] << endl;
    cerr << "done\n";
  }




//----------------------------------------------------------------------------//


  // debugging
  void Drawcalc::dump_values()
  {
    cerr << "dumping Drawcalc's values:\n";
    cerr << "  *width :             " << *width << endl;
    cerr << "  *channels :          " << *channels << endl;
    cerr << "  drawdata.size() :    " << drawdata.size() << endl;
    cerr << "  virtualWave.size() : " << virtualWave.size() << endl;
    cerr << "  FIXSTEP :            " << FIXSTEP << endl;
    cerr << "  *waveSize :          " << *waveSize << endl;
    cerr << "  *drawStart :         " << *drawStart << endl;
    cerr << "  *drawEnd :           " << *drawEnd << endl;
    cerr << "  *frameSize :         " << *frameSize << endl;
    cerr << "Drawcalc's values dumped\n";
  }




//----------------------------------------------------------------------------//



  void Drawcalc::data_changed()
  {
    return this->calcvirtualWave();
  }


} // end of namespace

// end of file



