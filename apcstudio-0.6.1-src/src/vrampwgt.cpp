// $Id: vrampwgt.cpp,v 1.2 2002/01/10 12:44:23 martinhenne Exp $

// (C) 2001 Martin Henne
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

#include <vrampwgt.h>
#include <guiheaders.h>
#include <debug.h>

namespace APC
{
  Vrampwgt::Vrampwgt(int x, int y, int w, int h, char* c) : Fl_Widget(x,y,w,h)
  {
    S=100; E=100;
  }

  void Vrampwgt::draw()
  {
    int X=x(), Y=y(), W=w(), H=h();
    fl_color(FL_BLUE);
    fl_rectf(X,Y,W,H);
    fl_color(FL_GREEN);
    fl_begin_polygon();
    fl_rectf(X,Y+H/4,W,H/2);
    fl_color(FL_WHITE);
//    fl_line(X,Y,X+W,Y+H);
    int M = Y+H/2; // vertical middle
    int SD = static_cast<int>((H/4.0)*(S/100.0)); // start difference
    int ED = static_cast<int>((H/4.0)*(E/100.0)); // end difference
    fl_line(X,M-SD,X+W,M-ED);
    fl_line(X,M+SD,X+W,M+ED);
    fl_end_polygon();
  }

  void Vrampwgt::setstart(int val)
  {
    S = val;
    this->redraw();
  }

  void Vrampwgt::setend(int val)
  {
    E = val;
    this->redraw();
  }

}

