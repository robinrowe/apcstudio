// $Id: help.h,v 1.7 2002/02/24 16:56:36 martinhenne Exp $

// (C) 2001 Martin Henne
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

/*
 * this class is aimed to provide online-help for several functions
 * by popping up text-windows with a SHORT helptext AND a suggestion
 * where to find deep information in the manual (chapter, side...)
 *
 * beside, this class should handle the commandline help also
 *
 * all should be static
 *
 */

#ifndef _help_h
#define _help_h

#include <globals.h>

namespace APC
{

  class Help
  {
    public:
    /// This dumps commandline parameters and syntax to stdout.
    static void usage();
    static void manual();
    static void chapter(const char* url);
  };

} // end of namespace

#endif
// end of file

