// $Id: debug.cpp,v 1.8 2002/01/24 00:27:05 martinhenne Exp $

// (C) 2001 Martin Henne
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.


#include <debug.h>
#include <streamcast.h>
#include <globals.h>
#include <string>
#include <iostream>
#include <ctime>

namespace APC
{
using std::string;
using std::cerr;
clock_t Debug::start;
clock_t Debug::end;
unsigned long Debug::msg_counter = 0;
unsigned long Debug::debug_level = 0;

  void Debug::init(const unsigned long& level)
  {
    Debug::debug_level = level;
  }

  void Debug::newline()
  {
    #ifndef NODEBUG
    if(debug_level != 0)
    {
      cerr << std::endl;
    }
    #endif
  }

  void Debug::start_timer()
  {
    #ifndef NODEBUG
    if(debug_level != 0)
    {
      std::cerr << "<" << ++msg_counter << ">";
      std::cerr << "-- STARTING TIMER" << std::endl;
    }
    Debug::start = clock();
    #endif
  }

  void Debug::stop_timer()
  {
    #ifndef NODEBUG
    Debug::end = clock();
    if(debug_level != 0)
    {
      std::cerr << "<" << ++msg_counter << ">";
      std::cerr << "-- TIMER: "
        << ((1.0*end-start)/CLOCKS_PER_SEC)
        << std::endl;
    }
    #endif
  }

  void Debug::val(const int& value, const int& level)
  {
    #ifndef NODEBUG
    if(debug_level&level != 0)
    {
      cerr << "<" << ++msg_counter << ">";
      cerr << value;
    }
    #endif
  }

  void Debug::val(const double& value, const int& level)
  {
    #ifndef NODEBUG
    if(debug_level&level != 0)
    {
      cerr << "<" << ++msg_counter << ">";
      cerr << value;
    }
    #endif
  }

  void Debug::val_cr(const int& value, const int& level)
  {
    #ifndef NODEBUG
    if(debug_level&level != 0)
    {
      cerr << "<" << ++msg_counter << ">";
      cerr << value << endl;
    }
    #endif
  }

  void Debug::val_cr(const double& value, const int& level)
  {
    #ifndef NODEBUG
    if(debug_level&level != 0)
    {
      cerr << "<" << ++msg_counter << ">";
      cerr << value << endl;
    }
    #endif
  }

}


