// $Id: lendian.cpp,v 1.7 2002/02/17 16:45:23 martinhenne Exp $ 

// (c) Martin Henne 2001, 
// Martin.Henne@ web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

#include <lendian.h>

namespace APC
{
  void lendian (const unsigned char& low,
                const unsigned char& high,
                short& result)
  {
    result &= 0;
    result |= high;   result <<=(8);
    result |= low;
  }



  void lendian (const unsigned char& lsb,
                const unsigned char& middle,
                const unsigned char& msb,
                int& result)
  {
    result &= 0;
    result |= msb;    result <<=(8);
    result |= middle; result <<=(8);
    result |= msb;
    if (result&=0x800000==0x800000)
      result *= -1;
  }



  void lendian (const unsigned char& lsb,
                const unsigned char& low,
                const unsigned char& high,
                const unsigned char& msb,
                int& result)
  {
    result &= 0;
    result |= msb;    result <<=(8);
    result |= high;   result <<=(8);
    result |= low;    result <<=(8);
    result |= lsb;
    if (result&=0x80000000==0x80000000)
      result ^= 0xffffffff;
  }

  void getshortsLow (short s,
                     unsigned char& c)
  {
    s &=0x00ff;
    c = static_cast<unsigned char>(s);
  }

  void getshortsHigh (short s,
                      unsigned char& c)
  {
    s &= 0xff00;
    s >>=(8);
    c = static_cast<unsigned char>(s); 
  }

  unsigned char getshortsLow (short s)
  {
    s &=0x00ff;
    return static_cast<unsigned char>(s);
  }

  unsigned char getshortsHigh (short s)
  {
    s &= 0xff00;
    s >>=(8);
    return static_cast<unsigned char>(s);
  }


}



