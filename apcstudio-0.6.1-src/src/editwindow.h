// $Id: editwindow.h,v 1.75 2002/02/15 23:53:41 martinhenne Exp $

// Copyright 2001 Martin Henne 2001
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

// This class combines a Fl_Window and a Apc_fl_waveedit widget with
// a Wavefile and provides several editing options.


#ifndef _editwindow_h
#define _editwindow_h


//------ STL ------//
#include <string>
#include <iostream>

//------ aPcStudio headers ------//
#include <globals.h>
#include <wavefile.h>
#include <apc_fl_waveedit.h>
#include <convert.h>
#include <play.h>
#include <overlay.h>
#include <effects.h>
#include <clipboard.h>
#include <record.h>
#include <wavedraw.h>
#include <mixpaste.h>
#include <reverb.h>
#include <undo.h>
class Windowwrapper;
class AmpEffects;

#include <guiheaders.h>

namespace APC
{
using namespace std;
/// This is the main editing window.
/**
 * This class representes the waveedit-window itself. It
 * containes the waveedit widget, scrollbars, buttons,
 * status boxes and stuff.
 */
  class Editwindow
  {
  public:
    /// an aggregation, we need to 'know' the wavefile
    Wavefile*            wfile;

    /// copy constructor
    Editwindow(const Editwindow&);

    /// This is called by normal- and copyconstructor
    void common_constructing();

    /// assignment operator
    Editwindow& operator=(const Editwindow&);

    /// the clipboard
    static Clipboard theClipboard;

    /// zoomFactor in percent. This has to be above or equal 100.
    long double          zoomFactor;

    /// construcor
    Editwindow           ();

    /// destructor
    ~Editwindow          ();

    /// called, when fileNewMenu is choosen or aPcstudio was started without arguments
    bool newfile         ();

    /// reads a new file; call 'this->init()' after calling read().
    void readfile        (const string filename);

    /// shows the window on the screen (makes it visible)
    void show            ();

    /// sets a few important values, if they have changed (channels, bitrate...)
    /** when an important value has been changed by a effect or by
      * manipulating it somehow, please do call init(). If not, please
      * DO NOT call init(), for it takes time and should not be called
      * very often during an expensive calculation or redraw-function.
      */
    void init            ();

    /// This sets up the windowwrapper (handle to vector of all editwindows)
    void wwindex           (const ulong& index, Windowwrapper*);

    /// You can call this, if the data has changed.
    void data_changed    ();

    /// Apply colorsettings of class 'Settings'. Useful for Theming.
    void apply_colors();

    /// clear the undo buffer
    void clear_undo_buf();

    /// Apply format settings to all objects, that need it.
    void set_format(const int& frequency,
                    const int& channels,
                    const int& bitRate);

    /// call this before changing the wavedata, to provide undo!
    /**
      *  returns 'false' if no undo possible and user want's to abort!
      */
    bool push_undo();

    /// relabels the main editwindow
    /** The label of this window containes the filename of the
      * actual processed file. It that name changes, this function
      * should be called. */
    void relabelWindow   (const char* arg);

    /// relabels a single status box (cursorposition, mono/stereo...)
    /** Every information in the main editwindow is shown within
      * status boxes. The name of the status box can be seen in
      * section 'the lower area of the editwindow' in this file.
      * the function needs to new the new value of the box of the
      * first argument and the name of the box as second argument.
      */
    void relabel         (string val, string boxname);

    /// this adjusts the editwindow's scrollbar's size and position (!! no redraws are done !!)
    void adjustScrollbar ();

    /// this checks the bitfield 'redraw_needed' and redraws what needs to be redrawn
    void doRedraw        ();
    void draw_cursor     ();

    /// are we recording?
    bool is_recording    ();

    //------ callbacks from here ------//
    /** @name Callbacks
      * These callback functions are called by several widgets,
      * like buttons and menus. Usually there is a pair of a
      * static and an inline function to handle callbacks. To
      * understand the idea behind this, please refer to the
      * FLTK manual at http://www.fltk.org.
      */
    //@{
    static void cb_newfile_ok             (Raisebutton*, void*);
    inline void cb_newfile_ok_i           (Raisebutton*, void*);
    static void cb_newfile_cancel         (Raisebutton*, void*);

    inline void cb_catchclose_i           (Fl_Widget*, void*);
    static void cb_catchclose             (Fl_Widget*, void*);

    inline void cb_fileNewMenu_i          (Fl_Widget*, void*);
    static void cb_fileNewMenu            (Fl_Widget*, void*);

    inline void cb_fileOpenMenu_i         (Fl_Widget*, void*);
    static void cb_fileOpenMenu           (Fl_Widget*, void*);

    inline void cb_fileOpenRawMenu_i      (Fl_Widget*, void*);
    static void cb_fileOpenRawMenu        (Fl_Widget*, void*);

    inline void cb_fileSaveMenu_i         (Fl_Widget*, void*);
    static void cb_fileSaveMenu           (Fl_Widget*, void*);

    inline void cb_fileSaveAsMenu_i       (Fl_Widget*, void*);
    static void cb_fileSaveAsMenu         (Fl_Widget*, void*);

    inline void cb_fileNewWindowMenu_i    (Fl_Widget*, void*);
    static void cb_fileNewWindowMenu      (Fl_Widget*, void*);

    inline void cb_fileNewWindowOpenMenu_i(Fl_Widget*, void*);
    static void cb_fileNewWindowOpenMenu  (Fl_Widget*, void*);

    inline void cb_filePreferencesMenu_i  (Fl_Widget*, void*);
    static void cb_filePreferencesMenu    (Fl_Widget*, void*);

    inline void cb_fileCloseMenu_i        (Fl_Widget*, void*);
    static void cb_fileCloseMenu          (Fl_Widget*, void*);

    inline void cb_fileQuitMenu_i         (Fl_Widget*, void*);
    static void cb_fileQuitMenu           (Fl_Widget*, void*);

    inline void cb_editUndoMenu_i         (Fl_Widget*, void*);
    static void cb_editUndoMenu           (Fl_Widget*, void*);

    inline void cb_editRedoMenu_i         (Fl_Widget*, void*);
    static void cb_editRedoMenu           (Fl_Widget*, void*);

    static void cb_editClearUndoMenu      (Fl_Widget*, void*);

    inline void cb_editCutMenu_i          (Fl_Widget*, void*);
    static void cb_editCutMenu            (Fl_Widget*, void*);

    inline void cb_editCopyMenu_i         (Fl_Widget*, void*);
    static void cb_editCopyMenu           (Fl_Widget*, void*);

    inline void cb_editPasteMenu_i        (Fl_Widget*, void*);
    static void cb_editPasteMenu          (Fl_Widget*, void*);

    inline void cb_editSelectAllMenu_i    (Fl_Widget*, void*);
    static void cb_editSelectAllMenu      (Fl_Widget*, void*);

    inline void cb_editSelectNoneMenu_i   (Fl_Widget*, void*);
    static void cb_editSelectNoneMenu     (Fl_Widget*, void*);

    inline void cb_editCsr2SelStartMenu_i (Fl_Widget*, void*);
    static void cb_editCsr2SelStartMenu   (Fl_Widget*, void*);

    inline void cb_editCsr2SelEndMenu_i   (Fl_Widget*, void*);
    static void cb_editCsr2SelEndMenu     (Fl_Widget*, void*);

    inline void cb_editSilenceMenu_i   (Fl_Widget*, void*);
    static void cb_editSilenceMenu     (Fl_Widget*, void*);

    inline void cb_viewzoomAllMenu_i      (Fl_Widget*, void*);
    static void cb_viewzoomAllMenu        (Fl_Widget*, void*);

    inline void cb_viewzoomOutMenu_i      (Fl_Widget*, void*);
    static void cb_viewzoomOutMenu        (Fl_Widget*, void*);

    inline void cb_viewzoomInMenu_i       (Fl_Widget*, void*);
    static void cb_viewzoomInMenu         (Fl_Widget*, void*);

    inline void cb_viewzoomSelectMenu_i   (Fl_Widget*, void*);
    static void cb_viewzoomSelectMenu     (Fl_Widget*, void*);

    inline void cb_viewzoomSetMenu_i      (Fl_Widget*, void*);
    static void cb_viewzoomSetMenu        (Fl_Widget*, void*);

    inline void cb_effectsReverseMenu_i   (Fl_Widget*, void*);
    static void cb_effectsReverseMenu     (Fl_Widget*, void*);

    inline void cb_effectsFadeMenu_i      (Fl_Widget*, void*);
    static void cb_effectsFadeMenu        (Fl_Widget*, void*);

    inline void cb_effectsFreeFadeMenu_i  (Fl_Widget*, void*);
    static void cb_effectsFreeFadeMenu    (Fl_Widget*, void*);

    inline void cb_effectsFadeInMenu_i    (Fl_Widget*, void*);
    static void cb_effectsFadeInMenu      (Fl_Widget*, void*);

    inline void cb_effectsFadeOutMenu_i   (Fl_Widget*, void*);
    static void cb_effectsFadeOutMenu     (Fl_Widget*, void*);

    inline void cb_effectsNoiseGateMenu_i (Fl_Widget*, void*);
    static void cb_effectsNoiseGateMenu   (Fl_Widget*, void*);

    inline void cb_effectsEchoMenu_i      (Fl_Widget*, void*);
    static void cb_effectsEchoMenu        (Fl_Widget*, void*);

    inline void cb_effectsReverbMenu_i      (Fl_Widget*, void*);
    static void cb_effectsReverbMenu        (Fl_Widget*, void*);

    static void cb_helpAboutMenu          (Fl_Widget*, void*);

    inline void cb_debugDumpMenu_i        (Fl_Widget*, void*);
    static void cb_debugDumpMenu          (Fl_Widget*, void*);

    inline void cb_debugDrawDataMenu_i    (Fl_Widget*, void*);
    static void cb_debugDrawDataMenu      (Fl_Widget*, void*);

    inline void cb_scrollBar_i            (Fl_Scrollbar*, void*);
    static void cb_scrollBar              (Fl_Scrollbar*, void*);

    inline void cb_zoomInButton_i         (Raisebutton*, void*);
    static void cb_zoomInButton           (Raisebutton*, void*);

    inline void cb_zoomOutButton_i        (Raisebutton*, void*);
    static void cb_zoomOutButton          (Raisebutton*, void*);

    inline void cb_zoomResetButton_i      (Raisebutton*, void*);
    static void cb_zoomResetButton        (Raisebutton*, void*);

    inline void cb_zoomSelectButton_i     (Raisebutton*, void*);
    static void cb_zoomSelectButton       (Raisebutton*, void*);

    inline void cb_zoomStatus_i           (Raisebutton*, void*);
    static void cb_zoomStatus             (Raisebutton*, void*);

    inline void cb_selectAllButton_i      (Raisebutton*, void*);
    static void cb_selectAllButton        (Raisebutton*, void*);

    static void cb_cutButton              (Raisebutton*, void*);
    static void cb_copyButton             (Raisebutton*, void*);
    static void cb_pasteButton            (Raisebutton*, void*);

    inline void cb_playButton_i           (Raisebutton*, void*);
    static void cb_playButton             (Raisebutton*, void*);

    inline void cb_recordButton_i           (Raisebutton*, void*);
    static void cb_recordButton             (Raisebutton*, void*);

    inline void cb_stopButton_i           (Raisebutton*, void*);
    static void cb_stopButton             (Raisebutton*, void*);

    inline void cb_pauseButton_i          (Raisebutton*, void*);
    static void cb_pauseButton            (Raisebutton*, void*);

    inline void cb_zoom_tool_i            (Fl_Widget*, void*);
    static void cb_zoom_tool              (Fl_Widget*, void*);

    static void cb_rasterswitch           (Fl_Widget*, void*);

    static void cb_digitswitch            (Fl_Widget*, void*);

    static void cb_reverse_btn            (Raisebutton*, void*);

    static void cb_amplitude_btn          (Raisebutton*, void*);
    inline void cb_amplitude_btn_i        (Raisebutton*, void*);

    static void cb_editMixpasteMenu       (Fl_Widget*, void*);
    inline void cb_editMixpasteMenu_i     (Fl_Widget*, void*);

    static void cb_helpManualMenu         (Fl_Widget*, void*);

    static void cb_mixer_btn              (Fl_Widget*, void*);

    static void cb_echo_btn               (Fl_Widget*, void*);
    inline void cb_echo_btn_i             (Fl_Widget*, void*);

    static void cb_reverb_btn             (Fl_Widget*, void*);
    inline void cb_reverb_btn_i           (Fl_Widget*, void*);

    //------ end of callbacks ------//
    //@}

    /// Get status-text of statusbox (stopped, playing....)
    string getstatus();

    /// Set status-text of statusbox
    void setstatus(string);

    //------ information about the wavedata ------//
    /** @name Information about the wavefile
     * These informations should be updated by the function 'init()'
     * which is called after reading a file ('::readfile()'). If a
     * effect or something else changes ot manipulates this importand
     * data, you should call 'init()' again. Other memberfunctions
     * assume that these values are correct!!!
     */
    //@{
    int            frequency; // was sampleRate
    int            bitRate;
    unsigned long  waveSize;
    short          frameSize;
    int            channels;
    Play           player;                         // play the soundfile
    Record         recorder;
    unsigned short redraw_needed;                  // bitfield
    bool           followPlayCursor;               // follow the cursor while playing?
    bool           need_filename;                  // if true, 'Save' calls 'Save As'
    bool           unsaved;                        // if true, prompt before exiting or loading
    int            maincolor;
    int            darkercolor;
    int            fontcolor;
    int            cursorcolor;
    //@}


  private:
    // friends
    friend class Apc_fl_waveedit;
    friend class Overlay;
    friend class Effects;
    friend class Record;
    friend class AmpEffects;
    friend class Mixpaste;
    friend class Copy;
    friend class Reverb;
    friend class Undo;
    friend class Undo_operation;

  /** @name Widgets
   * These widgets are provided by the great FLTK GUI Toolkit.
   * See http://www.fltk.org to learn about their technical
   * background.
   */
  //@{
      Fl_Group*                   upperGroup;      // containes ALL widgets of this class
      //Fl_Double_Window*           ew;              // containes waveedit widget and lower group
      Overlay*                    ew;
      Effects*                    myEffects;

      /// See _Windowwrapper_ class
      Windowwrapper*              ww;
      ulong                       ww_index;

      /// This class containes the undo functionality
      Undo undo;

      string                      windowLabel;
      /** @name The Menubar
        * If you want to add a new menuitem, do it here first. But
        * be aware, that there are several other places to add the
        * item. But all of them are within the class 'Editwindow'.
        */
      //@{
      Fl_Menu_Bar*                menubar;         // upper menubar - most applications have one
        static Fl_Menu_Item       menus[];         // menus in the menubar
        static Fl_Menu_Item*      fileMenu;
          static Fl_Menu_Item*    fileNewMenu;
          static Fl_Menu_Item*    fileOpenMenu;
          static Fl_Menu_Item*    fileOpenRawMenu; // open a pure datafile, ask for format (channels/bits...)
          static Fl_Menu_Item*    fileSaveMenu;
          static Fl_Menu_Item*    fileSaveAsMenu;
          static Fl_Menu_Item*    fileNewWindowMenu; // up/down-sampling, bit-converting...
          static Fl_Menu_Item*    fileNewWindowOpenMenu;
          static Fl_Menu_Item*    fileNewWindowNewMenu;
          static Fl_Menu_Item*    filePreferencesMenu;
          static Fl_Menu_Item*    fileCloseMenu;
          static Fl_Menu_Item*    fileQuitMenu;
        static Fl_Menu_Item*    editMenu;
          static Fl_Menu_Item*    editUndoMenu;
          static Fl_Menu_Item*    editRedoMenu;
          static Fl_Menu_Item*    editClearUndoMenu;
          static Fl_Menu_Item*    editCutMenu;
          static Fl_Menu_Item*    editCopyMenu;
          static Fl_Menu_Item*    editPasteMenu;
          static Fl_Menu_Item*    editMixpasteMenu;
          static Fl_Menu_Item*    editSelectAllMenu;
          static Fl_Menu_Item*    editSelectNoneMenu; // markEnd = markStart = 0;
          static Fl_Menu_Item*    editCursorIsMarkStartMenu; // setmarkStart(cursorPosition);
          static Fl_Menu_Item*    editCursorIsMarkEndMenu;   // setmarkEnd(cursorPosition);
          static Fl_Menu_Item*    editSilenceMenu;
        static Fl_Menu_Item*    viewMenu;
          static Fl_Menu_Item*    viewZoomAllMenu;
          static Fl_Menu_Item*    viewZoomOutMenu;
          static Fl_Menu_Item*    viewZoomInMenu;
          static Fl_Menu_Item*    viewZoomSelectionMenu;
          static Fl_Menu_Item*    viewZoomFactorMenu;
          static Fl_Menu_Item*    viewJumpToMenu; // setdrawStart(...)
            static Fl_Menu_Item*    viewJumpToStartMenu;
            static Fl_Menu_Item*    viewJumpToEndMenu;
            static Fl_Menu_Item*    viewJumpToCursorMenu;
            static Fl_Menu_Item*    viewJumpToMarkStartMenu;
            static Fl_Menu_Item*    viewJumpToMarkEndMenu;
            static Fl_Menu_Item*    viewJumpToFrameMenu;
            static Fl_Menu_Item*    viewJumpToTimeMenu;
        static Fl_Menu_Item*    effectsMenu;
          static Fl_Menu_Item*    effectsReverseMenu;
          static Fl_Menu_Item*    effectsFadeMenu;
            static Fl_Menu_Item*    effectsFreeFadeMenu;
            static Fl_Menu_Item*    effectsFadeInMenu;
            static Fl_Menu_Item*    effectsFadeOutMenu;
          static Fl_Menu_Item*    effectsNoiseGateMenu;
          static Fl_Menu_Item*    effectsEchoMenu;
          static Fl_Menu_Item*    effectsReverbMenu;
        static Fl_Menu_Item*    controlsMenu;
          static Fl_Menu_Item*    controlsPlayMenu;
          static Fl_Menu_Item*    controlsRecordMenu;
          static Fl_Menu_Item*    controlsStopMenu;
          static Fl_Menu_Item*    controlsPauseMenu;
        static Fl_Menu_Item*    helpMenu;
          static Fl_Menu_Item*    helpManualMenu;
          static Fl_Menu_Item*    helpAboutMenu;


     //@}
      /** @name The area, that shows the wavedata */
      //@{
      //Apc_fl_waveedit*            we;              // shows the wavedata, scrolls it, and so on...
      Wavedraw*                     we;
      Fl_Box*                       toolbar;
      Fl_Group*                     toolbar_group;
        Fl_Group*                     tools_group;
        Raisebutton*                  new_tool;
        Raisebutton*                  open_tool;
        Raisebutton*                  save_tool;
        Raisebutton*                  undo_tool;
        Raisebutton*                  redo_tool;
        Raisebutton*                  selall_tool;
        Raisebutton*                  cut_tool;
        Raisebutton*                  copy_tool;
        Raisebutton*                  paste_tool;
        Fl_Box*                       zoom_tool_box;
        Fl_Button*                    zoom_tool;
        string                        zoom_tool_label;
        Fl_Box*                       percent_symbol;
        Raisebutton*                  zoomin_tool;
        Raisebutton*                  zoomout_tool;
        Raisebutton*                  showall_tool;
        Raisebutton*                  zoomsel_tool;
        Raisebutton*                  raster_tool;
        Raisebutton*                  digit_tool;
        Fl_Group*                     tool_resizer;

      //@}
      /** @name The lower area of the editwindow
       * All these privat members of 'Editwindow' are
       * the widgets at the lower area of the Editwindow.
       * In brief, here you find the zoom buttons, the buttons
       * play, stop, pause, record ...., the status boxes, that
       * inform the user about the current cursorpositon, the
       * selected and the displayed area and so on...
       */
      //@{
      Fl_Group*                   lowerGroup;      // buttons, scrollbar, statusBoxes... below wavedata
        Fl_Scrollbar*             scrollBar;       // scrolls the visible wavedata
        Fl_Box*                   lowestBox;
        Fl_Group*                 lowest1Group;    // containes all buttons
          Fl_Group*               zoomGroup;       // shows logo, former zoombuttons
          Fl_Button*              logo;
          Fl_Group*               editGroup;       // containes effect buttons
          Fl_Box*                 editBox;
            Fl_Button*            reverse_btn;
            Fl_Button*            amplitude_btn;
            Fl_Button*            echo_btn;
            Fl_Button*            reverb_btn;
          Fl_Box*                 editBox2;
            Fl_Button*            mixer_btn;
            Fl_Group*             editResizeGroup; // between playcontrol and edit buttons

          Fl_Group*               playGroup;       // play|stop|pause... (buttons)
            Fl_Box*               playBox;
            Raisebutton*            backButton;
            Raisebutton*            stopButton;
            Raisebutton*            playButton;
            Raisebutton*            recordButton;
            Raisebutton*            pauseButton;
            Raisebutton*            forwardButton;
        Fl_Group*                 lowest2Group;
          Fl_Group*               text1Group;      // status|cursorposition (text)
            Fl_Box*               textBox_status;
            Fl_Box*               textBox_cursorposition;
          Fl_Group*               resize2Group;    // this one is resizable
          Fl_Group*               text2Group;      // start|end|length (text)
            Fl_Box*               textBox_start;
            Fl_Box*               textBox_end;
            Fl_Box*               textBox_length;
        Fl_Group*                 lowest3Group;
          Fl_Group*               statusGroup;
            Fl_Box*               statusBox_status;// playing|stopped|paused ...
            string                status_label;
            Fl_Box*               statusBox_cpos;  // cursorposition (e.g. 01:01.150)
            string                cpos_label;
            Fl_Box*               statusBox_size;  // wavesize
            string                size_label;
            Fl_Box*               statusBox_freq;  // frequency (e.g. 22 KHz)
            string                freq_label;
            Fl_Box*               statusBox_rate;  // bitrate (e.g. 16 Bit)
            string                rate_label;
            Fl_Box*               statusBox_chan;  // channels (e.g. mono|stereo|quadro)
            string                chan_label;
            Fl_Box*               statusBox_length;// length (e.g. 01:01.150)
            string                length_label;
          Fl_Group*               resize3Group;    // the resizable one
            Fl_Box*               textBox_view;
            Fl_Box*               textBox_selection;
          Fl_Group*               timesGroup;      // drawStart, drawEnd and stuff
            Fl_Box*               drawStart;       // shows      -"-       ::drawStart
            string                drawStartLabel;
            Fl_Box*               drawEnd;         // shows      -"-       ::drawEnd
            string                drawEndLabel;
            Fl_Box*               drawLength;
            string                drawLengthLabel;
            Fl_Box*               markStart;       // shows      -"-       ::markStart
            string                markStartLabel;
            Fl_Box*               markEnd;         // shows      -"-       ::markEnd
            string                markEndLabel;
            Fl_Box*               markLength;
            string                markLengthLabel;
      //@}
    //@}


    /** @name bitfield: redraw_needed
     * The int 'redraw_needed' represents a bitfield of what
     * part of the Editwindow needs to be redrawn. The const
     * private members of this group are pseudonyms to single
     * bits and should be used for bitwise manipulating the
     * 'redraw_needed' bitfield of class Editwindow. For example,
     * after doing a 'redraw_needed |= REDR_STATUS;' call the
     * function 'Editwindow::doRedraw();' to redraw AT LEAST the
     * status Box 'status' which shows 'playing', 'paused'...
     */
    //@{
    /// cursorposition needs redraw
    static const unsigned short REDR_CSRPOS   = 1;
    /// status needs redraw
    static const unsigned short REDR_STATUS   = 2;
    /// wave format status needs redraw
    static const unsigned short REDR_WAVEFMT  = 4;
    /// view start, end, length needs redraw
    static const unsigned short REDR_VIEWSTAT = 8;
    /// selection start, end, length needs redraw
    static const unsigned short REDR_SELSTAT  = 16;
    /// waveedit needs redraw
    static const unsigned short REDR_WAVE     = 32;
    /// everything needs redraw
    static const unsigned short REDR_ALL      = 65535;
    //@}
 }; // end of class Editwindow

} // end of namespace

// end of file

#endif

