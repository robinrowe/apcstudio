// $Id: askformat.cpp,v 1.9 2001/12/16 14:22:37 martinhenne Exp $

// (C) 2001 Martin Henne
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.


#include <iostream>
#include <wavefile.h>
#include <fmtchunk.h>
#include <askformat.h>
#include <configfile.h>
#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Check_Button.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Box.H>
#include <debug.h>
#include <commandline.h>
#include <settings.h>

#define WIDTH  250
#define HEIGTH 250

namespace APC
{
using namespace std;

  Wavefile*        Askformat::w;
  Fl_Window*       Askformat::win;
  Fl_Check_Button* Askformat::_8bit;
  Fl_Check_Button* Askformat::_16bit;
  Fl_Check_Button* Askformat::_11khz;
  Fl_Check_Button* Askformat::_22khz;
  Fl_Check_Button* Askformat::_44khz;
  Fl_Check_Button* Askformat::_mono;
  Fl_Check_Button* Askformat::_stereo;
  Fl_Button*       Askformat::_telephone;
  Fl_Button*       Askformat::_radio;
  Fl_Button*       Askformat::_cd;
  Fl_Button*       Askformat::ok;
  Fl_Button*       Askformat::cancel;
  Fl_Box*          Askformat::bitLabel;
  Fl_Box*          Askformat::speedLabel;
  Fl_Box*          Askformat::channelsLabel;
  bool             Askformat::done = false;
  bool             Askformat::returnval = false;

  void Askformat::cb_ok     (Fl_Button*, void*)
  {
    Fmt_chunk& fc = w->getfmt();

    if(_8bit  ->value() == 1)     fc.setwBitsPerSample   (8);
    if(_16bit ->value() == 1)     fc.setwBitsPerSample   (16);
    if(_11khz ->value() == 1)     fc.setdwSamplesPerSec  (11025);
    if(_22khz ->value() == 1)     fc.setdwSamplesPerSec  (22050);
    if(_44khz ->value() == 1)     fc.setdwSamplesPerSec  (44100);
    if(_mono  ->value() == 1)     fc.setwChannels        (1);
    if(_stereo->value() == 1)     fc.setwChannels        (2);

    w -> setBitrate    (fc.getwBitsPerSample());
    w -> setChannels   (fc.getwChannels());
    w -> setSamplerate (fc.getdwSamplesPerSec());

    done = true;
    returnval = true;
    win -> hide();
  }

  void Askformat::cb_cancel (Fl_Button*, void*)
  {
    done = true;
    returnval = false;
    win -> hide();
  }

  void Askformat::cb_telephone (Fl_Button*, void*)
  {
    _8bit  ->setonly();
    _22khz ->setonly();
    _mono  ->setonly();
  }

  void Askformat::cb_radio (Fl_Button*, void*)
  {
    _16bit  ->setonly();
    _22khz  ->setonly();
    _stereo ->setonly();
  }

  void Askformat::cb_cd (Fl_Button*, void*)
  {
    _16bit   ->setonly();
    _44khz   ->setonly();
    _stereo  ->setonly();
  }

  bool Askformat::ask (Wavefile* wave)
  {
    Debug::msg<string>("Askformat::ask(Wavefile*): called\n",Globals::DBG_FUNCTION_TRACE);
    done = false; // should be set to true by a callback
    w             = wave;
    int wincolor  = 25;
    int darkcol   = 27;
    int fontcol   = 7;
    wincolor      = Settings::MAIN_COLOR;
    darkcol       = Settings::MAIN_DARKER_COLOR;
    fontcol       = Settings::FONT_COLOR;
    // compose the window
    win           = new Fl_Window          (WIDTH,HEIGTH,"Specify the new Waveformat");
    win          -> color                  (wincolor);
    bitLabel      = new Fl_Box             (10, 10,WIDTH/2,20,"Bitrate:");
    bitLabel     -> labelcolor             (fontcol);
    bitLabel     -> labelfont              (FL_HELVETICA_BOLD);
    bitLabel     -> labelsize              (12);
    bitLabel     -> align                  (FL_ALIGN_LEFT|FL_ALIGN_INSIDE);
    Fl_Group bitGroup(30,30,WIDTH/2-30,40);
    _8bit         = new Fl_Check_Button    (30, 30,WIDTH/2,20,"8 bit");
    _8bit        -> labelcolor             (fontcol);
    _8bit        -> labelsize              (12);
    _8bit        -> down_box               (FL_DOWN_BOX);
    //_8bit        -> color                  (7,0);
    _8bit        -> type                   (FL_RADIO_BUTTON);
    _16bit        = new Fl_Check_Button    (30, 50,WIDTH/2,20,"16 bit");
    _16bit       -> labelcolor             (fontcol);
    _16bit       -> labelsize              (12);
    _16bit       -> down_box               (FL_DOWN_BOX);
    //_16bit       -> color                  (7,0);
    _16bit       -> type                   (FL_RADIO_BUTTON);
    bitGroup.end();
    speedLabel    = new Fl_Box             (WIDTH/2, 10,WIDTH/2,20,"Frequency:");
    speedLabel   -> labelcolor             (fontcol);
    speedLabel   -> labelfont              (FL_HELVETICA_BOLD);
    speedLabel   -> labelsize              (12);
    speedLabel   -> align                  (FL_ALIGN_LEFT|FL_ALIGN_INSIDE);
    Fl_Group freqGroup(WIDTH/2+20,30,WIDTH/2-20,60);
    _11khz        = new Fl_Check_Button    (WIDTH/2+20, 30,WIDTH/2,20,"11025 Hz");
    _11khz       -> labelcolor             (fontcol);
    _11khz       -> labelsize              (12);
    _11khz       -> down_box               (FL_DOWN_BOX);
    //_11khz       -> color                  (7,0);
    _11khz       -> type                   (FL_RADIO_BUTTON);
    _22khz        = new Fl_Check_Button    (WIDTH/2+20, 50,WIDTH/2,20,"22050 Hz");
    _22khz       -> labelcolor             (fontcol);
    _22khz       -> labelsize              (12);
    _22khz       -> down_box               (FL_DOWN_BOX);
    //_22khz       -> color                  (7,0);
    _22khz       -> type                   (FL_RADIO_BUTTON);
    _44khz        = new Fl_Check_Button    (WIDTH/2+20, 70,WIDTH/2,20,"44100 Hz");
    _44khz       -> labelcolor             (fontcol);
    _44khz       -> labelsize              (12);
    _44khz       -> down_box               (FL_DOWN_BOX);
    //_44khz       -> color                  (7,0);
    _44khz       -> type                   (FL_RADIO_BUTTON);
    freqGroup.end();
    channelsLabel = new Fl_Box             (10,110,WIDTH/2,20,"Channels:");
    channelsLabel-> labelcolor             (fontcol);
    channelsLabel-> labelfont              (FL_HELVETICA_BOLD);
    channelsLabel-> labelsize              (12);
    channelsLabel-> align                  (FL_ALIGN_LEFT|FL_ALIGN_INSIDE);
    Fl_Group chanGroup(30,130,WIDTH/2-30,40);
    _mono         = new Fl_Check_Button    (30,130,WIDTH/2,20,"mono");
    _mono        -> labelcolor             (fontcol);
    _mono        -> labelsize              (12);
    _mono        -> down_box               (FL_DOWN_BOX);
    //_mono        -> color                  (7,0);
    _mono        -> type                   (FL_RADIO_BUTTON);
    _stereo       = new Fl_Check_Button    (30,150,WIDTH/2,20,"stereo");
    _stereo      -> labelcolor             (fontcol);
    _stereo      -> labelsize              (12);
    _stereo      -> down_box               (FL_DOWN_BOX);
    //_stereo      -> color                  (7,0);
    _stereo      -> type                   (FL_RADIO_BUTTON);
    chanGroup.end();
    _telephone    = new Fl_Button          (WIDTH/2+20,110,80,20,"Telephone");
    _telephone   -> labelcolor             (fontcol);
    _telephone   -> color                  (wincolor);
    _telephone   -> labelsize              (12);
    _telephone   -> callback               ((Fl_Callback*)Askformat::cb_telephone);
    _radio        = new Fl_Button          (WIDTH/2+20,130,80,20,"Radio");
    _radio       -> labelcolor             (fontcol);
    _radio       -> color                  (wincolor);
    _radio       -> labelsize              (12);
    _radio       -> callback               ((Fl_Callback*)Askformat::cb_radio);
    _cd           = new Fl_Button          (WIDTH/2+20,150,80,20,"CD");
    _cd          -> labelcolor             (fontcol);
    _cd          -> color                  (wincolor);
    _cd          -> labelsize              (12);
    _cd          -> callback               ((Fl_Callback*)Askformat::cb_cd);
    ok            = new Fl_Button          (30,200,80,30," OK ");
    ok           -> labelcolor             (fontcol);
    ok           -> labelsize              (12);
    ok           -> color                  (wincolor);
    ok           -> callback               ((Fl_Callback*)Askformat::cb_ok);
    cancel        = new Fl_Button          (WIDTH/2+10,200,80,30,"Cancel");
    cancel       -> labelcolor             (fontcol);
    cancel       -> labelsize              (12);
    cancel       -> color                  (wincolor);
    cancel       -> callback               ((Fl_Callback*)Askformat::cb_cancel);
    win     -> end                    ();

    // show the window
    win->show();

    // wait, while win is visible
    while(win->visible() || done==false)
    {
      Fl::check();
      Fl::wait(0.05);
    }
    Debug::msg<string>("Askformat::ask(Wavefile*): leaving\n",Globals::DBG_FUNCTION_TRACE);
    return returnval;
  }

  /// This is never called
  Askformat::Askformat()
  {}

}


