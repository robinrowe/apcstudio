// $Id: drawcalc.h,v 1.12 2001/12/16 14:22:37 martinhenne Exp $

// (C) 2001 Martin Henne
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

/*
 * This class does all calculations that are needed to
 * display a waveform. The results are used by Apc_fl_waveedit::draw_wave();
 */


#ifndef _drawcalc_h
#define _drawcalc_h

#include <globals.h>
#include <vector>

namespace APC
{
using namespace std;
  // forward
  class Apc_fl_waveedit;

  class Drawcalc
  {
  private:
    bool                     calculating;
    Apc_fl_waveedit*         we;
    unsigned long*           waveSize;
    unsigned long*           drawStart;
    unsigned long*           drawEnd;
    int*                     channels;
    int*                     bitRate;
    double*                  XSTEP;
    short*                   frameSize;
    vector<short>            virtualWave;
    vector<short>            drawdata;
    const int                FIXSTEP; // 100
    int*                     pix_per_sample;
    int*                     width;
    int*                     heigth;
    void                     fit2screen();
    void                     calcvirtualWave    ();
    static void*             calcvirtualWave_thread(void*);
    int                      percent;
  friend class Apc_fl_waveedit;

  public:
    Drawcalc();
    /// Get a vector with all drawing information (min/max-pairs for each channel).
    vector<short>&           getdrawdata        ();

    /// Call this once before using other member functions.
    void                     init               (Apc_fl_waveedit*);

    /// Call this, if wavefile changed (e.g. cut/paste and so on).
    void                     data_changed       ();

    // debugging stuff
    void                     dump               (); // dump drawdata to stdout
    void                     resize_vec         (); // resize virtualWave +1OO/-1OO
    void                     dump_virtual       (); // dump virtualWave to stdout
    void                     dump_values        (); // dump values like *width, drawdata.size()....

  };

}

#endif


