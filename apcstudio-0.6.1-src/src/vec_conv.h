// $Id: vec_conv.h,v 1.1 2002/02/12 02:47:08 martinhenne Exp $

// (C) 2002 Martin Henne
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

#ifndef _vec_conv_h
#define _vec_conv_h

#include <vector>
#include <string>
#include <lendian.h>
#include <globals.h>

class Vec_conv
{
public:
  static std::vector<short>* uchar2short(const std::vector<uchar>& source,
                                 const ulong& startelement,
                                 ulong& targetlength_in_elements = 0);
  static std::vector<short>* uchar2int  (const std::vector<uchar>& source,
                                 const ulong& startelement,
                                 ulong& targetlength_in_elements = 0);
  static std::vector<uchar>* short2uchar(const std::vector<short>& source,
                                 const ulong& startelement,
                                 ulong& targetlength_in_elements = 0);
  static std::vector<uchar>* int2uchar  (const std::vector<int>& source,
                                 const ulong& startbyte,
                                 ulong& targetlength_in_elements = 0);
};


#endif

// end of file


