// $Id: main.cpp,v 1.40 2002/02/24 16:56:36 martinhenne Exp $

// Copyright 2001 Martin Henne 2001
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

/*
 *  This is the main()-function of aPcStudio.
 *
 */

#include <globals.h>
#include <FL/Fl.H>
#include <help.h>
#include <editwindow.h>
#include <string>
#include <configfile.h>
#include <popups.h>
#include <vector>
#include <commandline.h>
#include <iostream>
#include <debug.h>
#include <streamcast.h>
#include <windowwrapper.h>
#include <settings.h>
#include <fobjects.h>
#include <rand.h>
#include <algorithm>
#include <FL/Fl_File_Icon.H>
#include <guiheaders.h>
#include <askstart.h>

int main(int argc, char** argv)
{
  using namespace APC;
  using namespace std;

  // standard message for unexpected exceptions:
  std::string unexpected_error("Please send a bug report:");

  try { // this is the outermost try-block!

  // load mime-icons for the filebrowser
  Fl_File_Icon::load_system_icons();

  // Threads need this
  Fl::lock();

  // Tooltips fontsize for all widgets
  Fl_Tooltip::size(10);

  // parse commandline parameters
  Commandline::init(argc,argv);

  if(Commandline::check_for("d,debug-level"))
    std::cerr << "main(): called, reading settings\n";

  // Read Configfile
  Settings::read();

  // Initialize Randomizer
  Rand::init();

  if(Commandline::check_for("d,debug-level"))
    std::cerr << "main(): settings read, setting debug-level\n";

  // initialize Debug-class
  int Dlevel = 0;
  if(Commandline::check_for("d,debug-level",Dlevel))
  {
    cerr << "setting debug-level to " << Dlevel << endl;
    Debug::init(Dlevel);
  }

  Debug::msg<string>("** debugging activated!\n");

  // check, if aPcStudio runs for the first time
  // a few things needs configuring then
  bool first_time = true;
  Configfile::get("FIRST_TIME",first_time);
  if(first_time) {
    string message = string("I think you run aPcStudio for the first time!\n")+
                     string("Please adjust path's to online docu and mixer\n")+
                     string("application using 'Preferences' in 'File' menu.\n")+
                     string("Docu is also available at http://apcstudio.sf.net");
    Popups::warn(message,"First Start?");
  }
  Configfile::set("FIRST_TIME",(bool)false);

  // option -v or --version given? If yes, we're done.
  if(Commandline::check_for(string("v,version")))
  {
    std::cout << "aPcStudio "
              << Globals::sVERSION
              << " - (C) 2001, 2002 Martin Henne GNU/GPL V2"
              << std::endl
              << "Compiletime: "
              << Globals::CDATE << ", "
              << Globals::CTIME << std::endl
              << "Please support Linux\n";
    exit(0);
  }

  // option -h of --help given?
  if(Commandline::check_for(string("h,help")))
  {
    Help::usage();
    exit(0);
  }

  // looking for testers
  if(!Commandline::check_for("0,no-bother"))
  {
    string message = string("aPcStudio is work in progress. This version\n")+
                     string("is released to get tested, what is done so far.\n")+
                     string("Please consider becoming a beta-tester and send\n")+
                     string("email to the author (see help->about). Thanks.");
    Popups::error(message.c_str(),"Testers wanted!!!");
  }

  // getting all arguments, that are no parameters
  vector<string> files_to_open;
  files_to_open = Commandline::get_args(Globals::CLEXCLUDE);// "ibBfFCR"

  unsigned short files = files_to_open.size(); // how many files?

  Debug::msg<string>(string("opening ")+
             stream_cast<string>(files)+
             string(" files...\n"));


  Windowwrapper ww;
  APC::Settings::init_wrapper(static_cast<void*>(&ww));

  // one or more files specified at commandline
  try {
    if(files > 0)
    {
      for(unsigned short i=0; i<files; i++) // for each file
      {
        string f = static_cast<string>(files_to_open[i]);
        Editwindow* ew = new Editwindow();
        ulong wwi = ww.registrate(ew);
        ew->wwindex(wwi,&ww);
        ew->readfile(f);
        Debug::msg<string>("main(): file opened, now showing editwindow\n");
        ew->show();
        Debug::msg<string>("main(): editwindow shown\n");
      }
    }
  }
  catch(no_fileaccess) { // if one of the files failed to open!
    files_to_open.clear();
    files = 0;
    Popups::error("One or more files could not be opened!\nNo panic, we assume you hadn't specified any!");
  }
  catch(std::bad_alloc) {
    Popups::error("Hm, you seem to have not enough memory.\nI'm sorry, but I have to exit.");
    exit(0);
  }

  // if no files are specified at commandline,
  // we want to open a new window with or without
  // a file loaded:
  if(files == 0)
  {
    Editwindow* ew = NULL;
    bool use_last_opened = Settings::USE_LAST_OPENED;
    bool open_new = Commandline::check_for("n,newfile");

    std::string option("");

    if((open_new==false) && (use_last_opened==false)) {
      Askstart* as = new Askstart;
      option = as->ask();
    }

    if(option==string("new")) { // create a completely new file
      ew = new Editwindow;
      ulong wwi = ww.registrate(ew);
      ew->wwindex(wwi,&ww);
      if(!ew->newfile()) // if canceled, no file :-)
        ew = NULL;
    }

    if((use_last_opened==false) && option==string("open")) // pop up a filerequester
    {
      ew = new Editwindow;
      ulong wwi = ww.registrate(ew);
      ew->wwindex(wwi,&ww);
      const char* f = fl_file_chooser("Open File","*.wav","");
      if(f)
        ew->readfile(f);
      else {
        std::cout << "aPcStudio: filerequester cancel, exiting...\n";
        exit(0);
      }
    }

    if(use_last_opened && !open_new) // use last opened file
    {
      string lastfile = Settings::LAST_OPENED;
      ew = new Editwindow;
      ulong wwi = ww.registrate(ew);
      ew->wwindex(wwi,&ww);
      ew->readfile(lastfile);
    }

    if(ew!=NULL)
      ew->show();
  }

  return Fl::run();

  } // end of outermost try-block
  catch(exception& e) {
    Popups::error(e.what(),unexpected_error);
  }
  catch(...) {
    Popups::error("You found a serious bug! Please try to\nreproduce it and send a bug-report\nto apcstudio-bugs@@lists.sf.net!");
  }
}


