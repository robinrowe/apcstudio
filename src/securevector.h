// $Id: securevector.h,v 1.1 2001/12/03 14:32:59 martinhenne Exp $

// (C) 2001 Martin Henne
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

// This is an encapsulted vector with protected access.

#ifndef _charvec_h
#define _charvec_h

#include <vector>
#include <debug.h>

namespace APC
{
  using std::vector;
  using APC::Debug;

  /// This is an encapsulted vector with protected access.
  template<class T>
  class SecureVector
  {
    typedef unsigned char uchar;
    typedef unsigned long ulong;
 
  public:
//----------------------------------------------------------------------------//
    /// This is the prefered constructor. Please use it!
    explicit SecureVector(ulong size)
    {
      vec.resize(size);
      vec_size=vec.size();
    }

//----------------------------------------------------------------------------//
    /// This is for implicit type adaption.
    SecureVector(vector<T>& arg)
    {
      vec = arg;
      vec_size = vec.size();
    }

//----------------------------------------------------------------------------//
    /// Copy constructor.
    SecureVector(const SecureVector& other)
    {
      this->vec = other->vec;
      this->vec_size = other->size();
    }

//----------------------------------------------------------------------------//
    /// Destructor.
    ~SecureVector()
    {
      vec.clear();
    }

//----------------------------------------------------------------------------//
    /// Assignment operator.
    SecureVector& operator=(const SecureVector& rhs)
    {
      if(this == &rhs)
        return *this;
      this->vec = rhs.vec;
      this->vec_size = rhs.size();
      return *this;
    }

//----------------------------------------------------------------------------//
    /// Assignment of vector<T>.
    SecureVector& operator=(const vector<T>& rhs)
    {
      this->vec = rhs;
      this->vec_size = rhs.size();
      return *this;
    }

//----------------------------------------------------------------------------//
    /// Type adaption to vector<uchar>.
    operator vector<T>()
    {
      return vec;
    }

//----------------------------------------------------------------------------//
    /// Assign an element at position.
    inline void at(const ulong& pos, const T& val)
    {
      if(pos<vec_size)
      {
        vec[pos]=val;
        return;
      }
      std::cerr << "SecureVector::at(write): index out of range.\n";
    }

//----------------------------------------------------------------------------//
    /// Get value at position.
    inline T at(const ulong& pos) const
    {
      if(pos<vec_size)
        return vec[pos];
      std::cerr << "SecureVector::at(read): index out of range, returning value of last element.\n";
      return vec[vec_size-1];
    }

//----------------------------------------------------------------------------//
    /// Add a value at the end, resize vector +1.
    void push_back(const T& val)
    {
      vec.push_back(val);
      ++vec_size;
    }

//----------------------------------------------------------------------------//
    /// Resize vector.
    void resize(const ulong& newsize)
    {
      vec.resize(newsize);
      vec_size=vec.size();
    }

//----------------------------------------------------------------------------//
    /// Clear vector.
    void clear()
    {
      vec.clear();
      vec_size=vec.size();
    }

//----------------------------------------------------------------------------//
  private:
    SecureVector(); // no objects without size!
  
    vector<T> vec;
    ulong vec_size;
  };

} // end of namespace

#endif

