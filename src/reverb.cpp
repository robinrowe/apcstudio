// $Id: reverb.cpp,v 1.8 2002/02/24 16:56:36 martinhenne Exp $

// (C) 2001 Martin Henne
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

#include <reverb.h>
#include <editwindow.h>
#include <popups.h>
#include <sleeper.h>
#include <streamcast.h>
#include <effects.h>
#include <mixpaste.h>
#include <askreverb.h>
#include <debug.h>
#include <rand.h>
#include <datachunk.h>
#include <copy.h>
#include <busywin.h>
#include <fader.h>
#include <mstoframe.h>

void Reverb::perform(std::vector<uchar>& data,
                     const int& channels,
                     const int& bitRate,
                     const int& frequency,
                     ulong startframe,
                     ulong endframe,
                     int duration,
                     int volume,
                     int attack,
                     int delay,
                     int distance,
                     double random,
                     double ratio,
                     int channel)
{
  APC::Debug::msg<std::string>("Reverb::perform(): called\n",Globals::DBG_FUNCTION_TRACE);

  // get information about the data:
  const int frameSize           = (bitRate/8)*channels;
  const int duration_frames        = static_cast<int>(frequency/1000.0*(duration));

  // create reverb buffer
  Busywin* busy = new Busywin("Reverb may take a while...");
  busy->show(); Fl::check(); Fl::wait(0.1); Sleeper::dosleep(10);
  busy->tell("getting reverbration buffer...");

  // assign reverb buffer
  vector<uchar> reverb_buf;
  try { // this may fail, if no memory left
    APC::Copy::copy(data,reverb_buf,startframe,endframe,frameSize);
  }
  catch(std::bad_alloc) {
    APC::Popups::error("No memory left for reverb buffer!");
    return;
  }

  // configuring buffer's volume ramp
  double ratio_start, ratio_end;
  ratio_start = ratio_end = 1.0;
  if(ratio < 0) {
    ratio_start = -1.0 * ratio;
    ratio_end   = 1.0;
  }
  else {
    ratio_start = 1.0;
    ratio_end   = ratio;
  }

  // apply attack time fadein
  fader()(reverb_buf,0,mstoframe()(attack,frequency),ratio_start,ratio_end,bitRate,channels,frameSize,channel);

  APC::Debug::msg<std::string>("Reverb::perform(): checkpoint 1\n",Globals::DBG_FUNCTION_TRACE);

  // add delay to reverb start
  startframe += mstoframe()(delay,frequency);

  // randomizer factors
  int rand_fac = static_cast<int>(100.0*(1.0-random));

  // fade out reverb buffer a bit (testing)
  fader()(reverb_buf,0,reverb_buf.size()/frameSize,ratio_start,ratio_end,bitRate,channels,frameSize,channel);

  const int average_echo_distance = distance; // in ms
  const int echoes                = 1+static_cast<int>((duration*1.0)/average_echo_distance);
  const int framestep             = duration_frames / echoes;
  const double decay_fac          = 1.0 / echoes;
  double mixvolume   = 0.0;
  ulong position     = 0;
  string totell("");

  APC::Debug::msg<std::string>("Reverb::perform(): checkpoint 2\n",Globals::DBG_FUNCTION_TRACE);

  for(int i=0; i<echoes; ++i) {
    // informing the user about the progress
    totell = string("creating reverb, pass ") +
             stream_cast<string>(i+1) +
             string(" of ") +
             stream_cast<string>(echoes);
    busy->tell(totell);

    // setting next paste-position and reset mixvolume!
    position  = startframe + (i+1) * framestep;
    mixvolume = volume/100.0;

    // dissorting the echoes a little bit, sorry for the old casts
    position  -= (int)(framestep * (Rand::gen(0,rand_fac)/100.0));
    position  += (int)(framestep * (Rand::gen(0,rand_fac)/100.0));

    // fiddeling with the volume knob
    mixvolume = mixvolume * (Rand::gen(70,120)/100.0);

    // mix buffer and original waveform
    Mixpaste::overlay(data,
                      reverb_buf,
                      position,
                      bitRate,channels,frameSize,
                      mixvolume,
                      channel);

    //std::cerr << "decay_fac: " << decay_fac << std::endl;

    // fade buffer depending on decay factor
    fader()(reverb_buf,
            0,
            reverb_buf.size()/frameSize,
            1.0-decay_fac,
            1.0-decay_fac,
            bitRate,channels,frameSize,
            channel);
  }

  APC::Debug::msg<std::string>("Reverb::perform(): checkpoint 3\n",Globals::DBG_FUNCTION_TRACE);

  reverb_buf.clear();
  busy->hide();

  APC::Debug::msg<std::string>("Reverb::perform(): leaving\n",Globals::DBG_FUNCTION_TRACE);
  return;
}




// end of file
