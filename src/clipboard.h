// $Id: clipboard.h,v 1.4 2001/12/16 14:22:37 martinhenne Exp $

// (c) Martin Henne 2001, license: GNU/GPL V2
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

/*
 * This class is to exchange data between wavefile
 * objects. Therefore it contains a byte-vector.
 * THIS CLASS SHOULD BE USED STATIC
 */

#ifndef _clipboard_h
#define _clipboard_h

#include "globals.h"
#include "fmtchunk.h"
#include "datachunk.h"
#include <string>
#include <vector>

namespace APC
{
using namespace std;
  /// The clipboard should be used, to exchange wavedata between opened windows.
  /**
    * This class is static member of Editwindow.
    */
  class Clipboard
  {
  private:
    unsigned short  channels;           // 2 Bytes        default: 2
    unsigned long   frequency;          // 4 Bytes        default: 44100
    unsigned short  frameSize;          // 2 bytes        default: 2*(16/8)
    unsigned short  bitRate;            // 2 Bytes        default: 16
 
  public:
    void            setchannels       (unsigned short);
    void            setfrequency      (unsigned long );
    void            setframeSize      (unsigned short);
    void            setbitRate        (unsigned short);

    unsigned short  getchannels       ();
    unsigned long   getfrequency      ();
    unsigned short  getframeSize      ();
    unsigned short  getbitRate        ();


    vector<unsigned char> wave;                   // see 'datachunk.h'

    Clipboard  ();
    ~Clipboard (){}

  };
}

#endif


