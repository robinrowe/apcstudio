// generated by Fast Light User Interface Designer (fluid) version 1.0100

#ifndef askmixpaste_h
#define askmixpaste_h
#include <FL/Fl.H>
#include <globals.h>
#include <settings.h>
#include <streamcast.h>
#include <sleeper.h>
#include <popups.h>
#include <FL/Fl_Window.H>
#include <FL/Fl_Group.H>
#include <FL/Fl_Value_Slider.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Button.H>

class Askmixpaste {
  int* volume;
public:
  Askmixpaste();
private:
  Fl_Window *win;
  Fl_Group *theGroup;
  Fl_Value_Slider *slider;
  inline void cb_slider_i(Fl_Value_Slider*, void*);
  static void cb_slider(Fl_Value_Slider*, void*);
  Fl_Box *text0;
  Fl_Box *text1;
  Fl_Box *text2;
  Fl_Box *text3;
  Fl_Button *help_btn;
  inline void cb_help_btn_i(Fl_Button*, void*);
  static void cb_help_btn(Fl_Button*, void*);
  Fl_Button *cancel_btn;
  inline void cb_cancel_btn_i(Fl_Button*, void*);
  static void cb_cancel_btn(Fl_Button*, void*);
  Fl_Button *ok_btn;
  inline void cb_ok_btn_i(Fl_Button*, void*);
  static void cb_ok_btn(Fl_Button*, void*);
public:
  void ask(int& _volume);
};
#endif
