// apcstudio.cpp 
// Created by Robin Rowe 2020-05-19
// License GPL open source

#error 

#include <iostream>
using namespace std;

void Usage()
{	cout << "Usage: apcstudio " << endl;
}

enum
{	ok,
	invalid_args

};

int main(int argc,char* argv[])
{	cout << "apcstudio starting..." << endl;
	if(argc < 1)
	{	Usage();
		return invalid_args;
	}

	cout << "apcstudio done!" << endl;
	return ok;
}
