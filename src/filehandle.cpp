// filehandle.cpp

// (c) 2000 Martin Henne
// EMail: Martin.Henne@web.de

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <filehandle.h>
#include <fstream>
#include <iostream>
#include <time.h>
#include <vector>
#include <streamcast.h>



//----------------------------------------------------------------------------//
void FileHandle::appendLine(const string& filename,
                            const string& line)
{
	int CR=10; // carriage return
	int versuche=0;
	while (versuche < 1000000) // 1 Mio. Versuche, die Datei zu oeffnen
		{
			ofstream Datei(filename.c_str(), ios::app);
			++versuche;
			if (Datei)
				{
					Datei << line << endl /* static_cast<char>(CR) */;
					Datei.close();		
					if (versuche>2)
						{
							// Wenn mehr als zwei Versuche: Vermerk im Logfile!
							string fname="FILEHANDLE.LOG";
							ofstream File(fname.c_str(), ios::app);
							File << "FileHandle::appendLine(): Tries to write: " << stream_cast<string>(versuche) << " to file " << filename << (char)CR;
							File.close();
						}
					return;
				}
		}
	
	// PANIKFEHLER: File konnte nicht geoeffnet werden: muss ins Log-File!
	string fname="FILEHANDLE.LOG";
	ofstream File(fname.c_str(), ios::app);
	File << "FileHandle::appendLine(): could not open file: " << filename << static_cast<char>(CR);
	File << "  Tries: " << stream_cast<string>(versuche) << static_cast<char>(CR);
	File.close();
	cerr << "FileHandle::appendLine(): could not open file: " << filename << endl;

}



//----------------------------------------------------------------------------//

// append without trailing carriage return
void FileHandle::append(const string& filename,
                        const string& line)
{
	int CR=10; // carriage return
	int versuche=0;
	while (versuche < 1000000) // 1 Mio. Versuche, die Datei zu oeffnen
		{
			ofstream Datei(filename.c_str(), ios::app);
			versuche++;
			if (Datei)
				{
					Datei << line;
					Datei.close();		
					if (versuche>2)
						{
							// Wenn mehr als zwei Versuche: Vermerk im Logfile!
							string fname="FILEHANDLE.LOG";
							ofstream File(fname.c_str(), ios::app);
							File << "FileHandle::appendLine(nocr): Tries to write: "
                   << stream_cast<string>(versuche)
                   << " to file " << filename << (char)CR;
							File.close();
						}
					return;
				}
		}
	
	// PANIKFEHLER: File konnte nicht geoeffnet werden: muss ins Log-File!
	string fname="FILEHANDLE.LOG";
	ofstream File(fname.c_str(), ios::app);
	File << "FileHandle::appendLine(nocr): could not open file: "
       << filename << static_cast<char>(CR);
	File << "  Tries: " << stream_cast<string>(versuche)
       << static_cast<char>(CR);
	File.close();
	cerr << "FileHandle::appendLine(nocr): could not open file: "
       << filename << endl;

}




//----------------------------------------------------------------------------//

void FileHandle::clearFile(const string& filename)
{
	ofstream Datei(filename.c_str(), ios::trunc);
	Datei.close();
	return;
}

bool FileHandle::exists(const string& filename)
{
    ifstream file(filename.c_str(), ios::in);
    if (file)
    {
			file.close();
    	return true;
    }
    else
    {
			file.close();
			return false;
    }
}






//----------------------------------------------------------------------------//

// Gibt "(not found)" zurueck, falls nicht gefunden
string FileHandle::findLine(const string& filename,
                            const string& sub)
{
	ifstream file(filename.c_str(), ios::in);
  if (!file)
		{
			cerr << "FileHandle::findLine(): WARNING: coud not open file: "
           << filename << endl;
			return("(not found)");
		}

	string sline = "(not found)";
	char cline[200];
	for (int i=0; i<200; i++){cline[i]=' ';}
  cline[199]=0;

	int subPos=0;


	while(file.getline(cline, 199))
	{
		sline = static_cast<string>(cline);
		subPos = sline.find(sub);
		if (subPos>-1)
			break;
	}
	file.close();
	if (subPos<0 || sline.length()==0)
		return "(not found)";
	else
		return sline;
}






//----------------------------------------------------------------------------//

void FileHandle::showFile(const string& filename)
{
	ifstream file(filename.c_str(), ios::in);
	
  if (!file)
	{
		cerr << "FileHandle::showFile(): could not open file: " << filename << endl;
    return;
	}
	
	char l[501];
	while (file.getline(l, 500))
	{
		cout << l << endl;
	}
	
	file.close();
}






//----------------------------------------------------------------------------//

// durchsuche filename nach Optionsangaben a la ' prefix = /usr/local '
// oder ' option = "wert" ' usw. und gibt den Wert zurueck.
// Zeilen mit "#" oder "//" am Anfang werden ignoriert
string FileHandle::value(const string& filename,
                         const string& name)
{
	ifstream file(filename.c_str(), ios::in);
	if (!file) // Fehler beim File-Oeffnen?
	{
		cerr << "FileHandle::value(): couldn't open file " << filename << endl;
    cerr << "                     returning '(error)'\n";
		return string("(error)");	
	}
	
	int x; // fuer diverse Zwischenwerte in der while-Schleife
	string rest; // nimmt den Reststring nach dem '='-Zeichen auf
	char l[501];
	bool namefound=false;
	while (file.getline(l, 500))
	{
		string sl = static_cast<string>(l);
		bool elim = false;
		// falls substring gefunden und Zeile kein Kommentar:
		if (!sl.find(name) && sl.substr(0,1)!="#" && sl.substr(0,2)!="//")
		{
      // Alles hinter einem evtl. '#' ist Kommentar und wird abgezwickt
      int kommentFoundAt = sl.find("#"); // ist '-1', wenn nicht gefunden
      if (kommentFoundAt!=-1)
      {
        sl=sl.substr(0,kommentFoundAt);
        //cerr << "FileHandle::value(...): String snipped to: " << sl << endl;
      }

			x = sl.find("="); // Position des '='-Zeichens
			rest = sl.substr(0,x);
			if (!rest.find(name)) // ist 'name' links vom Gleichheitszeichen?
			{
				// eliminate unwanted characters
				do
				{
					elim = false;
					for (unsigned int i=0; i<rest.length(); i++)
					{
						// cerr << "FileHandle::value(): substring ->" << sl.substr(i,1) << "<-" << endl;
						if(rest.substr(i,1)=="\"" || rest.substr(i,1)=="\'")
							{
								// cerr << "FileHandle::value(): found interesting substring" << endl;
								rest.replace(i,1,"");
								elim=true;
								break;
							}
					}
				}while(elim==true);

  			// cerr << "FileHandle::value(): rest, length: " << rest << ", " << rest.length() << endl;
				// cerr << "FileHandle::value(): name, length: " << name << ", " << name.length() << endl;

				/* hier muessen die Leerzeichen vorne und hinten weg*/
				rest = leadtrail(rest);

			if (rest.length() == name.length())
			{
				namefound=true;
				rest = sl.substr(x+1,sl.length());
				// cerr << "FileHandle::value(): unpatched reststring ->" << rest << "<-" << endl;

				/* hier muessen die Leerzeichen vorne und hinten weg*/
				rest = leadtrail(rest);
				
				// eliminate unwanted characters
				do
				{
					elim = false;
					for (unsigned int i=0; i<rest.length(); i++)
					{
						// cerr << "FileHandle::value(): substring ->" << sl.substr(i,1) << "<-" << endl;
						if(rest.substr(i,1)=="\"" || rest.substr(i,1)=="\'")
							{
								// cerr << "FileHandle::value(): found interesting substring" << endl;
								rest.replace(i,1,"");
								elim=true;
								break;
							}
					}
				}while(elim==true);
			}
			}				
			// cerr << "FileHandle::value(): rest = ->" << rest << "<-" << endl;
		}
	}
	file.close();
	if (namefound==false)
		rest="(not found)";
	if (rest=="")
		rest="(undefined)";


	//cerr << "FileHandle::value(): returning value ->" << rest << "<-" << endl;
	return rest;
}







//----------------------------------------------------------------------------//

// Leerzeichen vorne und hinten abzwicken
string FileHandle::leadtrail(const string& Rest)
{
  string rest = Rest;
	// eliminate leading spaces
	while(rest.substr(0,1)==" ")
	{
		rest.replace(0,1,"");
		//cerr << "FileHandle::value(): eliminated leading space. rest is now ->" << rest << "<-" << endl;
	}
		// eliminate trailing spaces
	while(rest.substr(rest.length()-1,1)==" ")
	{
		rest.replace(rest.length()-1,1,"");
		//cerr << "FileHandle::value(): eliminated trailing space. rest is now ->" << rest << "<-" << endl;
	}
	return rest;
}






//----------------------------------------------------------------------------//

string FileHandle::getLine(const string& filename,
                           const int& linenumber)
{
	int counter=0;
	string back; // fuer die Rueckgabe
	ifstream file(filename.c_str(), ios::in);
	
  if (!file)
	{
		cerr << "FileHandle::getLine(): could not open file: " << filename << endl;
		return string("(not found)");
	}
	
	char l[501];
	while (file.getline(l, 500))
	{
		counter++;
		if (counter==linenumber)
		{
			back = static_cast<string>(l);
			file.close();
			return back;	
		}
	}
	
	back = "(not found)"; // this line doesn't exist in the file
	file.close();
	cerr << "FileHandle::getLine(linenumber): Too less lines in the file " << filename << endl;
	return back;
}






//----------------------------------------------------------------------------//

int FileHandle::findLineNumber(const string& filename,
                               const string& line)
{
	int counter=0;
	string sl;
	ifstream file(filename.c_str(), ios::in);
	
  if (!file)
	{
		cerr << "FileHandle::findLineNumber(): could not open file: " << filename << endl;
    return -1;
	}
	char l[501];
	while (file.getline(l, 500))
	{
		counter++;
		sl = static_cast<string>(l);
		if (sl==line)
		{
			file.close();
			// cerr << "FileHandle::findLineNumber(): found \'" << line << "\' in line " << counter << endl;
			return counter;	
		}
	}
	file.close();
	cerr << "FileHandle::findLineNumber(): could not find Line \"" << line << "\"" << endl;
	return -1;
}




//----------------------------------------------------------------------------//

int FileHandle::countLines(const string& filename)
{
  // cerr << "FileHandle::countLines(): entered" << endl;
	int counter=0;
	ifstream file(filename.c_str(), ios::in);	
	if (!file)
	{
		cerr << "FileHandle::countLines(): could not open file: " << filename << endl;
	}	
	char l[501];
	while(file.getline(l, 500))
	{
    // cerr << "FileHandle::countLines(): counter = " << counter << endl;
		counter++;
	}
	
	file.close();
  // cerr << "FileHandle::countLines(): returning " << counter << endl;
	return counter;
}






//----------------------------------------------------------------------------//

void FileHandle::swapLine(const string& filename,
                          const string& oldline,
                          const string& newline)
{
//	clearfile("tmp");
	const int lines = countLines(filename);
	int number = findLineNumber(filename, oldline);
  if(number==-1)
    return;
	vector<string> tmpfile(lines);

	int i=0;
	for (i=0; i<number; i++)
	{
		tmpfile[i]=getLine(filename, i+1);
	}

	tmpfile[number-1]=newline;

	for (i=number; i<lines; i++)
	{
		tmpfile[i]=getLine(filename, i+1);
	}

	clearFile(filename);
	for (i=0; i<lines; i++)
	{
		appendLine(filename,tmpfile[i]);	
	}
	return;
}







//----------------------------------------------------------------------------//

void FileHandle::insertLine(const string& filename,
                            const string& line,
                            const int& place)
{
//	clearfile("tmp");
	int lines = countLines(filename);
	vector<string> tmpfile(lines+1);

	int i=0;
	for (i=0; i<(place-1); i++)
	{
		tmpfile[i]=getLine(filename, i+1);
	}

	tmpfile[place-1]=line;

	for (i=(place-1); i<lines; i++)
	{
		tmpfile[i+1]=getLine(filename, i+1);
	}

	clearFile(filename);
	for (i=0; i<lines; i++)
	{
		appendLine(filename,tmpfile[i]);	
	}
	return;
}




//----------------------------------------------------------------------------//

void FileHandle::killLine(const string& filename,
                          const int& linenumber)
{
//	clearfile("tmp");
	int lines = countLines(filename);
	// cerr << "FileHandle::killLine(): File has " << lines << " lines" << endl;
	if (linenumber>lines)
	{
		cerr << "FileHandle::killLine(): file " << filename << " has just ";
		cerr << lines << " lines. Invalid linenumber: " << linenumber;
		cerr << ". I do nothing." << endl;
		return;
	}
	
	if (linenumber <= 0)
	{
	  cerr << "FileHandle::killLine(): invalid line number: " << linenumber << endl;
		return;
	}
	
	// vector erzeugen und mit leerstrings belegen
	vector<string> tmpfile(lines-1);
  for (unsigned int j=0; j<tmpfile.size(); j++)
	  tmpfile[j]=string("");

	int i=0;
  // get lines before
	for (int j=1; j<linenumber; j++)
	{
		tmpfile[i]=getLine(filename, j);
    // appendLine("first",tmpfile[i]);
		i++;
	}
  // get lines after
	for (int j=linenumber+1; j<=lines; j++)
	{
	    // cerr << "FileHandle::killLine(): getting Line " << j << endl;
	    tmpfile[i]=getLine(filename, j);
			// appendLine("second",tmpfile[i]);
			i++;
	}
	clearFile(filename);
	for (i=0; i<lines-1; i++)
	{
		appendLine(filename,tmpfile[i]);	
	}
	/*    ***** TODO -> falls altes file nicht mit CR endet,
	      ********** -> duerfte das neue eig. auch nicht mit CR
				********** -> enden. Tut es aber immer :-(
	*/	

	return;
}




//----------------------------------------------------------------------------//

unsigned long FileHandle::getsize(const string& file)
{
  unsigned long size = 0;
  ifstream infile;
  infile.open(file.c_str());
  if(infile.is_open())
  {
    infile.seekg(0,ios::end);
    size = infile.tellg();
    infile.close();
  }
  else
    cerr << "FileHandle::getsize(...): could not open file " << file << endl;

  return size;
}

// end of file
