// $Id: about.h,v 1.5 2002/01/27 21:18:35 martinhenne Exp $
// (C) 2001 Martin Henne
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

#ifndef _about_h
#define _about_h

#include <globals.h>
/*
#include <FL/Fl_Window.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Browser.H>
*/
#include <guiheaders.h>
#include <string>
#include <vector>
using namespace std;

// This will be replaced soon by a fluid-generated class

namespace APC
{
  class About
  {
  private:
    static Fl_Window*  win;
    static Fl_Box*     logo;
    static Fl_Box*     version;
    static Fl_Browser* authors;
    static Fl_Box*     copyright;
    static int         maincolor;
    static int         darkcolor;
    static int         fontcolor;

  public:
    About();
    ~About(){}
    void show();
    void hide();

    static vector<string> author;
    static vector<string> license;
    static vector<string> thanks;
    static vector<string> apc;
  };
}

#endif

