// $Id: configfile.cpp,v 1.13 2001/12/16 14:22:37 martinhenne Exp $

// (C) 2001 Martin Henne
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

/*
 * This class reads the configfile and assignes
 * the specified values...
 *
 */

#include <string>
#include <filehandle.h>
#include <streamcast.h>
#include <configfile.h>
#include <globals.h>
#include <iostream>


namespace APC
{
  // this looks for the configfile
  std::string Configfile::file()
  {
  using std::string;
    string fname   = "apcstudiorc";
    string lookfor = "";

    lookfor = "./"+fname;
    if(FileHandle::exists(lookfor)) // look for file "./apcstudiorc"
    {
      //cerr << "using " << lookfor << endl;
      return lookfor;
    }
    const char* home = getenv("HOME");
    if(home)
    {   lookfor = home;
        lookfor += "/.";
        lookfor += fname;
        if(FileHandle::exists(lookfor)) // look for file "$(HOME)/.apcstudiorc"
        {
          //cerr << "using " << lookfor << endl;
          return lookfor;
    }   }

    lookfor = "/etc/";
    lookfor += fname; // look for file "/etc/apcstudiorc"
    if(FileHandle::exists(lookfor))
    {
      //cerr << "using " << lookfor << endl;
      return lookfor;
    }

  cerr << "aPcStudio: configfile not found.\n";

    return fname;
  }

  void Configfile::get(const char* const VAL, int& ret)
  {
  using std::string;
    string t_val = static_cast<string>(VAL);
    string t_ret = "";
    t_ret = (FileHandle::value((Configfile::file()).c_str(),t_val));

    // errors while processing the configfile:
    if (t_ret=="(error)")
    {
      std::cerr << "Configfile: could not open configfile!\n"
                << "  -> using " << ret << " as " << VAL << endl;
      return;
    }
    if (t_ret=="(not found)")
    {
      std::cerr << "Configfile: corrupted or incomplete configfile!\n"
                << "  -> using " << ret << " as " << VAL << endl;
      return;
    }
    if (t_ret=="(undefined)")
    {
      std::cerr << "Configfile: undefined value in configfile!\n"
                << "  -> using " << ret << " as " << VAL << endl;
      return;
    }
    //cerr << "Configfile: returning int " << VAL << ": " << t_ret << endl;
    ret = stream_cast<int>(t_ret);
  //  std::cerr << "Configfile::get(char*, int&): returning " << ret
  //            << " for " << VAL << std::endl;
  }

  void Configfile::get(const char* const VAL, string& ret)
  {
  using std::string;
    string t_val = static_cast<string>(VAL);
    string t_ret = "";
    t_ret = (FileHandle::value((Configfile::file()).c_str(),t_val));

    // errors while processing the configfile:
    if (t_ret=="(error)")
    {
      std::cerr << "Configfile: could not open configfile!\n"
                << "  -> using " << ret << " as " << VAL << endl;
      return;
    }
      if (t_ret=="(not found)")
    {
      std::cerr << "Configfile: corrupted or incomplete configfile!\n"
                << "  -> using " << ret << " as " << VAL << endl;
      return;
    }
    if (t_ret=="(undefined)")
    {
      std::cerr << "Configfile: undefined value in configfile!\n"
                << "  -> using " << ret << " as " << VAL << endl;
      return;
    }

    ret = (FileHandle::value((Configfile::file()).c_str(),t_val));
  //  std::cerr << "Configfile::get(char*, string&): returning " << ret
  //            << " for " << VAL << std::endl;
  }


  void Configfile::get(const char* const VAL, bool& ret)
  {
  using std::string;
    string t_val = static_cast<string>(VAL);
    string t_ret = "";
    t_ret = (FileHandle::value((Configfile::file()).c_str(),t_val));

    // errors while processing the configfile:
    if (t_ret=="(error)")
    {
      std::cerr << "Configfile: could not open configfile!\n"
              << "  -> using " << ret << " as " << VAL << endl;
      return;
    }
    if (t_ret=="(not found)")
    {
      std::cerr << "Configfile: corrupted or incomplete configfile!\n"
                << "  -> using " << ret << " as " << VAL << endl;
      return;
    }
    if (t_ret=="(undefined)")
    {
      std::cerr << "Configfile: undefined value in configfile!\n"
                << "  -> using " << ret << " as " << VAL << endl;
      return;
    }
    if (t_ret=="yes" || t_ret=="true" || t_ret=="1" ||
        t_ret=="YES" || t_ret=="TRUE" || t_ret=="set" ||
        t_ret=="SET" || t_ret=="wahr" || t_ret=="WAHR" ||
        t_ret=="JA"  || t_ret=="ja"   || t_ret=="on" || t_ret=="ON")
      ret = true;
    else
      ret = false;
  }



  void Configfile::set(const char* const VAL, const string& arg)
  {
  using std::string;
    string old_line, new_line;
    old_line = FileHandle::findLine((Configfile::file()).c_str(),static_cast<string>(VAL));
    new_line = static_cast<string>(VAL) + " = " + arg;
    FileHandle::swapLine((Configfile::file()).c_str(),old_line,new_line);
  }


  void Configfile::set(const char* const VAL, const int& arg)
  {
  using std::string;
    string old_line, new_line;
    old_line = FileHandle::findLine((Configfile::file()).c_str(),static_cast<string>(VAL));
    new_line = static_cast<string>(VAL) + " = "
               + stream_cast<string>(arg);
    FileHandle::swapLine((Configfile::file()).c_str(),old_line,new_line);
  }


  void Configfile::set(const char* const VAL, const bool& Arg)
  {
  using std::string;
    string arg("yes");
    if(!Arg)
      arg="no";
    string old_line, new_line;
    old_line = FileHandle::findLine((Configfile::file()).c_str(),static_cast<string>(VAL));
    new_line = static_cast<string>(VAL) + " = " + arg;
    FileHandle::swapLine((Configfile::file()).c_str(),old_line,new_line);
  }

} // end of namespace

// end of file
