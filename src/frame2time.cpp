// $Id: frame2time.cpp,v 1.2 2002/02/11 20:09:55 martinhenne Exp $

// Copyright 2001 Martin Henne 2001
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

#include <frame2time.h>
#include <streamcast.h>
#include <string>

std::string frame2time::operator()(const int& frame, const int& frequency)
{
  using namespace std;
  unsigned int min = 0, sec = 0, msec = 0;            // minute, seconds, miliseconds
  sec  = static_cast<int>(frame/frequency);           // ALL seconds
  min  = static_cast<int>(sec/60);                    // minutes
  msec = static_cast<int>((frame-(sec*frequency))/(static_cast<int>(frequency/1000)));
  sec  = static_cast<int>(frame/frequency)-min*60;   // remaining seconds
  // now let's build a string like that: "01:01.150" (minutes:seconds.miliseconds)
  std::string s_min,s_sec,s_msec;
  if (min <  1) s_min  = "00";
  if (min < 10 && min >= 1) s_min  = "0"    + stream_cast<std::string>(min);
  if (min >=10) s_min  =                      stream_cast<std::string>(min);
  if (sec <  1) s_sec  = "00";
  if (sec < 10 && sec >= 1) s_sec  = "0"    + stream_cast<std::string>(sec);
  if (sec >=10) s_sec  =                      stream_cast<std::string>(sec);
  if (msec<  1) s_msec = "000";
  if (msec< 10 && msec >= 1) s_msec = "00"  + stream_cast<std::string>(msec);
  if (msec<100 && msec >= 10) s_msec = "0"  + stream_cast<std::string>(msec);
  if (msec>=100) s_msec =                     stream_cast<std::string>(msec);
  std::string ret = s_min + ":" + s_sec + "." + s_msec;
  return ret;
}
