// $Id: settings.h,v 1.12 2002/02/15 23:53:41 martinhenne Exp $

// (C) 2001 Martin Henne
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

#ifndef _settings_h
#define _settings_h

#include <string>

namespace APC
{
  using std::string;
  /// This class provides global settings depending on Commandline and Configfile.
  /**
    *  If a parameter is overwritten by passing it at the
    *  commandline, 'Settings' doesn't use the value, that
    *  is in the configfile.
    *
    */
  class Settings
  {
  public:
    static int MAIN_COLOR;
    static int MAIN_DARKER_COLOR;
    static int FONT_COLOR;
    static int NULL_LINE_COLOR;
    static int RASTER_COLOR;
    static int SELECTED_BACKGROUND_COLOR;
    static int SELECTED_WAVEDATA_COLOR;
    static int UNSELECTED_BACKGROUND_COLOR;
    static int UNSELECTED_WAVEDATA_COLOR;
    static int CURSOR_COLOR;
    static int BUTTON_COLOR;
    static int INFOBOX_COLOR;
    static int INFOTEXT_COLOR;
    static int DIGIT_COLOR;
    static int MARKS_COLOR;
    static int MAX_UNDOS;
    static string AUDIO_DEVICE;
    static string REC_DEVICE;
    static bool FOLLOW_PLAY_CURSOR;
    static string DEFAULT_DIRECTORY;
    static string HELP_DIR;
    static string LAST_OPENED;
    static bool USE_LAST_OPENED;
    static bool DISC_UNDO;
    static string TEMP_DIR;
    static string MIXER_DIR;
    static bool DRAW_RASTER;
    static bool DRAW_DIGITS;
    static bool SHOW_TIPOTD;
    static bool TIPOTD_SHOWN;
    static bool SEL_PASTED;

    static void* winwrapper;

    static bool initialized;
    static void read();
    static void write();
    static void reset();
    static void apply_colors();
    static void clear_undo_buf();
    static void init_wrapper(void* Windowwrapper);
  };

}

#endif



