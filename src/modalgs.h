// $Id: modalgs.h,v 1.2 2001/12/07 17:03:58 martinhenne Exp $

// (C) 2001 Martin Henne
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

/*
 * This file containes modified standard algorithms.
 * each one takes three arguments, an iterator to the
 * first element, one to the last element, and a
 * function object.
 *
 */

#ifndef _modalgs_h
#define _modalgs_h

namespace APC
{
  /// like std::for_each, but for two elements in one turn.
  template<class It, class FO>
  FO& for_each_pair(It start, It end, FO func)
  {
    while(start != end)
    {
      func(*start, *(++start));
      ++start;
    }
    return func;
  }

  template<class it, class FO>
  FO& for_each_ref(It& start, It& end, FO& func)
  {
    while(start!=end)
    {
      func(*start++);
    }
    return func;
  }

}

#endif

