// $Id: reverb.h,v 1.2 2002/02/21 21:53:34 martinhenne Exp $

// (C) 2001 Martin Henne
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

// this is the reverb effect

#ifndef _reverb_h
#define _reverb_h

#include <vector>
#include <globals.h>

class Reverb
{
public:
  static void perform(std::vector<uchar>& data,
                     const int& channels,
                     const int& bitRate,
                     const int& frequency,
                     ulong startframe,
                     ulong endframe,
                     int duration  = 0,  // how long is the reverb (ms)
                     int volume    = 0,  // in percent (0-100)
                     int attack    = 0,  // reverb fade in time (ms)
                     int delay     = 0,  // is the reverb a bit late? (ms)
                     int distance  = 0,  // average distance between echoes (ms)
                     double random = 0,  // random distance shift in percent/100
                     double ratio  = 0,  // volume ramp start/end ratio of reverb buffer
                     int channel   = 0); // 0-both 1-left 2-right
};

#endif

// end of file

