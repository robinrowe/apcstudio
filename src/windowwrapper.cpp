// $Id: windowwrapper.cpp,v 1.4 2002/02/15 23:53:41 martinhenne Exp $

// Copyright 2001 Martin Henne 2001
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

#include <windowwrapper.h>
#include <globals.h>
#include <debug.h>
#include <editwindow.h>
#include <filehandle.h>
#include <fobjects.h>
#include <vector>
#include <algorithm>

//----------------------------------------------------------------------------//

Windowwrapper::Windowwrapper(std::vector<APC::Editwindow*> arg)
{
  vec = arg;
  visible.resize(vec.size());
  for(ulong i=0; i<vec.size(); ++i)
    visible[i]=true;
}




//----------------------------------------------------------------------------//

bool Windowwrapper::find_unused_index(ulong& index) const
{
  for(index=0; index<vec.size(); ++index)
    if(vec[index]==NULL)
      return true;
  return false;
}



//----------------------------------------------------------------------------//

void Windowwrapper::apply_colors()
{
  for(ulong index=0; index<vec.size(); ++index)
    if(vec[index]!=NULL)
      vec[index]->apply_colors();
}


//----------------------------------------------------------------------------//

void Windowwrapper::clear_undo_buf()
{
  for(ulong index=0; index<vec.size(); ++index)
    if(vec[index]!=NULL)
      vec[index]->clear_undo_buf();
}




//----------------------------------------------------------------------------//

ulong Windowwrapper::registrate(APC::Editwindow* p)
{
  ulong index = 0;
  if(find_unused_index(index))
  {
    vec[index]=p;
    visible[index]=true;
  }
  else
  {
    vec.push_back(p);
    visible.push_back(true);
  }
  return index;
}




//----------------------------------------------------------------------------//

void Windowwrapper::unregistrate(ulong index)
{
  if(index>=vec.size())
    return;
  delete vec[index];
  vec[index]=NULL;
  visible[index]=false;
}





// end of file
