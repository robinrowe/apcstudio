// $Id: statusbar.h,v 1.3 2001/12/16 14:22:37 martinhenne Exp $

// (C) 2001 Martin Henne
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

/*
 * This class implements a statusbar within a popup window
 * It's a great world for GNU-Freaks, so I don't have to do
 * much work here, because of Bonfanti Alessandro's Progress-
 * Bar - Widget. Thank's Bon.
 *
 * The Win32-Version of aPcStudio maybe become Shareware
 * instead of GPL someday. In this case, Bon's code has to
 * be removed!
 *
 * By the way: Thanks to Bill Spitzak for the great FLTK.
 *
 */

#ifndef _statusbar_h
#define _statusbar_h

#include <globals.h>

/*//------  FLTK
#include <FL/Fl.H>
#include <FL/Fl_Widget.H>
#include <FL/fl_draw.H>
#include <FL/Fl_Box.H>
*/
#include <guiheaders.h>
#include <Fl_Progress_Bar.H>

//------  STL
#include <iostream>
#include <string>


namespace APC
{
using namespace std;
  class Statusbar
  {
  public:
    Statusbar           (string label = "please wait", string progressname = "working...");
    Fl_Progress_Bar*    bar;
    void                set(int percent);
    void                relabel(const string newlabel);
    void                progressname(const string newprogressname);
    void                show();
    void                hide();

  private:
    Fl_Window*          win;
    Fl_Box*             progressName;
    string              winlabel;
    string              progresslabel;

  }; // end of class

} // enf of namespace



#endif


