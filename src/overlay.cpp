// $Id: overlay.cpp,v 1.7 2002/02/11 20:09:55 martinhenne Exp $

// (C) 2001 Martin Henne
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.


#include <guiheaders.h>
#include <overlay.h>
#include <editwindow.h>
#include <apc_fl_waveedit.h>

namespace APC
{

  Overlay::Overlay(int x, int y, int w, int h, const char* label) : Fl_Overlay_Window(x,y,w,h,label)
  {
    X         = 0;
    cursorcolor = 7;
    Configfile::get("CURSOR_COLOR",cursorcolor);
  }

  Overlay::Overlay(int w, int h, const char* label) : Fl_Overlay_Window(w,h,label)
  {
    X         = 0;
    cursorcolor = 7;
    Configfile::get("CURSOR_COLOR",cursorcolor);
  }


  void Overlay::draw_overlay ()
  {
    t_ew      = static_cast<Editwindow*>(this->user_data());
    if(!t_ew->is_recording()) { // no cursor while recording!
      t_we      = static_cast<Apc_fl_waveedit*>((static_cast<Editwindow*>(this->user_data()))->we);
      X = t_we->frame2screen(t_we->getcursorPosition());
      fl_color(cursorcolor);
      fl_line(X,t_ew->we->y(), X, t_we->h()+t_ew->we->y());
    }
  }
}



