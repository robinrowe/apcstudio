// $Id: fader.h,v 1.2 2002/02/18 17:15:23 martinhenne Exp $

// (C) 2002 Martin Henne
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

#ifndef _fader_h
#define _fader_h

#include <vector>

// fading in/out, amplifying, silencing...

class fader
{
public:
  void operator()(std::vector<unsigned char>& data, // the wave data itself
                  const ulong& startframe,
                  const ulong& endframe,
                  const double& startfactor,        // 1.0 is 100%
                  const double& endfactor,          // 1.0 is 100%
                  const int& bitRate,
                  const int& channels,
                  const int& frameSize,
                  const int& channel = 0); // 0-both 1-left 2-right
};

#endif

