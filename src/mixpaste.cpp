// $Id: mixpaste.cpp,v 1.8 2002/02/19 22:53:50 martinhenne Exp $

// (C) 2001 Martin Henne
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

#include <guiheaders.h>
#include <busywin.h>
#include <popups.h>
#include <mixpaste.h>
#include <globals.h>
#include <sleeper.h>
#include <settings.h>
#include <editwindow.h>
#include <datachunk.h>
#include <apc_fl_waveedit.h>
#include <askmixpaste.h>
#include <debug.h>
#include <lendian.h>
#include <debug.h>

void Mixpaste::overlay(std::vector<unsigned char>& data,
                    const std::vector<unsigned char>& overlay,
                    const unsigned long& cursorframe,
                    const int& bitRate,
                    const int& channels,
                    const int& frameSize,
                    const double& volumefactor = 1.0,
                    const int& channel)
{
    APC::Debug::msg<string>("Mixpaste::overlay(): called\n",Globals::DBG_FUNCTION_TRACE);
    ulong cursorbyte = cursorframe * frameSize;
    const ulong overlaySize = overlay.size();
    const ulong waveSize = data.size();

    short tmpi  = 0;
    short tmpi2 = 0;
    short tmpi3 = 0;

    for(unsigned long i=0; i<overlaySize && (i+cursorbyte)<(waveSize-frameSize); i+=frameSize)
    {
      if(bitRate==8) {
          if(channel==1 || channel==0) {
          tmpi  = data[i+cursorbyte] - 0x80;
          tmpi2 = overlay[i] - 0x80;
          tmpi2 = static_cast<short>(tmpi2*volumefactor);
          tmpi3 = Mixpaste::mix_values(tmpi, tmpi2);
          if(tmpi3>127 || tmpi3<-127)
            tmpi3 = static_cast<short>(tmpi3*0.99);
          tmpi3 += 0x80;
          data[i+cursorbyte] = static_cast<unsigned char>(tmpi3);
        }
        if(channels==2 && (channel==2 || channel==0)) {
          tmpi  = data[i+cursorbyte+1] - 0x80;
          tmpi2 = overlay[i+1] - 0x80;
          tmpi2 = static_cast<short>(tmpi2*volumefactor);
          tmpi3 = Mixpaste::mix_values(tmpi, tmpi2);
          if(tmpi3>127 || tmpi3<-127)
            tmpi3 = static_cast<short>(tmpi3*0.99);
          tmpi3 += 0x80;
         data[i+cursorbyte+1] = static_cast<unsigned char>(tmpi3);
        }
      }
      if(bitRate==16) {
        if(channel==1 || channel==0) {
          APC::lendian(data[i+cursorbyte],data[i+cursorbyte+1],tmpi);
          APC::lendian(overlay[i],overlay[i+1],tmpi2);
          tmpi2 = static_cast<short>(tmpi2*volumefactor);
          tmpi3 = Mixpaste::mix_values(tmpi, tmpi2);
          APC::getshortsLow(tmpi3,data[i+cursorbyte]);
          APC::getshortsHigh(tmpi3,data[i+cursorbyte+1]);
        }
        if(channels==2 && (channel==2 || channel==0)) {
          APC::lendian(data[i+cursorbyte+2],data[i+cursorbyte+3],tmpi);
          APC::lendian(overlay[i+2],overlay[i+3],tmpi2);
          tmpi2 = static_cast<short>(tmpi2*volumefactor);
          tmpi3 = Mixpaste::mix_values(tmpi, tmpi2);
          APC::getshortsLow(tmpi3,data[i+cursorbyte+2]);
          APC::getshortsHigh(tmpi3,data[i+cursorbyte+3]);
        }
      }
      Fl::check();
    } // end of for-loop
  APC::Debug::msg<string>("Mixpaste::overlay(): leaving\n",Globals::DBG_FUNCTION_TRACE);
  return;
}



int Mixpaste::mix_values(const int& tmpi, const int& tmpi2)
{
  return (tmpi + tmpi2);
}




void Mixpaste::perform(APC::Editwindow* ew, const int& channel) {
    // new mixpaste code: (work in progress)

    int volume = 100; // volume of clipboard
    Askmixpaste amp;
    amp.ask(volume);

    if(volume==0) {
      APC::Debug::msg<string>("Mixpaste::perform(): mixing canceled by user!\n");
      return;
    }

    Busywin busyPaste("mixing sounds...");
    busyPaste.show(); Fl::wait(0.2); Fl::check();


    busyPaste.tell("saving undo data...");Fl::wait(0.2); Sleeper::dosleep(10); Fl::check();
    if(!ew->push_undo()) {
      busyPaste.hide(); Fl::wait(0.2); Sleeper::dosleep(10); Fl::check();
      return;
    }

    busyPaste.tell("performing function...");Fl::wait(0.2); Sleeper::dosleep(10); Fl::check();
    APC::Data_chunk& dc = ew->wfile->getdata();
    vector<unsigned char>& data = dc.getwave();
    vector<unsigned char>& clip = ew->theClipboard.wave;

    const int&   bitRate       = ew->bitRate;
    const int&   frameSize     = ew->frameSize;
    const int&   channels      = ew->channels;
    ulong tmpul = (((ew->theClipboard).wave).size());
    const ulong  clipsize      =  tmpul - (tmpul%ew->frameSize);
    const ulong  cursorpos     =  ew->we->getcursorPosition();

    Mixpaste::overlay(data,clip,cursorpos,bitRate,channels,frameSize,volume/100.0,channel);

    busyPaste.hide();

    // adjust new values
    string t_status = stream_cast<string>(clipsize) + " bytes mixed";
    ew->relabel(t_status,"statusBox_status");

    ew->data_changed();
    ew->redraw_needed |= (ew->REDR_WAVE);
    ew->doRedraw();
} // end of Mixpaste::perform()

// end of file
