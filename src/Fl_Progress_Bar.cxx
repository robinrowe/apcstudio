// Fl_Progress_Bar v1.0
// By Bonfanti Alessandro xjbon@tin.it

#include <stdio.h>
#include "Fl_Progress_Bar.H"


//-----------------------------------------------------------------------------
Fl_Progress_Bar::Fl_Progress_Bar(int x,int y,int w,int h): Fl_Box(x,y,w,h)
{
   type   = PBAR_NORMAL;
   perc   = 0.0;
   max    = 100.0;
}


//-----------------------------------------------------------------------------
Fl_Progress_Bar::Fl_Progress_Bar(int x,int y,int w,int h,char *c): Fl_Box(x,y,w,h,c)
{
   type = PBAR_NORMAL;
   perc = 0.0;
   max  = 100.0;
}


//-----------------------------------------------------------------------------
Fl_Progress_Bar::~Fl_Progress_Bar()
{
   type = PBAR_NORMAL;
   perc = 0.0;
   max  = 100.0;
}


//-----------------------------------------------------------------------------
void Fl_Progress_Bar::draw()
{
float inc;
char  str[10];


   inc = (float) (w()/max) * perc;

   switch(type)
   {
      case PBAR_NORMAL:

      // draw foreground
      fl_color(selection_color());
      fl_rectf(x(),y(),(int) inc,h());

      // draw background
//      if((int) inc < w());
      fl_color(color());
      fl_rectf((int) inc + x(),y(),w() - (int) inc,h());


      // default font
      if(!fl_font()) fl_font(1,12);

      sprintf(str,"%d%%",(int) perc);

      // draw string
      fl_color(labelcolor());
      fl_draw(str,x(),y(),w(),h(),FL_ALIGN_CENTER);
      //fl_draw(x(),y(),w(),h());

      break;
   }

   // draw frame box
   switch(box())
   {
      case FL_UP_FRAME:
      case FL_THIN_UP_FRAME:
      case FL_ENGRAVED_FRAME:
      case FL_DOWN_FRAME:
      case FL_THIN_DOWN_FRAME:
      case FL_EMBOSSED_FRAME:
      case FL_BORDER_FRAME:
      draw_box();
      break;
   }

   // draw label
   draw_label();
}


//------------------------------------------------------------------------------
void Fl_Progress_Bar::set_perc(int p,int redraw)
{
   if (perc<0)        perc = 0.0;
   else if (perc>100) perc = 100.0;
   else               perc = (float) p;

   if (redraw) this->redraw();
}


//------------------------------------------------------------------------------
void Fl_Progress_Bar::set_perc(float p,int redraw)
{
   if (perc<0)        perc = 0.0;
   else if (perc>100) perc = 100.0;
   else               perc = p;

   if (redraw) this->redraw();
}


//------------------------------------------------------------------------------
void Fl_Progress_Bar::set_max(float m)
{
   if (m<=0) return;
   max = m;
}

