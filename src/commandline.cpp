// $Id: commandline.cpp,v 1.5 2001/12/16 14:22:37 martinhenne Exp $

// (C) 2001 Martin Henne
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

/*
 * This class is a parser for commandline arguments.
 */

#include <commandline.h>
#include <streamcast.h>
#include <string>
#include <vector>
#include <iostream>
using namespace std;

  int    Commandline::_argc;
  char** Commandline::_argv;
  std::vector<string> Commandline::normal_args;
  std::vector<bool>   Commandline::ignore;


  bool     Commandline::check_for (const std::string& argname, int& val)
  {
    int argpos = check_pos(argname);
    if(argpos!=-1)
    {
      val = stream_cast<int>(_argv[argpos+1]);
      return true;
    }
    else
      return false;
  }



  bool     Commandline::check_for (const std::string& argname, unsigned long& val)
  {
    int argpos = check_pos(argname);
    if(argpos!=-1)
    {
      val = stream_cast<unsigned long>(_argv[argpos+1]);
      return true;
    }
    else
      return false;
  }







  bool     Commandline::check_for (const std::string& argname, std::string& val)
  {
    int argpos = check_pos(argname);
    if(argpos!=-1)
    {
      val = static_cast<std::string>(_argv[argpos+1]);
      return true;
    }
    else
      return false;
  }







  bool     Commandline::check_for (const std::string& argname)
  {
    if(check_pos(argname) != -1)
      return true;
    else
      return false;
  }






  std::vector<std::string>   Commandline::get_args   (const std::string& exclude)
  {
    int    i_tmp=0;
    string s_tmp= std::string("");

    // mark arguments to exclude in 'ignore'-vector
    for(unsigned long i=0; i<(exclude.length()); i++)
    { 
      s_tmp  = exclude.substr(i,1)+std::string(",dummy");
      i_tmp  = check_pos(s_tmp);
      if(i_tmp!=-1)
      {
        //cerr << "ignoring " << s_tmp << " at pos " << i_tmp+1 << endl;
        ignore[i_tmp+1] = true;
      }
    }

    // fill normal_args-vector
    i_tmp = check_pos(std::string("0,NULL"));

    // eliminate empty entries in 'normal_args'
    std::vector<std::string> without_empties(normal_args.size());
    i_tmp = 0; // now used as a counter
    for(unsigned long i=0; i<normal_args.size(); i++)
    {
      if(normal_args[i] != std::string(""))
        without_empties[i_tmp++] = normal_args[i];
    }
    without_empties.resize(i_tmp); // cut trailing empty entries
    normal_args = without_empties;
    return normal_args;
  }







  int      Commandline::check_pos (const std::string& argname)
  {
    // find short and long argumentname
    int commapos = argname.find(",");
    if(commapos==-1) // no comma found
    {
      cerr << "Commandline::check(): malformed argname string."
           << " returning '(unset)'.\n";
      return -1;
    }
    std::string shortarg = argname.substr(0,commapos);
    std::string longarg  = argname.substr(commapos+1,argname.length()-commapos);
//cerr << "short/long: ->" << shortarg << "/" << longarg << "<-" << endl;

    // convert old style argv** to a more useful C++ vector<string>
    vector<string> s_argv(_argc);
    for(unsigned short i=0; i<s_argv.size(); i++)
    {
      s_argv[i] = static_cast<std::string>(_argv[i]);
    }

    // look for the desired value
    for(unsigned short i=1; i<s_argv.size(); i++)
    {
      if(s_argv[i].substr(0,1)=="-") // it is a parameter
      {
        if(s_argv[i].substr(0,2)=="--") // it is a long parameter
        {
          if(s_argv[i].substr(2,s_argv[i].length()-2)==longarg)
          return i;
        }
        if(s_argv[i].substr(1,1) == shortarg) // it is a short parameter
        {
         return i;
        }
      }
      else // the value is no parameter
      {
        if(argname == std::string("0,NULL")) // put argument to normal_args vector
        {
          unsigned long counter = normal_args.size();
          normal_args.resize(counter+1);
          if(!ignore[i])
            normal_args[counter++]=s_argv[i];
          else
            normal_args[counter++]=std::string("");
        }
      }
    }
    return -1;
  }






  void Commandline::init (int argc, char** argv)
  {
    _argc = argc;
    _argv = argv;
    ignore.resize(_argc);
    for(unsigned long i = 0; i < ignore.size(); i++)
      ignore[i] = false;
  }

// end of file

