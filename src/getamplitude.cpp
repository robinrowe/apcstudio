// $Id: getamplitude.cpp,v 1.2 2002/02/18 17:15:23 martinhenne Exp $

// (C) 2002 Martin Henne
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

#include <getamplitude.h>
#include <lendian.h>
#include <iostream>

double getamplitude::operator() (std::vector<uchar>& data,
                 const unsigned long& frame,
                 const int& bitRate,
                 const int& channels,
                 const int& channel)
  {
    using namespace APC;
    const int frameSize = (bitRate/8)*channels;
    std::vector<uchar>& wave = data;

    short abs = 200; // absolute value
    ulong byte = frame * frameSize;
    if(byte>=wave.size()) {
      std::cerr << "getamplitude()(): index out of range!\n";
      return 0.0;
    }

    if(bitRate==8) { // 8 bit
      if(channels==1) { // 8 bit mono
        abs = wave[byte];
      } // 8m
      if(channels==2) { // 8 bit stereo
        if(channel==1) { // 8 bit stereo left
          abs = wave[byte];
        } // 8sl
        if(channel==2) { // 8 bit stereo right
          abs = wave[byte+1];
        } // 8sr
      } // 8s
    } // 8
    if(bitRate==16) { // 16 bit
      if(channels==1) { // 16 bit mono
        lendian(wave[byte], wave[byte+1], abs);
      } // 16m
      if(channels==2) { // 16 bit stereo
        if(channel==1) { // 16 bit stereo left
          lendian(wave[byte], wave[byte+1], abs);
        } // 16sl
        if(channel==2) { // 16 bit stereo right
          lendian(wave[byte+2], wave[byte+3], abs);
        } // 16sr
      } // 16s
    } // 16

    if(bitRate==8)
      return ((abs-0x80)/128.0);
    if(bitRate==16)
      return (abs/(65535.0/2.0));

    std::cerr << "getamplitude()(): if you see me, FIX ME, I'm a bug\n";
    return 0.0;
  }






// end of file
