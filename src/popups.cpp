// $Id: popups.cpp,v 1.15 2002/02/24 16:56:36 martinhenne Exp $

// see 'popups.h' for details

// (C) 2001 Martin Henne
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.


#include <popups.h>
#include <convert.h>
#include <guiheaders.h>
#include <iostream>
#include <sleeper.h>
#include <string>
#include <settings.h>
#include <debug.h>

// pixmaps for popup icons
#include "pix/sign_warning.xpm"
static Fl_Pixmap sign_warning_flxpm(sign_warning_xpm);
#include "pix/sign_error.xpm"
static Fl_Pixmap sign_error_flxpm(sign_error_xpm);
#include "pix/zoom.xpm"
static Fl_Pixmap zoom_flxpm(zoom_xpm);

namespace APC
{
using namespace std;
	// static declarations
  bool Popups::ask_bool;

  void Popups::cb_ask_true (Fl_Widget* o, void* v)
  {
    ask_bool = true;
    o->window()->hide();
  }

  void Popups::cb_ask_false (Fl_Widget* o, void* v)
  {
    ask_bool = false;
    o->window()->hide();
  }

  void Popups::cb_error (Fl_Widget* o, void* v)
  {
    o->window()->hide();
  }

  void Popups::cb_input_ok (Fl_Widget* o, void* v)
  {
  	o->window()->hide();
  	//error("Your input could not be handled at the moment.","Not implemented!");
  }

  void Popups::cb_input (Fl_Widget* o, void* v)
  {
  	Fl_Input* O = (Fl_Input*)(o);
    string* tmp = (string*)(O->user_data());
    *tmp = static_cast<string>(O->value());
  }

  void Popups::cb_input_cancel (Fl_Widget* o, void* v)
  {
  	o->window()->hide();
  }

  bool    Popups::ask              (string question, string title)
  {
    ask_bool = false;
    int heigth = 100;
    int width  = 400;
    Fl_Window       win            (Fl::w()/2-(width/2),
                                    Fl::h()/2-(heigth/2),
                                    width,
                                    heigth,
                                    title.c_str());
      win.color                    (APC::Settings::MAIN_COLOR);
      Fl_Box        sign           (20,20,32,32);
      sign.box                     (FL_FLAT_BOX);
      sign.color                   (APC::Settings::MAIN_COLOR);
      sign_warning_flxpm.label     (&sign);
      Fl_Box        text           (65,20,width-52,32,question.c_str());
      text.labelsize               (12);
      text.align                   (FL_ALIGN_LEFT|FL_ALIGN_INSIDE);
    Fl_Button        no            (width-80,heigth-30,70,25,"No");
    no.labelsize(12);
    Fl_Return_Button yes           (width-160,heigth-30,70,25,"Yes");
    yes.labelsize(12);
    yes.callback                   (cb_ask_true);
    no.callback                    (cb_ask_false);

    win.end                        ();
    win.show                       ();
    win.set_modal                  ();
    while(win.shown())
    {
      Fl::wait(0.2);
			Sleeper::dosleep(10);
    }

    return ask_bool;
  }


  void    Popups::error              (string question, string title)
  {
    int heigth = 100;
    int width  = 400;
    Fl_Window       win            (Fl::w()/2-(width/2),
                                    Fl::h()/2-(heigth/2),
                                    width,
                                    heigth,
                                    title.c_str());
      win.color                    (APC::Settings::MAIN_COLOR);
      Fl_Box        sign           (20,20,32,32);
      sign.box                     (FL_FLAT_BOX);
      sign.color                   (APC::Settings::MAIN_COLOR);
      sign_error_flxpm.label       (&sign);
      Fl_Box        text           (65,20,width-52,32,question.c_str());
      text.labelsize               (12);
      text.align                   (FL_ALIGN_LEFT|FL_ALIGN_INSIDE);
    Fl_Return_Button ok            (width-90,heigth-30,70,25,"OK");
    ok.labelsize(12);
    ok.callback                    (cb_error);

    win.end                        ();
    win.show                       ();
    win.set_modal                  ();

    while(win.shown())
    {
      Fl::wait(0.2);
      Sleeper::dosleep(10);
    }

    return;
  }





  void    Popups::tell              (string question, string title)
  {
    int heigth = 100;
    int width  = 400;
    Fl_Window       win            (Fl::w()/2-(width/2),
                                    Fl::h()/2-(heigth/2),
                                    width,
                                    heigth,
                                    title.c_str());
      win.color                    (APC::Settings::MAIN_COLOR);
      Fl_Box        sign           (20,20,32,32);
      sign.box                     (FL_FLAT_BOX);
      sign.color                   (APC::Settings::MAIN_COLOR);
      sign_warning_flxpm.label     (&sign);
      Fl_Box        text           (65,20,width-52,32,question.c_str());
      text.labelsize               (12);
      text.align                   (FL_ALIGN_LEFT|FL_ALIGN_INSIDE);
    Fl_Return_Button ok            (width-90,heigth-30,70,25,"OK");
    ok.labelsize(12);
    ok.callback                    (cb_error);

    win.end                        ();
    win.show                       ();
    win.set_modal                  ();

    while(win.shown())
    {
      Fl::wait(0.2);
			Sleeper::dosleep(10);
    }

    return;
  }



  void Popups::warn                   (string msg, string wintitle)
  {
    return Popups::tell(msg,wintitle);
  }



  string   Popups::input              (string question, string title)
  {
    string returnVal;
    int heigth = 100;
    int width  = 400;
    Fl_Window       win            (Fl::w()/2-(width/2),
                                    Fl::h()/2-(heigth/2),
                                    width,
                                    heigth,
                                    title.c_str());
      win.color                    (APC::Settings::MAIN_COLOR);
      Fl_Box        sign           (20,20,32,32);
      sign.box                     (FL_FLAT_BOX);
      sign.color                   (APC::Settings::MAIN_COLOR);
      sign_warning_flxpm.label     (&sign);
      if (title=="Zoom")
        zoom_flxpm.label           (&sign);

      Fl_Box        text           (75,10,width-52,25,question.c_str());
      text.labelsize               (12);
      text.align                   (FL_ALIGN_LEFT|FL_ALIGN_INSIDE);
      Fl_Input      input          (62,35,width-72,25,"");
    Fl_Button cancel               (width-80,heigth-30,70,25,"Cancel");
    cancel.labelsize(12);
    Fl_Return_Button ok            (width-160,heigth-30,70,25,"OK");
    ok.labelsize(12);
    ok.callback                    (cb_input_ok);
    cancel.callback                (cb_input_cancel);

    input.user_data                ((void*)&returnVal);
    input.callback                 (cb_input);
    input.when                     (FL_WHEN_CHANGED);
    win.end                        ();
    win.show                       ();
    win.set_modal                  ();

    while(win.shown())
    {
      Fl::wait(0.2);
      Sleeper::dosleep(10);
		}

    return returnVal;
  }

} // end of namespace

// end of file
