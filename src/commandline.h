// $Id: commandline.h,v 1.6 2001/12/16 18:32:02 martinhenne Exp $

// (C) 2001 Martin Henne
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

/*
 * This class is a parser for commandline arguments.
 */

#ifndef _commandline_h
#define _commandline_h

#include <string>
#include <vector>
using std::string;
using std::vector;

/// This class parses the commandline arguments 'argc' and '**argv'.
/**
  * usage example:
  * 
  * // ... somewhere in 'main(int argc, char** argv)' do that:
  *
  * Commandline::init(argc,argv);
  *
  * // somewhere else, you can use the static functions:
  *
  * if(Commandline::check_for(string("v,version")))
  *
  *   cout << "Version 0.5.1" << endl;
  *
  * vector<string> arguments;
  *
  * arguments = Commandline::get_args(string("dc")); // ignore parameters behind '-d' and '-c'
  *
  * if(arguments.size() > 0)
  *
  *   cout << "First argument, that was not a parameter, is "
  *        << arguments[0] << endl;
  *
  */
class Commandline
{
public:
  /// This sets the integer value of argument 'argname'.
  /**
    * It sets '0' if argument is not specified. 'argname'
    * must look like "v,version", which means the commandline
    * arguments '-v' or '--version'.
    */
  static bool     check_for (const string& argname, int& val);

  /// This sets the integer value of argument 'argname'.
  /**
    * It sets '0' if argument is not specified. 'argname'
    * must look like "v,version", which means the commandline
    * arguments '-v' or '--version'.
    */
  static bool     check_for (const string& argname, unsigned long& val);

  /// This sets the string value of argument 'argname'.
  /**
    * It sets "(unset)" if argument is not specified. 'argname'
    * must look like "v,version", which means the commandline
    * arguments '-v' or '--version'.
    */
  static bool     check_for (const string& argname, string& val);

  /// This returns the bool value of argument 'argname'.
  /**
    * It returns false if argument is not specified. 'argname'
    * must look like "v,version", which means the commandline
    * arguments '-v' or '--version'.
    */
  static bool     check_for (const string& argname);

  /// This returns commandline args, that doesn't start with '-'.
  /**
    *  The string 'exclude' must look like "dc", where every
    *  letter represents the short version of a parameter, that
    *  has a value behind it. This value is ignored by 'get_args()'.
    *  
    *  Example:
    *  ./play --channels 2 -d /dev/dsp -v sound.wav
    *  
    *  In this case, only 'sound.wav' should be recognized. To
    *  ignore '/dev/dsp', which is the parameter to '-d', you
    *  have to exclude "d". If 'd' is excluded, the value behind
    *  that option will be considered as it belongs to '-d'.
    *  To ignore '/dev/dsp' and '2' the exclude string should
    *  look like "dc", where 'c' is the short version of 'channels'.
    *  '-v' makes the software telling the version number, but it
    *  is NOT in the exclude string, because it is a standalone parameter
    *  that has no value.
    *
    *  To sum it up: If you only want to have 'sound.wav' in the
    *  returned string vector, the call should be:
    *
    *  vector<string> args;
    *  args = Commandline::get_args("dc");
    *
    */
  static vector<string>   get_args   (const string& exclude);

  /// This passes the parameters.
  static void init(int argc, char** argv);

  // public, because 'Editwindow' needs them
  static int    _argc;
  static char** _argv;

private:
  // no objects of this class!
  Commandline();

  /// This checks for the argument and returns it's argv[vector-index] or '-1'.
  /**
    * This is used by all 'check_for(...)' methods. In case
    * of checking just if a parameter is given, this is all
    * you need to do, in check_for(string);
    */
  static int check_pos(const string& argname);

  /// This one has every argument, that does not start with '-'.
  static vector<string> normal_args;

  /// This marks arguments, to ignore (internal use only).
  static vector<bool> ignore;
};

#endif

