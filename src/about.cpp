// $Id: about.cpp,v 1.7 2002/01/27 21:18:35 martinhenne Exp $

// (C) 2001 Martin Henne
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

#include <guiheaders.h>
#include <globals.h>
#include <about.h>
#include <configfile.h>
#include <streamcast.h>
#include <iostream>
#include <string>
#include <vector>

#include <pix/logo_128x128.xpm>
static Fl_Pixmap logo_pm(logo_128x128_xpm);

#define WIDTH  280
#define HEIGTH 360

Fl_Window*   APC::About::win;
Fl_Box*      APC::About::logo;
Fl_Box*      APC::About::version;
Fl_Browser*  APC::About::authors;
Fl_Box*      APC::About::copyright;
int          APC::About::maincolor;
int          APC::About::darkcolor;
int          APC::About::fontcolor;

/*

  ************************************
  *                                  *
  *   do not delete the folling      *
  *   comment! use it to copy/paste  *
  *   into the fltk-code-window of   *
  *   the Aboutwin class.            *
  *                                  *
  ************************************


author.push_back(string(""));
author.push_back(string(""));
author.push_back(string("(c) GNU/GPL 2001, 2002 Martin Henne"));
author.push_back(string("email: Martin.Henne@web.de"));
author.push_back(string(""));
author.push_back(string(" * * * * *"));
author.push_back(string(""));
author.push_back(string("main programming, documentation, website:"));
author.push_back(string("- Martin Henne"));
author.push_back(string(""));
author.push_back(string("help on testing:"));
author.push_back(string("- Stefan Grosse-Kappenberg"));
author.push_back(string("- Arne Dueselder"));
author.push_back(string("- Marcus Maul"));
author.push_back(string(""));
author.push_back(string("packaging:"));
author.push_back(string("- TODO"));
author.push_back(string(""));
author.push_back(string("internationalization:"));
author.push_back(string("- TODO"));
author.push_back(string(""));
author.push_back(string(" * * * * *"));
author.push_back(string(""));
author.push_back(string("website: http://apcstudio.sf.net"));
author.push_back(string("mailinglists: see http://sf.net/projects/apcstudio"));
author.push_back(string("bugreports: apcstudio-bugs@lists.sf.net"));
author.push_back(string("wishes: apcstudio-wishlist@lists.sf.net"));
author.push_back(string("announces: apcstudio-announce@lists.sf.net"));
author.push_back(string("help: apcstudio-users@lists.sf.net"));
author.push_back(string(""));
author.push_back(string(" * * * * *"));
author.push_back(string(""));

license.push_back(string(""));
license.push_back(string("
license.push_back(string("		    GNU GENERAL PUBLIC LICENSE);
license.push_back(string("		       Version 2, June 1991);
license.push_back(string(");
license.push_back(string(" Copyright (C) 1989, 1991 Free Software Foundation, Inc.);
license.push_back(string("                       59 Temple Place, Suite 330, Boston, MA  02111-1307  USA);
license.push_back(string(" Everyone is permitted to copy and distribute verbatim copies);
license.push_back(string(" of this license document, but changing it is not allowed.);
license.push_back(string(");
license.push_back(string("			    Preamble);
license.push_back(string(");
license.push_back(string("  The licenses for most software are designed to take away your);
license.push_back(string("freedom to share and change it.  By contrast, the GNU General Public);
license.push_back(string("License is intended to guarantee your freedom to share and change free);
license.push_back(string("software--to make sure the software is free for all its users.  This);
license.push_back(string("General Public License applies to most of the Free Software);
license.push_back(string("Foundation's software and to any other program whose authors commit to);
license.push_back(string("using it.  (Some other Free Software Foundation software is covered by);
license.push_back(string("the GNU Library General Public License instead.)  You can apply it to);
license.push_back(string("your programs, too.);
license.push_back(string(");
license.push_back(string("  When we speak of free software, we are referring to freedom, not
license.push_back(string("price.  Our General Public Licenses are designed to make sure that you
license.push_back(string("have the freedom to distribute copies of free software (and charge for
license.push_back(string("this service if you wish), that you receive source code or can get it
license.push_back(string("if you want it, that you can change the software or use pieces of it
license.push_back(string("in new free programs; and that you know you can do these things.
license.push_back(string("
license.push_back(string("  To protect your rights, we need to make restrictions that forbid
license.push_back(string("anyone to deny you these rights or to ask you to surrender the rights.
license.push_back(string("These restrictions translate to certain responsibilities for you if you
license.push_back(string("distribute copies of the software, or if you modify it.
license.push_back(string("
license.push_back(string("  For example, if you distribute copies of such a program, whether
license.push_back(string("gratis or for a fee, you must give the recipients all the rights that
license.push_back(string("you have.  You must make sure that they, too, receive or can get the
license.push_back(string("source code.  And you must show them these terms so they know their
license.push_back(string("rights.
license.push_back(string("
license.push_back(string("  We protect your rights with two steps: (1) copyright the software, and
license.push_back(string("(2) offer you this license which gives you legal permission to copy,
license.push_back(string("distribute and/or modify the software.
license.push_back(string("
license.push_back(string("  Also, for each author's protection and ours, we want to make certain
license.push_back(string("that everyone understands that there is no warranty for this free
license.push_back(string("software.  If the software is modified by someone else and passed on, we
license.push_back(string("want its recipients to know that what they have is not the original, so
license.push_back(string("that any problems introduced by others will not reflect on the original
license.push_back(string("authors' reputations.
license.push_back(string("
license.push_back(string("  Finally, any free program is threatened constantly by software
license.push_back(string("patents.  We wish to avoid the danger that redistributors of a free
license.push_back(string("program will individually obtain patent licenses, in effect making the
license.push_back(string("program proprietary.  To prevent this, we have made it clear that any
license.push_back(string("patent must be licensed for everyone's free use or not licensed at all.
license.push_back(string("
license.push_back(string("  The precise terms and conditions for copying, distribution and
license.push_back(string("modification follow.
license.push_back(string("
license.push_back(string("		    GNU GENERAL PUBLIC LICENSE
license.push_back(string("   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
license.push_back(string("
license.push_back(string("  0. This License applies to any program or other work which contains
license.push_back(string("a notice placed by the copyright holder saying it may be distributed
license.push_back(string("under the terms of this General Public License.  The "Program", below,
license.push_back(string("refers to any such program or work, and a "work based on the Program"
license.push_back(string("means either the Program or any derivative work under copyright law:
license.push_back(string("that is to say, a work containing the Program or a portion of it,
license.push_back(string("either verbatim or with modifications and/or translated into another
license.push_back(string("language.  (Hereinafter, translation is included without limitation in
license.push_back(string("the term "modification".)  Each licensee is addressed as "you".
license.push_back(string("
license.push_back(string("Activities other than copying, distribution and modification are not
license.push_back(string("covered by this License; they are outside its scope.  The act of
license.push_back(string("running the Program is not restricted, and the output from the Program
license.push_back(string("is covered only if its contents constitute a work based on the
license.push_back(string("Program (independent of having been made by running the Program).
license.push_back(string("Whether that is true depends on what the Program does.
license.push_back(string("
license.push_back(string("  1. You may copy and distribute verbatim copies of the Program's
license.push_back(string("source code as you receive it, in any medium, provided that you
license.push_back(string("conspicuously and appropriately publish on each copy an appropriate
license.push_back(string("copyright notice and disclaimer of warranty; keep intact all the
license.push_back(string("notices that refer to this License and to the absence of any warranty;
license.push_back(string("and give any other recipients of the Program a copy of this License
license.push_back(string("along with the Program.
license.push_back(string("
license.push_back(string("You may charge a fee for the physical act of transferring a copy, and
license.push_back(string("you may at your option offer warranty protection in exchange for a fee.
license.push_back(string("
license.push_back(string("  2. You may modify your copy or copies of the Program or any portion
license.push_back(string("of it, thus forming a work based on the Program, and copy and
license.push_back(string("distribute such modifications or work under the terms of Section 1
license.push_back(string("above, provided that you also meet all of these conditions:
license.push_back(string("
license.push_back(string("    a) You must cause the modified files to carry prominent notices
license.push_back(string("    stating that you changed the files and the date of any change.
license.push_back(string("
license.push_back(string("    b) You must cause any work that you distribute or publish, that in
license.push_back(string("    whole or in part contains or is derived from the Program or any
license.push_back(string("    part thereof, to be licensed as a whole at no charge to all third
license.push_back(string("    parties under the terms of this License.
license.push_back(string("
license.push_back(string("    c) If the modified program normally reads commands interactively
license.push_back(string("    when run, you must cause it, when started running for such
license.push_back(string("    interactive use in the most ordinary way, to print or display an
license.push_back(string("    announcement including an appropriate copyright notice and a
license.push_back(string("    notice that there is no warranty (or else, saying that you provide
license.push_back(string("    a warranty) and that users may redistribute the program under
license.push_back(string("    these conditions, and telling the user how to view a copy of this
license.push_back(string("    License.  (Exception: if the Program itself is interactive but
license.push_back(string("    does not normally print such an announcement, your work based on
license.push_back(string("    the Program is not required to print an announcement.)
license.push_back(string("
license.push_back(string("These requirements apply to the modified work as a whole.  If
license.push_back(string("identifiable sections of that work are not derived from the Program,
license.push_back(string("and can be reasonably considered independent and separate works in
license.push_back(string("themselves, then this License, and its terms, do not apply to those
license.push_back(string("sections when you distribute them as separate works.  But when you
license.push_back(string("distribute the same sections as part of a whole which is a work based
license.push_back(string("on the Program, the distribution of the whole must be on the terms of
license.push_back(string("this License, whose permissions for other licensees extend to the
license.push_back(string("entire whole, and thus to each and every part regardless of who wrote it.
license.push_back(string("
license.push_back(string("Thus, it is not the intent of this section to claim rights or contest
license.push_back(string("your rights to work written entirely by you; rather, the intent is to
license.push_back(string("exercise the right to control the distribution of derivative or
license.push_back(string("collective works based on the Program.
license.push_back(string("
license.push_back(string("In addition, mere aggregation of another work not based on the Program
license.push_back(string("with the Program (or with a work based on the Program) on a volume of
license.push_back(string("a storage or distribution medium does not bring the other work under
license.push_back(string("the scope of this License.
license.push_back(string("
license.push_back(string("  3. You may copy and distribute the Program (or a work based on it,
license.push_back(string("under Section 2) in object code or executable form under the terms of
license.push_back(string("Sections 1 and 2 above provided that you also do one of the following:
license.push_back(string("
license.push_back(string("    a) Accompany it with the complete corresponding machine-readable
license.push_back(string("    source code, which must be distributed under the terms of Sections
license.push_back(string("    1 and 2 above on a medium customarily used for software interchange; or,
license.push_back(string("
license.push_back(string("    b) Accompany it with a written offer, valid for at least three
license.push_back(string("    years, to give any third party, for a charge no more than your
license.push_back(string("    cost of physically performing source distribution, a complete
license.push_back(string("    machine-readable copy of the corresponding source code, to be
license.push_back(string("    distributed under the terms of Sections 1 and 2 above on a medium
license.push_back(string("    customarily used for software interchange; or,
license.push_back(string("
license.push_back(string("    c) Accompany it with the information you received as to the offer
license.push_back(string("    to distribute corresponding source code.  (This alternative is
license.push_back(string("    allowed only for noncommercial distribution and only if you
license.push_back(string("    received the program in object code or executable form with such
license.push_back(string("    an offer, in accord with Subsection b above.)
license.push_back(string("
license.push_back(string("The source code for a work means the preferred form of the work for
license.push_back(string("making modifications to it.  For an executable work, complete source
license.push_back(string("code means all the source code for all modules it contains, plus any
license.push_back(string("associated interface definition files, plus the scripts used to
license.push_back(string("control compilation and installation of the executable.  However, as a
license.push_back(string("special exception, the source code distributed need not include
license.push_back(string("anything that is normally distributed (in either source or binary
license.push_back(string("form) with the major components (compiler, kernel, and so on) of the
license.push_back(string("operating system on which the executable runs, unless that component
license.push_back(string("itself accompanies the executable.
license.push_back(string("
license.push_back(string("If distribution of executable or object code is made by offering
license.push_back(string("access to copy from a designated place, then offering equivalent
license.push_back(string("access to copy the source code from the same place counts as
license.push_back(string("distribution of the source code, even though third parties are not
license.push_back(string("compelled to copy the source along with the object code.
license.push_back(string("
license.push_back(string("  4. You may not copy, modify, sublicense, or distribute the Program
license.push_back(string("except as expressly provided under this License.  Any attempt
license.push_back(string("otherwise to copy, modify, sublicense or distribute the Program is
license.push_back(string("void, and will automatically terminate your rights under this License.
license.push_back(string("However, parties who have received copies, or rights, from you under
license.push_back(string("this License will not have their licenses terminated so long as such
license.push_back(string("parties remain in full compliance.
license.push_back(string("
license.push_back(string("  5. You are not required to accept this License, since you have not
license.push_back(string("signed it.  However, nothing else grants you permission to modify or
license.push_back(string("distribute the Program or its derivative works.  These actions are
license.push_back(string("prohibited by law if you do not accept this License.  Therefore, by
license.push_back(string("modifying or distributing the Program (or any work based on the
license.push_back(string("Program), you indicate your acceptance of this License to do so, and
license.push_back(string("all its terms and conditions for copying, distributing or modifying
license.push_back(string("the Program or works based on it.
license.push_back(string("
license.push_back(string("  6. Each time you redistribute the Program (or any work based on the
license.push_back(string("Program), the recipient automatically receives a license from the
license.push_back(string("original licensor to copy, distribute or modify the Program subject to
license.push_back(string("these terms and conditions.  You may not impose any further
license.push_back(string("restrictions on the recipients' exercise of the rights granted herein.
license.push_back(string("You are not responsible for enforcing compliance by third parties to
license.push_back(string("this License.
license.push_back(string("
license.push_back(string("  7. If, as a consequence of a court judgment or allegation of patent
license.push_back(string("infringement or for any other reason (not limited to patent issues),
license.push_back(string("conditions are imposed on you (whether by court order, agreement or
license.push_back(string("otherwise) that contradict the conditions of this License, they do not
license.push_back(string("excuse you from the conditions of this License.  If you cannot
license.push_back(string("distribute so as to satisfy simultaneously your obligations under this
license.push_back(string("License and any other pertinent obligations, then as a consequence you
license.push_back(string("may not distribute the Program at all.  For example, if a patent
license.push_back(string("license would not permit royalty-free redistribution of the Program by
license.push_back(string("all those who receive copies directly or indirectly through you, then
license.push_back(string("the only way you could satisfy both it and this License would be to
license.push_back(string("refrain entirely from distribution of the Program.
license.push_back(string("
license.push_back(string("If any portion of this section is held invalid or unenforceable under
license.push_back(string("any particular circumstance, the balance of the section is intended to
license.push_back(string("apply and the section as a whole is intended to apply in other
license.push_back(string("circumstances.
license.push_back(string("
license.push_back(string("It is not the purpose of this section to induce you to infringe any
license.push_back(string("patents or other property right claims or to contest validity of any
license.push_back(string("such claims; this section has the sole purpose of protecting the
license.push_back(string("integrity of the free software distribution system, which is
license.push_back(string("implemented by public license practices.  Many people have made
license.push_back(string("generous contributions to the wide range of software distributed
license.push_back(string("through that system in reliance on consistent application of that
license.push_back(string("system; it is up to the author/donor to decide if he or she is willing
license.push_back(string("to distribute software through any other system and a licensee cannot
license.push_back(string("impose that choice.
license.push_back(string("
license.push_back(string("This section is intended to make thoroughly clear what is believed to
license.push_back(string("be a consequence of the rest of this License.
license.push_back(string("
license.push_back(string("  8. If the distribution and/or use of the Program is restricted in
license.push_back(string("certain countries either by patents or by copyrighted interfaces, the
license.push_back(string("original copyright holder who places the Program under this License
license.push_back(string("may add an explicit geographical distribution limitation excluding
license.push_back(string("those countries, so that distribution is permitted only in or among
license.push_back(string("countries not thus excluded.  In such case, this License incorporates
license.push_back(string("the limitation as if written in the body of this License.
license.push_back(string("
license.push_back(string("  9. The Free Software Foundation may publish revised and/or new versions
license.push_back(string("of the General Public License from time to time.  Such new versions will
license.push_back(string("be similar in spirit to the present version, but may differ in detail to
license.push_back(string("address new problems or concerns.
license.push_back(string("
license.push_back(string("Each version is given a distinguishing version number.  If the Program
license.push_back(string("specifies a version number of this License which applies to it and "any
license.push_back(string("later version", you have the option of following the terms and conditions
license.push_back(string("either of that version or of any later version published by the Free
license.push_back(string("Software Foundation.  If the Program does not specify a version number of
license.push_back(string("this License, you may choose any version ever published by the Free Software
license.push_back(string("Foundation.
license.push_back(string("
license.push_back(string("  10. If you wish to incorporate parts of the Program into other free
license.push_back(string("programs whose distribution conditions are different, write to the author
license.push_back(string("to ask for permission.  For software which is copyrighted by the Free
license.push_back(string("Software Foundation, write to the Free Software Foundation; we sometimes
license.push_back(string("make exceptions for this.  Our decision will be guided by the two goals
license.push_back(string("of preserving the free status of all derivatives of our free software and
license.push_back(string("of promoting the sharing and reuse of software generally.
license.push_back(string("
license.push_back(string("			    NO WARRANTY
license.push_back(string("
license.push_back(string("  11. BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY
license.push_back(string("FOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW.  EXCEPT WHEN
license.push_back(string("OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES
license.push_back(string("PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED
license.push_back(string("OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
license.push_back(string("MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE ENTIRE RISK AS
license.push_back(string("TO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU.  SHOULD THE
license.push_back(string("PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING,
license.push_back(string("REPAIR OR CORRECTION.
license.push_back(string("
license.push_back(string("  12. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING
license.push_back(string("WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR
license.push_back(string("REDISTRIBUTE THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES,
license.push_back(string("INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING
license.push_back(string("OUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED
license.push_back(string("TO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY
license.push_back(string("YOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER
license.push_back(string("PROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE
license.push_back(string("POSSIBILITY OF SUCH DAMAGES.
license.push_back(string("
license.push_back(string("		     END OF TERMS AND CONDITIONS
license.push_back(string("

thanks.push_back(string(""));

apc.push_back(string(""));

*/

vector<string> APC::About::author;
vector<string> APC::About::license;
vector<string> APC::About::thanks;
vector<string> APC::About::apc;


namespace APC
{
using namespace std;

  About::About()
  {
    maincolor = 25;
    darkcolor = 27;
    fontcolor = 7;
    Configfile:: get            ("MAIN_COLOR",maincolor);
    Configfile:: get            ("MAIN_DARKER_COLOR",darkcolor);
    Configfile:: get            ("FONT_COLOR",fontcolor);
    win        = new Fl_Window  (Fl::w()/2-WIDTH/2,
                                 Fl::h()/2-HEIGTH/2,
                                 WIDTH,
                                 HEIGTH,
                                 "About aPcStudio");
    win       -> color          (maincolor);
    logo       = new Fl_Box     (WIDTH/2-64,10,128,128,"");
    logo_pm    . label          (logo);
    version    = new Fl_Box     (10,145,WIDTH-20,20,Globals::pcVERSION);
    version   -> labelcolor     (fontcolor);
    authors    = new Fl_Browser (0,173,WIDTH,HEIGTH-173-30);
    authors   -> color          (darkcolor);
    authors   -> textcolor      (fontcolor);
    authors   -> textsize       (12);
    copyright  = new Fl_Box     (10,HEIGTH-30,WIDTH-20,30,"(c) 2001 Martin Henne GNU/GPL");
    copyright -> labelcolor     (fontcolor);
    win       -> end            ();
  }

   
  void About::show()
  {
    const int lines = 70;
    unsigned int i=0;
    string text[lines];
    text[i++]  = "";
    text[i++]  = "";
    text[i++]  = "";
    text[i++]  = "";
    text[i++]  = "@c@i@bMain Programming:";
    text[i++]  = "";
    text[i++]  = "@c@bMartin Henne";
    text[i++]  = "@c(Martin.Henne@ web.de)";
    text[i++]  = "@chttp://apcstudio.sf.net";
    text[i++]  = "";
    text[i++]  = "";
    text[i++]  = "";
    text[i++]  = "";
    text[i++]  = "";
    text[i++]  = "@_";
    text[i++]  = "";
    text[i++]  = "@c@i@bThanks to:";
    text[i++]  = "";
    text[i++]  = "@c@bArne Dueselder";
    text[i++]  = "@c(Arne.Dueselder@ web.de)";
    text[i++]  = "@c- reverse effect";
    text[i++] = "@c- fade effect";
    text[i++] = "@c- noise gate";
    text[i++] = "";
    text[i++] = "";
    text[i++] = "@c@bStefan Grosse-Kappenberg";
    text[i++] = "@c(Stefan@ Grosse-Kappenberg.de)";
    text[i++] = "@c- drawing code speedup";
    text[i++] = "@c- mouse draging code";
    text[i++] = "@c- debugging";
    text[i++] = "";
    text[i++] = "";
    text[i++] = "@cThis programm was developed with";
    text[i++] = "@csupport by the members of the";
    text[i++] = "@c@iAachen Programming Club";
    text[i++] = "@chttp://aachenprogrammingclub.de";
    text[i++] = "";
    text[i++] = "@cThe sourcecode is friendly hosted";
    text[i++] = "@cby http://sourceforge.net, the";
    text[i++] = "@c#1 supporter for the GNU community";
    text[i++] = "";
    text[i++] = "@cThe Aachen Programming Club is";
    text[i++] = "@csupported by the The Department of";
    text[i++] = "@cInformatics in Mechanical Engineering (IMA)";
    text[i++] = "@cat Aachen University Of Technology (RWTH)";
    text[i++] = "";   
    text[i++] = "";
    text[i++] = string("@c This software uses the famous FLTK ")+stream_cast<string>(FL_VERSION);
    text[i++] = "@chttp://www.fltk.org";
    text[i++] = "";
    text[i++] = "";
    text[i++] = "@c@b@iPlease support Linux";
    text[i++] = "";
    text[i++] = "";
    text[i++] = "";

    for(int i=0; i<lines; i++)
    {
      authors->insert(i+1,text[i].c_str());
    }

    win->show();
  }

  void About::hide()
  {
    win->hide();
  }

} // end of namespace



