// $Id: apc_fl_waveedit.h,v 1.55 2002/02/16 03:18:56 martinhenne Exp $

// Copyright 2001 Martin Henne 2001
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

#ifndef _apc_fl_waveedit_h
#define _apc_fl_waveedit_h

#include <globals.h>
/*
#include <FL/Fl.H>
#include <FL/Fl_Widget.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Menu_Bar.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Scrollbar.H>
#include <FL/Fl_Group.H>
*/
#include <guiheaders.h>

#include <iostream>
#include "wavefile.h"
#include <strstream>
#include <configfile.h>
#include <drawcalc.h>
#include <streamcast.h>


namespace APC
{
  using namespace std;
  class Editwindow; // forward declaration for being a friend

  /// A custom widget, that can display a wavefile graphically.
  /**
   * This class shows the wavefile graphically. It can be used
   * as a widget within a window. In aPcStudio it is a private
   * member (and a component) of the class 'Editwindow', which
   * it is heavily tied to. E.g. it manipulates the informations
   * about the displayed data, that are provided as statusboxes
   * within 'Editwindow', directly (using a pointer to it).
   *
   * This class handles also the mouse actions like marking a
   * part of the waveform by draging or pushing a mousebutton.
   * It has members called 'markStart' and 'markEnd' with represents
   * the marked sample frames (not bytes!).
   *
   * The drawing colors are provided by the configuration file
   * (apcstudiorc). It this cannot be found or is corrupted,
   * the class uses standard values.
   */
  class Apc_fl_waveedit : Fl_Widget
  {
  public:
    /// construct the widget with position and size parameters in pixels
    Apc_fl_waveedit                   (int x, int y, int w, int h);
    ~Apc_fl_waveedit                  ();

    /// This is for debugging issues
    bool use_old_drawcode;

    /// 'draw()' checks, if waveform is supported. Then it calls all other draw functions.
    void            draw                 ();
    void            draw_background      ();
    void            draw_hraster         ();
    void            draw_vraster         ();
    void            draw_wave            ();
    void            draw_virtual_wave    ();
    void            draw_close_wave      ();
    void            draw_null_lines      ();
    void            draw_labels          ();
    void            draw_marks           ();
    void            draw_while_recording ();
    void            draw_digits          (const int& pix, const char* text);

    /// This draws the cursor, but does nothing else (just the vertical line!).
    void            draw_cursor       ();

    /// This calles 'setzoomFactor()' after multiplying it.
    void            multiplyZoom      (double arg);

    /// This is the only function, that writes 'zoomFactor' directly!
    /** ::setzoomFactor() recalculates 'visualFrames' and 'drawEnd',
      * it then sets them, but it does no redraw!
      * returnvalue sets the value, that is used, in case
      * the given value is out of range and was corrected.
      */
    double          setzoomFactor     (double arg);
    double          getzoomFactor     ();

    /// set zoomFactor back to 100%, this calles 'setzoomFactor()'
    void            resetzoomFactor   ();

    /// this updates all status boxes of Editwindow.
    /** please check, if this is really needed, because it takes time */
    void            updateDrawStatus  ();

    void            setdrawStart      (unsigned long        frame);
    unsigned long   getdrawStart      ();
    void            setdrawEnd        (unsigned long);
    unsigned long   getdrawEnd        ();
    void            setvisualFrames   (unsigned long);
    unsigned long   getvisualFrames   ();
    unsigned long   getmarkStart      ();
    unsigned long   getmarkEnd        ();
    double          getXSTEP          ();
    void            doRedraw          ();

    /// Called with a sample frame, this returnes string like '01:02.150'.
    string          frame2time        (unsigned long);

    /// This is not implemented, yet.
    unsigned long   time2frame        (string);

    /// This is ONLY called by 'fame2time()'.
    string          byte2time         (int arg);

    /// At what screen position (pixels) is the frame?
    int             frame2screen      (unsigned long);

    /// This returnes the frame number, according to screen position.
    unsigned long   screen2frame      (int);

    /// This returnes the byte positon of the frame at the screen.
    unsigned long   screen2byte       (int arg);

    /// This is only called by 'frame2screen()'.
    int             byte2screen       (unsigned long);

    /// This converts milliseconds to pixels
    int             ms2pix            (int ms);
    int             pix2ms            (int x_pixel);
    int             frame2ms          (int frame);

    /// this just sets 'cursorPosition' to 'frame', but doesn't redraw or update anything!
    void            setcursorPosition (unsigned long frame);

    /// This returnes the frameposition of the cursor.
    unsigned long   getcursorPosition ();

    /// this updates Editwindow's statusbox for the cursor position
    void            relabel_cursorPosition();

    /// Connect the class to a wavefile. Before this, we can't see anything.
    /** this function has to set all important values like waveSize,
      * bitRate, channels and so on... All this is needed to draw the
      * the waveform at the screen.
      */
    void            setdata           (Wavefile *arg);

    /// calls '::setdata(Wavefile*)'
    void            init              (Editwindow*);

    /// This sets the 'user_data' of the custom widget.
    /** Usually, this is a generic pointer to the Editwindow
      * Object, which this object belongs to.
      */
    void            set_user_data     (void* v);

    /// Sets the format information of the wavefile
    void            set_format        (const int& _frequency,
                                       const int& _channels,
                                       const int& _bitRate);

    /// This returns a pointer to the Wavefile.
    Wavefile*       getdata           ();

    /// Get the window width.
    int             getwidth          ();

    /// Call this, if the wavedata changes (effects, cut, paste ....).
    void data_changed();

    /// Apply color settings.
    void apply_colors();
 
    /** @name mouse and keyboard events
      * these events take care of pushing and draging... */
    //@{
    void            setmarks_drag       ();
    void            setmarks_push       ();
    int             handle              (int anyEvent);
    int             shortcut            (int anyEvent);
    void            setmarkStart     (unsigned long frame);
    void            setmarkEnd       (unsigned long frame);
    //@}

    void            setwaveSize      (unsigned long size);
    int             getframeSize     ();
    int             getpixpersample  ();

  protected:
    // friends
    //friend class Editwindow;
    friend class Overlay;
    friend class Drawcalc;

    /// a handle to the wavedata, we want to display
    Wavefile*       data;

    /// This makes all necessary calculations for ::draw_wave()
    Drawcalc*       dcalc;

    /// This holds a pointer to this->user_data()
    Editwindow*     editwindow;

    /// true, if data was assigned already
    bool            dataAssigned;      // to test, if there has been a
                                       // Wavefile passed to 'data'

    /// is the mouse busy with dragging functions?
    bool            dragging;
    bool            dragleft;
    bool            dragright;
    bool            doubleclick;
		
    /// a zoomFactor of 100 shows the whole wavefile
    double          zoomFactor;

    /// this is the first sampleframe, that is shown to the left
    unsigned long   drawStart;

    /// this is the last sampleframe, that is shown to the right
    unsigned long   drawEnd;

    /// the first marked sampleframe
    unsigned long   markStart;

    /// the last marked sampleframe
    unsigned long   markEnd;

    /// screen x-coordinate of markStart (calculated by ::draw())
    int             screen_markStart;

    /// screen x-coordinate of markEnd (calculated by ::draw())
    int             screen_markEnd;

    int             frequency;
    int             bitRate;
    unsigned long   waveSize;
    short           frameSize;
    int             channels;

    /// how many frames between drawStart and drawEnd are visible
    int             visualFrames;

    /** @name drawing colors
      * Their values are read from the configfile. Have
      * a look at the constructor and read the configfile.
      */
    //@{
    int             linecolor;
    int             backcolor;
    int             selcolor;
    int             selbgcolor;
    int             unselcolor;
    int             unselbgcolor;
    int             zerocolor;
    int             rastercolor;
    int             cursorcolor;
    //@}

    /// At this x-screen-position, the mouse started it's drag.
    /** This is used by the mouse event handlers. */
    int             dragStart;

    /// How many frames cannot be displayes, due to the screen resolution?
    /** This is manipulated and used by the draw functions. */

    double          XSTEP; // was int

    /// If XSTEP tends to get smaller than 1, we need more pixels per sample.
    /** This should be set, wherever XSTEP is calculated! */
    int             pix_per_sample;

    /// How high is the widget (pixels)? Set and used by draw functions.
    int             heigth;

    /// How wide is the widget (pixels)? Set and used by draw functions.
    int             width;

    /// was ::draw() entered without leaving?
    bool            drawing;

    /// On what frame is the cursor?
    /** This position can be freely used for effects or other stuff,
      * that needs to know where the cursor is. It is set by the
      * mouse handling events. User can shift-click or shift-drag to
      * cause the cursor to move. Playing a sound also moves the
      * cursor!!!
      */
    unsigned long   cursorPosition;

    /// for debuging
    void dumpDrawData();
    void draw_debug_line();

 };


} // end of namespace

#endif


// end of file

