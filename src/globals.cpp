// $Id: globals.cpp,v 1.21 2002/02/18 17:15:23 martinhenne Exp $

// (C) 2001 Martin Henne
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

// This file contains global declarations for 'aPcStudio'

#define RELEASE_VERSION

#include <globals.h>
#include <FL/Fl.H>
#include <string>
using namespace std;

const char* Globals::CTIME = __TIME__;
const char* Globals::CDATE = __DATE__;

#ifdef RELEASE_VERSION
std::string Globals::sVERSION("v0.6.1 alpha");
#else
std::string Globals::sVERSION = std::string("(CVS: ")+
                                std::string(Globals::CDATE)+
                                std::string(")");
#endif

const char* Globals::pcVERSION = sVERSION.c_str();
std::string Globals::WEB_URL   = "http://apcstudio.sf.net";

Fl_Cursor Globals::mouse = FL_CURSOR_DEFAULT;

// -d --debug-level
string Globals::CLEXCLUDE = "d";

int Globals::DBG_NORMAL            = 128;
int Globals::DBG_SEGFAULT_CAUGHT   = 256;
int Globals::DBG_WHILE_LOOPS       = 512;
int Globals::DBG_FOR_LOOPS         = 1024;
int Globals::DBG_DRAWCALC_LOW      = 2048;
int Globals::DBG_DRAWCALC_HIGH     = (4096+2048); // 6144
int Globals::DBG_FUNCTION_TRACE    = 8192;
int Globals::DBG_DRAG              = 16384;
int Globals::DEFAULT_DEBUG_LEVEL   = 0;

int Globals::NO_WRITE_ACCESS      = -5;
int Globals::NO_READ_ACCESS       = -4;
int Globals::OUT_OF_MEMORY        = -3;
int Globals::NO_FILE_SPECIFIED    = -2;
int Globals::OTHER_ERROR          = -1;
int Globals::SUCCESS              =  0;
