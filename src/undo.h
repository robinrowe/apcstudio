// $Id: undo.h,v 1.2 2002/02/09 02:55:44 martinhenne Exp $

// Copyright 2001 Martin Henne 2001
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.


#ifndef _undo_h
#define _undo_h

#include <deque>
#include <undooperation.h>

namespace APC
{
using std::deque;
class Editwindow;

  /// This class handles Undo-Operations.
  /** It should be a static element of "Editwindow". */
  class Undo
  {
  public:
    Undo();
    ~Undo(); // call delete for all undo-operations!

    /// Perform one undo-operation and pop it from the stack.
    /** The returnvalue indicates success. */
    bool perform(Editwindow*);

    /// Put one new Operation on the stack.
    /** The returnvalue indicates success. */
    bool put(Undo_operation*);

    /// Clear all Undo-Operations.
    void clear();

  private:
    deque<Undo_operation*> lifo_stack;
    ulong counter;
  };

} // end of namespace

#endif

// end of file


