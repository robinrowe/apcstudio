// $Id: mixpaste.h,v 1.4 2002/02/19 22:53:50 martinhenne Exp $

// (C) 2001 Martin Henne
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

// This is the 'Edit' -> 'mix paste' - pulldown menu function

#ifndef _mixpaste_h
#define _mixpaste_h

#include <vector>

namespace APC {
  class Editwindow;
}

class Mixpaste
{
public:
  /// Ask for loudness ratio (orig-data/overlay-data) and call ::overlay()
  static void perform(APC::Editwindow* ew, const int& channel = 0);

  /// This function mixes 'overlay' into 'data' from 'cursorframe' on.
  static void overlay(std::vector<unsigned char>& data,    // this will be modified!
                      const std::vector<unsigned char>& overlay,
                      const unsigned long& cursorframe,
                      const int& bitRate,
                      const int& channels,
                      const int& frameSize,
                      const double& volumefactor,
                      const int& channel = 0); // 0-both 1-left 2-right

  /// Mix values of one single sample for only one channel
  static int mix_values(const int& tmpi, const int& tmpi2);
};

#endif

// end of file
