// $Id: statusbar.cpp,v 1.5 2001/12/16 14:22:37 martinhenne Exp $

// (C) 2001 Martin Henne
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

/*
 * This class implements a statusbar within a popup window
 * It's a great world for GNU-Freaks, so I don't have to do
 * much work here, because of Bonfanti Alessandro's Progress-
 * Bar - Widget. Thank's Bon.
 *
 * The Win32-Version of aPcStudio maybe become Shareware
 * instead of GPL someday. In this case, Bon's code has to
 * be removed!
 * 
 * By the way: Thanks to Bill Spitzak for the great FLTK.
 *
 */


#include <guiheaders.h>
#include <Fl_Progress_Bar.H>
#include <iostream>
#include <string>
#include <statusbar.h>

#define WIDTH  200
#define HEIGTH 80


namespace APC
{
using namespace std;
  // construct
  Statusbar::Statusbar           (string label, 
                                  string progressname)
  {
    // cerr << "Statusbar: constructor\n";
    winlabel       = label;
    progresslabel  = progressname;
    win            = new Fl_Window( (Fl::w()/2)-(WIDTH/2),
                                    (Fl::h()/2)-(HEIGTH/2),
                                    WIDTH, 
                                    HEIGTH,
                                    winlabel.c_str() );
      progressName = new Fl_Box (0,0,WIDTH,HEIGTH/2,progresslabel.c_str());
      progressName->labelsize(12);
      bar          = new Fl_Progress_Bar(20,HEIGTH/2, WIDTH-40,(HEIGTH/2)-20);
      bar->selection_color(15); // dark blue
      bar->labelcolor(7);       // white
      bar->box(FL_DOWN_FRAME);
    win->end();
  }

  // else stuff
  void                Statusbar::set                (int arg)
  {
    bar->set_perc(arg,1);
    bar->redraw();
    Fl::check();
  }


  void                Statusbar::relabel            (const string newlabel)
  {
    this->winlabel = newlabel;
    win->label(winlabel.c_str());
    win->redraw();
    Fl::wait(0.1);
  }


  void                Statusbar::progressname       (const string newprogressname)
  {
    this->progresslabel = newprogressname;
    progressName->label(progresslabel.c_str());
    progressName->redraw();
    Fl::wait(0.1);
  }


  void                Statusbar::show()
  {
//cerr << "1\n";
    bar->set_perc(0);     // 0 percent at startup
//cerr << "2\n";
    bar->set_max(100.0);  // max 100.0 percent
//cerr << "3\n";
    win->show();
//cerr << "4\n";
    Fl::wait(0.1);
//cerr << "5\n";
  }


  void                Statusbar::hide()
  {
    bar->set_perc(100);
    win->hide();
    Fl::wait(0.1);
  }

} // end of namespace

// end of file


