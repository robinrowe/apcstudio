// $Id: paste.h,v 1.1 2001/12/09 22:40:01 martinhenne Exp $

// (C) 2001 Martin Henne
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.


#ifndef _paste_h
#define _paste_h

#include <vector>
#include <globals.h>
#include <debug.h>
#include <string>

namespace APC
{
using namespace std;
typedef vector<uchar>::iterator iterator;

  class Paste
  {
  public:
    static void perform(vector<uchar>& vec,
                        const vector<uchar>& clip,
                        iterator pos);
  private:
    Paste();
  };


}

#endif