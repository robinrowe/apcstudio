// $Id: raisebutton.h,v 1.1 2001/12/16 14:21:39 martinhenne Exp $
//
// Raisebutton-functionality by Francesco Bradascio
//
// Button header file for the Fast Light Tool Kit (FLTK).
//
// Copyright 1998-2001 by Bill Spitzak and others.
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Library General Public
// License as published by the Free Software Foundation; either
// version 2 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Library General Public License for more details.
//
// You should have received a copy of the GNU Library General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
// USA.
//
// Please report all bugs and problems to "fltk-bugs@fltk.org".
//

#ifndef _raisebutton_h
#define _raisebutton_h

#include <FL/Fl_Widget.H>

// values for type()
#define FL_TOGGLE_BUTTON	1
#define FL_RADIO_BUTTON		(FL_RESERVED_TYPE+2)
#define FL_HIDDEN_BUTTON	3 // for Forms compatability

class Raisebutton : public Fl_Widget {

  int shortcut_;
  char value_;
  char oldval;
  uchar down_box_;

private:
  uchar hilighted_;
  uchar hilighted_box_;
  uchar hilighted_color_;
  uchar unhilighted_box_;
  uchar unhilighted_color_;

  Fl_Boxtype unhilighted_box() const {return (Fl_Boxtype)unhilighted_box_;}
  void unhilighted_box(Fl_Boxtype b) {unhilighted_box_ = b;}
  Fl_Color unhilighted_color() const {return (Fl_Color)unhilighted_color_;}
  void unhilighted_color(uchar c) {unhilighted_color_ = c;}

protected:

  virtual FL_EXPORT void draw();

public:

  virtual FL_EXPORT int handle(int);
  FL_EXPORT Raisebutton(int,int,int,int,const char * = 0);
  FL_EXPORT int value(int);
  char value() const {return value_;}
  int set() {return value(1);}
  int clear() {return value(0);}
  FL_EXPORT void setonly(); // this should only be called on FL_RADIO_BUTTONs
  int shortcut() const {return shortcut_;}
  void shortcut(int s) {shortcut_ = s;}
  Fl_Boxtype down_box() const {return (Fl_Boxtype)down_box_;}
  void down_box(Fl_Boxtype b) {down_box_ = b;}

  // back compatability:
#if 0
  void shortcut(const char *s) {shortcut(fl_old_shortcut(s));}
#endif
  Fl_Color down_color() const {return selection_color();}
  void down_color(uchar c) {selection_color(c);}

  void hilighted(uchar onoff);
  Fl_Boxtype hilighted_box() const {return (Fl_Boxtype)hilighted_box_;}
  void hilighted_box(Fl_Boxtype b) {hilighted_box_ = b;}
  Fl_Color hilighted_color() const {return (Fl_Color)hilighted_color_;}
  void hilighted_color(uchar c) {hilighted_color_ = c;}
};

#endif
