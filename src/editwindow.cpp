// $Id: editwindow.cpp,v 1.124 2002/02/21 22:47:26 martinhenne Exp $

// (C) 2001 Martin Henne
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

// This class combines a Fl_Double_Window and a Apc_fl_waveedit widget
// with a Wavefile and provides several editing options. It is the actual
// main window of aPcStudio. Maybe there will never be another main
// windows, wich was planned first, but just another mainframe-class.

#include <unistd.h>
#include <editwindow.h>
#include <help.h>
#include <iostream>
#include <string>
#include <vector>

#ifndef WIN32
#include <sys/socket.h>
#endif

#include <wavefile.h>
#include <apc_fl_waveedit.h>
#include <passtwo.h>
#include <popups.h>
#include <sleeper.h>
#include <effects.h>
#include <lendian.h>
#include <overlay.h>
#include <clipboard.h>
#include <about.h>
#include <askformat.h>
#include <record.h>
#include <wavedraw.h>
#include <debug.h>
#include <windowwrapper.h>
#include <commandline.h>
#include <preferences.h>
#include <settings.h>
#include <busywin.h>
#include <guiheaders.h>
#include <streamcast.h>
#include <ampeffects.h>
#include <tipoftheday.h>
#include <wavespecs.h>
#include <smallspecs.h>
#include <asklength.h>
#include <askzoom.h>
#include <aboutwin.h>
#include <mixpaste.h>
#include <copy.h>
#include <reverb.h>
#include <undo.h>
#include <undooperation.h>
#include <frame2time.h>
#include <mute.h>
#include <askchannel.h>
#include <askreverb.h>
#include "globals.h"
using namespace std;


//----------------------------------------------------------------------------//

// pixmaps for buttons:

#include "pix/zoomin.xpm"
static Fl_Pixmap zoomin_flxpm(zoomin_xpm);

#include "pix/zoomall.xpm"
static Fl_Pixmap zoomall_flxpm(zoomall_xpm);

#include "pix/zoomout.xpm"
static Fl_Pixmap zoomout_flxpm(zoomout_xpm);

#include "pix/zoomsel.xpm"
static Fl_Pixmap zoomsel_flxpm(zoomsel_xpm);

#include "pix/cut.xpm"
static Fl_Pixmap cut_flxpm(cut_xpm);

#include "pix/copy.xpm"
static Fl_Pixmap copy_flxpm(copy_xpm);

#include "pix/paste.xpm"
static Fl_Pixmap paste_flxpm(paste_xpm);

#include "pix/selall.xpm"
static Fl_Pixmap selall_flxpm(selall_xpm);

#include "pix/back.xpm"
static Fl_Pixmap back_flxpm(back_xpm);

#include "pix/stop.xpm"
static Fl_Pixmap stop_flxpm(stop_xpm);

#include "pix/play.xpm"
static Fl_Pixmap play_flxpm(play_xpm);

#include "pix/pause.xpm"
static Fl_Pixmap pause_flxpm(pause_xpm);

#include "pix/forward.xpm"
static Fl_Pixmap forward_flxpm(forward_xpm);

#include "pix/record.xpm"
static Fl_Pixmap record_flxpm(record_xpm);

#include "pix/logo_128x128.xpm"
static Fl_Pixmap logo_flxpm(logo_128x128_xpm);

#include "pix/face_50.xpm"
static Fl_Pixmap face_flxpm(face_50_xpm);

#include "pix/new.xpm"
static Fl_Pixmap new_flxpm(new_xpm);

#include "pix/open.xpm"
static Fl_Pixmap open_flxpm(open_xpm);

#include "pix/save.xpm"
static Fl_Pixmap save_flxpm(save_xpm);

#include "pix/undo.xpm"
static Fl_Pixmap undo_flxpm(undo_xpm);

#include "pix/redo.xpm"
static Fl_Pixmap redo_flxpm(redo_xpm);

#include "pix/showall.xpm"
static Fl_Pixmap showall_flxpm(showall_xpm);

#include "pix/rasterswitch.xpm"
static Fl_Pixmap raster_flxpm(rasterswitch_xpm);

#include "pix/digitswitch.xpm"
static Fl_Pixmap digit_flxpm(digitswitch_xpm);

#include "pix/reverse.xpm"
static Fl_Pixmap reverse_flxpm(reverse_xpm);

#include "pix/amplitute.xpm"
static Fl_Pixmap amplitude_flxpm(amplitute_xpm);

#include "pix/mixer.xpm"
static Fl_Pixmap mixer_flxpm(mixer_xpm);

#include "pix/echo.xpm"
static Fl_Pixmap echo_flxpm(echo_xpm);

#include "pix/reverb.xpm"
static Fl_Pixmap reverb_flxpm(reverb_xpm);

/*
// small icon for taskbar and titlebar
#include "pix/apc_icon.xbm"
Pixmap Icon = XCreateBitmapFromData(fl_display,
                                    DefaultRootWindow(fl_display),
                                    apc_icon_bits,
                                    apc_icon_width,
                                    apc_icon_height);
*/




//----------------------------------------------------------------------------//

namespace APC
{
using namespace std;
  Clipboard Editwindow::theClipboard;

  Fl_Menu_Item Editwindow::menus[] =
  { // TODO: shortcuts for all frequently used functions
    {"&File",FL_ALT+'f',0,0,FL_SUBMENU},
    {"New ",0,reinterpret_cast<Fl_Callback*>(Editwindow::cb_fileNewMenu),0,0},
    {"&Open ",FL_CTRL+'o',(Fl_Callback*)Editwindow::cb_fileOpenMenu,0,0},
    {"Open raw ",0,(Fl_Callback*)Editwindow::cb_fileOpenRawMenu,0,FL_MENU_DIVIDER},
    {"Save ",FL_CTRL+'s',(Fl_Callback*)Editwindow::cb_fileSaveMenu,0,0},
    {"Save As... ",0,(Fl_Callback*)Editwindow::cb_fileSaveAsMenu,0,0},
  {"New Window ",0,0,0,FL_SUBMENU|FL_MENU_DIVIDER},
      {"Open File",0,reinterpret_cast<Fl_Callback*>(Editwindow::cb_fileNewWindowOpenMenu),0,0},
      {"Empty Window",0,reinterpret_cast<Fl_Callback*>(Editwindow::cb_fileNewWindowMenu),0,0},
      {0}, // end of 'new window' submenu
    {"Preferences ",0,(Fl_Callback*)Editwindow::cb_filePreferencesMenu,0,0},
    {"Close ",FL_ALT+'w',(Fl_Callback*)Editwindow::cb_fileCloseMenu,0,0},
    {"&Quit ",FL_ALT+'q',(Fl_Callback*)Editwindow::cb_fileQuitMenu,0,0},
    {0}, // end of file submenu
    {"Edit ",0,0,0,FL_SUBMENU},
    {"Undo ",FL_CTRL+'z',reinterpret_cast<Fl_Callback*>(Editwindow::cb_editUndoMenu),0,0},
    {"Redo ",0,reinterpret_cast<Fl_Callback*>(Editwindow::cb_editRedoMenu),0,0},
    {"Clear Undo Buffer ",0,reinterpret_cast<Fl_Callback*>(cb_editClearUndoMenu),0,FL_MENU_DIVIDER},
    {"Cut ",FL_CTRL+'x',(Fl_Callback*)Editwindow::cb_editCutMenu,0,0},
    {"Copy ",FL_CTRL+'c',(Fl_Callback*)Editwindow::cb_editCopyMenu,0,0},
    {"Paste ",FL_CTRL+'v',(Fl_Callback*)Editwindow::cb_editPasteMenu,0,0},
    {"Mix-Paste...",0,reinterpret_cast<Fl_Callback*>(Editwindow::cb_editMixpasteMenu),0,FL_MENU_DIVIDER},
    {"select all ",FL_CTRL+'a',(Fl_Callback*)Editwindow::cb_editSelectAllMenu,0,0},
    {"select nothing ",0,(Fl_Callback*)Editwindow::cb_editSelectNoneMenu,0,0},
  {"cursor = selection start  ",FL_ALT+FL_Home,(Fl_Callback*)Editwindow::cb_editCsr2SelStartMenu,0,0},
  {"cursor = selection end  ",FL_ALT+FL_End,(Fl_Callback*)Editwindow::cb_editCsr2SelEndMenu,0,FL_MENU_DIVIDER},
    {"digital silence  ",0,(Fl_Callback*)Editwindow::cb_editSilenceMenu,0,0},
    {0}, // end of edit submenu
    {"View",0,0,0,FL_SUBMENU},
    {"show whole wave ",0,(Fl_Callback*)Editwindow::cb_viewzoomAllMenu,0,0},
    {"zoom out ",FL_CTRL+'-',(Fl_Callback*)Editwindow::cb_viewzoomOutMenu,0,0},
    {"zoom in ",FL_CTRL+'+',(Fl_Callback*)Editwindow::cb_viewzoomInMenu,0,0},
    {"zoom to selection ",0,(Fl_Callback*)Editwindow::cb_viewzoomSelectMenu,0,0},
    {"set zoom factor... ",0,(Fl_Callback*)Editwindow::cb_viewzoomSetMenu,0,FL_MENU_DIVIDER},
    {"jump to ",0,0,0,FL_SUBMENU},
    {"start of file ",FL_Home,0,0,0},
    {"end of file ",FL_End,0,0,FL_MENU_DIVIDER},
    {"cursor position ",0,0,0,0},
    {"selection start ",0,0,0,0},
    {"selection end ",0,0,0,FL_MENU_DIVIDER},
    {"specify sampleframe... ",0,0,0,0},
    {"specify time... ",0,0,0,0},
    {0}, // end of jump to menu
    {0}, // end of view menu
    {"Effects",0,0,0,FL_SUBMENU},
    {"Reverse",0,(Fl_Callback*)Editwindow::cb_effectsReverseMenu,0,0},
    {"Fade",0,0,0,FL_SUBMENU},
    {"Fade Menu...",0,(Fl_Callback*)Editwindow::cb_effectsFreeFadeMenu,0,0},
    {"Fade In",0,(Fl_Callback*)Editwindow::cb_effectsFadeInMenu,0,0},
    {"Fade Out",0,(Fl_Callback*)Editwindow::cb_effectsFadeOutMenu,0,0},
    {0}, // end of fade submenu
    {"Noise Gate  ",0,(Fl_Callback*)Editwindow::cb_effectsNoiseGateMenu,0,0},
    {"Echo",0,(Fl_Callback*)Editwindow::cb_effectsEchoMenu,0,0},
    {"Reverb",0,reinterpret_cast<Fl_Callback*>(Editwindow::cb_effectsReverbMenu),0,0},
    {0}, // end of effects menu
    {"Controls",0,0,0,FL_SUBMENU},
    {"Play",0,0,0,0},
    {"Record  ",0,0,0,0},
    {"Stop",0,0,0,0},
    {"Pause",0,0,0,0},
    {0}, // end of controlls menu
    {"Help",0,0,0,FL_SUBMENU},
    {"Manual  ",FL_F+1,reinterpret_cast<Fl_Callback*>(Editwindow::cb_helpManualMenu),0,0}, // F1 = Help
    {"About  ",0,(Fl_Callback*)Editwindow::cb_helpAboutMenu,0,0},
    {0}, // end of help menu
    {0}  // end of all menus
  };


//----------------------------------------------------------------------------//


  //----------------------------------------------------------------- FILE
  Fl_Menu_Item* Editwindow::fileMenu                   = Editwindow::menus + 0;
  Fl_Menu_Item* Editwindow::fileNewMenu                = Editwindow::menus + 1;
  Fl_Menu_Item* Editwindow::fileOpenMenu               = Editwindow::menus + 2;
  Fl_Menu_Item* Editwindow::fileOpenRawMenu            = Editwindow::menus + 3;
  //-- divider
  Fl_Menu_Item* Editwindow::fileSaveMenu               = Editwindow::menus + 4;
  Fl_Menu_Item* Editwindow::fileSaveAsMenu             = Editwindow::menus + 5;
  Fl_Menu_Item* Editwindow::fileNewWindowMenu          = Editwindow::menus + 6;
  Fl_Menu_Item* Editwindow::fileNewWindowOpenMenu      = Editwindow::menus + 7;
  Fl_Menu_Item* Editwindow::fileNewWindowNewMenu       = Editwindow::menus + 8;
  //--------------------------------------------------------------- NEW WINDOW
  Fl_Menu_Item* Editwindow::filePreferencesMenu        = Editwindow::menus + 10;
  Fl_Menu_Item* Editwindow::fileCloseMenu              = Editwindow::menus + 11;
  Fl_Menu_Item* Editwindow::fileQuitMenu               = Editwindow::menus + 12;

  //----------------------------------------------------------------- EDIT
  Fl_Menu_Item* Editwindow::editMenu                   = Editwindow::menus + 14;
  Fl_Menu_Item* Editwindow::editUndoMenu               = Editwindow::menus + 15;
  Fl_Menu_Item* Editwindow::editRedoMenu               = Editwindow::menus + 16;
  Fl_Menu_Item* Editwindow::editClearUndoMenu          = Editwindow::menus + 17;
  //-- divider
  Fl_Menu_Item* Editwindow::editCutMenu                = Editwindow::menus + 18;
  Fl_Menu_Item* Editwindow::editCopyMenu               = Editwindow::menus + 19;
  Fl_Menu_Item* Editwindow::editPasteMenu              = Editwindow::menus + 20;
  Fl_Menu_Item* Editwindow::editMixpasteMenu           = Editwindow::menus + 21;
  //-- divider
  Fl_Menu_Item* Editwindow::editSelectAllMenu          = Editwindow::menus + 22;
  Fl_Menu_Item* Editwindow::editSelectNoneMenu         = Editwindow::menus + 23;
  Fl_Menu_Item* Editwindow::editCursorIsMarkStartMenu  = Editwindow::menus + 24;
  Fl_Menu_Item* Editwindow::editCursorIsMarkEndMenu    = Editwindow::menus + 25;
  Fl_Menu_Item* Editwindow::editSilenceMenu            = Editwindow::menus + 26;

  //----------------------------------------------------------------- VIEW
  Fl_Menu_Item* Editwindow::viewMenu                   = Editwindow::menus + 28;
  Fl_Menu_Item* Editwindow::viewZoomAllMenu            = Editwindow::menus + 29;
  Fl_Menu_Item* Editwindow::viewZoomOutMenu            = Editwindow::menus + 30;
  Fl_Menu_Item* Editwindow::viewZoomInMenu             = Editwindow::menus + 31;
  Fl_Menu_Item* Editwindow::viewZoomSelectionMenu      = Editwindow::menus + 32;
  Fl_Menu_Item* Editwindow::viewZoomFactorMenu         = Editwindow::menus + 33;
  //-- divider
  Fl_Menu_Item* Editwindow::viewJumpToMenu             = Editwindow::menus + 34;
  Fl_Menu_Item* Editwindow::viewJumpToStartMenu        = Editwindow::menus + 35;
  Fl_Menu_Item* Editwindow::viewJumpToEndMenu          = Editwindow::menus + 36;
  //-- divider
  Fl_Menu_Item* Editwindow::viewJumpToCursorMenu       = Editwindow::menus + 37;
  Fl_Menu_Item* Editwindow::viewJumpToMarkStartMenu    = Editwindow::menus + 38;
  Fl_Menu_Item* Editwindow::viewJumpToMarkEndMenu      = Editwindow::menus + 39;
  //-- divider
  Fl_Menu_Item* Editwindow::viewJumpToFrameMenu        = Editwindow::menus + 40;
  Fl_Menu_Item* Editwindow::viewJumpToTimeMenu         = Editwindow::menus + 41;

  //----------------------------------------------------------------- EFFECTS
  Fl_Menu_Item* Editwindow::effectsMenu                = Editwindow::menus + 44;
  Fl_Menu_Item* Editwindow::effectsReverseMenu         = Editwindow::menus + 45;
  Fl_Menu_Item* Editwindow::effectsFadeMenu            = Editwindow::menus + 46;
  Fl_Menu_Item* Editwindow::effectsFreeFadeMenu        = Editwindow::menus + 47;
  Fl_Menu_Item* Editwindow::effectsFadeInMenu          = Editwindow::menus + 48;
  Fl_Menu_Item* Editwindow::effectsFadeOutMenu         = Editwindow::menus + 49;
  //-- divider
  Fl_Menu_Item* Editwindow::effectsNoiseGateMenu       = Editwindow::menus + 51;
  Fl_Menu_Item* Editwindow::effectsEchoMenu            = Editwindow::menus + 52;
  Fl_Menu_Item* Editwindow::effectsReverbMenu          = Editwindow::menus + 53;

  //------------------------------------------------------------- CONTROLS
  Fl_Menu_Item* Editwindow::controlsMenu               = Editwindow::menus + 55;
  Fl_Menu_Item* Editwindow::controlsPlayMenu           = Editwindow::menus + 56;
  Fl_Menu_Item* Editwindow::controlsRecordMenu         = Editwindow::menus + 57;
  Fl_Menu_Item* Editwindow::controlsStopMenu           = Editwindow::menus + 58;
  Fl_Menu_Item* Editwindow::controlsPauseMenu          = Editwindow::menus + 59;

  //----------------------------------------------------------------- HELP
  Fl_Menu_Item* Editwindow::helpMenu                   = Editwindow::menus + 61;
  Fl_Menu_Item* Editwindow::helpManualMenu             = Editwindow::menus + 62;
  Fl_Menu_Item* Editwindow::helpAboutMenu              = Editwindow::menus + 63;











//----------------------------------------------------------------------------//


  Editwindow::Editwindow()
  {
    Debug::msg<string>("Editwindow: constructing\n",Globals::DBG_FUNCTION_TRACE);
    // setting menu labelsize
    for(unsigned long i = 0; i < 64; i++)
      menus[i].labelsize(12);

    followPlayCursor = Settings::FOLLOW_PLAY_CURSOR;
    maincolor        = Settings::MAIN_COLOR;
    darkercolor      = Settings::MAIN_DARKER_COLOR;
    fontcolor        = Settings::FONT_COLOR;
    cursorcolor      = Settings::CURSOR_COLOR;

      redraw_needed           = 0;
    common_constructing();
    Debug::msg<string>("Editwindow: constructed\n",Globals::DBG_FUNCTION_TRACE);
  }







//----------------------------------------------------------------------------//

  Editwindow::Editwindow(const Editwindow& other)
  {
    Debug::msg<string>("Editwindow: copy constructor\n",Globals::DBG_FUNCTION_TRACE);
    followPlayCursor = other.followPlayCursor;
    maincolor = other.maincolor;
    darkercolor = other.darkercolor;
    fontcolor = other.fontcolor;
    cursorcolor = other.cursorcolor;

    redraw_needed           = other.redraw_needed;
    common_constructing();
    Debug::msg<string>("Editwindow: leaving copy constructor\n",Globals::DBG_FUNCTION_TRACE);
  }












//----------------------------------------------------------------------------//

  void Editwindow::common_constructing()
  {
      wfile                   =  new Wavefile;
      windowLabel             = "(unnamed) - aPcStudio " + Globals::sVERSION + " (C) 2001 M. Henne";
      ew                      = new Overlay(700,430,windowLabel.c_str());
//      ew                      -> icon                ((char*)Icon);
      ew                      -> size_range          (700,430);
      ew                      -> color               (maincolor); // GREEN
      ew                      -> callback            ((Fl_Callback*)cb_catchclose);
      ew                      -> when                (FL_WHEN_NOT_CHANGED|FL_WHEN_ENTER_KEY_ALWAYS);
      myEffects               =  new Effects;
      ww                      =  new Windowwrapper;
        menubar               =  new Fl_Menu_Bar(0,0,700,25);
        menubar               -> menu(menus);
        menubar               -> labelsize(12);
        menubar               -> box(FL_FLAT_BOX);
        menubar               -> color(Settings::MAIN_COLOR);
          toolbar             =  new Fl_Box(0,25,700,25);
          toolbar             -> color(Settings::MAIN_COLOR);
          toolbar             -> box(FL_FLAT_BOX);
          toolbar_group       =  new Fl_Group(0,25,700,25);
          tools_group         =  new Fl_Group(0,25,695,25);
            new_tool          =  new Raisebutton(5,26,20,20);
            new_tool          -> color(Settings::MAIN_COLOR);
            new_tool          -> box(FL_FLAT_BOX);
            new_tool          -> hilighted_box(FL_UP_BOX);
            new_tool          -> callback(reinterpret_cast<Fl_Callback*>(cb_fileNewMenu));
            new_tool          -> tooltip("create new file in this window");
            new_flxpm.label(new_tool);

            open_tool          =  new Raisebutton(30,26,20,20);
            open_tool          -> color(Settings::MAIN_COLOR);
            open_tool          -> box(FL_FLAT_BOX);
            open_tool          -> hilighted_box(FL_UP_BOX);
            open_tool          -> callback(reinterpret_cast<Fl_Callback*>(cb_fileOpenMenu));
            open_tool          -> tooltip("open another file in this window");
            open_flxpm.label(open_tool);

            save_tool          =  new Raisebutton(55,26,20,20);
            save_tool          -> color(Settings::MAIN_COLOR);
            save_tool          -> box(FL_FLAT_BOX);
            save_tool          -> hilighted_box(FL_UP_BOX);
            save_tool          -> callback(reinterpret_cast<Fl_Callback*>(cb_fileSaveMenu));
            save_tool          -> tooltip("save file to disk");
            save_flxpm.label(save_tool);

            undo_tool          =  new Raisebutton(90,26,20,20);
            undo_tool          -> color(Settings::MAIN_COLOR);
            undo_tool          -> box(FL_FLAT_BOX);
            undo_tool          -> hilighted_box(FL_UP_BOX);
            undo_tool          -> callback(reinterpret_cast<Fl_Callback*>(cb_editUndoMenu));
            undo_tool          -> tooltip("undo last operation");
            undo_flxpm.label(undo_tool);

            redo_tool          =  new Raisebutton(115,26,20,20);
            redo_tool          -> color(Settings::MAIN_COLOR);
            redo_tool          -> box(FL_FLAT_BOX);
            redo_tool          -> hilighted_box(FL_UP_BOX);
            redo_tool          -> callback(reinterpret_cast<Fl_Callback*>(cb_editRedoMenu));
            redo_tool          -> tooltip("redo is not implemented yet");
            redo_flxpm.label(redo_tool);

            selall_tool          =  new Raisebutton(145,26,20,20);
            selall_tool          -> color(Settings::MAIN_COLOR);
            selall_tool          -> box(FL_FLAT_BOX);
            selall_tool          -> hilighted_box(FL_UP_BOX);
            selall_tool          -> callback(reinterpret_cast<Fl_Callback*>(cb_editSelectAllMenu));
            selall_tool          -> tooltip("select the whole wave");
            selall_flxpm.label(selall_tool);

            cut_tool          =  new Raisebutton(170,26,20,20);
            cut_tool          -> color(Settings::MAIN_COLOR);
            cut_tool          -> box(FL_FLAT_BOX);
            cut_tool          -> hilighted_box(FL_UP_BOX);
            cut_tool          -> callback(reinterpret_cast<Fl_Callback*>(cb_editCutMenu));
            cut_tool          -> tooltip("cut selection and copy to clipboard");
            cut_flxpm.label(cut_tool);

            copy_tool          =  new Raisebutton(195,26,20,20);
            copy_tool          -> color(Settings::MAIN_COLOR);
            copy_tool          -> box(FL_FLAT_BOX);
            copy_tool          -> hilighted_box(FL_UP_BOX);
            copy_tool          -> callback(reinterpret_cast<Fl_Callback*>(cb_editCopyMenu));
            copy_tool          -> tooltip("copy selection to clipboard");
            copy_flxpm.label(copy_tool);

            paste_tool          =  new Raisebutton(220,26,20,20);
            paste_tool          -> color(Settings::MAIN_COLOR);
            paste_tool          -> box(FL_FLAT_BOX);
            paste_tool          -> hilighted_box(FL_UP_BOX);
            paste_tool          -> callback(reinterpret_cast<Fl_Callback*>(cb_editPasteMenu));
            paste_tool          -> tooltip("insert clipboard at cursorposition");
            paste_flxpm.label(paste_tool);

            zoom_tool_box       =  new Fl_Box(260,26,40,20,"zoom:");
            zoom_tool_box      ->  labelsize(12);

            zoom_tool           =  new Fl_Button(300,26,50,20,"100");
            zoom_tool_label     =  string("100");
            zoom_tool          ->  label(zoom_tool_label.c_str());
            zoom_tool          ->  align(FL_ALIGN_LEFT|FL_ALIGN_INSIDE);
            zoom_tool          ->  labelsize(12);
            zoom_tool          ->  tooltip("choose zoomfactor and press ENTER [100-inf]");
            zoom_tool          ->  callback(reinterpret_cast<Fl_Callback*>(cb_zoom_tool));
            zoom_tool          ->  box(FL_DOWN_BOX);
            zoom_tool          ->  color(FL_WHITE);
            percent_symbol      =  new Fl_Box(350,26,20,20,"%");
            percent_symbol     ->  labelsize(12);
            percent_symbol     ->  color(Settings::MAIN_COLOR);
            percent_symbol     ->  box(FL_FLAT_BOX);

            zoomin_tool          =  new Raisebutton(375,26,20,20);
            zoomin_tool          -> color(Settings::MAIN_COLOR);
            zoomin_tool          -> box(FL_FLAT_BOX);
            zoomin_tool          -> hilighted_box(FL_UP_BOX);
            zoomin_tool          -> callback(reinterpret_cast<Fl_Callback*>(cb_zoomInButton));
            zoomin_tool          -> tooltip("zoom in");
            zoomin_flxpm.label(zoomin_tool);

            zoomout_tool          =  new Raisebutton(400,26,20,20);
            zoomout_tool          -> color(Settings::MAIN_COLOR);
            zoomout_tool          -> box(FL_FLAT_BOX);
            zoomout_tool          -> hilighted_box(FL_UP_BOX);
            zoomout_tool          -> callback(reinterpret_cast<Fl_Callback*>(cb_zoomOutButton));
            zoomout_tool          -> tooltip("zoom out");
            zoomout_flxpm.label(zoomout_tool);

            showall_tool          =  new Raisebutton(425,26,20,20);
            showall_tool          -> color(Settings::MAIN_COLOR);
            showall_tool          -> box(FL_FLAT_BOX);
            showall_tool          -> hilighted_box(FL_UP_BOX);
            showall_tool          -> callback(reinterpret_cast<Fl_Callback*>(cb_zoomResetButton));
            showall_tool          -> tooltip("display whole wave");
            showall_flxpm.label(showall_tool);

            zoomsel_tool          =  new Raisebutton(450,26,20,20);
            zoomsel_tool          -> color(Settings::MAIN_COLOR);
            zoomsel_tool          -> box(FL_FLAT_BOX);
            zoomsel_tool          -> hilighted_box(FL_UP_BOX);
            zoomsel_tool          -> callback(reinterpret_cast<Fl_Callback*>(cb_zoomSelectButton));
            zoomsel_tool          -> tooltip("zoom to selection");
            zoomsel_flxpm.label(zoomsel_tool);

            raster_tool          =  new Raisebutton(475,26,20,20);
            raster_tool          -> color(Settings::MAIN_COLOR);
            raster_tool          -> box(FL_FLAT_BOX);
            raster_tool          -> hilighted_box(FL_UP_BOX);
            raster_tool          -> callback(reinterpret_cast<Fl_Callback*>(cb_rasterswitch));
            raster_tool          -> tooltip("draw raster on/off");
            raster_flxpm.label(raster_tool);

            digit_tool          =  new Raisebutton(500,26,20,20);
            digit_tool          -> color(Settings::MAIN_COLOR);
            digit_tool          -> box(FL_FLAT_BOX);
            digit_tool          -> hilighted_box(FL_UP_BOX);
            digit_tool          -> callback(reinterpret_cast<Fl_Callback*>(cb_digitswitch));
            digit_tool          -> tooltip("label the vertical rasterlines with times (on/off)");
            digit_flxpm.label(digit_tool);

          tools_group          ->  end();
        tool_resizer            =  new Fl_Group(695,25,5,25);
        tool_resizer           ->  end();
        toolbar_group          ->  end();
        toolbar_group          ->  resizable(tool_resizer);

        upperGroup            =  new Fl_Group        (0,50,700,230);
          we                  =  new Wavedraw(0,50,700,230);

        upperGroup            -> end();
        lowerGroup            =  new Fl_Group        (0,280,700,150);
          scrollBar           =  new Fl_Scrollbar    (0,280,700,20,"");
          scrollBar           -> type                (FL_HORIZONTAL);
          scrollBar           -> user_data           (static_cast<void*>(we));
          scrollBar           -> callback            ((Fl_Callback*)cb_scrollBar);
          scrollBar           -> color               (Settings::MAIN_DARKER_COLOR);
          lowestBox           =  new Fl_Box          (0,300,700,150);
          lowestBox           -> box                 (FL_DOWN_BOX);
          lowestBox           -> color               (maincolor);
          lowest1Group        =  new Fl_Group        (0,300,700,70);
            zoomGroup         =  new Fl_Group        (0,300,70,70);
              logo            =  new Fl_Button       (10,310,50,50);
              logo            -> box(FL_FLAT_BOX);
              face_flxpm.label(logo);
            zoomGroup         -> end();
            editGroup         =  new Fl_Group        (70,300,450,70);

              editBox         =  new Fl_Box          (70,310,290,50);
              editBox         -> box(FL_DOWN_BOX);
              editBox         -> color(Settings::MAIN_DARKER_COLOR);

              reverse_btn     =  new Fl_Button       (80,320,30,30);
              reverse_btn     -> color               (Settings::BUTTON_COLOR);
              reverse_btn     -> callback            (reinterpret_cast<Fl_Callback*>(cb_reverse_btn));
              reverse_btn     -> tooltip             ("reverse selected wavedata");
              reverse_flxpm.label(reverse_btn);

              amplitude_btn     =  new Fl_Button       (120,320,30,30);
              amplitude_btn     -> color               (Settings::BUTTON_COLOR);
              amplitude_btn     -> callback            (reinterpret_cast<Fl_Callback*>(cb_amplitude_btn));
              amplitude_btn     -> tooltip             ("amplitude based effects\n- fade in, fade out\n- amplify\n- noise gate");
              amplitude_flxpm.label(amplitude_btn);

              echo_btn     =  new Fl_Button            (160,320,30,30);
              echo_btn     -> color                    (Settings::BUTTON_COLOR);
              echo_btn     -> callback                 (reinterpret_cast<Fl_Callback*>(cb_echo_btn));
              echo_btn     -> tooltip                  ("echo");
              echo_flxpm.label(echo_btn);

              reverb_btn     =  new Fl_Button          (200,320,30,30);
              reverb_btn     -> color                  (Settings::BUTTON_COLOR);
              reverb_btn     -> callback               (reinterpret_cast<Fl_Callback*>(cb_reverb_btn));
              reverb_btn     -> tooltip                ("reverb");
              reverb_flxpm.label(reverb_btn);

              editBox2        =  new Fl_Box            (370,310,50,50);
              editBox2        -> box(FL_DOWN_BOX);
              editBox2        -> color(Settings::MAIN_DARKER_COLOR);

              mixer_btn       =  new Fl_Button      (380,320,30,30);
              mixer_btn       -> color              (Settings::BUTTON_COLOR);
              mixer_btn       -> callback           (reinterpret_cast<Fl_Callback*>(cb_mixer_btn));
              mixer_btn       -> tooltip            ("open mixer");
              mixer_flxpm.label(mixer_btn);

              editResizeGroup   =  new Fl_Group     (510,300,10,70);
            editGroup         -> end();
            editGroup         -> resizable(editResizeGroup);

            playGroup         =  new Fl_Group        (510,300,190,70);
            playBox           =  new Fl_Box           (520,310,170,50);
            playBox           -> box(FL_DOWN_BOX);
            playBox           -> color(Settings::MAIN_DARKER_COLOR);
              stopButton      =  new Raisebutton       (530,320,30,30);
              stopButton      -> tooltip             ("stop playing");
              stop_flxpm.label(stopButton);
              stopButton      -> callback            ((Fl_Callback*)cb_stopButton);
              playButton      =  new Raisebutton       (570,320,30,30);
              playButton      -> tooltip             ("left click - play selection\nCTRL + left click - play unselected,\nSHIFT + left click - play from cursor to end");
              play_flxpm.label(playButton);
              playButton      -> callback           ((Fl_Callback*)cb_playButton);
              recordButton    =  new Raisebutton       (610,320,30,30);
              recordButton    -> tooltip             ("sample to selection");
              record_flxpm.label(recordButton);
              recordButton    -> user_data           ((void*)(we));
              recordButton    -> callback            ((Fl_Callback*)Editwindow::cb_recordButton);
              pauseButton     =  new Raisebutton       (650,320,30,30);
              pauseButton     -> tooltip             ("pause");
              pause_flxpm.label(pauseButton);
              pauseButton     -> callback            ((Fl_Callback*)cb_pauseButton);
            playGroup         -> end();
          lowest1Group        -> resizable           (editGroup);
          lowest1Group        -> end();
          lowest2Group        =  new Fl_Group        (0,370,700,40);
            text1Group        =  new Fl_Group        (0,370,400,40);
              textBox_status  =  new Fl_Box          (0,370,150,20,"status:");
              textBox_status  -> labelsize           (14);
              textBox_status  -> labelfont           (1); // helvetica bold
              textBox_cursorposition =  new Fl_Box   (200,370,250,20,"cursor:");
              textBox_cursorposition -> labelsize    (14);
              textBox_cursorposition -> labelfont    (1);
            text1Group        -> end();
            resize2Group      =  new Fl_Group        (400,370,100,40);
            resize2Group      -> end();
            text2Group        =  new Fl_Group        (500,370,200,40);
              textBox_start   =  new Fl_Box          (500,370,65,20,"start:");
              textBox_start   -> labelsize           (14);
              textBox_start   -> labelfont           (1);
              textBox_end     =  new Fl_Box          (565,370,65,20,"end:");
              textBox_end     -> labelsize           (14);
              textBox_end     -> labelfont           (1);
              textBox_length  =  new Fl_Box          (630,370,70,20,"length:");
              textBox_length  -> labelsize           (14);
              textBox_length  -> labelfont           (1);
            text2Group        -> end();
          lowest2Group        -> resizable           (resize2Group);
          lowest2Group        -> end();
          lowest3Group        =  new Fl_Group        (0,390,700,40);
            statusGroup       =  new Fl_Group        (0,390,400,40);
              statusBox_status=  new Fl_Box          (0,390,250,20,"(stopped)");
              statusBox_status-> labelsize           (12);
              statusBox_status-> box                 (FL_DOWN_BOX);
              statusBox_status-> color               (Settings::INFOBOX_COLOR);
              statusBox_status-> labelcolor          (Settings::INFOTEXT_COLOR);
              statusBox_cpos  =  new Fl_Box          (250,390,150,20,"00:00.000");
              statusBox_cpos  -> labelsize           (12);
              statusBox_cpos  -> box                 (FL_DOWN_BOX);
              statusBox_cpos  -> color               (Settings::INFOBOX_COLOR);
              statusBox_cpos  -> labelcolor          (Settings::INFOTEXT_COLOR);
              statusBox_size  =  new Fl_Box          (0,410,75,20,"0 KB");
              statusBox_size  -> labelsize           (12);
              statusBox_size  -> box                 (FL_DOWN_BOX);
              statusBox_size  -> color               (Settings::INFOBOX_COLOR);
              statusBox_size  -> labelcolor          (Settings::INFOTEXT_COLOR);
              statusBox_freq  =  new Fl_Box          (75,410,75,20,"00.0 KHz");
              statusBox_freq  -> labelsize           (12);
              statusBox_freq  -> box                 (FL_DOWN_BOX);
              statusBox_freq  -> color               (Settings::INFOBOX_COLOR);
              statusBox_freq  -> labelcolor          (Settings::INFOTEXT_COLOR);
              statusBox_rate  =  new Fl_Box          (150,410,75,20,"0 Bit");
              statusBox_rate  -> labelsize           (12);
              statusBox_rate  -> box                 (FL_DOWN_BOX);
              statusBox_rate  -> color               (Settings::INFOBOX_COLOR);
              statusBox_rate  -> labelcolor          (Settings::INFOTEXT_COLOR);
              statusBox_chan  =  new Fl_Box          (225,410,75,20,"(m/s)");
              statusBox_chan  -> labelsize           (12);
              statusBox_chan  -> box                 (FL_DOWN_BOX);
              statusBox_chan  -> color               (Settings::INFOBOX_COLOR);
              statusBox_chan  -> labelcolor          (Settings::INFOTEXT_COLOR);
              statusBox_length=  new Fl_Box          (300,410,100,20,"00:00.000");
              statusBox_length-> labelsize           (12);
              statusBox_length-> box                 (FL_DOWN_BOX);
              statusBox_length-> color               (Settings::INFOBOX_COLOR);
              statusBox_length-> labelcolor          (Settings::INFOTEXT_COLOR);
            statusGroup       -> end();
            resize3Group      =  new Fl_Group        (400,370,100,40);
              textBox_view    =  new Fl_Box          (400,390,100,20,"view:");
              textBox_view    -> labelsize           (14);
              textBox_view    -> labelfont           (1);
              textBox_view    -> align               (FL_ALIGN_RIGHT|FL_ALIGN_INSIDE);
              textBox_selection = new Fl_Box         (400,410,100,20,"selection:");
              textBox_selection-> labelsize          (14);
              textBox_selection-> labelfont          (1);
              textBox_selection-> align              (FL_ALIGN_RIGHT|FL_ALIGN_INSIDE);
            resize3Group      -> end();
            timesGroup        =  new Fl_Group        (500,390,200,40);
              drawStart       =  new Fl_Box          (500,390,65,20,"00:00.000");
              drawStart       -> labelsize           (12);
              drawStart       -> box                 (FL_DOWN_BOX);
              drawStart       -> color               (Settings::INFOBOX_COLOR);
              drawStart       -> labelcolor          (Settings::INFOTEXT_COLOR);
              drawEnd         =  new Fl_Box          (565,390,65,20,"00:00.000");
              drawEnd         -> labelsize           (12);
              drawEnd         -> box                 (FL_DOWN_BOX);
              drawEnd         -> color               (Settings::INFOBOX_COLOR);
              drawEnd         -> labelcolor          (Settings::INFOTEXT_COLOR);
              drawLength      =  new Fl_Box          (630,390,70,20,"00:00.000");
              drawLength      -> labelsize           (12);
              drawLength      -> box                 (FL_DOWN_BOX);
              drawLength      -> color               (Settings::INFOBOX_COLOR);
              drawLength      -> labelcolor          (Settings::INFOTEXT_COLOR);
              markStart       =  new Fl_Box          (500,410,65,20,"00:00.000");
              markStart       -> labelsize           (12);
              markStart       -> box                 (FL_DOWN_BOX);
              markStart       -> color               (Settings::INFOBOX_COLOR);
              markStart       -> labelcolor          (Settings::INFOTEXT_COLOR);
              markEnd         =  new Fl_Box          (565,410,65,20,"00:00.000");
              markEnd         -> labelsize           (12);
              markEnd         -> box                 (FL_DOWN_BOX);
              markEnd         -> color               (Settings::INFOBOX_COLOR);
              markEnd         -> labelcolor          (Settings::INFOTEXT_COLOR);
              markLength      =  new Fl_Box          (630,410,70,20,"00:00.000");
              markLength      -> labelsize           (12);
              markLength      -> box                 (FL_DOWN_BOX);
              markLength      -> color               (Settings::INFOBOX_COLOR);
              markLength      -> labelcolor          (Settings::INFOTEXT_COLOR);
            timesGroup        -> end();
          lowest3Group        -> resizable           (resize3Group);
          lowest3Group        -> end();
        lowerGroup            -> end();
      ew                      -> resizable           (upperGroup);
      ew                      -> end                 ();

    editRedoMenu -> deactivate();
    viewJumpToMenu -> deactivate();
    controlsMenu -> deactivate();
  }




//----------------------------------------------------------------------------//

  Editwindow::~Editwindow()
  {
    Debug::msg<string>("Editwindow: destructing\n",Globals::DBG_FUNCTION_TRACE);
    // 1.
    Debug::msg<string>("Editwindow: destructor 1\n",Globals::DBG_FUNCTION_TRACE);
    delete markLength; markLength = NULL;
    delete drawStart ; drawStart  = NULL;
    delete drawEnd   ; drawEnd    = NULL;
    delete drawLength; drawLength = NULL;
    delete markStart ; markStart  = NULL;
    delete markEnd   ; markEnd    = NULL;
    delete timesGroup; timesGroup = NULL;

    // 2.
    Debug::msg<string>("Editwindow: destructor 2\n",Globals::DBG_FUNCTION_TRACE);
    delete textBox_selection  ;textBox_selection   = NULL;
    delete textBox_view       ;textBox_view        = NULL;
    delete resize3Group       ;resize3Group        = NULL;

    // 3.
    Debug::msg<string>("Editwindow: destructor 3\n",Globals::DBG_FUNCTION_TRACE);
    delete statusBox_length   ;statusBox_length  = NULL;
    delete statusBox_chan     ;statusBox_chan    = NULL;
    delete statusBox_rate     ;statusBox_rate    = NULL;
    delete statusBox_freq     ;statusBox_freq    = NULL;
    delete statusBox_size     ;statusBox_size    = NULL;
    delete statusBox_cpos     ;statusBox_cpos    = NULL;
    delete statusBox_status   ;statusBox_status  = NULL;
    delete statusGroup        ;statusGroup       = NULL;

    // 4.
    Debug::msg<string>("Editwindow: destructor 4\n",Globals::DBG_FUNCTION_TRACE);
    delete lowest3Group       ; lowest3Group     = NULL;

    // 5.
    Debug::msg<string>("Editwindow: destructor 5\n",Globals::DBG_FUNCTION_TRACE);
    delete textBox_length     ; textBox_length   = NULL;
    delete textBox_end        ; textBox_end      = NULL;
    delete textBox_start      ; textBox_start    = NULL;
    delete text2Group         ; text2Group       = NULL;

    // 6.
    Debug::msg<string>("Editwindow: destructor 6\n",Globals::DBG_FUNCTION_TRACE);
    delete resize2Group       ;resize2Group    = NULL;

    // 7.
    Debug::msg<string>("Editwindow: destructor 7\n",Globals::DBG_FUNCTION_TRACE);
    delete textBox_cursorposition ; textBox_cursorposition  = NULL;
    delete textBox_status         ; textBox_status          = NULL;
    delete text1Group             ; text1Group              = NULL;
    delete lowest2Group           ; lowest2Group            = NULL;

    // 8.
    Debug::msg<string>("Editwindow: destructor 8\n",Globals::DBG_FUNCTION_TRACE);
    delete pauseButton            ; pauseButton         = NULL;
    delete recordButton           ; recordButton        = NULL;
    delete playButton             ; playButton          = NULL;
    delete stopButton             ; stopButton          = NULL;
    delete playGroup              ; playGroup           = NULL;

    // 9.
    Debug::msg<string>("Editwindow: destructor 9\n",Globals::DBG_FUNCTION_TRACE);
    delete reverb_btn             ; reverb_btn          = NULL;
    delete echo_btn               ; echo_btn            = NULL;
    delete amplitude_btn          ; amplitude_btn       = NULL;
    delete reverse_btn            ; reverse_btn         = NULL;

    // 10.
    Debug::msg<string>("Editwindow: destructor 10\n",Globals::DBG_FUNCTION_TRACE);
    delete editResizeGroup        ; editResizeGroup    = NULL;
    delete mixer_btn              ; mixer_btn          = NULL;
    delete editBox2               ; editBox2           = NULL;
    delete editBox                ; editBox            = NULL;
    delete editGroup              ; editGroup          = NULL;

    // 11.
    Debug::msg<string>("Editwindow: destructor 11\n",Globals::DBG_FUNCTION_TRACE);
    delete zoomGroup              ; zoomGroup         = NULL;

    // 12.
    Debug::msg<string>("Editwindow: destructor 12\n",Globals::DBG_FUNCTION_TRACE);
    delete lowest1Group           ; lowest1Group    = NULL;

    // 13.
    Debug::msg<string>("Editwindow: destructor 13\n",Globals::DBG_FUNCTION_TRACE);
    delete lowestBox              ; lowestBox        = NULL;
    delete scrollBar              ; scrollBar        = NULL;
    delete lowerGroup             ; lowerGroup       = NULL;

    // 14.
    Debug::msg<string>("Editwindow: destructor 14\n",Globals::DBG_FUNCTION_TRACE);
    delete we                     ; we             = NULL;
    delete upperGroup             ; upperGroup     = NULL;

    // 15.
    Debug::msg<string>("Editwindow: destructor 15\n",Globals::DBG_FUNCTION_TRACE);
    delete digit_tool             ; digit_tool     = NULL;
    delete raster_tool            ; raster_tool    = NULL;
    delete zoomsel_tool           ; zoomsel_tool   = NULL;
    delete showall_tool           ; showall_tool   = NULL;
    delete zoomout_tool           ; zoomout_tool   = NULL;
    delete zoomin_tool            ; zoomin_tool    = NULL;
    delete percent_symbol         ; percent_symbol = NULL;
    delete zoom_tool              ; zoom_tool      = NULL;
    delete zoom_tool_box          ; zoom_tool_box  = NULL;
    delete paste_tool             ; paste_tool     = NULL;
    delete copy_tool              ; copy_tool      = NULL;
    delete cut_tool               ; cut_tool       = NULL;
    delete selall_tool            ; selall_tool    = NULL;
    delete redo_tool              ; redo_tool      = NULL;
    delete undo_tool              ; undo_tool      = NULL;
    delete save_tool              ; save_tool      = NULL;
    delete open_tool              ; open_tool      = NULL;
    delete new_tool               ; new_tool       = NULL;
    delete toolbar_group          ; toolbar_group  = NULL;
    delete tool_resizer           ; tool_resizer   = NULL;
    delete tools_group            ; tools_group    = NULL;
    delete toolbar                ; toolbar        = NULL;
    delete menubar                ; menubar        = NULL;

    // 16.
    Debug::msg<string>("Editwindow: destructor 16\n",Globals::DBG_FUNCTION_TRACE);
    Debug::msg<string>("Editwindow: destructor 16.1\n",Globals::DBG_FUNCTION_TRACE);
    delete myEffects              ; myEffects        = NULL;
    Debug::msg<string>("Editwindow: destructor 16.2\n",Globals::DBG_FUNCTION_TRACE);
    delete ew                     ; ew               = NULL;
    Debug::msg<string>("Editwindow: destructor 16.3\n",Globals::DBG_FUNCTION_TRACE);
    delete wfile                  ; wfile            = NULL;

    Debug::msg<string>("Editwindow: destructed\n",Globals::DBG_FUNCTION_TRACE);
  }












//----------------------------------------------------------------------------//


  Editwindow& Editwindow::operator=(const Editwindow& arg)
  {
    std::cerr << "Illegal use of Editwindow's assignment-operator, expect problems...\n";
    return *this;
    //exit(1);
  }









//----------------------------------------------------------------------------//

  void Editwindow::wwindex(const ulong& arg, Windowwrapper* pww)
  {
    ww_index = arg;
    ww = pww;
  }

//----------------------------------------------------------------------------//
  void Editwindow::set_format(const int& _frequency,
                              const int& _channels,
                              const int& _bitRate)
  {
    // first: some error checking
    if(_bitRate != 8 && _bitRate != 16) {
      cerr << "aPcStudio: unsupported bitRate: " << _bitRate << endl;
      Popups::error("Bitrate unsupported!");
      exit(1);
    }

    if(_channels != 1 && _channels != 2) {
      cerr << "aPcStudio: unsupported channels: " << _channels << endl;
      Popups::error("Only mono and stereo are supported! Expect problems.");
    }

    if(_frequency > 48000 || frequency < 1) {
      cerr << "aPcStudio: very strange frequency: " << _frequency << endl;
      Popups::error("Very strange frequency! Expect problems.");
    }

    // now apply settings to the Editwindow
    this->frequency = _frequency;
    this->channels  = _channels;
    this->bitRate   = _bitRate;

    // adjust info boxes, that inform the user
    this->freq_label = stream_cast<string>(_frequency/1000.0);
    this->rate_label = stream_cast<string>(_bitRate);
    if(_channels == 1) chan_label = string("mono");
    if(_channels == 2) chan_label = string("stereo");
    if(_channels!=1 && _channels!=2) chan_label = ("unsupported");

    this->statusBox_freq->label(freq_label.c_str());
    this->statusBox_rate->label(rate_label.c_str());
    this->statusBox_chan->label(chan_label.c_str());

    this->statusBox_freq->redraw();
    this->statusBox_rate->redraw();
    this->statusBox_chan->redraw();

    // now inform the related objects about the changes:

    // inform Wavefile* wfile (this informs Fmt_chunk):
    this->wfile->setbitRate(_bitRate);
    this->wfile->setchannels(_channels);
    this->wfile->setfrequency(_frequency);

    // inform Wavedraw* we;
    this->we->set_format(_frequency, _channels, _bitRate);
  }





//----------------------------------------------------------------------------//

  void Editwindow::cb_newfile_cancel  (Raisebutton* o, void* v)
  {
    std::cerr << "::newfile(): aborted!\n";
    exit(1);
  }







//----------------------------------------------------------------------------//

  void Editwindow::cb_newfile_ok      (Raisebutton* o, void* v)
  {
    ((Editwindow*)(o->window()->user_data()))->cb_newfile_ok_i(o,v);
  }

  inline void Editwindow::cb_newfile_ok_i    (Raisebutton* o, void* v)
  {
    cerr << "Editwindow::cb_newfile_ok_i(): fixme\n";
  }



//----------------------------------------------------------------------------//

  bool Editwindow::newfile            ()
  {
    Debug::msg<string>("Editwindow::newfile(): called\n",Globals::DBG_FUNCTION_TRACE);

    // get wavefile specifications via requester
    Smallspecs* sms = new Smallspecs;
    sms->bitRate = 0;
    sms->channels = 0;
    sms->frequency = 0;
    Wavespecs ws(sms);
    ws.show();
    bool specs_done = ws.ask(); // false, if 'cancel' pressed

    if(specs_done) // 'ok' pressed, then apply settings to the whole application
    {
      wfile->setSamplerate(sms->frequency);
      wfile->setChannels(sms->channels);
      wfile->setBitrate(sms->bitRate);
      vector<unsigned char>& vec = wfile->getWave          ();
      this->channels             = wfile->getChannels      ();
      this->frequency            = wfile->getSamplerate    ();
      this->bitRate              = wfile->getBitrate       ();
      this->frameSize            = bitRate*channels/8;

      Asklength al;
      al.show();
      int length_in_secs = al.get_seconds();

      vec.resize(frameSize*frequency*length_in_secs); // one minute
      this->waveSize             = vec.size();

      // make shure, that we have silence!
      if(bitRate==8)
      {
        for(unsigned long i=0; i<waveSize; ++i)
          vec[i] = 0x80;
      }
      if(bitRate==16)
      {
        for(ulong i=0; i<waveSize; ++i)
          vec[i] = 0;
      }

      Data_chunk& dc             = wfile->getdata          ();
      dc.setchunkSize            (waveSize);
      we->setdata(wfile);
      this->init();
      this->we->setmarkStart(0);
      this->we->setmarkEnd(this->waveSize/this->frameSize);
      this->we->setdrawStart(0);
      this->we->setdrawEnd(waveSize/frameSize);
      this->we->setvisualFrames(waveSize/frameSize);
      this->we->setzoomFactor(100);
      this->adjustScrollbar();
      this->data_changed();
      this->redraw_needed |= REDR_ALL;
      this->doRedraw();
      Debug::msg<string>("Editwindow::newfile(): leaving\n",Globals::DBG_FUNCTION_TRACE);
      return true;
    }
    Debug::msg<string>("Editwindow::newfile(): leaving\n",Globals::DBG_FUNCTION_TRACE);
    return false;
  }









//----------------------------------------------------------------------------//

  void Editwindow::readfile           (const string arg)
  {
    Debug::msg<string>("Editwindow::readfile(): called\n",Globals::DBG_FUNCTION_TRACE);
    this->relabelWindow(arg.c_str());  // windowtitel should show the filename
    wfile->read(arg);     // read wavefile. a status bar should be implemented sometimes
    we->setdata(wfile);   // pass wavefile to 'Wavedraw'-widget
    this->init();         // read common vals from chunks (samplerate, wavesize...)
    this->we->setmarkStart(0);
    this->we->setmarkEnd(this->waveSize/this->frameSize);
    Configfile::set("LAST_OPENED",arg);
    Debug::msg<string>("Editwindow::readfile(): leaving\n",Globals::DBG_FUNCTION_TRACE);
  }






//----------------------------------------------------------------------------//

  void Editwindow::show               ()
  {
    Debug::msg<string>("Editwindow::show(): called\n",Globals::DBG_FUNCTION_TRACE);
    Fl::visual(FL_DOUBLE|FL_INDEX);
    int endrange = (waveSize/frameSize);
    scrollBar->range(0,endrange);
    //ew->redraw_overlay(); // in case of overlay code
    //ew->show(Commandline::_argc, Commandline::_argv);
    ew->show();
    Debug::msg<string>("Editwindow::show(): showing tip of the day\n",Globals::DBG_FUNCTION_TRACE);
    TipOfTheDay tip;

    if(!Commandline::check_for("0,no-bother"))
      tip.show();

    while(tip.visible())
    {
      Fl::check();
      Fl::wait(0.1);
    }
    Debug::msg<string>("Editwindow::show(): leaving\n",Globals::DBG_FUNCTION_TRACE);
  }











//----------------------------------------------------------------------------//

  void Editwindow::init               ()
  {
    Debug::msg<string>("Editwindow::init(): called\n",Globals::DBG_FUNCTION_TRACE);
    Fmt_chunk  &fc           = wfile->getfmt();
    Data_chunk &dc           = wfile->getdata();
    vector<unsigned char> &w = dc.getwave();

    this->frequency         = fc.getdwSamplesPerSec();
    this->bitRate            = fc.getwBitsPerSample();
    this->channels           = fc.getwChannels();
    this->waveSize           = w.size();
    this->zoomFactor         = 100;
    this->frameSize          = (channels*bitRate)/8;

    freq_label               = stream_cast<string>(frequency/1000.0);
    freq_label              += " KHz";
    rate_label               = stream_cast<string>(bitRate);
    rate_label              += " Bit";
    chan_label               = "";
    switch (channels)
    {
      case 1 : chan_label    = "mono" ; break;
      case 2 : chan_label    = "stereo" ; break;
      case 4 : chan_label    = "quadro" ; break;

      default : chan_label   = stream_cast<string>(channels)+" chan.";
    }
    size_label               = stream_cast<string>(static_cast<int>(waveSize/1024));
    size_label              += " KB";
    status_label             = "stopped";

    statusBox_size           ->label(size_label.c_str());
    statusBox_rate           ->label(rate_label.c_str());
    statusBox_freq           ->label(freq_label.c_str());
    statusBox_chan           ->label(chan_label.c_str());
    statusBox_status         ->label(status_label.c_str());
    statusBox_length         ->label(length_label.c_str());

    playButton               ->user_data((void*)(this));
    pauseButton              ->user_data((void*)(this));
    stopButton               ->user_data((void*)(this));
/*    selectAllButton          ->user_data((void*)(this)); */

    we->setmarkStart(0);
    we->setmarkEnd(waveSize/frameSize);

    ew->user_data((void*)(this));
    we->set_user_data((void*)(this));
    player.myHandle = (void*)(we);
    recorder.init(this);
    recorder.recording = false;

    myEffects->ew = this;
    we->init(this);

    relabel(we->frame2time(waveSize/frameSize),"statusBox_length");
    this->adjustScrollbar();

    this->redraw_needed = REDR_ALL;
    this->doRedraw();
    Debug::msg<string>("Editwindow::init(): leaving\n",Globals::DBG_FUNCTION_TRACE);
  }









//----------------------------------------------------------------------------//

  void Editwindow::data_changed       ()
  {
    // TODO: consider calling ::init() from here.
    Data_chunk &dc              = wfile->getdata();
    frequency = wfile->getfrequency();
    bitRate   = wfile->getbitRate();
    channels  = wfile->getchannels();
    frameSize = (bitRate/8) * channels;
    vector<unsigned char>& data = dc.getwave();
    this->waveSize = data.size();
    size_label               = stream_cast<string>(static_cast<int>(waveSize/1024));
    size_label              += " KB";
    this->relabel(size_label,"statusBox_size");
    length_label             = frame2time()(waveSize/frameSize, frequency);
    this->relabel(length_label,"statusBox_length");
    return we->data_changed();
  }











//----------------------------------------------------------------------------//

  void Editwindow::apply_colors()
  {
    Debug::msg<string>("Editwindow::apply_colors(): called\n",Globals::DBG_FUNCTION_TRACE);
    ew              -> color               (Settings::MAIN_COLOR);
    menubar         -> color               (Settings::MAIN_COLOR);
    toolbar         -> color               (Settings::MAIN_COLOR);
    Debug::msg<string>("Editwindow::apply_colors(): tools\n",Globals::DBG_FUNCTION_TRACE);
    new_tool        -> color               (Settings::MAIN_COLOR);
    open_tool       -> color               (Settings::MAIN_COLOR);
    save_tool       -> color               (Settings::MAIN_COLOR);
    undo_tool       -> color               (Settings::MAIN_COLOR);
    redo_tool       -> color               (Settings::MAIN_COLOR);
    selall_tool     -> color               (Settings::MAIN_COLOR);
    cut_tool        -> color               (Settings::MAIN_COLOR);
    copy_tool       -> color               (Settings::MAIN_COLOR);
    paste_tool      -> color               (Settings::MAIN_COLOR);
    percent_symbol  -> color               (Settings::MAIN_COLOR);
    zoomin_tool     -> color               (Settings::MAIN_COLOR);
    zoomout_tool    -> color               (Settings::MAIN_COLOR);
    showall_tool    -> color               (Settings::MAIN_COLOR);
    zoomsel_tool    -> color               (Settings::MAIN_COLOR);
    raster_tool     -> color               (Settings::MAIN_COLOR);
    digit_tool      -> color               (Settings::MAIN_COLOR);
    Debug::msg<string>("Editwindow::apply_colors(): lower area\n",Globals::DBG_FUNCTION_TRACE);
    scrollBar       -> color               (Settings::MAIN_DARKER_COLOR);
    lowestBox       -> color               (Settings::MAIN_COLOR);
    reverse_btn     -> color               (Settings::BUTTON_COLOR);
    amplitude_btn   -> color               (Settings::BUTTON_COLOR);
    echo_btn        -> color               (Settings::BUTTON_COLOR);
    reverb_btn      -> color               (Settings::BUTTON_COLOR);
    Debug::msg<string>("Editwindow::apply_colors(): playgroup buttons\n",Globals::DBG_FUNCTION_TRACE);
    stopButton      -> color               (Settings::BUTTON_COLOR);
    playButton      -> color               (Settings::BUTTON_COLOR);
    recordButton    -> color               (Settings::BUTTON_COLOR);
    pauseButton     -> color               (Settings::BUTTON_COLOR);
    Debug::msg<string>("Editwindow::apply_colors(): text boxes\n",Globals::DBG_FUNCTION_TRACE);
    textBox_status  -> color               (Settings::MAIN_COLOR);
    textBox_cursorposition -> color        (Settings::MAIN_COLOR);
    textBox_start   -> color               (Settings::MAIN_COLOR);
    textBox_end     -> color               (Settings::MAIN_COLOR);
    textBox_length  -> color               (Settings::MAIN_COLOR);
    textBox_status  -> labelcolor          (Settings::FONT_COLOR);
    textBox_cursorposition -> labelcolor   (Settings::FONT_COLOR);
    textBox_start   -> labelcolor          (Settings::FONT_COLOR);
    textBox_end     -> labelcolor          (Settings::FONT_COLOR);
    textBox_length  -> labelcolor          (Settings::FONT_COLOR);
    statusBox_status-> color               (Settings::INFOBOX_COLOR);
    statusBox_status-> labelcolor          (Settings::INFOTEXT_COLOR);
    statusBox_cpos  -> color               (Settings::INFOBOX_COLOR);
    statusBox_cpos  -> labelcolor          (Settings::INFOTEXT_COLOR);
    statusBox_size  -> color               (Settings::INFOBOX_COLOR);
    statusBox_size  -> labelcolor          (Settings::INFOTEXT_COLOR);
    statusBox_freq  -> color               (Settings::INFOBOX_COLOR);
    statusBox_freq  -> labelcolor          (Settings::INFOTEXT_COLOR);
    statusBox_rate  -> color               (Settings::INFOBOX_COLOR);
    statusBox_rate  -> labelcolor          (Settings::INFOTEXT_COLOR);
    statusBox_chan  -> color               (Settings::INFOBOX_COLOR);
    statusBox_chan  -> labelcolor          (Settings::INFOTEXT_COLOR);
    statusBox_length-> color               (Settings::INFOBOX_COLOR);
    statusBox_length-> labelcolor          (Settings::INFOTEXT_COLOR);
    textBox_view    -> color               (Settings::MAIN_COLOR);
    textBox_selection->color               (Settings::MAIN_COLOR);
    textBox_view    -> labelcolor          (Settings::FONT_COLOR);
    textBox_selection->labelcolor          (Settings::FONT_COLOR);
    drawStart       -> color               (Settings::INFOBOX_COLOR);
    drawStart       -> labelcolor          (Settings::INFOTEXT_COLOR);
    drawEnd         -> color               (Settings::INFOBOX_COLOR);
    drawEnd         -> labelcolor          (Settings::INFOTEXT_COLOR);
    drawLength      -> color               (Settings::INFOBOX_COLOR);
    drawLength      -> labelcolor          (Settings::INFOTEXT_COLOR);
    markStart       -> color               (Settings::INFOBOX_COLOR);
    markStart       -> labelcolor          (Settings::INFOTEXT_COLOR);
    markEnd         -> color               (Settings::INFOBOX_COLOR);
    markEnd         -> labelcolor          (Settings::INFOTEXT_COLOR);
    markLength      -> color               (Settings::INFOBOX_COLOR);
    markLength      -> labelcolor          (Settings::INFOTEXT_COLOR);
    Debug::msg<string>("Editwindow::apply_colors(): ok, calling we->apply_colors()\n",Globals::DBG_FUNCTION_TRACE);
    we->apply_colors();
    ew->redraw();
    Debug::msg<string>("Editwindow::apply_colors(): leaving\n",Globals::DBG_FUNCTION_TRACE);
  }






//----------------------------------------------------------------------------//

  void Editwindow::relabelWindow      (const char* arg)
  {
    windowLabel = " - aPcStudio " + Globals::sVERSION + " - (C) 2001 Martin Henne";
    windowLabel = static_cast<string>(arg) + windowLabel;
    ew->label(windowLabel.c_str());
  }

  // relabel status boxes. string argument is name of box-object
  void Editwindow::relabel            (string s_val, string arg)
  {
    string tmp = "";
    if (arg == "drawStart")
    {
      drawStartLabel = s_val;
      drawStart->label(drawStartLabel.c_str());
      drawStart->redraw();
      return;
    }
    if (arg == "drawEnd")
    {
      drawEndLabel = s_val;
      drawEnd->label(drawEndLabel.c_str());
      drawEnd->redraw();
      return;
    }

    if (arg == "drawLength")
    {
      drawLengthLabel = s_val;
      drawLength->label(drawLengthLabel.c_str());
      drawLength->redraw();
      return;
    }

    if (arg == "markStart")
    {
      markStartLabel = s_val;
      markStart->label(markStartLabel.c_str());
      markStart->redraw();
      return;
    }

    if (arg == "markEnd")
    {
      markEndLabel = s_val;
      markEnd->label(markEndLabel.c_str());
      markEnd->redraw();
      return;
    }

    if (arg == "markLength")
    {
      markLengthLabel = s_val;
      markLength->label(markLengthLabel.c_str());
      markLength->redraw();
      return;
    }

    if (arg == "statusBox_cpos") // no redraw here! use doRedraw()!
    {
      cpos_label = s_val;
      statusBox_cpos->label(cpos_label.c_str());
      return;
    }

    if (arg == "statusBox_status")
    {
      status_label = s_val;
      statusBox_status->label(status_label.c_str());
      statusBox_status->redraw();
      return;
    }

    if (arg == "statusBox_length")
    {
      length_label = s_val;
      statusBox_length->label(length_label.c_str());
      statusBox_length->redraw();
      return;
    }

    if (arg == "statusBox_size")
    {
      size_label = s_val;
      statusBox_size->label(size_label.c_str());
      statusBox_size->redraw();
      return;
    }


    cerr << "Editwindow::relabel(): cannot relabel statusbox: "
         << arg << endl;
    return;

  }





//----------------------------------------------------------------------------//

  string Editwindow::getstatus()
  {
    string stat(static_cast<string>(this->statusBox_status->label()));
    return stat;
  }





//----------------------------------------------------------------------------//

  void Editwindow::setstatus (string newstatus)
  {
    return this->relabel(newstatus,"statusBox_status");
  }





//----------------------------------------------------------------------------//

  // set the slidersize of the scrollbar
  void Editwindow::adjustScrollbar                ()
  {
    unsigned long visualFrames = (we->getvisualFrames()); // how many frames are visible?

    /*
     *  description of arguments for value(...) (by B. Spitzak):
     *
     *  value(int position, int size, int top, int total)
     *
     *  -+- TOP
     *   |
     *   |
     *   |
     *  +-+ POSITION
     *  | |
     *  | |
     *  +-+ POSITION+SIZE
     *   |
     *   |
     *   |
     *   |
     *   |
     *  -+- TOP+TOTAL
     *
     */
    scrollBar->value(we->getdrawStart(),visualFrames,0,(waveSize/frameSize));
    scrollBar->linesize(we->getvisualFrames()/50); // stepwidth of arrows
    return;
  }









//----------------------------------------------------------------------------//

  // redraw, what needs to be redrawn. check headerfile for bitfield information
  void Editwindow::doRedraw                       ()
  {
    if((redraw_needed&REDR_CSRPOS)!=0)
    {
      statusBox_cpos->redraw();
    }

    if((redraw_needed&REDR_STATUS)!=0)
    {
      statusBox_status->redraw();
    }

    if((redraw_needed&REDR_WAVEFMT)!=0)
    {
      statusBox_size->redraw();
      statusBox_freq->redraw();
      statusBox_rate->redraw();
      statusBox_chan->redraw();
      statusBox_length->redraw();
    }

    if((redraw_needed&REDR_VIEWSTAT)!=0)
    {
      drawStartLabel  = we->frame2time(we->getdrawStart());
      drawEndLabel    = we->frame2time(we->getdrawEnd());
      drawLengthLabel = we->frame2time(we->getdrawEnd()-we->getdrawStart());
      this->relabel(drawStartLabel,"drawStart");
      this->relabel(drawEndLabel,"drawEnd");
      this->relabel(drawLengthLabel,"drawLength");
      drawStart->redraw();
      drawEnd->redraw();
      drawLength->redraw();
    }

    if((redraw_needed&REDR_SELSTAT)!=0)
    {
      markStartLabel   = we->frame2time(we->getmarkStart());
      markEndLabel     = we->frame2time(we->getmarkEnd());
      markLengthLabel  = we->frame2time(we->getmarkEnd()-we->getmarkStart());
      this->relabel(markStartLabel,"markStart");
      this->relabel(markEndLabel,"markEnd");
      this->relabel(markLengthLabel,"markLength");
      markStart->redraw();
      markEnd->redraw();
      markLength->redraw();
    }

    if((redraw_needed&REDR_WAVE)!=0)
    {
      this->we->doRedraw();
    }

    // clear bitfield and exit
    redraw_needed = 0;
    return;
  }







//----------------------------------------------------------------------------//

  // overlay code
  void Editwindow::draw_cursor()
  {
    ew->redraw_overlay();
    return;
  }









//----------------------------------------------------------------------------//

  // callbacks from here



  // catch the window-close-event
  inline void Editwindow::cb_catchclose_i         (Fl_Widget* o, void* v)
  {
    this->cb_stopButton_i(stopButton, NULL);
    this->cb_fileCloseMenu_i(o,v);
  }

  void Editwindow::cb_catchclose                  (Fl_Widget* o, void* v)
  {
    ((Editwindow*)(o->user_data()))->cb_catchclose_i(o,v);
  }



//----------------------------------------------------------------------------//

  inline void Editwindow::cb_zoomSelectButton_i   (Raisebutton* o, void* v)
  {
    // calculate new zoomFactor:
    double calcZoom =
      100 *
      static_cast<double>
      (((waveSize*1.0)/(1.0*frameSize))/(we->getmarkEnd()-(we->getmarkStart())));

    // what is the maximum zoomFactor?
    double maxZoom = 100.0 * ((waveSize*1.0)/(frameSize*1.0))/(ew->w());

    if(calcZoom > maxZoom) {
      calcZoom = maxZoom;
      std::cerr << "maxZoom: " << maxZoom << std::endl;
    }

    we->setzoomFactor(calcZoom);
    we->setdrawStart(we->getmarkStart());
    this->adjustScrollbar();
    zoom_tool_label = stream_cast<string>(static_cast<int>(we->getzoomFactor()));
    zoom_tool->label(zoom_tool_label.c_str());
    zoom_tool->redraw();
    redraw_needed |= REDR_ALL;
    this->doRedraw();
  }

  void Editwindow::cb_zoomSelectButton            (Raisebutton* o, void* v)
  {
    ((Editwindow*)(o->window()->user_data()))->cb_zoomSelectButton_i(o,v);
  }




//----------------------------------------------------------------------------//

  // menu bar callbacks

  inline void Editwindow::cb_fileNewMenu_i (Fl_Widget* o, void* v)
  {
    newfile();
      return;
  }

  void Editwindow::cb_fileNewMenu (Fl_Widget* o, void* v)
  {
    ((Editwindow*)(o->window()->user_data()))->cb_fileNewMenu_i(o,v);
  }




//----------------------------------------------------------------------------//

  inline void Editwindow::cb_fileOpenMenu_i       (Fl_Widget* o, void* v)
  {
    char* filename=fl_file_chooser("Open File","*.wav","");
    if (filename==NULL)
      return;
    string filename_s=static_cast<string>(filename);
    player.playing=false;
    try {
      this->readfile(filename_s);
      this->init();
      we->doRedraw();
      ew->redraw();
      statusBox_freq->redraw();
      need_filename = false;
    }
    catch(no_fileaccess) {
      Popups::error("Couldn't open File!");
    }
    catch(exception& e) {
      Popups::error(e.what());
    }
  }

  void Editwindow::cb_fileOpenMenu                (Fl_Widget* o, void* v)
  {
    ((Editwindow*)(o->window()->user_data()))->cb_fileOpenMenu_i(o,v);
  }







//----------------------------------------------------------------------------//

  void Editwindow::cb_fileOpenRawMenu_i           (Fl_Widget* o, void* v)
  {
    char* filename=fl_file_chooser("Open Raw File","*.*","");
    if(filename==NULL)
      return;
    string sfile = static_cast<string>(filename);
    unsigned long size = Textfile::getsize(sfile);
    Data_chunk& dc = wfile->getdata();
    dc.setchunkSize(size);
    vector<byte>& vec = (vector<byte>&) dc.getwave();
    vec.resize(size);
    dc.read_raw(sfile);

    // get wavefile specifications via requester
    Smallspecs* sms = new Smallspecs;
    sms->bitRate = 0;
    sms->channels = 0;
    sms->frequency = 0;
    Wavespecs ws(sms);
    ws.show();
    bool specs_done = false;

    while(!specs_done) {
      specs_done = ws.ask(); // false, if 'cancel' pressed
      if(!specs_done)
        Popups::error("Pleas specify wavesettings!");
    }

    if(specs_done) // 'ok' pressed, then apply settings to the whole application
    {
      wfile->setSamplerate(sms->frequency);
      wfile->setChannels(sms->channels);
      wfile->setBitrate(sms->bitRate);
      vector<unsigned char>& vec = wfile->getWave          ();
      this->channels             = wfile->getChannels      ();
      this->frequency           = wfile->getSamplerate    ();
      this->bitRate              = wfile->getBitrate       ();
      this->frameSize            = bitRate*channels/8;
      this->waveSize             = vec.size();

      Data_chunk& dc             = wfile->getdata          ();
      dc.setchunkSize            (waveSize);
      we->setdata(wfile);
      this->init();
      this->we->setmarkStart(0);
      this->we->setmarkEnd(this->waveSize/this->frameSize);
      this->we->setdrawStart(0);
      this->we->setdrawEnd(waveSize/frameSize);
      this->we->setvisualFrames(waveSize/frameSize);
      this->we->setzoomFactor(100);
      this->adjustScrollbar();
      this->data_changed();
      this->need_filename = true;
    }
    return;
  }



  void Editwindow::cb_fileOpenRawMenu             (Fl_Widget* o, void* v)
  {
    ((Editwindow*)(o->window()->user_data()))->cb_fileOpenRawMenu_i(o,v);
  }







//----------------------------------------------------------------------------//

  void Editwindow::cb_fileSaveAsMenu_i            (Fl_Widget* o, void* v)
  {
    char* filename=fl_file_chooser("Save File As...","*.wav","newfile.wav");
    if(filename == NULL)
      return;

    std::string arg = static_cast<std::string>(filename);

    if(!Commandline::check_for("0,no-bother")) {
      if(Textfile::exists(arg)) {
        string msg = arg + string("\nexists! Do you want to overwrite it?");
        if(!Popups::ask(msg,"Overwrite File?"))
          return;
      }
    }

    string filename_s = static_cast<string>(filename);
    wfile->setfilename(filename_s);
    // TODO: adjust filename of fmt- and data-chunk
    wfile->write(filename_s);
    Configfile::set("LAST_OPENED",filename_s);
    this->relabelWindow(filename);
    //this->init(); // TODO: check, if this should be activated again
    this->redraw_needed |= REDR_ALL;
    this->doRedraw();
    need_filename=false;
  }

  void Editwindow::cb_fileSaveAsMenu              (Fl_Widget* o, void* v)
  {
    ((Editwindow*)(o->window()->user_data()))->cb_fileSaveAsMenu_i(o,v);
  }








//----------------------------------------------------------------------------//

  void Editwindow::cb_fileSaveMenu_i              (Fl_Widget* o, void* v)
  {
    if(need_filename==true)
      return (this->cb_fileSaveAsMenu_i(o,v));

    string fname = wfile->getfilename();
    wfile->write(fname);
    Configfile::set("LAST_OPENED",fname);
  }

  void Editwindow::cb_fileSaveMenu                (Fl_Widget* o, void* v)
  {
    ((Editwindow*)(o->window()->user_data()))->cb_fileSaveMenu_i(o,v);
  }








//----------------------------------------------------------------------------//

  void Editwindow::cb_fileNewWindowMenu_i(Fl_Widget* o, void* v)
  {
    Debug::msg<string>("ew::cb_fileNewWindowMenu_i(): called\n",Globals::DBG_FUNCTION_TRACE);
    Editwindow* pew = new Editwindow;
    ulong wwi = ww->registrate(pew);
    pew->wwindex(wwi,ww);
    if(pew->newfile())
      pew->show();
    else
    {
      ww->unregistrate(wwi);
    }
    Debug::msg<string>("ew::cb_fileNewWindowMenu_i(): leaving\n",Globals::DBG_FUNCTION_TRACE);
  }


  void Editwindow::cb_fileNewWindowMenu (Fl_Widget* o, void* v)
  {
    ((Editwindow*)(o->window()->user_data()))->cb_fileNewWindowMenu_i(o,v);
  }






//----------------------------------------------------------------------------//

  void Editwindow::cb_fileNewWindowOpenMenu_i(Fl_Widget* o, void* v)
  {
    Debug::msg<string>("ew::cb_fileNewWindowOpenMenu_i(): called\n",Globals::DBG_FUNCTION_TRACE);
    Editwindow* pew = new Editwindow;
    ulong wwi = ww->registrate(pew);
    pew->wwindex(wwi,ww);

    const char* filename = fl_file_chooser("Choose A File","*.wav","");
    if(filename)
    {
      pew->readfile(static_cast<string>(filename));
      pew->show();
    }

    else
    {
      ww->unregistrate(wwi);
    }
    Debug::msg<string>("ew::cb_fileNewWindowOpenMenu_i(): leaving\n",Globals::DBG_FUNCTION_TRACE);
  }

  void Editwindow::cb_fileNewWindowOpenMenu (Fl_Widget* o, void* v)
  {
    ((Editwindow*)(o->window()->user_data()))->cb_fileNewWindowOpenMenu_i(o,v);
  }







//----------------------------------------------------------------------------//

  inline void Editwindow::cb_filePreferencesMenu_i       (Fl_Widget* o, void* v)
  {
    Preferences myPrefs;
    myPrefs.show();
    while(myPrefs.visible())
      Fl::wait(0.1);
  }




  void Editwindow::cb_filePreferencesMenu                (Fl_Widget* o, void* v)
  {
    ((Editwindow*)(o->window()->user_data()))->cb_filePreferencesMenu_i(o,v);
  }







//----------------------------------------------------------------------------//

  inline void Editwindow::cb_fileQuitMenu_i       (Fl_Widget* o, void* v)
  {
    bool answer = true;

    if(!Commandline::check_for("0,no-bother"))
      answer = Popups::ask("Really Quit aPcStudio?","I need to know something:");

    vector<unsigned char>& waveChars = (wfile->getWave ());

    if(answer==true)
    {
      // TODO: heavily test this, if more than 1 files are opened!!!
      waveChars.clear                                     ();
      ew->hide                                            ();
      wfile->clear();
      ww->unregistrate(ww_index);
      exit(0);
    }
  }


  void Editwindow::cb_fileQuitMenu                (Fl_Widget* o, void* v)
  {
    ((Editwindow*)(o->window()->user_data()))->cb_fileQuitMenu_i(o,v);
  }



//----------------------------------------------------------------------------//

  inline void Editwindow::cb_fileCloseMenu_i       (Fl_Widget* o, void* v)
  {
    bool answer = true;

    // TODO: should depend on, wether wave was changed or not!
    if(!Commandline::check_for("0,no-bother"))
      answer = Popups::ask("close this window?","I need to know something:");

    vector<unsigned char>& waveChars = (wfile->getWave ());

    if(answer==true)
    {
      // TODO: heavily test this, if more than 1 files are opened!!!
      waveChars.clear                                     ();
      ew->hide                                            ();
      wfile->clear();
      ww->unregistrate(ww_index);
    }
  }

  void Editwindow::cb_fileCloseMenu                (Fl_Widget* o, void* v)
  {
    ((Editwindow*)(o->window()->user_data()))->cb_fileCloseMenu_i(o,v);
  }




//----------------------------------------------------------------------------//

  inline void Editwindow::cb_effectsReverseMenu_i (Fl_Widget* o, void* v)
  {
    if(!this->push_undo())
      return;
    myEffects->reverse();
    this->data_changed();
      redraw_needed |= (REDR_ALL);
      this->doRedraw();

  }


  inline void Editwindow::cb_editCopyMenu_i       (Fl_Widget* o, void* v)
  {
    // new code:
    Copy::perform(this);
    return;

    // old, verified, working code:
    Data_chunk& dc = wfile->getdata();
    vector<unsigned char>& data = dc.getwave();

    theClipboard.setchannels  (this->channels);
    theClipboard.setfrequency (this->frequency);
    theClipboard.setframeSize (this->frameSize);
    theClipboard.setbitRate   (this->bitRate);
    // TODO: check for available memory in the whole code

    unsigned long start = we->getmarkStart()*frameSize;
    unsigned long end   = we->getmarkEnd()  *frameSize;
    unsigned long vec_size = end-start;

    Busywin myBar("copying to memory");

    try {
      (theClipboard.wave).resize(vec_size);

      ulong divider = (end-start)/100;

      myBar.show(); Fl::check();

      for(unsigned long i=start; i<end; i++)
      {

        if(i%divider == 0)
          Fl::check();
/*          myBar.set(stat++); */
        theClipboard.wave[i-start] = data[i];
      }
    }
    catch(std::bad_alloc) {
      (theClipboard.wave).clear();
      Popups::error("Out of memory! Clipboard cleared!");
    }
    myBar.hide();

    std::string t_status = string("");

    if((theClipboard.wave).size() > 0)
      t_status = stream_cast<string>(vec_size) + " byte clipped";
    else
      t_status = string("clipboard is now empty!");

    this->relabel(t_status,"statusBox_status");
  }








//----------------------------------------------------------------------------//

  void Editwindow::cb_editCopyMenu                (Fl_Widget* o, void* v)
  {
    Editwindow* tmpEW = (static_cast<Editwindow*>(o->window()->user_data()));
    tmpEW->cb_editCopyMenu_i(o,v);
  }



//----------------------------------------------------------------------------//

  void Editwindow::cb_editPasteMenu_i              (Fl_Widget* o, void* v)
  {
    if(!this->push_undo())
      return;

    Data_chunk& dc = wfile->getdata();
    vector<unsigned char>& data = dc.getwave();

    Busywin busyPaste("inserting data...");
    busyPaste.show(); Fl::wait(0.2); Fl::check();

    const unsigned long t_clipsize = ((theClipboard.wave).size());
    const unsigned long t_wavesize = data.size();
    const unsigned long t_newsize  = t_wavesize+t_clipsize;

    try {
      data.resize(t_wavesize+t_clipsize);
    }
    catch(std::bad_alloc) {
      Popups::error("Out of memory! Could not paste clipboard into wave!");
      busyPaste.hide(); Fl::check(); Fl::wait(0.1); Sleeper::dosleep(10);
      return;
    }

    this->waveSize = data.size();
    this->we->setwaveSize(t_wavesize+t_clipsize);
    const unsigned long t_cursorbyte = we->getcursorPosition()*frameSize;
    const unsigned long t_cursorpos = we->getcursorPosition();
    const unsigned long t_oldmarkStart = we->getmarkStart();
    const unsigned long t_oldmarkEnd = we->getmarkEnd();
    // const unsigned long t_olddrawEnd = we->getdrawEnd(); // unused
    const unsigned long t_oldvisualFrames = we->getvisualFrames();

    // make a 'clipsized hole' at cursorPosition
    unsigned long next_byteto_move = t_wavesize-1;
    unsigned long moved_byte = t_newsize-1;
    while(next_byteto_move >= t_cursorbyte)
    {
      data[moved_byte--] = data[next_byteto_move--];
      if(moved_byte%1000 == 0)
      Fl::check();
    }

    // put clipboard into the hole
    for(unsigned long i=0; i<(t_clipsize); i++)
    {
      if(i%1000 == 0)
        Fl::check();
      data[i+t_cursorbyte] = theClipboard.wave[i];
    }

    busyPaste.hide();

    // adjust new values
    string t_status = stream_cast<string>(t_clipsize) + " byte pasted";
    relabel(t_status,"statusBox_status");
    we->setvisualFrames(t_oldvisualFrames);

    // set new markStart/markEnd (depending on Settings)
    if((t_oldmarkStart > t_cursorpos) && Settings::SEL_PASTED == false) {
      we->setmarkEnd(t_oldmarkEnd+(t_clipsize/((bitRate/8)*channels)));
      we->setmarkStart(t_oldmarkStart+(t_clipsize/((bitRate/8)*channels)));
    }

    if((t_oldmarkStart < t_cursorpos &&
       t_oldmarkEnd > t_cursorpos) && Settings::SEL_PASTED == false) {
      we->setmarkEnd(t_oldmarkEnd+t_clipsize/((bitRate/8)*channels));
    }

    if(Settings::SEL_PASTED == true) {
      if(t_cursorpos <= t_oldmarkEnd) {
        we->setmarkStart(1);
        we->setmarkEnd(t_cursorpos+(t_clipsize/((bitRate/8)*channels)));
        we->setmarkStart(t_cursorpos);
      }
      else {
        we->setmarkEnd(t_cursorpos+(t_clipsize/((bitRate/8)*channels)));
        we->setmarkStart(t_cursorpos);
      }
    }

    int t_newzoomFactor = (100*((data.size()/we->getframeSize()))/t_oldvisualFrames);
    we->setzoomFactor(t_newzoomFactor);
    relabel(we->frame2time(data.size()/we->getframeSize()),"statusBox_length");
    relabel(stream_cast<string>(data.size()/1024)+" KB","statusBox_size");

    this->data_changed();
    // redraw the waveform; TODO: status boxes!
    this->redraw_needed |= (REDR_WAVE|REDR_WAVEFMT);
    this->doRedraw();
    adjustScrollbar();
  }


  void Editwindow::cb_editPasteMenu                (Fl_Widget* o, void* v)
  {
    Editwindow* tmpEW = (static_cast<Editwindow*>(o->window()->user_data()));
    tmpEW->cb_editPasteMenu_i(o,v);
  }






//----------------------------------------------------------------------------//

  void Editwindow::cb_editCutMenu_i              (Fl_Widget* o, void* v)
  {
    Debug::msg<string>("Editwindow::cb_editCutMenu_i(): entered\n",Globals::DBG_FUNCTION_TRACE);
    Data_chunk& dc = wfile->getdata();
    vector<unsigned char>& data = dc.getwave();
    const unsigned long t_clipsize = frameSize*(we->getmarkEnd()-we->getmarkStart());
    try {
      (theClipboard.wave).resize(t_clipsize);
    }
    catch (std::bad_alloc) {
      (theClipboard.wave).clear();
      Popups::error("Out of memory! Clipboard cleared!");
    }

    const unsigned long t_wavesize = data.size();
    const unsigned long t_newsize  = t_wavesize-t_clipsize;
    const unsigned long t_markStartByte = we->getmarkStart()*frameSize;
    const unsigned long t_markEndByte   = we->getmarkEnd()*frameSize;

    if(t_newsize < (frequency/100.0)*frameSize) {
      return Popups::error("You cannot cut all wavedata! You should\nuse 'edit->digital silence' instead!");
    }

    Debug::msg<string>("Editwindow::cb_editCutMenu_i(): checkpoint 1\n",Globals::DBG_FUNCTION_TRACE);


    Busywin busy1("clipping...");
    busy1.show();
    Fl::check();
    Debug::msg<string>("Editwindow::cb_editCutMenu_i(): checkpoint 2\n",Globals::DBG_FUNCTION_TRACE);
    // Save data for undo-operation!
    busy1.tell("saving undo data..."); Fl::wait(0.1); Sleeper::dosleep(10); Fl::check();
    if(!this->push_undo()) {
      busy1.hide(); Fl::check();
      return;
    }
    Debug::msg<string>("Editwindow::cb_editCutMenu_i(): checkpoint 3\n",Globals::DBG_FUNCTION_TRACE);
    // copy marked area to clipboard
    if((theClipboard.wave).size() > 0) {
      busy1.tell("copying to clipboard..."); Fl::wait(0.1); Sleeper::dosleep(10); Fl::check();
      Debug::msg<string>("Editwindow::cb_editCutMenu_i(): checkpoint 4\n",Globals::DBG_FUNCTION_TRACE);
      for(unsigned long i=0; i<t_clipsize && (i+t_markStartByte)<waveSize; i++)
      {
        if(i%1000 == 0)
          Fl::check();
        theClipboard.wave[i] = data[t_markStartByte + i];
      }
    }
    busy1.hide();
    Debug::msg<string>("Editwindow::cb_editCutMenu_i(): checkpoint 5\n",Globals::DBG_FUNCTION_TRACE);
    // move area that is right from selection to markStart
    Busywin busy2("cutting...");
    busy2.show();
    ulong loopEnd = t_wavesize-t_markEndByte;
    if(loopEnd>=waveSize) loopEnd = waveSize;
    for(unsigned long i=0; i<loopEnd; i++)
    {
        data[i+t_markStartByte] = data[i+t_markEndByte];
    }
    Debug::msg<string>("Editwindow::cb_editCutMenu_i(): checkpoint 6\n",Globals::DBG_FUNCTION_TRACE);
    // check if too much is cutted!
    ulong minsize = frequency/100;
    minsize -= minsize%frameSize;
    ulong fillbytes = 0;
    if(t_newsize < minsize) {
      fillbytes = minsize - t_newsize;
    }
    Debug::msg<string>("Editwindow::cb_editCutMenu_i(): checkpoint 7\n",Globals::DBG_FUNCTION_TRACE);
    // now resize vector (= cut)
    data.resize(t_newsize + fillbytes - (fillbytes%frameSize));

    // fill 'fillbytes', if size is too small
    if(fillbytes > 0) {
      for(ulong i=t_newsize; i<data.size(); ++i) {
        if(bitRate == 8)
          data[i]=0x80;
        if(bitRate == 16)
          data[i]=0;
      }
    }

    busy2.hide();

    // important data changed, let's adapt a few values...
    this->waveSize = data.size();
    this->we->setwaveSize(t_newsize);

    // redraw the waveform; TODO: status boxes!
    this->redraw_needed |= (REDR_WAVE);
    this->doRedraw();

    unsigned long t_drawEnd = this->we->getdrawEnd()+(t_clipsize/frameSize);
    if(t_drawEnd > t_newsize/frameSize)
    {
      t_drawEnd = t_newsize/frameSize;
      this->we->setzoomFactor(100);
      this->we->setdrawStart(0);
    }
    this->we->setdrawEnd(t_drawEnd);

    this->we->setmarkStart(this->we->getcursorPosition());
    this->we->setmarkEnd(this->we->getcursorPosition()+frameSize);

    string t_status = stream_cast<string>(t_clipsize) + " byte cut+clipped";
    this->relabel(t_status,"statusBox_status");

    string t_size = stream_cast<string>(t_newsize/1024) + " KB";
    this->relabel(t_size,"statusBox_size");

    string t_length = stream_cast<string>(we->frame2time(t_newsize/frameSize));
    this->relabel(t_length,"statusBox_length");

    this->data_changed();

    redraw_needed |= (REDR_ALL);
    doRedraw();
    adjustScrollbar();
    Debug::msg<string>("Editwindow::cb_editCutMenu_i(): leaving\n",Globals::DBG_FUNCTION_TRACE);
  }


  void Editwindow::cb_editCutMenu                (Fl_Widget* o, void* v)
  {
    Editwindow* tmpEW = (static_cast<Editwindow*>(o->window()->user_data()));
    tmpEW->cb_editCutMenu_i(o,v);
  }






//----------------------------------------------------------------------------//

  void Editwindow::cb_editSelectAllMenu_i        (Fl_Widget* o, void* v)
  {
    we->setmarkStart(0);
    we->setmarkEnd(waveSize/frameSize);
    redraw_needed |= REDR_WAVE|REDR_SELSTAT;
    doRedraw();
  }



  void Editwindow::cb_editSelectAllMenu           (Fl_Widget* o, void* v)
  {
    Editwindow* tmpEW = (static_cast<Editwindow*>(o->window()->user_data()));
    tmpEW->cb_editSelectAllMenu_i(o,v);
  }







//----------------------------------------------------------------------------//

  // TODO: selecting NOTHING is actually not possible, fix that
  void Editwindow::cb_editSelectNoneMenu_i        (Fl_Widget* o, void* v)
  {
    we->setmarkStart(0);
    we->setmarkEnd(1);
    redraw_needed |= REDR_WAVE|REDR_SELSTAT;
    doRedraw();
  }


  void Editwindow::cb_editSelectNoneMenu           (Fl_Widget* o, void* v)
  {
    Editwindow* tmpEW = (static_cast<Editwindow*>(o->window()->user_data()));
    tmpEW->cb_editSelectNoneMenu_i(o,v);
  }






//----------------------------------------------------------------------------//

  void Editwindow::cb_editCsr2SelStartMenu_i       (Fl_Widget* o, void* v)
  {
    we->setcursorPosition(we->getmarkStart());
    we->relabel_cursorPosition();
    redraw_needed |= REDR_ALL;
    doRedraw();
  }


  void Editwindow::cb_editCsr2SelStartMenu         (Fl_Widget* o, void* v)
  {
    Editwindow* tmpEW = (static_cast<Editwindow*>(o->window()->user_data()));
    tmpEW->cb_editCsr2SelStartMenu_i(o,v);
  }




//----------------------------------------------------------------------------//

  void Editwindow::cb_editCsr2SelEndMenu_i       (Fl_Widget* o, void* v)
  {
    we->setcursorPosition(we->getmarkEnd());
    we->relabel_cursorPosition();
    redraw_needed |= REDR_ALL;
    doRedraw();
  }


  void Editwindow::cb_editCsr2SelEndMenu           (Fl_Widget* o, void* v)
  {
    Editwindow* tmpEW = (static_cast<Editwindow*>(o->window()->user_data()));
    tmpEW->cb_editCsr2SelEndMenu_i(o,v);
  }







//----------------------------------------------------------------------------//

  void Editwindow::cb_editSilenceMenu_i            (Fl_Widget* o, void* v)
  {
    Debug::msg<string>("Editwindow::cb_editSilenceMenu_i() entered\n",Globals::DBG_FUNCTION_TRACE);

    if(!this->push_undo())
      return;

    Data_chunk& dc = wfile->getdata();
    vector<unsigned char>& vec = dc.getwave();

    int channel = 0;
    Askchannel ac;
    channel = ac.ask("Mute which channel?");

    mute()(vec,
           we->getmarkStart(),
           we->getmarkEnd(),
           this->bitRate,
           this->channels,
           this->frameSize,
           channel); // channel, 0-both

    this->data_changed();
    redraw_needed |= REDR_WAVE;
    doRedraw();
    Debug::msg<string>("Editwindow::cb_editSilenceMenu_i() leaving\n",Globals::DBG_FUNCTION_TRACE);
  }



  void Editwindow::cb_editSilenceMenu              (Fl_Widget* o, void* v)
  {
    Editwindow* tmpEW = (static_cast<Editwindow*>(o->window()->user_data()));
    tmpEW->cb_editSilenceMenu_i(o,v);
  }







//----------------------------------------------------------------------------//

  // viewmenu from here

  void Editwindow::cb_viewzoomAllMenu           (Fl_Widget* o, void* v)
  {
    Editwindow* tmpEW = (static_cast<Editwindow*>(o->window()->user_data()));
    Raisebutton* b = tmpEW->showall_tool;
    tmpEW->cb_zoomResetButton_i(b,v);
  }

  void Editwindow::cb_viewzoomOutMenu           (Fl_Widget* o, void* v)
  {
    Editwindow* tmpEW = (static_cast<Editwindow*>(o->window()->user_data()));
    Raisebutton* b = tmpEW->zoomout_tool;
    tmpEW->cb_zoomOutButton_i(b,v);
  }





//----------------------------------------------------------------------------//

  void Editwindow::cb_viewzoomInMenu           (Fl_Widget* o, void* v)
  {
    Editwindow* tmpEW = (static_cast<Editwindow*>(o->window()->user_data()));
    Raisebutton* b = tmpEW->zoomin_tool;
    tmpEW->cb_zoomInButton_i(b,v);
  }

  void Editwindow::cb_viewzoomSelectMenu           (Fl_Widget* o, void* v)
  {
    Editwindow* tmpEW = (static_cast<Editwindow*>(o->window()->user_data()));
    Raisebutton* b = tmpEW->zoomsel_tool;
    tmpEW->cb_zoomSelectButton_i(b,v);
  }






//----------------------------------------------------------------------------//

  void Editwindow::cb_viewzoomSetMenu           (Fl_Widget* o, void* v)
  {
    ((Editwindow*)(o->window()->user_data()))->cb_viewzoomSetMenu_i(o,v);
  }

  void Editwindow::cb_viewzoomSetMenu_i           (Fl_Widget* o, void* v)
  {
    Askzoom az;
    int newZoom = az.ask(stream_cast<string>(we->getzoomFactor()));

    if(newZoom==0) {
      return;
    }

    we->setzoomFactor(newZoom);
    we->setdrawStart(we->getmarkStart());
    this->adjustScrollbar();
    zoom_tool_label = stream_cast<string>(static_cast<int>(we->getzoomFactor()));
    zoom_tool->label(zoom_tool_label.c_str());
    zoom_tool->redraw();

    redraw_needed |= REDR_ALL;
    this->doRedraw();
  }

  // end of viewmenu

  void Editwindow::cb_effectsReverseMenu          (Fl_Widget* o, void* v)
  {
    Editwindow* tmpEW = (static_cast<Editwindow*>(o->window()->user_data()));
    tmpEW->cb_effectsReverseMenu_i(o,v);
  }






//----------------------------------------------------------------------------//

  inline void Editwindow::cb_effectsFreeFadeMenu_i    (Fl_Widget* o, void* v)
  {
    Debug::msg<string>("Editwindow::cb_effectsFreeFadeMenu_i(): called\n",Globals::DBG_FUNCTION_TRACE);
    // remark: push_undo() is called from gui!
    AmpEffects ae(this);
    ae.mode(2);
    ae.show();
    while(ae.visible())
    {
      Fl::check();
      Sleeper::dosleep(10);
      Fl::wait(0.01);
    }

    Debug::msg<string>("Editwindow::cb_effectsFreeFadeMenu_i(): leaving\n",Globals::DBG_FUNCTION_TRACE);
  }


  void Editwindow::cb_effectsFreeFadeMenu             (Fl_Widget* o, void* v)
  {
    return ((Editwindow*)(o->window()->user_data()))->cb_effectsFreeFadeMenu_i(o,v);
  }





//----------------------------------------------------------------------------//


  inline void Editwindow::cb_effectsFadeInMenu_i    (Fl_Widget* o, void* v)
  {
    if(!this->push_undo())
      return;
    myEffects->fade(0.0, 100.0);
    this->data_changed();
      redraw_needed |= REDR_ALL;
      this->doRedraw();
  }


  void Editwindow::cb_effectsFadeInMenu             (Fl_Widget* o, void* v)
  {
    return ((Editwindow*)(o->window()->user_data()))->cb_effectsFadeInMenu_i(o,v);
  }






//----------------------------------------------------------------------------//

  inline void Editwindow::cb_effectsFadeOutMenu_i    (Fl_Widget* o, void* v)
  {
    if(!this->push_undo())
      return;

    myEffects->fade(100.0, 0.0);
    this->data_changed();
      redraw_needed |= REDR_ALL;
      this->doRedraw();
  }


  void Editwindow::cb_effectsFadeOutMenu             (Fl_Widget* o, void* v)
  {
    return ((Editwindow*)(o->window()->user_data()))->cb_effectsFadeOutMenu_i(o,v);
  }









//----------------------------------------------------------------------------//

  inline void Editwindow::cb_effectsNoiseGateMenu_i    (Fl_Widget* o, void* v)
  {
    // remark: push_undo() is called by the gui!
    AmpEffects ae(this);
    ae.mode(3);
    ae.show();
    while(ae.visible())
    {
      Fl::check();
      Sleeper::dosleep(10);
      Fl::wait(0.01);
    }

    this->data_changed();
      redraw_needed |= REDR_ALL;
      this->doRedraw();
  }


  void Editwindow::cb_effectsNoiseGateMenu             (Fl_Widget* o, void* v)
  {
    return ((Editwindow*)(o->window()->user_data()))->cb_effectsNoiseGateMenu_i(o,v);
  }










//----------------------------------------------------------------------------//

  void Editwindow::cb_effectsEchoMenu_i             (Fl_Widget* o, void* v)
  {
    myEffects->echo(this);
  }


  void Editwindow::cb_effectsEchoMenu               (Fl_Widget* o, void* v)
  {
    return ((Editwindow*)(o->window()->user_data()))->cb_effectsEchoMenu_i(o,v);
  }







//----------------------------------------------------------------------------//

  void Editwindow::cb_helpAboutMenu                    (Fl_Widget* o, void* v)
  {
    Debug::msg<string>("Editwindow::cb_helpAboutMenu(): called\n");
/*
    About a;
    a.show();
*/
    Aboutwin* aw = new Aboutwin;
    aw->show();
    while(aw->visible()) {
      Fl::wait(0.1);
      Sleeper::dosleep(10);
    }
    delete aw; aw = NULL;
    Debug::msg<string>("Editwindow::cb_helpAboutMenu(): leaving\n");
  }



  /// This function dumps the waveform to stdout (menu->debug->dump).
  void Editwindow::cb_debugDumpMenu_i                  (Fl_Widget* o, void* v)
  {

    Data_chunk& dc = wfile->getdata();
    vector<unsigned char>& vec = dc.getwave();
    short t_val = 0;

    unsigned long counter = stream_cast<unsigned long>(Popups::input("Dump how many frames (<=0 means all)?","Debug question..."));

    cerr << "counter: " << counter << endl;
    if (counter <= 0)
      counter = we->getvisualFrames();

    for (unsigned long i=we->getdrawStart()*frameSize; i<(we->getdrawStart()*frameSize)+counter*frameSize; i+=frameSize)
    {
      switch(bitRate)
      {
        case 8  : cout << "(8) byte: " << i << ", " << abs(vec[i]) << endl; break;
        case 16 : lendian(vec[i], vec[i+1], t_val);
                  cout << "(16) byte: " << i << ", " << t_val << endl; break;
        default : cout << "::cb_debugDumpMenu_i(): stupid bitRate: " << bitRate << endl;
      }
    }
  }

  void Editwindow::cb_debugDumpMenu                    (Fl_Widget* o, void* v)
  {
    return ((Editwindow*)(o->window()->user_data()))->cb_debugDumpMenu_i(o,v);
  }






//----------------------------------------------------------------------------//

  void Editwindow::cb_debugDrawDataMenu_i              (Fl_Widget* o, void* v)
  {
    std::cerr << "\n<-- dumping draw data -->\n\n";
    cerr << "drawStart:      " << we->getdrawStart() << endl;
    cerr << "drawEnd:        " << we->getdrawEnd() << endl;
    cerr << "width:          " << we->getwidth() << endl;
    cerr << "ew->w():        " << ew->w() << endl;
    cerr << "visualFrames:   " << we->getvisualFrames() << endl;
    cerr << "XSTEP:          " << we->getXSTEP() << endl;
    cerr << "pix_per_sample: " << we->getpixpersample() << endl;
    cerr << "zoomFactor:     " << we->getzoomFactor() << endl;
    cerr << "markStart:      " << we->getmarkStart() << endl;
    cerr << "markEnd:        " << we->getmarkEnd() << endl;
    cerr << "cursorPosition: " << we->getcursorPosition() << endl;
    cerr << "bitRate:        " << this->bitRate << endl;
    cerr << "channels:       " << this->channels << endl;
    cerr << "frameSize:      " << this->frameSize << endl;
    cerr << "waveSize:       " << this->waveSize << endl;
    cerr << "\n<-- draw data dumped --> \n\n";
  }


  void Editwindow::cb_debugDrawDataMenu                (Fl_Widget* o, void* v)
  {
    return ((Editwindow*)(o->window()->user_data()))->cb_debugDrawDataMenu_i(o,v);
  }






//----------------------------------------------------------------------------//

  // scroll the wave data
  inline void Editwindow::cb_scrollBar_i          (Fl_Scrollbar* o, void* v)
  {
    long val = o->value();
    if (val < 0)
      val = 0;
    we->setdrawStart(val);
    redraw_needed |= (REDR_WAVE|REDR_VIEWSTAT);
    this->doRedraw();
  }

  void Editwindow::cb_scrollBar            (Fl_Scrollbar* o, void* v)
  {
    ((Editwindow*)(o->window()->user_data()))->cb_scrollBar_i(o,v);
  }





//----------------------------------------------------------------------------//

  // zoom IN, OUT, RESET
  inline void Editwindow::cb_zoomInButton_i       (Raisebutton* o, void* v)
  {
    // zoom In,
    we->multiplyZoom((4.0/3.0)/*2*/);
    this->adjustScrollbar();

    zoom_tool_label = stream_cast<string>(static_cast<int>(we->getzoomFactor()));
    zoom_tool->label(zoom_tool_label.c_str());
    zoom_tool->redraw();

    redraw_needed |= (REDR_WAVE|REDR_VIEWSTAT);
    this->doRedraw();
  }

  void Editwindow::cb_zoomInButton         (Raisebutton* o, void* v)
  {
    ((Editwindow*)(o->window()->user_data()))->cb_zoomInButton_i(o,v);
  }





//----------------------------------------------------------------------------//

  inline void Editwindow::cb_zoomOutButton_i      (Raisebutton* o, void* v)
  {
    // zoom Out
    we->multiplyZoom(0.75/*0.5*/);
    this->adjustScrollbar();

    zoom_tool_label = stream_cast<string>(static_cast<int>(we->getzoomFactor()));
    zoom_tool->label(zoom_tool_label.c_str());
    zoom_tool->redraw();

    redraw_needed |= (REDR_WAVE|REDR_VIEWSTAT);
    this->doRedraw();

  }

  void Editwindow::cb_zoomOutButton        (Raisebutton* o, void* v)
  {
    ((Editwindow*)(o->window()->user_data()))->cb_zoomOutButton_i(o,v);
  }







//----------------------------------------------------------------------------//

  inline void Editwindow::cb_zoomResetButton_i    (Raisebutton* o, void* v)
  {
    we->setdrawStart(0);
    we->setvisualFrames(waveSize/frameSize);
    we->setdrawEnd(waveSize/frameSize);
    we->setzoomFactor(100);
    zoom_tool_label = string("100");
    zoom_tool->label(zoom_tool_label.c_str());
    zoom_tool->redraw();

    this->adjustScrollbar();

    redraw_needed |= (REDR_WAVE|REDR_VIEWSTAT);
    this->doRedraw();
  }



  void Editwindow::cb_zoomResetButton      (Raisebutton* o, void* v)
  {
    Editwindow* myObj = static_cast<Editwindow*>(o->window()->user_data());
    myObj->cb_zoomResetButton_i(o,v);
  }






//----------------------------------------------------------------------------//

  // zoomStatus
  inline void Editwindow::cb_zoomStatus_i  (Raisebutton* o, void* v)
  {
    // get string of zoom status from input popup window
    string szf = Popups::input("Please enter a zoom factor","Zoom");
    double zf  = stream_cast<double>(szf); // convert to double
    this->we->setzoomFactor(zf);

    redraw_needed |= (REDR_WAVE|REDR_VIEWSTAT);
    this->doRedraw();
  }

  void Editwindow::cb_zoomStatus           (Raisebutton* o, void* v)
  {
    ((Editwindow*)(o->window()->user_data()))->cb_zoomStatus_i(o,v);
  }






//----------------------------------------------------------------------------//

  // editGroup
  inline void Editwindow::cb_selectAllButton_i  (Raisebutton* o, void* v)
  {
    we->setmarkStart(0);
    we->setmarkEnd(waveSize/frameSize);
    redraw_needed |= (REDR_WAVE|REDR_SELSTAT);
    this->doRedraw();
  }

  void Editwindow::cb_selectAllButton  (Raisebutton* o, void* v)
  {
    ((Editwindow*)(o->window()->user_data()))->cb_selectAllButton_i(o,v);
  }



  void Editwindow::cb_cutButton        (Raisebutton* o, void* v)
  {
    ((Editwindow*)(o->window()->user_data()))->cb_editCutMenu_i(o,v);
  }

  void Editwindow::cb_copyButton        (Raisebutton* o, void* v)
  {
    ((Editwindow*)(o->window()->user_data()))->cb_editCopyMenu_i(o,v);
  }

  void Editwindow::cb_pasteButton        (Raisebutton* o, void* v)
  {
    ((Editwindow*)(o->window()->user_data()))->cb_editPasteMenu_i(o,v);
  }






//----------------------------------------------------------------------------//

  // playGroup
  void Editwindow::cb_playButton           (Raisebutton* o, void* v)
  {
    APC::Debug::msg<std::string>("ew::cb_playButton(): called\n",Globals::DBG_FUNCTION_TRACE);
    ((Editwindow*)(o->window()->user_data()))->cb_playButton_i(o,v);
    APC::Debug::msg<std::string>("ew::cb_playButton(): leaving\n",Globals::DBG_FUNCTION_TRACE);
  }



  void Editwindow::cb_playButton_i         (Raisebutton* o, void* v)
  {
    // if we play already, there's nothing to do
    APC::Debug::msg<std::string>("ew::cb_playButton_i(): called\n",Globals::DBG_FUNCTION_TRACE);
    if(player.playing == true && player.paused==false) {
    APC::Debug::msg<std::string>("ew::cb_playButton_i(): leaving (already playing)\n",Globals::DBG_FUNCTION_TRACE);
      return;
    }

    // play Wavefile
    followPlayCursor          = Settings::FOLLOW_PLAY_CURSOR;
    Wavefile&          tmpWF  = *(this->wfile);

    // check if shift was pressed while playButton was clicked!
    // and start the right play-thread
    int state = Fl::event_state();

    if((state&FL_CTRL)==FL_CTRL) // play unselected
    {
      relabel("playing unselected","statusBox_status");
      player.play_unselected = true;
      player.play(tmpWF,NULL,we->getcursorPosition(), (waveSize/frameSize));
    }

    if((state&FL_SHIFT)==FL_SHIFT) // play from cursorposition to end
    {
      relabel("playing from cursorpos.","statusBox_status");
      player.play_unselected = false;
      player.play(tmpWF,NULL,we->getcursorPosition(), (waveSize/frameSize));
    }

    if((state&FL_SHIFT)!=FL_SHIFT &&
       (state&FL_CTRL) !=FL_CTRL)
    {
      relabel("playing selection","statusBox_status");
      player.play_unselected = false;
      player.play(tmpWF,NULL,we->getmarkStart(), we->getmarkEnd()); // play selection
    }

    unsigned long oldpos=1, newpos=0;
    while(player.playing)
    {
      // TODO: redraw cursorline only, if it's screenposition
      // has changed, not, if only cursorpos changed.
    	while (newpos == oldpos && player.playing)
    	{
    		Fl::check();
        Fl::wait(0.01); // this INCREDIBLY reduces CPU usage

    		newpos = we->getcursorPosition();

    		// check if stop-button pressed or waveform completely played
        if (player.playing == false)
        {
          break;
        }

        // follow playCursor, if it scrolls out of the window
        if (this->followPlayCursor==true &&
           (we->getcursorPosition() > we->getdrawEnd()))
        {
          we->setdrawStart(we->getdrawEnd());
          we->setdrawEnd(we->getdrawStart()+we->getvisualFrames());
          this->adjustScrollbar();
          this->redraw_needed |= REDR_ALL;
          this->doRedraw();
        }

        // redraw cursor (or all) if needed
        if (oldpos != newpos)
        {
          this->draw_cursor();
    	    we->relabel_cursorPosition();
          redraw_needed |= (REDR_CSRPOS);
    	    this->doRedraw();
        }
    	}
    	oldpos = newpos;
    }

    relabel("stopped","statusBox_status");
    redraw_needed |= REDR_STATUS;
    doRedraw();
    APC::Debug::msg<std::string>("ew::cb_playButton_i(): leaving\n",Globals::DBG_FUNCTION_TRACE);
    return;
  }







//----------------------------------------------------------------------------//

  void Editwindow::cb_recordButton         (Raisebutton* o, void* v)
  {
    ((Editwindow*)(o->window()->user_data()))->cb_recordButton_i(o,v);
  }

  void Editwindow::cb_recordButton_i       (Raisebutton* o, void* v)
  {
    if(recorder.recording==true && recorder.paused==true) {
      recorder.paused = false;
      return;
    }

    if(recorder.recording==true && recorder.paused==false) {
      return;
    }

    if(!push_undo()) {
      if(!Popups::ask("No undo possible, do you still want to record sound?"))
        return;
    }
    recorder.record_selection();

    this->data_changed();
    redraw_needed |= REDR_ALL;
    doRedraw();
  }









//----------------------------------------------------------------------------//

  void Editwindow::cb_zoom_tool         (Fl_Widget* o, void* v)
  {
    ((Editwindow*)(o->window()->user_data()))->cb_zoom_tool_i(o,v);
  }

  void Editwindow::cb_zoom_tool_i       (Fl_Widget* o, void* v)
  {
    Askzoom az;
    int newZoom = az.ask(stream_cast<string>(we->getzoomFactor()));

    if(newZoom==0) {
      return;
    }

    we->setzoomFactor(newZoom);
    we->setdrawStart(we->getmarkStart());
    this->adjustScrollbar();
    zoom_tool_label = stream_cast<string>(static_cast<int>(we->getzoomFactor()));
    zoom_tool->label(zoom_tool_label.c_str());
    zoom_tool->redraw();

    redraw_needed |= REDR_ALL;
    this->doRedraw();
/*
    Fl_Int_Input* Obj = static_cast<Fl_Int_Input*>(o);
    int newval = static_cast<int>(we->setzoomFactor(stream_cast<double>(Obj->value())));
    string s_newval = stream_cast<string>(newval);
    Obj->value(s_newval.c_str());
    this->adjustScrollbar();
    redraw_needed |= REDR_ALL;
    doRedraw();*/
  }







//----------------------------------------------------------------------------//

  void Editwindow::cb_rasterswitch         (Fl_Widget* o, void* v)
  {
    if(Settings::DRAW_RASTER)
      Settings::DRAW_RASTER = false;
    else
      Settings::DRAW_RASTER = true;
    o->window()->redraw();
  }

  void Editwindow::cb_digitswitch         (Fl_Widget* o, void* v)
  {
    if(Settings::DRAW_DIGITS)
      Settings::DRAW_DIGITS = false;
    else
      Settings::DRAW_DIGITS = true;
    o->window()->redraw();
  }


//----------------------------------------------------------------------------//

  void Editwindow::cb_stopButton           (Raisebutton* o, void* v)
  {
    ((Editwindow*)(o->user_data()))->cb_stopButton_i(o,v);
  }

  inline void Editwindow::cb_stopButton_i  (Raisebutton* o, void* v)
  {
    player.playing        = false; // stopping sound
    recorder.recording    = false;
    relabel("stopped","statusBox_status");
    redraw_needed |= (REDR_STATUS);
    this->doRedraw();
  }






//----------------------------------------------------------------------------//

  void Editwindow::cb_pauseButton           (Raisebutton* o, void* v)
  {
    ((Editwindow*)(o->user_data()))->cb_pauseButton_i(o,v);
  }

  inline void Editwindow::cb_pauseButton_i  (Raisebutton* o, void* v)
  {
    if(player.playing == false &&
       recorder.recording == false) {
      cerr << "pause button: nothing to do...\n";
      return;
    }

    if((player.paused==true && player.playing==true))
    {
      player.paused         = false; // goin' on
      relabel("playing","statusBox_status");
      return;
    }
    if((player.paused==false && player.playing==true))
    {
      player.paused         = true;
      relabel("** playing paused **","statusBox_status");
      return;
    }
    if((recorder.paused==true && recorder.recording==true))
    {
      recorder.paused         = false; // goin' on
      relabel("recording","statusBox_status");
      return;
    }
    if((recorder.paused==false && recorder.recording==true))
    {
      recorder.paused         = true;
      relabel("** rec paused **","statusBox_status");
      return;
    }

    cerr << "hm, something went wrong: playing/paused, recording/paused:\n";
    cerr << player.playing << "/" << player.paused << ", "
         << recorder.recording << "/" << recorder.paused << endl;
  }






//----------------------------------------------------------------------------//

  void Editwindow::cb_reverse_btn            (Raisebutton* o, void* v)
  {
    ((Editwindow*)(o->window()->user_data()))->cb_effectsReverseMenu_i(o,v);
  }





//----------------------------------------------------------------------------//

  void Editwindow::cb_amplitude_btn            (Raisebutton* o, void* v)
  {
    ((Editwindow*)(o->window()->user_data()))->cb_amplitude_btn_i(o,v);
  }


  inline void Editwindow::cb_amplitude_btn_i   (Raisebutton* o, void* v)
  {
    // remark: push_undo() is called from gui!
    AmpEffects ae(this);
    ae.mode(1);
    ae.show();
    while(ae.visible())
    {
      Fl::check();
      Sleeper::dosleep(10);
      Fl::wait(0.01);
    }
  }





//----------------------------------------------------------------------------//

  void Editwindow::cb_helpManualMenu (Fl_Widget* o, void* v)
  {
    if(Textfile::exists(Settings::HELP_DIR+string("/index.html")))
      Help::manual();
    else
      Popups::error("Please check the path to the HTML documentation\n(File->preferences->paths)");
  }



//----------------------------------------------------------------------------//

  void Editwindow::cb_editUndoMenu (Fl_Widget* o, void* v)
  {
    ((Editwindow*)(o->window()->user_data()))->cb_editUndoMenu_i(o,v);
  }

//----------------------------------------------------------------------------//

  void Editwindow::cb_editUndoMenu_i (Fl_Widget* o, void* v)
  {
    if(!undo.perform(this)) {
      string msg = string("Only ") +
                   stream_cast<string>(Settings::MAX_UNDOS) +
                   string(" undos possible. To change this\n") +
                   string("see 'File->Preferences->undo'");
      Popups::error(msg,"Undo-Error!");
    }
    redraw_needed |= REDR_ALL;
    this->doRedraw();
  }

//----------------------------------------------------------------------------//

  void Editwindow::cb_editRedoMenu (Fl_Widget* o, void* v)
  {
    ((Editwindow*)(o->window()->user_data()))->cb_editRedoMenu_i(o,v);
  }

//----------------------------------------------------------------------------//

  void Editwindow::cb_editRedoMenu_i (Fl_Widget* o, void* v)
  {
    Popups::warn("Sorry, redo is not implemented!");
  }


//----------------------------------------------------------------------------//
  // TODO: make this less linux specific (fork() is linux only)!
  void Editwindow::cb_mixer_btn(Fl_Widget* o, void* v)
  {
    Debug::msg<string>("Editwindow::cb_mixer_btn(): called\n",Globals::DBG_FUNCTION_TRACE);
    if(Textfile::exists(Settings::MIXER_DIR)) {
      pid_t pid1;
      pid1 = fork();
      if(pid1 == 0) {
        system((Settings::MIXER_DIR+string(" &")).c_str());
        exit(0);
      }
      else
        return;
    }
    else
      Popups::error("Please check path to the mixer application! \n(File->Preferences->paths)");
    Debug::msg<string>("Editwindow::cb_mixer_btn(): leaving\n",Globals::DBG_FUNCTION_TRACE);
  }




//----------------------------------------------------------------------------//

  void Editwindow::cb_editMixpasteMenu(Fl_Widget* o, void* v)
  {
    ((Editwindow*)(o->window()->user_data()))->cb_editMixpasteMenu_i(o,v);
  }

  void Editwindow::cb_editMixpasteMenu_i(Fl_Widget* o, void* v)
  {
    Mixpaste::perform(this);
  }




//----------------------------------------------------------------------------//

  void Editwindow::cb_echo_btn_i(Fl_Widget* o, void* v)
  {
    myEffects->echo(this);
  }


  void Editwindow::cb_echo_btn(Fl_Widget* o, void* v)
  {
    ((Editwindow*)(o->window()->user_data()))->cb_echo_btn_i(o,v);
  }

 //----------------------------------------------------------------------------//

  void Editwindow::cb_effectsReverbMenu_i(Fl_Widget* o, void* v)
  {
    // set default values! // TODO: find good values, apply to Askreverb::set_default()
    int    duration    = 350;        int    volume     = 10;
    int    attack      = 10;         int    delay      = 5;
    int    distance    = 50;         double random     = 0.45;
    double ratio       = 0.5;        int    channel    = 0;

    // ask user to change values (nice gui)
    Askreverb ar;
    ar.ask(duration,volume,attack,delay,distance,random,ratio,channel);
    if(volume==0) {
      APC::Debug::msg<string>("ew::cb_effectsReverbMenu_i(): reverb canceled by user!\n");
      return;
    }

    // save undo data
    if(!this->push_undo()) {
      return;
    }

    // finally, call Reverb::perform()
    std::vector<uchar>& data = wfile->getWave();
    Reverb::perform(data,channels,bitRate,frequency,
                    we->getmarkStart(),we->getmarkEnd(),
                    duration,volume,attack,delay,
                    distance,random,ratio,channel);

    this->relabel("reverbration done","statusBox_status");
    this->data_changed();
    this->redraw_needed |= (this->REDR_WAVE);
    this->doRedraw();
    this->adjustScrollbar();
  }


  void Editwindow::cb_effectsReverbMenu(Fl_Widget* o, void* v)
  {
    ((Editwindow*)(o->window()->user_data()))->cb_effectsReverbMenu_i(o,v);
  }

//----------------------------------------------------------------------------//

  void Editwindow::cb_reverb_btn_i(Fl_Widget* o, void* v)
  {
    this->cb_effectsReverbMenu_i(o,v);
  }


  void Editwindow::cb_reverb_btn(Fl_Widget* o, void* v)
  {
    ((Editwindow*)(o->window()->user_data()))->cb_reverb_btn_i(o,v);
  }


//----------------------------------------------------------------------------//

  bool Editwindow::push_undo()
  {
    Data_chunk& dc = wfile->getdata();
    vector<unsigned char>& data = dc.getwave();
    Undo_operation* uo = NULL;
    try {
    uo = new Undo_operation(this->frequency,
                                            this->channels,
                                            this->bitRate,
                                            this->we->getmarkStart(),
                                            this->we->getmarkEnd(),
                                            this->we->getdrawStart(),
                                            this->we->getcursorPosition(),
                                            this->we->getzoomFactor(),
                                            data);
    }
    catch(std::bad_alloc) {
      Popups::error("Out of memory! Can't save undo data!"); // attention! uo is in bad state!
      uo = NULL;
    }
    bool can_undo = false;
    if(uo != NULL)
      can_undo = undo.put(uo);
    if(!can_undo || uo == NULL) {
      bool do_instead = Popups::ask("No undo possible! Perform operation?");
      if(!do_instead)
        return false;
    }
    return true;
  }




//----------------------------------------------------------------------------//

  bool Editwindow::is_recording()
  {
    return recorder.recording;
  }



//----------------------------------------------------------------------------//

  void Editwindow::clear_undo_buf()
  {
    undo.clear();
  }

  void Editwindow::cb_editClearUndoMenu(Fl_Widget* o, void* v)
  {
    ((Editwindow*)(o->window()->user_data()))->clear_undo_buf();
  }


} // end of namespace


// end of file

