// $Id: copy.h,v 1.2 2002/02/01 15:39:27 martinhenne Exp $

// (C) 2001 Martin Henne
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.


#ifndef _copy_h
#define _copy_h

#include <vector>
#include <globals.h>
#include <debug.h>
#include <string>
#include <editwindow.h>

namespace APC
{
using namespace std;
typedef vector<uchar>::iterator uchar_iterator;

  class Copy
  {
  public:
    static void perform(Editwindow*);
    static void copy(const vector<uchar>& source,
                     vector<uchar>& target, // this will be modified!
                     const ulong& startframe,
                     const ulong& endframe,
                     const int& frameSize);
  private:
    Copy();
  };


}

#endif

// end of file
