// $Id: datachunk.cpp,v 1.24 2002/02/19 22:53:50 martinhenne Exp $

// (C) 2001 Martin Henne
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

/*
 * This class should be used inside a class,
 * that represents a wavefile in accompany
 * with a 'fmt ' chunk.
 */

#include "globals.h"
#include "fmtchunk.h"
#include "datachunk.h"
#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <statusbar.h>
#include <popups.h>
#include <pthread.h>
#include <filehandle.h>
#include <sleeper.h>
#include <debug.h>
#include <commandline.h>
#include <busywin.h>
#include <lendian.h>
#include "datachunk.h"

#undef read
#undef write
#undef close

namespace APC
{
using namespace std;
  Data_chunk::Data_chunk(string filename)            // call '::read()'
  {
    //cerr << "Data_chunk(string): constructing\n";
    this->read(filename);
    //cerr << "Data_chunk(string): constructed\n";
  }




  void              Data_chunk::setchunkSize  (unsigned long   arg)
  {
    if (arg <= 0)
    {
      cerr << "Data_chunk::setchunkSize(): got bad value: "
           << arg << endl;
    }
    chunkSize = arg;
    return;
  }


  void* Data_chunk::read_thread (void* v)
  {
    Data_chunk* o = static_cast<Data_chunk*>(v);
    o->reading = true;
    o->infile.read((char*) &(o->wave)[0],o->chunkSize);
    Sleeper::dosleep(100); // 100 milliseconds
    o->reading = false;
    return v;
  }


  int               Data_chunk::read          (string filename)
  {
    // first get position of datachunk within wavefile
    Debug::msg<string>("Data_chunk::read(string): entered\n",Globals::DBG_FUNCTION_TRACE);
    Debug::msg<string>("Data_chunk::read(string): got filename: >>");
    Debug::msg<string>(filename);
    Debug::msg<string>("<<\n");

    if(FileHandle::exists(filename)==false)
      throw no_fileaccess();

    int       IDpos    =    findIDpos(filename);
    if (IDpos == Globals::OTHER_ERROR)
    {
      cerr << "Data_chunk::read(string): could not get position of datachunk in "
           << "file " << filename << endl;
      Debug::msg<string>("Data_chunk::read(string): leaving\n",Globals::DBG_FUNCTION_TRACE);
      return Globals::OTHER_ERROR;
    }
    Debug::msg<string>("Data_chunk::findIDpos(string): checkpoint 1\n",Globals::DBG_FUNCTION_TRACE);
    // now open file and check if it could be successfully opened
    //ifstream                infile;
    infile.open(filename.c_str()/*, ios::in|ios::out|ios::binary*/);
    Debug::msg<string>("Data_chunk::findIDpos(string): checkpoint 2\n",Globals::DBG_FUNCTION_TRACE);
    if (!infile.is_open())
    {
      cerr << "Data_chunk::read(string): could not open file "
           << filename << endl;
      Debug::msg<string>("Data_chunk::read(string): leaving\n",Globals::DBG_FUNCTION_TRACE);
      return Globals::OTHER_ERROR;
    }
    Debug::msg<string>("Data_chunk::findIDpos(string): checkpoint 3\n",Globals::DBG_FUNCTION_TRACE);
    // next is, to get the datachunk's size
    infile.seekg(IDpos+4); // set to begin of chunkSize
    char tmpChar;
    unsigned long tmpLong = 0; // chunkSize is in little endian Version and must be calculatet

    Debug::msg<string>("Data_chunk::findIDpos(string): checkpoint 4\n",Globals::DBG_FUNCTION_TRACE);
    infile.get(tmpChar);tmpLong += abs(tmpChar);
    infile.get(tmpChar);tmpLong += abs(tmpChar)*0x100;
    infile.get(tmpChar);tmpLong += abs(tmpChar)*0x10000;
    infile.get(tmpChar);tmpLong += abs(tmpChar)*0x1000000;

    Debug::msg<string>("Data_chunk::findIDpos(string): checkpoint 5\n",Globals::DBG_FUNCTION_TRACE);
    // now read the wave data
    setchunkSize(tmpLong);
    wave.resize(chunkSize); // memory is allocated from here on!

    Debug::msg<string>("Data_chunk::findIDpos(string): checkpoint 6\n",Globals::DBG_FUNCTION_TRACE);
    // start filereading in a single thread
//    pthread_t myThread;
    reading = true;
//    pthread_create(&myThread,NULL,Data_chunk::read_thread,(void*)(this));
    read_thread(this);
    Debug::msg<string>("Data_chunk::findIDpos(string): checkpoint 7\n",Globals::DBG_FUNCTION_TRACE);
    // hang on, untill file is in memory
#if 0
    Busywin busy("reading file...");
    busy.show();
    while(reading)
    {
      Fl::check();
      Fl::wait(0.1);
      Sleeper::dosleep(10);
      if(!reading)
        break;
        
    }
#endif
    Debug::msg<string>("Data_chunk::findIDpos(string): checkpoint 8\n",Globals::DBG_FUNCTION_TRACE);
    cerr << endl;
    // ready. close file end return
    infile.close();
//    busy.hide();
    //cerr << "Data_chunk::read(string): chunkSize: " << chunkSize << " Bytes. leaving." << endl;
    Debug::msg<string>("Data_chunk::read(string): leaving\n",Globals::DBG_FUNCTION_TRACE);
    return Globals::SUCCESS;
  }// end of ::read()


  void Data_chunk::read_raw                    (const string filename)
  {
    Debug::msg<string>("Data_chunk::read_raw(string): calling\n",Globals::DBG_FUNCTION_TRACE);
    ulong size = Textfile::getsize(filename);
    wave.resize(size);
    infile.open(filename.c_str());
    if(infile.is_open())
    {
//cerr << "Data_chunk::read_raw(): starting ifstream::read()\n";
      infile.read((char*) &wave[0],size);
//cerr << "Data_chunk::read_raw(): ending ifstream::read()\n";
    Sleeper::dosleep(100); // 100 milliseconds
      infile.close();
    Debug::msg<string>("Data_chunk::read_raw(): leaving\n",Globals::DBG_FUNCTION_TRACE);
    }
    else
    {
      cerr << "Data_chunk::read_raw(): could not open file " << filename << endl;
    }
    Debug::msg<string>("Data_chunk::read_raw(string): leaving\n",Globals::DBG_FUNCTION_TRACE);
    return;
  }






/*########################################################################################################*/
  int               Data_chunk::write          (const string filename, Busywin* busy)
  {
    // attention!!! Wavefile::write() should call Wavefile::write_dummy(...) before calling this one.
    Debug::msg<string>("Data_chunk::write(): called\n",Globals::DBG_FUNCTION_TRACE);
    Debug::msg<string>("Data_chunk::write(): checkpoint 1\n",Globals::DBG_FUNCTION_TRACE);
    ofstream ofile;
    ofile.open(filename.c_str(), ios::app|ios::binary); // append binary
    Debug::msg<string>("Data_chunk::write(): checkpoint 2\n",Globals::DBG_FUNCTION_TRACE);
    if(!ofile.is_open())
    {
      cerr << "Data_chunk::write(): couldn't write to " << filename << endl;
      Debug::msg<string>("Data_chunk::write(): leaving\n",Globals::DBG_FUNCTION_TRACE);
      return Globals::NO_WRITE_ACCESS;
    }

    Debug::msg<string>("Data_chunk::write(): checkpoint 3\n",Globals::DBG_FUNCTION_TRACE);

    if(busy == NULL) {
      Busywin* tmpbusy = new Busywin(std::string("Saving file"));
      busy = tmpbusy;
      busy->show();
      Fl::wait(0.1);
      Fl::check();
    }

    Debug::msg<string>("Data_chunk::write(): checkpoint 4\n",Globals::DBG_FUNCTION_TRACE);

    if(busy != NULL)
      busy->tell("writing data to file...");

    Debug::msg<string>("Data_chunk::write(): checkpoint 5\n",Globals::DBG_FUNCTION_TRACE);
/*
    // old code
    for (unsigned int i=0; i<wave.size(); i++)
    {
      ofile.put(wave[i]);
      if(i%1000 == 0)
        Fl::check();
    }*/

    ofile.write((char*) &wave[0],wave.size()); // new code

    ofile.close();

    Debug::msg<string>("Data_chunk::write(): checkpoint 6\n",Globals::DBG_FUNCTION_TRACE);

    if(busy != NULL)
      busy->hide();

    Debug::msg<string>("Data_chunk::write(): leaving\n",Globals::DBG_FUNCTION_TRACE);
    // attention!!! 'Wavefile::write()' should call Fmt_chunk::write() after this!!!
    // and Wavefile::write() should write the correct chunksize and filesize to header!
   // end of ::write()
    return 0;
}

/*########################################################################################################*/


  int Data_chunk::findIDpos          (const string& arg) const
  {
    Debug::msg<string>("Data_chunk::findIDpos(string): entered\n",Globals::DBG_FUNCTION_TRACE);
    int pos = -1;
    char tmpChar = 'A';
    ifstream ifs;
    ifs.open(arg.c_str(), ios::binary);

    // check if file exists and if it is readable
    if (!ifs.is_open())
    {
      std::cerr << "Data_chunk::findIDpos(): couldn't open file "
                << arg << endl;
      Debug::msg<string>("Data_chunk::findIDpos(string): leaving with error\n",Globals::DBG_FUNCTION_TRACE);
      return Globals::OTHER_ERROR; // is '-1' in this case
    }

    // look for 'data' in the whole file
    while(!ifs.eof())
    {
      ifs.get(tmpChar);
      if(tmpChar=='d')
      {
        ifs.get(tmpChar);
        if(tmpChar=='a')
        {
          ifs.get(tmpChar);
          if(tmpChar=='t')
          {
            ifs.get(tmpChar);
            if(tmpChar=='a') // found 'data' !!
            {
              pos = (int) ifs.tellg();
              pos -= 4;
              //std::cerr << "Fmt_chunk::findIDpos(): found 'data' at pos "
              //          << pos << endl;
              break;
            }
          }
        }
      }
    } // end of while
    Debug::msg<string>("Data_chunk::findIDpos(string): leaving\n",Globals::DBG_FUNCTION_TRACE);
    return pos; // pos = -1 if 'data' not found !!!
  } // end of findIDpos(*)

/*#########################################################################################################*/

  void Data_chunk::clear()
  {
    wave.clear();
  }


  int Data_chunk::getSample(const unsigned long& frame,
                            const int& frameSize,
                            const int& channels,
                            const int& channel)
  {
    // vector<uchar> is called wave.
    short ret_val = 0;
    ulong byte = frame * frameSize;
    if(byte>=wave.size()) { return 0;
    }
    if(frameSize==1)
      return wave[byte];
    if(frameSize==2 && channel==2)
      return wave[byte+1];
    if(frameSize==2 && channels==1) {
      lendian(wave[byte], wave[byte+1], ret_val);
      return ret_val;
    }
    if(frameSize==4 && channel==1) {
      lendian(wave[byte], wave[byte+1], ret_val);
      return ret_val;
    }
    if(frameSize==4 && channel==2) {
      lendian(wave[byte+2], wave[byte+3], ret_val);
      return ret_val;
    }
    return 200;
  }


//----------------------------------------------------------------------------//

  double Data_chunk::getAmplitude(const unsigned long& frame,
                                  const int& bitRate,
                                  const int& channels,
                                  const int& channel)
  {
    const int frameSize = (bitRate/8)*channels;
    int abs=this->getSample(frame,frameSize,channels,channel);
    if(bitRate==8)
      return ((abs-0x80)/128.0);
    if(bitRate==16)
      return (abs/(65535.0/2.0));
    return 0.0;
  }



  void Data_chunk::setAmplitude (const double& amplitude,
                                 const unsigned long& frame,
                                 const int& bitRate,
                                 const int& channels,
                                 const int& channel)
  {
    // get some information
    vector<unsigned char>& data = wave;
    const int frameSize = (bitRate/8)*channels;
    short newval = 0;
    short maxval = 65535/2;
    if(bitRate==8) maxval = 127;
    newval = static_cast<short>(0.5 + (amplitude*maxval));

    // beware of clipping
    if(amplitude>1.0) newval = maxval;
    if(amplitude<-1.0) newval = -1 * maxval;

    // now change the bytes in the wave data
    if(bitRate==16) { // 16 bit
      if(channels==2) { // 16 bit stereo
        if(channel==1) { // 16 bit stereo left channel
          data[frame*frameSize]   = getshortsLow(newval);
          data[frame*frameSize+1] = getshortsHigh(newval);
          return;
        } // end of 16 bit stereo left channel
        if(channel==2) { // 16 bit stereo right channel
          data[frame*frameSize+2] = getshortsLow(newval);
          data[frame*frameSize+3] = getshortsHigh(newval);
          return;
        } // end of 16 bit stereo right channel
      } // end of 16 bit stereo
      if(channels==1) { // 16 bit mono
        data[frame*frameSize]   = getshortsLow(newval);
        data[frame*frameSize+1] = getshortsHigh(newval);
        return;
      } // end of 16 bit mono
    } // end of 16 bit

    if(bitRate==8) { // 8 bit
      if(channels==1) { // 8 bit mono
        data[frame] = static_cast<unsigned char>(newval + 128);
        return;
      } // end of 8 bit mono
      if(channels==2) { // 8 bit stereo
        if(channel==1) { // 8 bit stereo left channel
          data[frame*frameSize] = static_cast<unsigned char>(newval + 128);
          return;
        } // end of 8 bit stereo left channel
        if(channel==2) { // 8 bit stereo right channel
          data[frame*frameSize+1] = static_cast<unsigned char>(newval + 128);
          return;
        } // end of 8 bit stereo right channel
      } // end of 8 bit stereo
    } // end of 8 bit

  }

} // end of namespace APC

// end of file


