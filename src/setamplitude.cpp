// $Id: setamplitude.cpp,v 1.1 2002/02/17 16:45:23 martinhenne Exp $

// (C) 2002 Martin Henne
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

#include <setamplitude.h>
#include <globals.h>
#include <lendian.h>

  void setamplitude::operator() (std::vector<uchar>& data,
                                 const double& amplitude,
                                 const unsigned long& frame,
                                 const int& bitRate,
                                 const int& channels,
                                 const int& channel)
  {
    using namespace APC;
    // get some information
    const int frameSize = (bitRate/8)*channels;
    short newval = 0;
    short maxval = 65535/2;
    if(bitRate==8) maxval = 127;
    newval = static_cast<short>(0.5 + (amplitude*maxval));

    // beware of clipping
    if(amplitude>1.0) newval = maxval;
    if(amplitude<-1.0) newval = -1 * maxval;

    // now change the bytes in the wave data
    if(bitRate==16) { // 16 bit
      if(channels==2) { // 16 bit stereo
        if(channel==1) { // 16 bit stereo left channel
          data[frame*frameSize]   = getshortsLow(newval);
          data[frame*frameSize+1] = getshortsHigh(newval);
          return;
        } // end of 16 bit stereo left channel
        if(channel==2) { // 16 bit stereo right channel
          data[frame*frameSize+2] = getshortsLow(newval);
          data[frame*frameSize+3] = getshortsHigh(newval);
          return;
        } // end of 16 bit stereo right channel
      } // end of 16 bit stereo
      if(channels==1) { // 16 bit mono
        data[frame*frameSize]   = getshortsLow(newval);
        data[frame*frameSize+1] = getshortsHigh(newval);
        return;
      } // end of 16 bit mono
    } // end of 16 bit

    if(bitRate==8) { // 8 bit
      if(channels==1) { // 8 bit mono
        data[frame] = static_cast<unsigned char>(newval + 128);
        return;
      } // end of 8 bit mono
      if(channels==2) { // 8 bit stereo
        if(channel==1) { // 8 bit stereo left channel
          data[frame*frameSize] = static_cast<unsigned char>(newval + 128);
          return;
        } // end of 8 bit stereo left channel
        if(channel==2) { // 8 bit stereo right channel
          data[frame*frameSize+1] = static_cast<unsigned char>(newval + 128);
          return;
        } // end of 8 bit stereo right channel
      } // end of 8 bit stereo
    } // end of 8 bit

  }


