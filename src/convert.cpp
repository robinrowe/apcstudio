// convert.cpp

// (c) 2001 Martin Henne
// Martin.Henne@web.de
// GNU Library General Public License

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

// IMPORTANT NOTE!!!
// Please don't use any of the functions above!
// use 'stream_cast<T>' instead, which is defined
// in the headerfile 'convert.h'. stream_casting is
// the best way to do that convert stuff! The 
// functions above are old and I wrote them before
// I got knowlegde about stream casting and templates.
//
// Martin
/*
#include "convert.h"
#include <string>
#include <vector>
#include <strstream>

Convert::Convert ()
{
  //std::cerr << "  ** using GNU/GPL Mclasses Convert, (c) 2001 by Martin Henne" << std::endl;
}

Convert::~Convert ()
{
  //std::cerr << "Convert: destructor" << endl;
}

Convert& Convert::operator = (const Convert &arg){return *this;}

string Convert::int2string(int y)
{
  cerr << "Convert::int2string(): called. Plz use stream_cast<T> instead\n";
  if (y==0)
  {
    return string("0");
  }
    
  int x=y;
  int teiler = 1000000000; // maximum convertable value
  double erg=0.0;
  int ierg=0;
  string back;
  if (x<0)
  {
    x=x*(-1);
  }

  string s[10];
  s[0]=string("0");s[5]=string("5");
  s[1]=string("1");s[6]=string("6");
  s[2]=string("2");s[7]=string("7");
  s[3]=string("3");s[8]=string("8");
  s[4]=string("4");s[9]=string("9");

  while (teiler > 0)
  {
    erg = x/teiler;
    ierg = static_cast<int>(erg);
    switch (ierg)
    {
      case 0 : back = back + s[0]; break;
      case 1 : back = back + s[1]; break;
      case 2 : back = back + s[2]; break;
      case 3 : back = back + s[3]; break;
      case 4 : back = back + s[4]; break;
      case 5 : back = back + s[5]; break;
      case 6 : back = back + s[6]; break;
      case 7 : back = back + s[7]; break;
      case 8 : back = back + s[8]; break;
      case 9 : back = back + s[9]; break;
    }		
    x = x-(ierg * teiler);
    teiler = teiler/10;
  }

  // eliminate leading zeros
  int initl=back.length();
  for (int i=0; i<initl; i++)
  {
    if (back.substr(0,1)=="0")
    back = back.substr(1,back.length()-1);
  }
	
  if (y<0)
  {
    back = "-" + back;
  }

  return back;
}

int Convert::string2int (string s)
{
  // convert string to char
  cerr << "Convert::string2int(): called. Plz use stream_cast<T> instead!\n";
  const int laenge = s.length();
  vector<char> c_s(laenge+1); // space for trailing '\0'
  string s_tmp;
  int z=0;
  while (z<laenge)
  {
    s_tmp = s.substr(z,1);
    c_s[z] = *s_tmp.c_str();
    z++;
  }
  c_s[laenge]=0;
  int i_s=0;

  // convert to char[]
  i_s = atoi(&c_s[0]);
  return i_s;
}
*/
// end of file

