// filehandle.h

// (C) 2000 Martin Henne
// EMail: Martin.Henne@web.de

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

// Diese Klasse hilft bei Zugriffen auf Textdateien. Die unterschiedlichen
// Methoden koennen Zeilen an Dateien anfuegen, Zeilen in Dateien einfuegen,
// Zeilen aus Dateien loeschen, Zeilen austauschen, eine Datei anlegen bzw.
// loeschen, Zeilen finden usw. ...

// Angaben ueber Zeilennummern beginnen IMMER bei 1, nicht bei 0!
// Mit anderen Worten: Die erste Zeile ist Zeile 1, die zweite 2 usw.



#ifndef _FILEHANDLE_H
#define _FILEHANDLE_H

#include <globals.h>
#include <string>
using namespace std;

// class name is also 'Textfile' due to typedef

class FileHandle
{
public:
  // Zeile an File anfuegen (am Fileende)
  static void appendLine       (const string& filename,
                                const string& line);
  // w.o., aber ohne Carriage Return (funzt nich so recht)
  static void append           (const string& filename,
                                const string& line);
  // Zeile einfuegen an Stelle der Zeile 'place'
  static void insertLine       (const string& filename,
                                const string& line,
                                const int& place);
  // Alle Zeilen in File loeschen. Damit kann man auch Files erzeugen
  static void clearFile        (const string& filename);
  // zeile mit 'sub'-string in 'filename'-File finden.
  // Gibt die erste gefundene Zeile zurueck
  // '(not found)' falls nicht gefunden
  static string findLine       (const string& filename,
                                const string& sub);

  // Zeilennummer ermitteln, erste Zeile = 1
  static int findLineNumber    (const string& filename,
                                const string& line);
  // File auf STDOUT ausgeben
  static void showFile         (const string& filename);
  // suche ' name = "value" ' - Zeile und gib value zurueck
  // value() erkennt '#'-Zeichen als Kommentar-Anfang
  // gibt "(error)", wenn file nicht geoeffnet werden kann
  // gibt "(not found)", wenn 'name' nicht existiert.
  // gibt "(undefined)", wenn kein wert vergeben wurde
  static string value          (const string& filename,
                                const string& name);	
  // entferne Leerzeichen vorne und hinten am string
  static string leadtrail      (const string& s);
  // Aus oldline wird newline -> Vertauschung
  static void swapLine         (const string& filename,
                                const string& oldline,
                                const string& newline);
  // liefere x-te Zeile im File (erste Zeile = 1)
  static string getLine        (const string& file,
                                const int& linenumber);
  // Zeilen zaehlen
  static int countLines        (const string& filename);
  // Existiert das File und kann es gelesen werden?
  static bool exists           (const string& filename);
  // loescht die x-te Zeile (erste Zeile=1)
  static void killLine         (const string& filename,
                                const int& linenumber);
  // liefert die Dateigroesse in Byte
  static unsigned long getsize (const string& filename);
};

typedef FileHandle Textfile;

#endif
