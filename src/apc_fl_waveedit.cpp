// $Id: apc_fl_waveedit.cpp,v 1.96 2002/02/16 20:00:50 martinhenne Exp $

// Copyright 2001 Martin Henne 2001
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

/*
 * See 'apc_fl_waveedit.h' for a description of that class
 */

#include <iostream>
#include <strstream>
#include <vector>
#include <cstdlib>
#include <pthread.h>
#include <guiheaders.h>
#include "wavefile.h"
#include "apc_fl_waveedit.h"
#include "datachunk.h"
#include "fmtchunk.h"
#include "passtwo.h"
#include "editwindow.h"
#include "configfile.h"
#include <lendian.h>
#include <popups.h>
#include <drawcalc.h>
#include <commandline.h>
#include <debug.h>
#include <settings.h>
#include <sleeper.h>

// this fixed a mingw32 bug in my version
#ifdef WIN32
//  typedef int pid_t;
#endif



//------ forward declarations ------//
//class Editwindow;


namespace APC
{
using namespace std;
  // construct, destruct
  Apc_fl_waveedit::Apc_fl_waveedit(int x, int y, int w, int h) : Fl_Widget(x,y,w,h)
  {
    Debug::msg<string>("Apc_fl_waveedit: constructing\n",Globals::DBG_FUNCTION_TRACE);
    use_old_drawcode = false;
    drawing      = false;
    dcalc        = new Drawcalc;
    dataAssigned = false;    // no wavefile passed yet
    zoomFactor   = 100;      // default value
    drawStart    = 0;        // draw wavefile from the beginning
    markStart    = 0;
    rastercolor  = 42;       // 42 is a light gray
    selcolor     = FL_GREEN;
    selbgcolor   = 15;       // 27 is dark green, 15 is blue
    zerocolor    = FL_RED;   // NULL - line
    unselcolor   = 30;       // dark gray
    unselbgcolor = FL_BLACK; // black
    cursorcolor  = FL_WHITE;
    zerocolor    = Settings::NULL_LINE_COLOR;
    rastercolor  = Settings::RASTER_COLOR;
    selbgcolor   = Settings::SELECTED_BACKGROUND_COLOR;
    selcolor     = Settings::SELECTED_WAVEDATA_COLOR;
    unselcolor   = Settings::UNSELECTED_WAVEDATA_COLOR;
    unselbgcolor = Settings::UNSELECTED_BACKGROUND_COLOR;
    cursorcolor  = Settings::CURSOR_COLOR;
    linecolor    = selcolor;
    backcolor    = selbgcolor;
    cursorPosition = 0;
    dragging     = false;
    dragleft     = false;
    dragright    = false;
    Debug::msg<string>("Apc_fl_waveedit: constructed\n",Globals::DBG_FUNCTION_TRACE);
  }






//----------------------------------------------------------------------------//

  Apc_fl_waveedit::~Apc_fl_waveedit               ()
  {
    Debug::msg<string>("Wavedraw: destructor\n",Globals::DBG_FUNCTION_TRACE);
  }







//----------------------------------------------------------------------------//

  void        Apc_fl_waveedit::draw               ()
  {
    Debug::msg<string>("Apc_fl_waveedit: draw() entered\n",Globals::DBG_FUNCTION_TRACE);
    if(drawing) return;

    drawing = true;

    // recording drawmode?
    if(editwindow->is_recording()) {
      this->draw_while_recording();
      drawing = false;
      return;
    }

    // drawing is impossible, if drawcalc is still calculating
    if(dcalc->calculating)
    {
      const char scanning_wave[] = "If this text doesn't disappear,\nplease report a bug to Martin.Henne@web.de";
      fl_font(FL_HELVETICA,20);
      fl_draw(scanning_wave,0,0,w(),h(),FL_ALIGN_CENTER);
      drawing = false;
      return;
    }

    // recalculate important values (in case of window resize and stuff...)
    if(this->h() != this->heigth || this->heigth < 255)
    {
      this->heigth = this->h();    // get heigth
    }
    if(this->w() != width || this->width < 700)
    {
      this->width  = this->w();    // get width
    }

    // visualFrames depends on the zoomFactor, the window width and the waveSize !!!
    visualFrames = static_cast<int>(((waveSize*1.0)/frameSize)/(zoomFactor/100.0));
    this->XSTEP  = visualFrames/(width*1.0);
    screen_markStart = frame2screen(getmarkStart());
    screen_markEnd   = frame2screen(getmarkEnd());

    // TODO: check, why is this necessary?
    if (zoomFactor==100)
      setdrawStart(0);

    if(XSTEP < 1.0)
    {
      XSTEP = 1.0;
    }

    if(dataAssigned==true) // only if a we have a wavefile
    {


      // draw the background
      this->draw_background();
      // draw the raster
      if(Settings::DRAW_RASTER) {
        this->draw_hraster();
        this->draw_vraster();
      }

      // draw NULL lines
      this->draw_null_lines();

      // wait for calculations of 'Drawcalc'
      if(dcalc->calculating)
      {
        while(dcalc->calculating)
        {
          Fl::wait(0.1);
          Sleeper::dosleep(10);
          Debug::msg<string>("Wavedraw::draw(): waiting for calculation\n",Globals::DBG_DRAWCALC_LOW);
        }
      }

      // draw the wave itself
      if(XSTEP>(2.5*(dcalc->FIXSTEP)) && use_old_drawcode==false)
        this->draw_virtual_wave();
      else
        this->draw_wave();

      this->draw_marks();

      // draw labels
      this->draw_labels();

      // draw the cursor
      this->draw_cursor();
    }
  drawing = false;
  Debug::msg<string>("Apc_fl_waveedit: leaving draw()\n",Globals::DBG_FUNCTION_TRACE);
  } // end of draw()

//----------------------------------------------------------------------------//

  void Apc_fl_waveedit::draw_while_recording() {
    int width  = this->w();
    int heigth = this->h();
    fl_color(Settings::SELECTED_BACKGROUND_COLOR);
    fl_rectf(0+x(),0+y(),w(),h());
    fl_color(Settings::DIGIT_COLOR);
    fl_font(FL_HELVETICA,18);
    fl_draw(">>> recording <<<",0,y()+30,w(),heigth*0.25,FL_ALIGN_CENTER);
    fl_font(FL_HELVETICA,16);
    fl_draw((string("recording pos: ")+frame2time(cursorPosition)).c_str(),0,y()+heigth*0.75-10,w(),20,FL_ALIGN_CENTER);
    fl_draw((string("recording time: ")+frame2time(cursorPosition-markStart)).c_str(),0,y()+heigth*0.75+10,w(),20,FL_ALIGN_CENTER);

    ulong startframe = 0;
    if(cursorPosition-width>0)
      startframe = cursorPosition-(width*frameSize);
    Data_chunk& dc = editwindow->wfile->getdata();
    int xcount = 0; double oldamp = 0.0;
    fl_color(Settings::SELECTED_WAVEDATA_COLOR);


    for(int i=0;i<width*frameSize;i+=frameSize) {
      double amp = dc.getAmplitude(i+startframe,bitRate,channels,1);
      fl_line(x()+xcount++,(int)(y()+heigth/2+oldamp*heigth/2),
              x()+xcount,(int)(y()+heigth/2+amp*heigth/2));
      oldamp = amp;
    }
/*
    for(int i=0;i<width*frameSize;i+=frameSize) {
      double amp = dc.getAmplitude(i+startframe,bitRate,channels,1);
      fl_point(x()+xcount++,(int)(y()+heigth/2+amp*heigth/2));
    }
*/

    return;
  }

//----------------------------------------------------------------------------//


  void Apc_fl_waveedit::draw_labels()
  {
    if(channels == 2) {
//      fl_color(Settings::CURSOR_COLOR);
      fl_color(Settings::DIGIT_COLOR);
//      fl_font(FL_HELVETICA_BOLD,11);
      fl_font(FL_HELVETICA,11);
      fl_draw("left channel",x()+5,y()+5,w(),20,FL_ALIGN_LEFT);
      fl_draw("right channel",x()+5,y()+h()/2+5,w(),20,FL_ALIGN_LEFT);
    }
    if(channels == 1) {
      fl_color(Settings::DIGIT_COLOR);
      fl_font(FL_HELVETICA_BOLD,11);
      fl_draw("(mono)",x()+5,y()+5,w(),20,FL_ALIGN_LEFT);
    }
  }




//----------------------------------------------------------------------------//

  void Apc_fl_waveedit::draw_background                ()
  {
    Debug::msg<string>("Apc_fl_waveedit: draw_background() entered\n",Globals::DBG_FUNCTION_TRACE);
    // two boxes for unselected data
    // (drawStart to markStart, markEnd to drawEnd)
    fl_color(Settings::UNSELECTED_BACKGROUND_COLOR);
    if(screen_markStart>0)
      fl_rectf(0,+y(),screen_markStart,h());
    if(screen_markEnd<w())
      fl_rectf(screen_markEnd,+y(),w(),h());
    // one box for selected data
    // (from markStart to markEnd)
    fl_color(Settings::SELECTED_BACKGROUND_COLOR);
    fl_rectf(screen_markStart,
             +y(),
             (screen_markEnd-screen_markStart+pix_per_sample),
             h());
    Debug::msg<string>("Apc_fl_waveedit: leaving draw_background()\n",Globals::DBG_FUNCTION_TRACE);
  }

//----------------------------------------------------------------------------//

  void Apc_fl_waveedit::draw_marks                ()
  {
    Debug::msg<string>("Apc_fl_waveedit: draw_marks() entered\n",Globals::DBG_FUNCTION_TRACE);
    fl_color(Settings::MARKS_COLOR);
    fl_line(screen_markStart,y(),screen_markStart,y()+h());
    fl_line(screen_markEnd,y(),screen_markEnd,y()+h());
    Debug::msg<string>("Apc_fl_waveedit: draw_marks() leaving\n",Globals::DBG_FUNCTION_TRACE);
  }

//----------------------------------------------------------------------------//

  void Apc_fl_waveedit::draw_hraster               ()
  {
    Debug::msg<string>("Apc_fl_waveedit: draw_hraster() entered\n",Globals::DBG_FUNCTION_TRACE);
    // draw a raster
    int hsegments    = 20 * channels ; // works with channels, but looks ugly.

    fl_color(Settings::RASTER_COLOR);

    // horizontal raster lines
    double line_step = 1.0 / hsegments;
    double next_line = line_step;
    for(int i=0; i<hsegments; i++)
    {
      fl_line(0,(heigth*next_line)+y(),width,(heigth*next_line)+y());
      next_line += line_step;
    }

    Debug::msg<string>("Apc_fl_waveedit: draw_hraster() leaving\n",Globals::DBG_FUNCTION_TRACE);
    return;
  }

//----------------------------------------------------------------------------//

  void Apc_fl_waveedit::draw_vraster               ()
  {
    Debug::msg<string>("Apc_fl_waveedit: draw_vraster() entered\n",Globals::DBG_FUNCTION_TRACE);

    int hstep        = 0 /*width/vsegments*/;
    // this is to set the vertical lines to useful positions
    double visibleSeconds = (1.0*visualFrames) / frequency;

    if(visibleSeconds>600.0 && hstep==0) {
      hstep = ms2pix(60*1000);           // every 60 seconds
    }
    if(visibleSeconds>300.0 && hstep==0) {
      hstep = ms2pix(30*1000);           // every 30 seconds
    }
    if(visibleSeconds>150.0 && hstep==0) {
      hstep = ms2pix(15*1000);           // every 15 seconds
    }
    if(visibleSeconds>100.0 && hstep==0) {
      hstep = ms2pix(30*1000);           // every 10 seconds
    }
    if(visibleSeconds>50.0 && hstep==0) {
      hstep = ms2pix(10*1000);            // every 5 seconds
    }
    if(visibleSeconds>25.0 && hstep==0) {
      hstep = ms2pix(5*1000);            // every 2 seconds
    }
    if(visibleSeconds>12.0 && hstep==0) {
      hstep = ms2pix(2*1000);            // every second
    }
    if(visibleSeconds>6.0 && hstep==0) {
      hstep = ms2pix(1000);               // every 0.5 seconds
    }
    if(visibleSeconds>2.4 && hstep==0) {
      hstep = ms2pix(500);               // every 0.2 seconds
    }
    if(visibleSeconds>1.0 && hstep==0) {
      hstep = ms2pix(200);               // every 0.1 seconds
    }
    if(visibleSeconds>0.5 && hstep==0) {
      hstep = ms2pix(100);               // every 0.05 seconds
    }
    if(visibleSeconds>0.2 && hstep==0) {
      hstep = ms2pix(50);               // every 0.02 seconds
    }

    if(hstep == 0)
      hstep = ms2pix(10);

    string digit = "0:00.000";
    int linecounter = 0;

    fl_color(Settings::RASTER_COLOR);
    // vertical raster lines
    for (int i=0; i<width; i+=hstep) //##
    {
      fl_color(Settings::RASTER_COLOR);
      fl_line(i,0+y(),i,heigth+y());
      if(Settings::DRAW_DIGITS) {
        digit = frame2time(screen2frame(i));
        draw_digits(i,digit.c_str());
      }
      ++linecounter;
    }

    Debug::msg<string>("Apc_fl_waveedit: draw_vraster() leaving\n",Globals::DBG_FUNCTION_TRACE);
    return;
  }

//----------------------------------------------------------------------------//

  void Apc_fl_waveedit::draw_digits(const int& pix, const char* text)
  {
    fl_color(Settings::DIGIT_COLOR);
    fl_font(FL_HELVETICA,9);
    fl_draw(text,x()+pix,y()+h()-20,20,20,FL_ALIGN_LEFT);
  }


//----------------------------------------------------------------------------//

  // now draw the NULL-Line (the 'horizon' of digital silence)
  void Apc_fl_waveedit::draw_null_lines()
  {
    fl_color(Settings::NULL_LINE_COLOR);
    switch(channels)
    {
      case 1 : fl_line(0,heigth/2+y(), width, heigth/2+y()); break;
      case 2 : fl_line(0,heigth*0.25+y(), width, heigth*0.25+y());
               fl_line(0,heigth*0.75+y(), width, heigth*0.75+y());
               fl_color(Settings::MAIN_COLOR); // channel-divider
               fl_line(0,heigth*0.50+y(), width, heigth*0.50+y());
               break;
    }
  }




//----------------------------------------------------------------------------//

  void Apc_fl_waveedit::draw_virtual_wave()
  {
    Debug::msg<string>("Apc_fl_waveedit: draw_virtual_wave() entered\n",Globals::DBG_FUNCTION_TRACE);
    vector<short>& ddata = dcalc->getdrawdata();
    int x_coord = 0;
    int oldminL = 0xffff;
    int oldmaxL = 0;
    int oldminR = 0xffff;
    int oldmaxR = 0;

    for(unsigned long i=0; i<ddata.size(); i+=(2*channels))
    {
      x_coord = i/(2*channels);
      // choose draw color depending on selection
      if((screen2frame(x_coord)<getmarkStart() ||
          screen2frame(x_coord)>getmarkEnd())  &&
          linecolor == selcolor)
        linecolor = unselcolor;
      if(screen2frame(x_coord)>=getmarkStart() &&
         screen2frame(x_coord)<=getmarkEnd()   &&
         linecolor == unselcolor)
        linecolor = selcolor;

      // set draw color
      fl_color(linecolor);

      // draw left channel (or mono channel)
      fl_line  (x_coord,ddata[i+0]+y(),x_coord,ddata[i+1]+y());
      // draw right channel
      if(channels==2)
        fl_line(x_coord,ddata[i+2]+y(),x_coord,ddata[i+3]+y());
      // next pixel

/*
      // fillines
      if(pix_per_sample == 1 && x_coord<(ddata.size()-channels*2))
      {
        if(ddata[i+0] > oldmaxL)
          fl_line(x_coord,oldmaxL+y(),x_coord+1,ddata[i+0]+y());
        if(ddata[i+1] < oldminL)
          fl_line(x_coord,oldminL+y(),x_coord+1,ddata[i+1]+y());
        if(channels==2)
        {
          if(ddata[i+2] > oldmaxR)
            fl_line(x_coord,oldmaxR+y(),x_coord+1,ddata[i+2]+y());
          if(ddata[i+3] < oldminR)
            fl_line(x_coord,oldminR+y(),x_coord+1,ddata[i+3]+y());
        }
      }
*/
      oldminL = ddata[i+0];
      oldmaxL = ddata[i+1];
      if(channels==2)
      {
        oldminR = ddata[i+2];
        oldmaxR = ddata[i+3];
      }
      //x_coord++;
    }
    Debug::msg<string>("Apc_fl_waveedit: leaving draw_virtual_wave()\n",Globals::DBG_FUNCTION_TRACE);
    return;
  }

//----------------------------------------------------------------------------//
//##
  void Apc_fl_waveedit::draw_close_wave()
  {
    Debug::msg<string>("Apc_fl_waveedit: draw_close_wave() entered\n",Globals::DBG_FUNCTION_TRACE);
    /*
     *  remark: :.draw() guarantees, that the following values
     *  are correct: width, heigth, visualFrames, screen_markStart/_markEnd
     */
//    Data_chunk& dc = data->getdata();
//    vector<uchar>& vec = dc.getwave();

//    double ratio = width / (visualFrames*1.0);

    Debug::msg<string>("Apc_fl_waveedit: draw_close_wave() leaving\n",Globals::DBG_FUNCTION_TRACE);
  }







//----------------------------------------------------------------------------//

  void Apc_fl_waveedit::draw_wave                 ()
  {
    Debug::msg<string>("Apc_fl_waveedit: draw_wave() entered\n",Globals::DBG_FUNCTION_TRACE);
    //cerr << "::draw_mono_wave(): called\n";
    Data_chunk            &dc          = data->getdata();
    vector<unsigned char> &wave        = dc.getwave();

    // initialize start- and endpoints for lines:
    int       x1      = 0;    // x position
    int       x2      = 0;
    int       newMaxL = 0;    // y position
    int       newMinL = 0;
    int       oldMinL = 0;
    int       oldMaxL = 0;
    int       newMaxR = 0;    // y position
    int       newMinR = 0;
    int       oldMinR = 0;
    int       oldMaxR = 0;
    short     tmpL    = 0;    // for temporary sample values
    short     tmpR    = 0;
/*
    // initialize pitchlevel to zeroline
    switch (bitRate)
    {
      case 8:  newMinL = newMinR = 0x0;    newMaxL = newMaxR = 0xff;  tmpL = tmpR = 0x80;
      case 16: newMinL = newMinR = -32767; newMaxL = newMaxR = 32767; tmpL = tmpR = 0x0;
    }
*/
    // main drawing loop
    for(unsigned long i=drawStart*frameSize; i<drawEnd*frameSize; i+=static_cast<int>(XSTEP*frameSize))
    {
      switch (bitRate)
      {
        case 8:  newMinL = newMinR = 0xff;  newMaxL = newMaxR = 0;      break;
        case 16: newMinL = newMinR = 32767; newMaxL = newMaxR = -32767; break;
      }
      // calculate extrema in intervall [i,i+XSTEP]
      if ((i+XSTEP) <= (waveSize-waveSize%frameSize))
      {
        // in case it's a mono wave:
          for(unsigned long ii=(i-i%frameSize); ii<(i+XSTEP*frameSize); ii+=frameSize)
          {
            switch (bitRate)
            {
              case 8:  tmpL = wave[ii]; break;
              case 16: lendian(wave[ii], wave[ii+1], tmpL); break;
              default: cerr << "Wavedraw::draw_wave(): you should not see this (1)\n";
            }

            newMaxL = (tmpL > newMaxL ? tmpL : newMaxL);
            newMinL = (tmpL < newMinL ? tmpL : newMinL);
/*
            if(tmpL>newMaxL)
              newMaxL = tmpL;
            if(tmpL<newMinL)
              newMinL = tmpL;

*/
        }
        // And this is for stereo
        if(channels == 2)
        {
          for(unsigned long ii=(i-i%frameSize); ii<(i+XSTEP*frameSize); ii+=frameSize)
          {
            switch (bitRate)
            {
              case 8:  tmpR = wave[ii+1];
                       break;
              case 16: lendian(wave[ii+2], wave[ii+3], tmpR);
                       break;
              default: cerr << "Wavedraw::draw_wave(): you should not see this (2)\n";
            }

            newMaxR = (tmpR > newMaxR ? tmpR : newMaxR);
            newMinR = (tmpR < newMinR ? tmpR : newMinR);
/*
            if(tmpR>newMaxR)
              newMaxR = tmpR;
            if(tmpR<newMinR)
              newMinR = tmpR;
*/
          }

        }
      }

      // We have the extremums. Now shrink value to screen dimensions.
      // VMUTLI means 'vertical multiplicator'
      float VMULTI = 0;
      switch (bitRate)
      {
        case 8:  VMULTI = ((heigth/(channels*2.0))/(0x7f)); break;
        case 16: VMULTI = (heigth/(channels*2.0))/(0x7fff); break;
      }

      // now convert to vertical screen dimensions
      if(channels==1) // mono
      {
        switch(bitRate)
        {
          case 8 :  newMaxL = -1*(static_cast<int>(newMaxL*(VMULTI)))+(heigth);
                    newMinL = -1*(static_cast<int>(newMinL*(VMULTI)))+(heigth);
                    break;
          case 16 : newMaxL = -1*(static_cast<int>(newMaxL*(VMULTI)))+(heigth/2);
                    newMinL = -1*(static_cast<int>(newMinL*(VMULTI)))+(heigth/2);
                    break;
        }
      }
      else // stereo
      {
        switch(bitRate)
        {
          case 8 :  newMaxR = -1*(static_cast<int>(newMaxR*(VMULTI)))+(heigth);
                    newMinR = -1*(static_cast<int>(newMinR*(VMULTI)))+(heigth);
                    newMaxL = -1*(static_cast<int>(newMaxL*(VMULTI)))+(heigth/2);
                    newMinL = -1*(static_cast<int>(newMinL*(VMULTI)))+(heigth/2);
                    break;
          case 16 : newMaxL = -1*(static_cast<int>(newMaxL*(VMULTI)))+(int)(heigth*0.25);
                    newMinL = -1*(static_cast<int>(newMinL*(VMULTI)))+(int)(heigth*0.25);
                    newMaxR = -1*(static_cast<int>(newMaxR*(VMULTI)))+(int)(heigth*0.75);
                    newMinR = -1*(static_cast<int>(newMinR*(VMULTI)))+(int)(heigth*0.75);
                    break;
        }
      }

      // choose drawcolor depending on marked section
      // 'screen_mark....' are set by ::draw()
      if ( x1>=screen_markStart &&
           x1<=screen_markEnd   &&
           linecolor==unselcolor)
      {
        linecolor=selcolor;
      }
      if ( x1<screen_markStart ||
           x1>screen_markEnd   &&
           linecolor==selcolor)
      {
        linecolor=unselcolor;
      }
      fl_color(linecolor);

      for (int iii=0; iii<pix_per_sample; iii++)
      {/*
        // fill lines between sample frames
        if(oldMinL>newMaxL && pix_per_sample<=1)
        {
          fl_line(x1-1,oldMinL+y(),x1,newMaxL+y());
        }
        if(oldMaxL<newMinL && pix_per_sample<=1)
        {
          fl_line(x1-1,oldMaxL+y(),x1,newMinL+y());
        }*/
        // draw vertical line from max to min
        fl_line(x1,newMaxL+y(),x2,newMinL+y());

        if(channels==2)
        {
          if(oldMinR>newMaxR && pix_per_sample<=1)
          {
            fl_line(x1-1,oldMinR+y(),x1,newMaxR+y());
          }
          if(oldMaxR<newMinR && pix_per_sample<=1)
          {
            fl_line(x1-1,oldMaxR+y(),x1,newMinR+y());
          }
          // draw vertical line from max to min
          fl_line(x1,newMaxR+y(),x2,newMinR+y());
        }

        x1++;
        x2++;
      }

      oldMinL = newMinL; oldMinR = newMinR;
      oldMaxL = newMaxL; oldMaxR = newMaxR;
    }
    Debug::msg<string>("Apc_fl_waveedit: draw_wave() leaving\n",Globals::DBG_FUNCTION_TRACE);
    //------  end of new drawing code
  }





//----------------------------------------------------------------------------//

void Apc_fl_waveedit::draw_cursor                   ()
  {
    Debug::msg<string>("Apc_fl_waveedit: draw_cursor() (no leaving message)\n",Globals::DBG_FUNCTION_TRACE);
    fl_color(Settings::CURSOR_COLOR);
    // next two line are overlay code
    Editwindow* t_ew = static_cast<Editwindow*>(this->user_data());
    return t_ew->draw_cursor();

    // this is in case of no overlay!
    /*
    fl_color(Settings::CURSOR_COLOR);
    int x = frame2screen(getcursorPosition());
    fl_line(x,0+y(), x, heigth+y());
    */
  }






//----------------------------------------------------------------------------//

  void Apc_fl_waveedit::multiplyZoom                  (double arg)
  {
    Debug::msg<string>("Apc_fl_waveedit: multiplyZoom() entered\n",Globals::DBG_FUNCTION_TRACE);
    double tmp = zoomFactor;
    double tmp2 = this->setzoomFactor(tmp*arg);
    if(tmp2 != tmp)
    {
      Debug::msg<string>("Wavedraw::multiplyZoom(): zoomvals differ! \n");
    }
    Debug::msg<string>("Apc_fl_waveedit: multiplyZoom() leaving\n",Globals::DBG_FUNCTION_TRACE);
    return;
  }








//----------------------------------------------------------------------------//

  // no redrawing in this function!!!
  double Apc_fl_waveedit::setzoomFactor                  (double arg)
  {
    Debug::msg<string>("Apc_fl_waveedit: setzoomFactor() entered\n",Globals::DBG_FUNCTION_TRACE);
    if(pix_per_sample >=1.1 && arg > zoomFactor)
    {
      cerr << "::setzoomFactor(): maximum zoomFactor reached\n";
      Popups::error("maximum zoomfactor reached!","No Panic...");
      return arg;
    }
    Debug::msg<string>("Apc_fl_waveedit: setzoomFactor() checkpoint 1\n",Globals::DBG_FUNCTION_TRACE);
    if(arg<100)
      arg = 100;
    pix_per_sample=1;
    visualFrames = 100*static_cast<int>(((waveSize*1.0)/frameSize)/arg);
    Debug::msg<string>("Apc_fl_waveedit: setzoomFactor() checkpoint 2\n",Globals::DBG_FUNCTION_TRACE);
    if(visualFrames < getwidth())
    {
      if(visualFrames<=0)
        visualFrames = getwidth();
      Debug::msg<string>("Wavedraw::setzoomFactor(): visFrames, width: ",Globals::DBG_FUNCTION_TRACE);
      Debug::val(visualFrames);  Debug::val(getwidth());
      unsigned long new_visibleFrames = getwidth() / static_cast<int>(getwidth()/visualFrames);
      Popups::error("Maximum zoom factor reached!");
      arg = 100*(waveSize/(frameSize*new_visibleFrames));
      visualFrames = new_visibleFrames;
      pix_per_sample = getwidth() / visualFrames;
    }
    Debug::msg<string>("Apc_fl_waveedit: setzoomFactor() checkpoint 3\n",Globals::DBG_FUNCTION_TRACE);
    // ok, now change zoomFactor
    zoomFactor = arg;
    if (zoomFactor==100)
      this->setdrawStart(0);
    setdrawEnd(getdrawStart()+visualFrames);
    Debug::msg<string>("Apc_fl_waveedit: setzoomFactor() leaving\n",Globals::DBG_FUNCTION_TRACE);
    return arg;
  }






//----------------------------------------------------------------------------//

  double Apc_fl_waveedit::getzoomFactor      ()
  {
    return this->zoomFactor;
  }






//----------------------------------------------------------------------------//


  void Apc_fl_waveedit::resetzoomFactor           ()
  {
    zoomFactor=100;
    this->draw();
    Editwindow* handle = static_cast<Editwindow*>(this->user_data());
    handle->adjustScrollbar();
  }






//----------------------------------------------------------------------------//


  void Apc_fl_waveedit::updateDrawStatus()
  {
    // update statusBoxes of Editwindow

    Editwindow* handle = static_cast<Editwindow*>(this->user_data());
    handle->redraw_needed |= (handle->REDR_VIEWSTAT|handle->REDR_SELSTAT);
    handle->doRedraw();
    return;
  }








//----------------------------------------------------------------------------//

  // this does NO redraw!!!
  void Apc_fl_waveedit::setdrawStart              (unsigned long frame)
  {
  	//cerr << "Apc_fl_waveedit::setdrawStart(): called with value " << frame << endl;
    if(zoomFactor==100)
    {
      drawStart = 0;
      visualFrames = waveSize/frameSize;
      setdrawEnd(visualFrames);
      return;
    }

    if(frame<0) // too small
    {
      this->drawStart = 0;
    }

    if(frame>((waveSize/frameSize)-visualFrames)) // too big
    {
      unsigned long goodval = (waveSize/frameSize) - visualFrames;
      drawStart = goodval;
    }

    if(frame<=((waveSize/frameSize)-visualFrames) && frame>0) // ok
    {
      this->drawStart = frame/*-(frame%frameSize)*/; // adjust to next suitable frame
    }

    setdrawEnd(getdrawStart()+getvisualFrames());
    return;
 }







//----------------------------------------------------------------------------//

  unsigned long Apc_fl_waveedit::getdrawStart     ()
  {
    return drawStart;
  }







//----------------------------------------------------------------------------//


  void Apc_fl_waveedit::setdrawEnd                  (unsigned long frame)
  {
    if (frame <= waveSize/frameSize)
      this->drawEnd = frame/*-(frame%frameSize)*/;
  }






//----------------------------------------------------------------------------//

  unsigned long Apc_fl_waveedit::getdrawEnd         ()
  {
    return (this->drawEnd);
  }






//----------------------------------------------------------------------------//


  void Apc_fl_waveedit::setvisualFrames             (unsigned long frames)
  {
    if (frames<=(waveSize/frameSize) && frames>=0)
      visualFrames = frames/*-(frames%frameSize)*/;
    else
    {
      cerr << "Apc_fl_waveedit::setvisualFrames(): I don't do anything, "
           << frames << " doesn't make sense to me.\n";
    }
  }








//----------------------------------------------------------------------------//

  unsigned long Apc_fl_waveedit::getvisualFrames    ()
  {
    return (this->visualFrames);
  }







//----------------------------------------------------------------------------//


  unsigned long Apc_fl_waveedit::getmarkStart        ()
  {
    return (this->markStart);
  }





//----------------------------------------------------------------------------//

  unsigned long Apc_fl_waveedit::getmarkEnd       ()
  {
    if(markEnd <= waveSize)
      return (this->markEnd);
    else
      return (waveSize/frameSize);
  }




//----------------------------------------------------------------------------//

  double Apc_fl_waveedit::getXSTEP ()
  {
    return (this->XSTEP);
  }






//----------------------------------------------------------------------------//

  void Apc_fl_waveedit::doRedraw                  ()
  {
    Fl::lock();
    if(dcalc->calculating)
    {
      //Statusbar myBar("please wait","scanning wave...");
      //myBar.show();
      while(dcalc->calculating)
      {
        //myBar.set(dcalc->percent);
        Fl::wait(0.02);
        Fl::check();
      }
      //myBar.hide();
    }
    this->redraw();
    Fl::unlock(); Fl::awake();
  }







//----------------------------------------------------------------------------//

  string Apc_fl_waveedit::frame2time                 (unsigned long arg)
  {
    // note: time is only frame-depended, so this is legal
    return byte2time(arg);
  }








//----------------------------------------------------------------------------//

  unsigned long Apc_fl_waveedit::time2frame          (string)
  {
    // TODO: remove dirty hack
    unsigned long t_ret = 0x0; // temporary return value
    cerr << "Apc_fl_waveedit::time2frame(): not implemented, returning 0\n";
    return t_ret;
  }



//----------------------------------------------------------------------------//

  int Apc_fl_waveedit::frame2screen                   (unsigned long frame)
  {
    return byte2screen(frame*frameSize);
  }




//----------------------------------------------------------------------------//

  unsigned long Apc_fl_waveedit::screen2frame          (int x)
  {
    return (screen2byte(x)/frameSize);
  }





//----------------------------------------------------------------------------//


  // this is actually 'frame2time()', cause time is
  // only frame dependend
  string Apc_fl_waveedit::byte2time                 (int bytes)
  {
    unsigned int min = 0, sec = 0, msec = 0;            // minute, seconds, miliseconds
    sec  = static_cast<int>(bytes/frequency);          // ALL seconds
    min  = static_cast<int>(sec/60);                    // minutes
    msec = static_cast<int>((bytes-(sec*frequency))/(static_cast<int>(frequency/1000)));
    sec  = static_cast<int>(bytes/frequency)-min*60;   // remaining seconds
    // now let's build a string like that: "01:01.150" (minutes:seconds.miliseconds)
    string s_min,s_sec,s_msec;
    if (min <  1) s_min  = "00";
    if (min < 10 && min >= 1) s_min  = "0"    + stream_cast<string>(min);
    if (min >=10) s_min  =                      stream_cast<string>(min);
    if (sec <  1) s_sec  = "00";
    if (sec < 10 && sec >= 1) s_sec  = "0"    + stream_cast<string>(sec);
    if (sec >=10) s_sec  =                      stream_cast<string>(sec);
    if (msec<  1) s_msec = "000";
    if (msec< 10 && msec >= 1) s_msec = "00"  + stream_cast<string>(msec);
    if (msec<100 && msec >= 10) s_msec = "0"  + stream_cast<string>(msec);
    if (msec>=100) s_msec =                     stream_cast<string>(msec);
    string ret = s_min + ":" + s_sec + "." + s_msec;
    return ret;
  }








//----------------------------------------------------------------------------//

  unsigned long Apc_fl_waveedit::screen2byte      (int x)
  {
    unsigned long pointed_frame;

    if (pix_per_sample > 1) // for close zoomFactors
      pointed_frame = x/pix_per_sample;
    else
      pointed_frame = static_cast<int>(x*(getvisualFrames()/(width*1.0)));
    unsigned long  long_byte          = (getdrawStart()+pointed_frame) * frameSize;
    return long_byte;
  }







//----------------------------------------------------------------------------//


  int Apc_fl_waveedit::byte2screen                (unsigned long byte)
  {

    if (byte<=(getdrawStart()*frameSize))
    {
      return 0;
    }

    if (byte>=(getdrawEnd()*frameSize))
    {
      return this->w();
    }

    byte         -= (getdrawStart()*frameSize);
    // ATTENTION!!! '(byte*width)' can cause an overflow!
    // therefore 'byte' is outside the brackets!!!
    double screenX   = byte*((getwidth()*1.0)/(visualFrames*frameSize*1.0));
    return static_cast<int>(screenX/**pix_per_sample*/);
  }





//----------------------------------------------------------------------------//


  // this does NO redraw!!!
  void Apc_fl_waveedit::setcursorPosition              (unsigned long frame)
  {
    if (frame==cursorPosition) // don't work more than necessary :-)
    {
      return;
    }

    if (frame>(waveSize/frameSize)) // is it behind the last frame?
    {
      return;
    }
    cursorPosition = frame;
  }






//----------------------------------------------------------------------------//

  unsigned long Apc_fl_waveedit::getcursorPosition              ()
  {
    return cursorPosition;
  }




//----------------------------------------------------------------------------//


  // This does no redraw!
  void Apc_fl_waveedit::relabel_cursorPosition         ()
  {
  	Editwindow* tmpEW = (Editwindow*)(this->user_data());
  	tmpEW->relabel(this->frame2time(this->cursorPosition), "statusBox_cpos");
    tmpEW->redraw_needed |= tmpEW->REDR_CSRPOS;
  }







//----------------------------------------------------------------------------//


  // filling the wave
  void        Apc_fl_waveedit::setdata            (Wavefile* arg)
  {
    Debug::msg<string>("Wavedraw::setdata(): called\n",Globals::DBG_FUNCTION_TRACE);
    width              =  this->w();
    data               =  arg;   // assign wavedata
    Fmt_chunk&  fmt    =  data->getfmt();
    Data_chunk& dc     =  data->getdata();
    waveSize           =  dc.getchunkSize();
    frequency         =  fmt.getdwSamplesPerSec();
    bitRate            =  fmt.getwBitsPerSample();
    channels           =  fmt.getwChannels();
    zoomFactor         =  100; // 100%
    drawStart          =  0;
    frameSize          =  (channels*bitRate)/8;
    drawEnd            =  waveSize/frameSize;
    markStart          =  0;
    markEnd            =  drawEnd;
    visualFrames       =  waveSize/frameSize;
    XSTEP              =  visualFrames/(1.0*width);
    pix_per_sample     =  1;     // TODO: wrong for waveSize < width !!!
    dcalc             ->  init(this);
    setcursorPosition(frameSize);
    dataAssigned       =  true;  // now it is assigned
    dcalc             ->  data_changed();
    Debug::msg<string>("Wavedraw::setdata(): leaving\n",Globals::DBG_FUNCTION_TRACE);
  }






//----------------------------------------------------------------------------//

  void Apc_fl_waveedit::init (Editwindow* ew)
  {
    editwindow = ew;
    return (this->setdata(ew->wfile));
  }





//----------------------------------------------------------------------------//

  void Apc_fl_waveedit::set_user_data(void* v)
  {
    this->user_data(v);
  }





//----------------------------------------------------------------------------//

  Wavefile* Apc_fl_waveedit::getdata ()
  {
    return data;
  }




//----------------------------------------------------------------------------//


  int Apc_fl_waveedit::getwidth()
  {
    return this->w();
  }






//----------------------------------------------------------------------------//

  void Apc_fl_waveedit::setmarks_drag             ()
  {
    if(!dragging)
      dragging = true;

    int x = Fl::event_x(); // get mouse-position;
    int state = Fl::event_state();

    if (Fl::event_button() == FL_MIDDLE_MOUSE)
    {
      setcursorPosition(screen2frame(x));
      this->relabel_cursorPosition();
      ((Editwindow*)this->user_data())->draw_cursor();
    }

    if (Fl::event_is_click())
    {
      dragStart = x;
    }

    // check if waveform has to scroll left, when mouse leaves window to the left
    if (x <= 0)
    {
      if(!dragleft) {
        dragleft=true;
        dragStart=w()-1;
      }
      int tmp_x = x * (-1);
      unsigned long newval = (this->getdrawStart())-(screen2frame(tmp_x)-(this->getdrawStart()));
      if (newval > (waveSize*frameSize))
        newval = 0;
      this->setdrawStart(newval);
      this->setmarkStart(newval);
      this->editwindow->adjustScrollbar();
      this->doRedraw();
      return;
    }


    // mouse leaves the window to the right, scroll right
    if (x > w())
    {
      if(!dragright) {
        dragright=true;
        dragStart=1;
      }
      int diff = screen2frame(x)-getdrawEnd();
      Debug::msg<string>("Wavedraw::setmarks_drag(): rightscroll, diff = ",Globals::DBG_DRAG);
      Debug::val_cr(diff,Globals::DBG_DRAG);
      this->setdrawStart(this->getdrawStart()+diff);
      this->setmarkEnd(this->getdrawEnd());
      this->editwindow->adjustScrollbar();
      this->doRedraw();
      return;
    }

    if (((state&FL_BUTTON1)==FL_BUTTON1) &&  // left button pressed
         (state&FL_SHIFT)!=FL_SHIFT)         // no shift pressed
    {
      if (dragStart<=x)
      {
        setmarkEnd(screen2frame(x));
        setmarkStart(screen2frame(dragStart));
        this->doRedraw();
      }
      else
      {
        setmarkStart(screen2frame(x));
        setmarkEnd(screen2frame(dragStart));
        this->doRedraw();
      }
    }


    if ((state&(FL_BUTTON1|FL_SHIFT))==(FL_BUTTON1|FL_SHIFT)) // left and shift
    {
      setcursorPosition(screen2frame(x));
      this->relabel_cursorPosition();
      ((Editwindow*)this->user_data())->draw_cursor();
    }

    this->updateDrawStatus();
    return;
  }








//----------------------------------------------------------------------------//


  void Apc_fl_waveedit::setmarks_push             ()
  {
    // are we still busy with dragging?
    if(dragging)
      return;

    int screenCoord = Fl::event_x();
    int state = Fl::event_state();

    dragStart = screenCoord;

    if (Fl::event_button() == FL_RIGHT_MOUSE)
    {
      setmarkEnd(screen2frame(screenCoord));
      this->doRedraw();
      this->updateDrawStatus();
    }

    if ((Fl::event_button() == FL_LEFT_MOUSE) &&
        ((state&FL_SHIFT)!=FL_SHIFT))
    {
      setmarkStart(screen2frame(screenCoord));
      this->doRedraw();
      this->updateDrawStatus();
    }

    if (((Fl::event_button() == FL_LEFT_MOUSE) &&
        ((state&FL_SHIFT)==FL_SHIFT)) ||
        Fl::event_button() == FL_MIDDLE_MOUSE)
    {
      setcursorPosition(screen2frame(screenCoord));
      this->relabel_cursorPosition();
      ((Editwindow*)this->user_data())->draw_cursor();
      this->updateDrawStatus();
    }

  }








//----------------------------------------------------------------------------//

  int Apc_fl_waveedit::handle                     (int anyEvent)
  {
    switch (anyEvent)
    {
      case FL_DRAG : // dragging with the mouse
        this->setmarks_drag(); return anyEvent;
      case FL_PUSH : // a single pressed mousebutton
        if(Fl::event_clicks() != 0) {
          this->setmarkStart(0);
    			this->setmarkEnd(waveSize/frameSize);
    			this->editwindow->redraw_needed = this->editwindow->REDR_ALL;
    			this->editwindow->doRedraw();
  		  	this->window()->redraw();
		    	return 0;
        }
	      this->setmarks_push(); return anyEvent;
      case FL_SHORTCUT : // keyboard shortcut
        return this->shortcut(anyEvent);
      case FL_RELEASE : // mouse button is released: no dragging anymore
        this->dragging  = false;
        this->dragleft  = false;
        this->dragright = false;
        return anyEvent;
    }

    return 0;
  }






//----------------------------------------------------------------------------//

  void Apc_fl_waveedit::setmarkStart               (unsigned long frame)
  {
    if(frame>(waveSize/frameSize)) {
      Debug::msg<string>("Wavedraw::setmarkStart(): got bad frame\n");
//      markStart = (waveSize/frameSize)-frameSize;
      return;
    }
    if(frame<=getmarkEnd() && !dragright) {
      this->markStart = frame;
      return;
    }
    // we shouldn't be here:
    Debug::msg<string>("Wavedraw::setmarkStart(): FIX THIS BUG!!!\n");
  }




//----------------------------------------------------------------------------//

  void Apc_fl_waveedit::setmarkEnd                 (unsigned long frame)
  {
    if(frame>(waveSize/frameSize)) {
      Debug::msg<string>("Wavedraw::setmarkEnd(): got bad frame\n");
//      markEnd = (waveSize/frameSize);
      return;
    }
    if(frame>=getmarkStart() &&
       frame<=(waveSize/frameSize) &&
       !dragleft) {
      this->markEnd=frame;
      return;
    }
    // we shouldn't be here:
    Debug::msg<string>("Wavedraw::setmarkEnd(): FIX THIS BUG!!!\n");
  }







//----------------------------------------------------------------------------//

  void Apc_fl_waveedit::setwaveSize                (unsigned long size)
  {
    // TODO: check a few things, don't thrust 'size'
    waveSize = size;
  }









//----------------------------------------------------------------------------//

  int Apc_fl_waveedit::getframeSize                ()
  {
    return this->frameSize;
  }




//----------------------------------------------------------------------------//

  int Apc_fl_waveedit::getpixpersample             ()
  {
    return this->pix_per_sample;
  }






//----------------------------------------------------------------------------//

  void Apc_fl_waveedit::dumpDrawData              ()
  {
    std::cerr << "<-- dumping Wavedraw's values -->\n";
    cerr << "drawStart:      " << this->getdrawStart() << endl;
    cerr << "drawEnd:        " << this->getdrawEnd() << endl;
    cerr << "width:          " << this->getwidth() << endl;
    cerr << "we->w():        " << this->w() << endl;
    cerr << "visualFrames:   " << this->getvisualFrames() << endl;
    cerr << "XSTEP:          " << this->getXSTEP() << endl;
    cerr << "pix_per_sample: " << this->getpixpersample() << endl;
    cerr << "zoomFactor:     " << this->getzoomFactor() << endl;
    cerr << "markStart:      " << this->getmarkStart() << endl;
    cerr << "markEnd:        " << this->getmarkEnd() << endl;
    cerr << "cursorPosition: " << this->getcursorPosition() << endl;
    cerr << "bitRate:        " << this->bitRate << endl;
    cerr << "channels:       " << this->channels << endl;
    cerr << "frameSize:      " << this->frameSize << endl;
    cerr << "waveSize:       " << this->waveSize << endl;
    cerr << "<-- Wavedraw's vals dumped -->\n";
  }



//----------------------------------------------------------------------------//

  void Apc_fl_waveedit::draw_debug_line              ()
  {
    for(ulong i=0; i<5; ++i) {
      for(ulong ii=0; ii<120; ++ii)
        cerr << "*";
      cerr << endl;
    }
  }




//----------------------------------------------------------------------------//

  int Apc_fl_waveedit::shortcut                   (int arg)
  {
    int state = Fl::event_state();
    if((state&=FL_ALT) == FL_ALT) // handle 'ALT'-Shortcuts
    {
      switch(Fl::event_key()) // which key was pressed in combination with ALT?
      {
        case 'd' : this->dumpDrawData(); return 1;
        case '1' : dcalc->dump(); return 1;
        case '2' : this->data_changed(); this->doRedraw(); return 1;
        case '3' : if(use_old_drawcode)
                   {
                     cerr << "using new drawcode\n";
                     use_old_drawcode=false;
                   }
                   else
                   {
                     cerr << "using old drawcode\n";
                     string msg = "";
                     msg = string("You switched to the old, slow drawcode. On\n") +
                           string("big wavefiles, this code is somewhat slow.\n") +
                           string("You can switch to the new code with ALT+\"3\".");
                     if(!Commandline::check_for("0,no-bother"))
                       Popups::error(msg,"Warning");
                     else
                       cerr << msg << endl;
                     use_old_drawcode=true;
                   }
                   doRedraw();
                   return 1;
        case '4' : dcalc->resize_vec(); return 1;
        case '5' : dcalc->dump_virtual(); return 1;
        case '6' : dcalc->dump_values(); return 1;
        case '0' : this->draw_debug_line(); return 1;
        default  : return 0; // '0' means, event is still unhandled.
      }
    }

    return 0;
  }




//----------------------------------------------------------------------------//

  void Apc_fl_waveedit::data_changed()
  {
    this->waveSize = editwindow->waveSize;
    return dcalc->data_changed();
  }



//----------------------------------------------------------------------------//

  void Apc_fl_waveedit::apply_colors()
  {
    linecolor    = Settings::SELECTED_WAVEDATA_COLOR;
    backcolor    = Settings::SELECTED_BACKGROUND_COLOR;
    selcolor     = linecolor;
    selbgcolor   = backcolor;
    unselcolor   = Settings::UNSELECTED_WAVEDATA_COLOR;
    unselbgcolor = Settings::UNSELECTED_BACKGROUND_COLOR;
    zerocolor    = Settings::NULL_LINE_COLOR;
    rastercolor  = Settings::RASTER_COLOR;
    cursorcolor  = Settings::CURSOR_COLOR;
    this->doRedraw();
  }


//----------------------------------------------------------------------------//

  void Apc_fl_waveedit::set_format(const int& _frequency,
                                   const int& _channels,
                                   const int& _bitRate)
  {
    this->frequency = _frequency;
    this->channels  = _channels;
    this->bitRate   = _bitRate;
  }



//----------------------------------------------------------------------------//

  int Apc_fl_waveedit::ms2pix(int ms)
  {
    ms /= frameSize;
    ms = static_cast<int>((frequency/1000.0) * ms);
    // ATTENTION!!! '(ms*width)' can cause an overflow!
    // therefore 'ms' is outside the brackets!!!
    double screenX   = ms*((width*1.0)/visualFrames*frameSize);
    return static_cast<int>(screenX/**pix_per_sample*/);
  }


//----------------------------------------------------------------------------//

  int Apc_fl_waveedit::pix2ms(int pix)
  {

    double ms = (pix * frameSize) / ((frequency/1000.0) * (width*1.0)/visualFrames * frameSize);
    return static_cast<int>(ms);
  }



//----------------------------------------------------------------------------//

  int Apc_fl_waveedit::frame2ms(int frame)
  {
    return 1000*static_cast<int>(frame/frequency);
  }




//----------------------------------------------------------------------------//






} // end of namespace

// end of file


