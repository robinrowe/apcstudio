// $Id: globals.h,v 1.38 2002/02/18 17:15:23 martinhenne Exp $

// (C) 2001 Martin Henne
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

// This file contains global declarations for 'aPcStudio'

// Define this, if you compile on windows plattforms (mingw/cygwin).
//#define WIN32

#ifndef _globals_h
#define _globals_h

#include <FL/Fl.H>
#include <cstddef>
#include <string>

typedef int RESULT;
typedef unsigned char    uchar;
//typedef unsigned char    byte;
typedef unsigned long    ulong;
typedef unsigned short   ushort;

class Globals
{
public:
  static  Fl_Cursor mouse;
  static  const char* pcVERSION;
  static  std::string sVERSION;
  static  std::string WEB_URL;
  static  const char* CTIME; // compile time
  static  const char* CDATE; // compile date

  /// Short version of commandline arguments, that have a value.
  static  std::string CLEXCLUDE;

  // debuglevel bitmask
  // first eigth bits are reserved for beta-state

  /// DEBUG: mouse dragging stuff
  static int DBG_DRAG;
  /// DEBUG: Message, if aPcStudio tried to write beyond a field index.
  static  int DBG_SEGFAULT_CAUGHT;
  /// DEBUG: Heavy output in while loops.
  static  int DBG_WHILE_LOOPS;
  /// DEBUG: An output for each for-loop.
  static  int DBG_FOR_LOOPS;
  /// DEBUG: Drawcalc low debugging
  static  int DBG_DRAWCALC_LOW;
  /// DEBUG: Drawcalc heavy debugging
  static  int DBG_DRAWCALC_HIGH;
  /// DEBUG: follow functions to find the one, that causes the crash.
  static  int DBG_FUNCTION_TRACE;
  /// DEBUG: dump values before and after function calls and so on...
  static  int DBG_NORMAL;
  /// DEBUG: Default debugging.
  static  int DEFAULT_DEBUG_LEVEL;

  // all but loops: 14592
  // function trace & segfaults: 8448

  static  int NO_WRITE_ACCESS;
  static  int NO_READ_ACCESS;
  static  int OUT_OF_MEMORY;
  static  int NO_FILE_SPECIFIED;
  static  int OTHER_ERROR;
  static  int SUCCESS;

};

#endif


