// convert.h

// OUTDATED!!! USE streamcast.h INSTEAD !!!

// (c) 2001 Martin Henne
// Martin.Henne@web.de
// GNU Library General Public License

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

// This class is for common convert tasks, mainly for converting
// string to int and vice versa. Please just use the stream_cast<T>
// function, for the others are outdated.
//
// Martin

#ifndef _CONVERT_H
#define _CONVERT_H

#include <globals.h>
#include <string>
using std::string;
#include <strstream>
using std::strstream;
//using std::stream;
#include <iostream>
using std::cerr;

class Convert
{
public:
  Convert& operator = (const Convert &arg);
  Convert ();
  ~Convert ();

  int string2int (string); // old, use stream_cast instead
  string int2string (int); // old, use stream_cast instead

  // convert any value (double/float/int) to string and back
  // must be inline due to compiler limitations
  template <typename T, typename S>
  T stream_cast(S const& val)
  {
    strstream stream;
    stream << val;
    T rc;
    stream >> rc;
    return rc;
  }
};


#endif
