// $Id: askformat.h,v 1.4 2001/12/16 14:22:37 martinhenne Exp $

// (C) 2001 Martin Henne
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.


#ifndef _askformat_h
#define _askformat_h

#include <globals.h>
#include <iostream>
#include <wavefile.h>
#include <fmtchunk.h>
/*
#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Check_Button.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Box.H>
*/
#include <guiheaders.h>

namespace APC
{
using namespace std;
  /// This class brings up a window that askes for bitrate, channels and so on...
  /**
    * It directly writes the values into the given Wavefile.
    */
  class Askformat
  {
  public:
    /// callback for the 'OK' Button
    static void cb_ok     (Fl_Button*, void*);

    /// callback for the 'Cancel' Button
    static void cb_cancel (Fl_Button*, void*);

    /// callback for 'Telephone'
    static void cb_telephone (Fl_Button*, void*);

    /// callback for 'radio'
    static void cb_radio (Fl_Button*, void*);

    /// callback for 'cd'
    static void cb_cd (Fl_Button*, void*);

    /// This is the memberfunction, that should be called (Askformat::ask(wfile);)
    static bool ask (Wavefile* wave);

  private:
    Askformat();
    static Wavefile*  w;

    static Fl_Window* win;

    // check buttons
    static Fl_Check_Button* _8bit;
    static Fl_Check_Button* _16bit;
    static Fl_Check_Button* _11khz;
    static Fl_Check_Button* _22khz;
    static Fl_Check_Button* _44khz;
    static Fl_Check_Button* _mono;
    static Fl_Check_Button* _stereo;

    // buttons
    static Fl_Button*       ok;
    static Fl_Button*       cancel;

    // quality presets
    static Fl_Button*       _telephone;
    static Fl_Button*       _radio;
    static Fl_Button*       _cd;

    // labels
    static Fl_Box*          bitLabel;
    static Fl_Box*          speedLabel;
    static Fl_Box*          channelsLabel;

    // leave if all is done
    static bool             done;
    static bool             returnval;
 };

}

#endif

