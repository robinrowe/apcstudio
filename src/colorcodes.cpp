// colorcodes.cpp 
// Created by Robin Rowe 2020-05-19
// License GPL open source

#include <iostream>
using namespace std;

void Usage()
{	cout << "Usage: colorcodes " << endl;
}

enum
{	ok,
	invalid_args

};

int main(int argc,char* argv[])
{	cout << "colorcodes starting..." << endl;
	if(argc < 1)
	{	Usage();
		return invalid_args;
	}

	cout << "colorcodes done!" << endl;
	return ok;
}
