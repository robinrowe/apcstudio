// $Id: fmtchunk.cpp,v 1.9 2001/12/16 14:22:37 martinhenne Exp $

// (C) 2001 Martin Henne
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

// This is the 'fmt '-chunk of a wavefile


#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>
#include <vector>
#include <fmtchunk.h>
#include <globals.h>
using namespace std;

#undef get

namespace APC
{
using std::cerr; using std::endl;
  // construct, destruct

  /* set default values */
  Fmt_chunk::Fmt_chunk() : ID("fmt ")
  {
    //cerr << "Fmt_chunk: constructing\n";
    file               = "(unset)";
    //ID                 = "fmt ";
    chunkSize          = 16;
    wFormatTag         = 1;
    wChannels          = 2;
    dwSamplesPerSec    = 44100;
    dwAvgBytesPerSec   = 44100*2*2; // samplespersec * channels * (bitspersample/8)  
    wBlockAlign        = 2*(16/8);  // channels * (bitspersample/8)
    wBitsPerSample     = 16;
    //cerr << "Fmt_chunk: constructed\n";
  }

  /* get vals from file */
  Fmt_chunk::Fmt_chunk(const string filename) : ID("fmt ")
  {
    //cerr << "Fmt_chunk(string): constructing\n";
    this->file = filename;
    this->read(this->file);
    //cerr << "Fmt_chunk(string): constructed\n";
  }

  Fmt_chunk::~Fmt_chunk()
  {
    //std::cerr << "Fmt_chunk: destroyed\n";
  }

/*########################################################################################################*/
 
  // setting up the fmt-chunk values
  void Fmt_chunk::setchunkSize        (const long           arg)
  {
    if (arg>=16)
      chunkSize = arg;
    else
    {
      std::cerr << "Fmt_chunk::setchunkSize(): wrong value: "
                << arg << ". Using default (16)!" << endl;
      chunkSize = 16;
    }
  }

  void Fmt_chunk::setwFormatTag       (const short          arg)
  {
    if (arg != 1)
      std::cerr << "Fmt_chunk::setwFormatTag(): wrong value: "
                << arg << ". Compressed data not supported. Using default (1)!" << endl;
    wFormatTag = 1;
  }

  void Fmt_chunk::setwChannels        (const unsigned short arg)
  {
    if (arg >= 1 && arg <= 6 )
      wChannels = arg;
    else
    {
      std::cerr << "Fmt_chunk::setwChannels(): wrong value: "
                << arg << ", using default (2)!" << endl;
      wChannels = 2;
    }
  }

  void Fmt_chunk::setdwSamplesPerSec  (const unsigned long  arg)
  {
    if (arg == 44100 ||
        arg == 22050 ||
        arg == 11025)
    {
      dwSamplesPerSec = arg;
    }
    else
    {
      std::cerr << "Fmt_chunk::setdwSamplesPerSec(): unusual value: "
                << arg << ", using it anyway." << endl;
      dwSamplesPerSec = arg;
    }
  }

  void Fmt_chunk::setdwAvgBytesPerSec (const unsigned long  arg)
  {
    // I deleted all tests here. As I expected, they
    // produced wrong values, because not every value
    // that are needed to calculate 'dwAvgBytesPerSec'
    // are set, when this method is called. (mh)
    dwAvgBytesPerSec = arg;
  }

  void Fmt_chunk::setwBlockAlign      (const unsigned short arg)
  {
    wBlockAlign = arg;
  }

  void Fmt_chunk::setwBitsPerSample   (const unsigned short arg)
  {
    if(arg == 8 || arg == 16 || arg == 24 || arg == 32)
      wBitsPerSample = arg;
    else
    {
      std::cerr << "Fmt_chunk::setwBitsPerSample(): EVIL value: "
                << arg << ", using it anyway!" << endl;
      wBitsPerSample = arg;
    }
  }

/*########################################################################################################*/
 
  int Fmt_chunk::write ()
  {
    if (this->file=="(unset)")
    {
      std::cerr << "Fmt_chunk::write(): I don't know which file to write to :-(\n";
      exit(1);
    }
    else
      return this->write(this->file);
  }

/*########################################################################################################*/

  int Fmt_chunk::write (const string afile) const  // to a file
  {
    //cerr << "Fmt_chunk::write(string): entered!\n";
    int IDpos;                      // Position of the chunkID
    IDpos = findIDpos(afile);

    unsigned short tuShort;         // temporary unsigned short
    unsigned long  tuLong;          // temporary unsigned long

     //check if file exists and can be opened
    ifstream inFile;
    inFile.open(afile.c_str(), ios::in|ios::out|ios::binary);
    if(!inFile.is_open()) // file doesn't exist!
    {
      inFile.close();
      std::cerr << "Fmt_chunk::write(...): cannot write file " << afile << endl;
      return Globals::NO_WRITE_ACCESS;
    }

    if (IDpos == Globals::OTHER_ERROR)
    {
      cerr << "Fmt_chunk::write(...): couldn't get 'fmt '-ID!\n";
      inFile.close();
      return IDpos;
    }
/*
    if(getchunkSize(afile) < 16)
    {
      inFile.close();
      cerr << "Fmt_chunk::write(...): chunkSize to small. Don't wanna overwrite some information!\n";
      return OTHER_ERROR;
    }
*/
    // TODO: remove this open-close-open - nonsense
    inFile.close(); // tests made. now open for writing!
    fstream oFile;
    oFile.open(afile.c_str(), ios::in|ios::out|ios::binary);

    // TODO: remove test number 142352435. All is tested already
    if (!oFile.is_open())
    {
      cerr << "Fmt_chunk::write(...): could not open file " << afile << endl;
      return Globals::OTHER_ERROR;
    }
    oFile.seekp(IDpos+4);

    // chunkSize
    tuLong   =             chunkSize;
    tuLong  &= 0x000000ff;                       oFile.put(static_cast<char>(tuLong));
    tuLong   =             chunkSize;
    tuLong  &= 0x0000ff00; tuLong >>=(8);        oFile.put(static_cast<char>(tuLong));
    tuLong   =             chunkSize;
    tuLong  &= 0x00ff0000; tuLong >>=(16);       oFile.put(static_cast<char>(tuLong));
    tuLong   =             chunkSize;
    tuLong  &= 0xff000000; tuLong >>=(24);       oFile.put(static_cast<char>(tuLong));

    // wFormatTag
    oFile.put(1);
    oFile.put(0);

    // wChannels
    tuShort  =             getwChannels();
    tuShort<<=(8) ;        tuShort>>=(8) ;       oFile.put(static_cast<char>(tuShort));
    tuShort  =             getwChannels();
                           tuShort>>=(8) ;       oFile.put(static_cast<char>(tuShort));
    // dwSamplesPerSec
    tuLong   =             getdwSamplesPerSec();
    tuLong<<=(24) ;        tuLong>>=(24) ;       oFile.put(static_cast<char>(tuLong));
    tuLong   =             getdwSamplesPerSec();
    tuLong<<=(16) ;        tuLong>>=(24) ;       oFile.put(static_cast<char>(tuLong));
    tuLong   =             getdwSamplesPerSec();
    tuLong<<=(8)  ;        tuLong>>=(24) ;       oFile.put(static_cast<char>(tuLong));
    tuShort  =             getdwSamplesPerSec();
                           tuLong>>=(24) ;       oFile.put(static_cast<char>(tuLong));

    // dwAvgBytesPerSec
    tuLong   =             getdwAvgBytesPerSec();
    tuLong<<=(24) ;        tuLong>>=(24) ;       oFile.put(static_cast<char>(tuLong));
    tuLong   =             getdwAvgBytesPerSec();
    tuLong<<=(16) ;        tuLong>>=(24) ;       oFile.put(static_cast<char>(tuLong));
    tuLong   =             getdwAvgBytesPerSec();
    tuLong<<=(8)  ;        tuLong>>=(24) ;       oFile.put(static_cast<char>(tuLong));
    tuShort  =             getdwAvgBytesPerSec();
                           tuLong>>=(24) ;       oFile.put(static_cast<char>(tuLong));

    // wBlockAlign
    tuShort  =             getwBlockAlign();
    tuShort<<=(8) ;        tuShort>>=(8) ;       oFile.put(static_cast<char>(tuShort));
    tuShort  =             getwBlockAlign();
                           tuShort>>=(8) ;       oFile.put(static_cast<char>(tuShort));

    // wBitsPerSample
    tuShort  =             getwBitsPerSample();
    tuShort<<=(8) ;        tuShort>>=(8) ;       oFile.put(static_cast<char>(tuShort));
    tuShort  =             getwBitsPerSample();
                           tuShort>>=(8) ;       oFile.put(static_cast<char>(tuShort));
 
    oFile.close();
    //cerr << "Fmt_chunk::write(...): leaving!\n";
    return Globals::SUCCESS;
  } // end of ::write(*)

/*########################################################################################################*/

  int Fmt_chunk::read  ()
  {
    //std::cerr << "Fmt_chunk::read(): called\n";
    /* exit if no file is passed to 'read()' */
    if (this->file=="(unset)")
    {
      std::cerr << "Fmt_chunk::read(): I don't know which file to read from :-(\n";
      return Globals::NO_FILE_SPECIFIED;
    }
    else
      return this->read(this->file);
  }

/*########################################################################################################*/

  int Fmt_chunk::read  (const string afile)        // from a file
  {
    //cerr << "Fmt_chunk::read(string): entering\n";
    this->file = afile;            // assigning a file;
    char tmpChar='A';     // to read the Bytes
    int filePos = 0;               // count read Bytes
 //   void* generic;                 // to recieve the return value of istream::get(char)
    string sid;                    // string-version of char* id;

    // open the wave file
    std::ifstream* aFile = new std::ifstream;
    aFile->open(this->file.c_str(), ios::in);
    if (!aFile->is_open())
    {
      std::cerr << "Fmt_chunk::read(string): could not open file: "
                << this->file << endl;
      delete aFile;
      aFile=NULL;
      return Globals::NO_READ_ACCESS;
    }

    chunkSize = getchunkSize(afile);
    filePos   = findIDpos(afile)+8;
    if (filePos-8 == Globals::OTHER_ERROR)
    {
      cerr << "Fmt_chunk::read(...): couldn't find chunkID in file "
           << afile << endl;
      return Globals::OTHER_ERROR;
    }

    aFile->seekg(filePos);

    /* now read the rest of the chunk */
    vector<unsigned short> chunkVals(chunkSize);
    for(int i=0; i<chunkSize; i++)
    {
      //generic = 
        aFile->get(tmpChar); 
        filePos++;/*std::cerr << tmpChar << ", ";*/ 
      chunkVals[i]=abs(tmpChar);
      //std::cerr << "Fmt_chunk::read(): chunkVals[" << i << "] = " << chunkVals[i] << endl;
    }

    /* chunk completely read. so now calculate the important values and set them. */
    setwFormatTag        ( chunkVals[1] *0x100 +
                           chunkVals[0] ); //std::cerr << "wFormatTag       = " << wFormatTag << endl;
    setwChannels         ( chunkVals[3] *0x100 + 
                           chunkVals[2] ); //std::cerr << "wChannels        = " << wChannels << endl;
    setdwSamplesPerSec   ( chunkVals[7] *0x1000000 +
                           chunkVals[6] *0x10000 +
                           chunkVals[5] *0x100 +
                           chunkVals[4] ); //std::cerr << "dwSamplesPerSec  = " << dwSamplesPerSec << endl;
    setdwAvgBytesPerSec  ( chunkVals[11]*0x1000000 +
                           chunkVals[10]*0x10000 +
                           chunkVals[9] *0x100 +
                           chunkVals[8] ); //std::cerr << "dwAvgBytesPerSec = " << dwAvgBytesPerSec << endl;
    setwBlockAlign       ( chunkVals[13]*0x100 +
                           chunkVals[12]); //std::cerr << "wBlockAlign      = " << wBlockAlign << endl;
    setwBitsPerSample    ( chunkVals[15]*0x100 +
                           chunkVals[14]); //std::cerr << "wBitsPerSample   = " << wBitsPerSample << endl;

    /* format chunk read, everything set. nothing left to do */
    aFile->close();
    delete aFile;
    aFile = NULL;
    //cerr << "Fmt_chunk::read(...): leaving\n";
    return Globals::SUCCESS;
  } // end of '::read(string)'

/*########################################################################################################*/

  int Fmt_chunk::findIDpos          (const string arg) const
  {
    int pos = -1;
    char tmpChar = 'A';
    ifstream ifs;
    ifs.open(arg.c_str(), ios::binary);

    /* check if file exists and if it is readable */
    if (!ifs.is_open()) 
    {
      std::cerr << "Fmt_chunk::findIDpos(): couldn't open file "
                << arg << endl;
      return pos; // is '-1' in this case
    }

    /* look for 'fmt ' in the whole file */
    while(!ifs.eof())
    { 
      ifs.get(tmpChar);
      if(tmpChar=='f')
      {
        ifs.get(tmpChar);
        if(tmpChar=='m')
        {
          ifs.get(tmpChar);
          if(tmpChar=='t')
          { 
            ifs.get(tmpChar);
            if(tmpChar==' ') // found 'fmt ' !!
            {
              pos = (ifs.tellg());
               pos -= 4;
              //std::cerr << "Fmt_chunk::findIDpos(): found 'fmt ' at pos "
              //          << pos << endl;
              break;
            }
          }
        }
      }
    } // end of while
 
    return pos; // pos = -1 if 'fmt ' not found !!!
  } // end of findIDpos(*)

/*#######################################################################################################*/

  int Fmt_chunk::getchunkSize             (const string filename) const
  {
    //cerr << "entering 'Fmt_chunk::getchunkSize(const string)\n";
    int tmpSize                = 0;
    char tmpChar      = 'A';
    ifstream ifs;
    int fmtPos                 = findIDpos(filename);

    /* check if ID was found */
    if (fmtPos == Globals::OTHER_ERROR)
    {
      cerr << "Fmt_chunk::getchunkSize(*): couldn't get Position of 'fmt ' chunk\n";
      return fmtPos;
    }

    /* check if file exists and if it is readable */
    ifs.open(filename.c_str(), ios::binary);
    if (!ifs.is_open())
    {
      cerr << "Fmt_chunk::getchunkSize(const string): couldn't open file "
           << filename << endl;
      //cerr << "leaving '::getchunkSize(const string)\n";
      return Globals::OTHER_ERROR;
    }

    /* read next 4 bytes after chunkID and do the calculations */
    ifs.seekg(fmtPos+4);
    /* add next four byte, which are in little endian order to a long double */
    ifs.get(tmpChar); tmpSize+=abs(tmpChar) * 0x1;      
    ifs.get(tmpChar); tmpSize+=abs(tmpChar) * 0x100;    
    ifs.get(tmpChar); tmpSize+=abs(tmpChar) * 0x10000;  
    ifs.get(tmpChar); tmpSize+=abs(tmpChar) * 0x1000000;

    ifs.close();
    //cerr << "leaving '::getchunkSize(const string)\n";
    return tmpSize;
  } // end of '::getchunkSize(const string)'

/*########################################################################################################*/

} // end of namespace APC

// operator overloading '<<'
std::ostream& operator<<(std::ostream& s, const APC::Fmt_chunk& f)
{
  return s << "ID                 : '" << f.getID               () << "'" << endl
           << "chunkSize          : "  << f.getchunkSize        ()        << endl
           << "wFormatTag         : "  << f.getwFormatTag       ()        << endl
           << "wChannels          : "  << f.getwChannels        ()        << endl
           << "dwSamplesPerSec    : "  << f.getdwSamplesPerSec  ()        << endl
           << "dwAvgBytesPerSec   : "  << f.getdwAvgBytesPerSec ()        << endl
           << "wBlockAlign        : "  << f.getwBlockAlign      ()        << endl
           << "wBitsPerSample     : "  << f.getwBitsPerSample   ()        << endl;
}

// end of file

