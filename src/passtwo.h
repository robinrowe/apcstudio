// $Id: passtwo.h,v 1.2 2001/11/06 14:58:36 martinhenne Exp $

// (C) 2001 Martin Henne
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

/*
 *  This is a simple struct, that can be used to pass two
 *  generic pointers (void*) to any function. I needed this
 *  to get proper acces from various callbacks to more then
 *  one widget.
 *
 */

#ifndef _passtwo_h
#define _passtwo_h

#include <globals.h>

namespace APC
{

  struct Passtwo
  {
    void* arg1;
    void* arg2;
  };

}

#endif

