// $Id: fmtchunk.h,v 1.6 2001/12/16 14:22:37 martinhenne Exp $

// Copyright 2001 Martin Henne 2001
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.


/*
 * This class should be used inside a 'wave'-class
 * for it represents a part of a wavefile. the
 * format chunk ('fmt ') is that part of a wavefile,
 * that containes the information of the format of
 * the wavedata, like bitrate and samplerate (see below).
 *
 */

/*
 * ID:
 * must be allways 'fmt '
 *
 * chunkSize:
 * the length of the whole chunk in bytes WITHOUT
 * the first 8 bytes containing 'ID' and 'chunkSize'.
 * Usually something round about 16 (means 16 bytes
 * is the length of the chunk). To calculate that
 * value: chunkSize = sizeOf(wFormatTag)+sizeOf(vChannels)
 * +sizeOf(dwSamplesPerSecond)+sizeOf(dwAwgBytesPerSecond)
 * +sizeOf(wBlockAlign)+sizeOf(wBitsPerSample);
 *
 * wFormatTag:
 * if it is '1', data is stored uncompressed. This class
 * doesn't support compressed WAV-Format, because there
 * is no unique standard. If any other value than 1,
 * then 'data'-chunk uses some weird kind of compression.
 *
 * wChannels:
 * contains the number of audio channels.
 * 1 = mono, 2 = stereo
 * 4 and 6 are also valid values for rich audio freaks
 *
 * dwSamplesPerSecond:
 * Unit: Hertz (44.1 kHz, 22.05 kHz...)
 * this should be 44100 for CD-quality
 * valid values: 44100Hz, 22050hz, 11025Hz
 * other values are allowed but unusual and evil
 *
 * dwAvgBytesPerSec:
 * calculated: wChannels * (wBitsPerSample/8) * wSamplesPerSec
 * Nothing more needs to be mentioned here
 *
 * wBlockAlign:
 * calculate this value with the following formula:
 * " wChannels * (wBitsPerSample/8); "
 * This value is the size of a sample frame in
 * bytes. it is 2 byte for a 16 bit mono sample,
 * 4 bytes for a 16 bit stereo sample and so on...
 *
 * wBitsPerSample:
 * Bitresolution of each sample point. use 16 for
 * CD-quality. good values are a multiple of 8, like
 * 8, 16, 24. theoretically, other values are valid,
 * but please don't use them. 8 and 16 should do fine.
 *
 */

#ifndef _fmtchunk_h
#define _fmtchunk_h


#include "globals.h"
#include <string>

namespace APC
{
using namespace std;

class Wavefile;

  class Fmt_chunk
  {
  //friends
  friend class Wavefile;

  private:
    string          file;                // optional       default: '(unset)'

    // format chunk content from here
    string          ID;                  // always 'fmt '  default: 'fmt '
    long            chunkSize;           // 4 Bytes        default: 16
    short           wFormatTag;          // 2 Bytes        dafault: 1
    unsigned short  wChannels;           // 2 Bytes        default: 2
    unsigned long   dwSamplesPerSec;     // 4 Bytes        default: 44100
    unsigned long   dwAvgBytesPerSec;    // 4 Bytes        default: 44100*2*(16/8)
    unsigned short  wBlockAlign;         // 2 bytes        default: 2*(16/8)
    unsigned short  wBitsPerSample;      // 2 Bytes        default: 16
    /* if there will ever be data below this, please adjust 'set/getchunkSize()' methods */  
 
  public:
    // construct, destruct
    Fmt_chunk(); // sets all numbers to default and all char* to '(unset)'
    Fmt_chunk(string filename); // calls 'read(char*)'
    ~Fmt_chunk();
   
    // reading the attributes
    string          getfile               () const {return file;}
    const string    getID                 () const {return "fmt ";}
    long            getchunkSize          () const {return chunkSize;} // OVERLOADED!
    short           getwFormatTag         () const {return wFormatTag;}
    unsigned short  getwChannels          () const {return wChannels;}
    unsigned long   getdwSamplesPerSec    () const {return dwSamplesPerSec;}
    unsigned long   getdwAvgBytesPerSec   () const {return dwSamplesPerSec*wBlockAlign;}
    unsigned short  getwBlockAlign        () const {return (wChannels*(wBitsPerSample/8));}
    unsigned short  getwBitsPerSample     () const {return wBitsPerSample;}

    unsigned short  channels              () {return getwChannels();}
    unsigned long   frequency             () {return getdwSamplesPerSec();}
    unsigned short  bitrate               () {return getwBitsPerSample();}

    // setting attributes for writing a new wave
    void            setfile               (string                arg) {file=arg;}
    void            setID                 (string                arg) {/* do nothing */}
    void            setchunkSize          (const long            arg);
    void            setwFormatTag         (const short           arg);
    void            setwChannels          (const unsigned short  arg);
    void            setdwSamplesPerSec    (const unsigned long   arg);
    void            setdwAvgBytesPerSec   (const unsigned long   arg);
    void            setwBlockAlign        (const unsigned short  arg);
    void            setwBitsPerSample     (const unsigned short  arg);
   
    // some other useful functions
    int  write                       (const string arg) const;   // write the chunk to a file
    int  write                       ();                         // as above, uses 'file'
    int  read                        (const string arg);         // read the chunk from a file
    int  read                        ();                         // as above, uses 'file'
    int  findIDpos                   (const string arg) const;   // get position of 'fmt ' within wavefile
    int  getchunkSize                (const string arg) const;   // OVERLOADED! get length of chunk in bytes
  };
}

std::ostream& operator<<(std::ostream&, const APC::Fmt_chunk&);

#endif


