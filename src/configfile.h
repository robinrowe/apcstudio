// $Id: configfile.h,v 1.11 2001/12/16 14:22:37 martinhenne Exp $

// (C) 2001 Martin Henne
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

/*
 * This class reads the configfile and assignes
 * the specified values...
 *
 */

#ifndef _configfile_h
#define _configfile_h

#include <globals.h>
#include <string>
#include <filehandle.h>
#include <convert.h>
#include <globals.h>

namespace APC
{
using namespace std;
  /// This reads values from a configfile.
  /**
   * The values must be of this syntax:
   * VALUENAME = value
   * '#' is considered to be a comment and therefore ignored.
   * 'Configfile' makes heavily use of my older class 'FileHandle'.
   * If something is buggy, have a look at those files, too.
   */
  class Configfile
  {
  public:
    Configfile() {}
    void read()
    {}

    // get value of 'VAL' in CONFIGFILE and pass it to 'ret'
    // use it like this:

    /*
     *  int bgcolor = 0;
     *  Configfile::get("BACKGROUNDCOLOR",bgcolor); // bgcolor holds the int-value now
     */

    /// This returns the filename of the configfile (for internal use). Read more.
    /**
      * Filename is 'CONFIGFILE' in Header (default: apcstudiorc)
      * ::file() then looks (in that order) for:
      *
      *   1.  ./apcstudiorc
      *   2.  ~/.apcstudiorc
      *   3.  /etc/apcstudiorc
      *
      */
    static string file();

    /// Read the integer value 'VAL' and pass it to 'ret'
    static void get(const char* const VAL, int&    ret);

    /// Read the string value 'VAL' and pass it to 'ret'
    static void get(const char* const VAL, string& ret);

    /// return the boolean of VAL within 'ret'
    /**
     * In the configfile itself, true means:
     *   ja, yes, true, 1, ON, on
     * All other values are returned as false.
     */
    static void get(const char* const VAL, bool& ret);

    /// Set VAL to arg.
    static void set(const char* const VAL, const string& arg);
    static void set(const char* const VAL, const int& arg);
    static void set(const char* const VAL, const bool& arg);
  };

}

#endif

// end of file

