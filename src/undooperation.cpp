// $Id: undooperation.cpp,v 1.3 2002/02/15 23:53:41 martinhenne Exp $

// Copyright 2001 Martin Henne 2001
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

#include <globals.h>
#include <undooperation.h>
#include <editwindow.h>
#include <datachunk.h>
#include <frame2time.h>
#include <vector>
#include <string>
#include <sleeper.h>

namespace APC
{
  Undo_operation::Undo_operation(const int& _frequency,
                   const int& _channels,
                   const int& _bitRate,
                   const ulong& _markStart,
                   const ulong& _markEnd,
                   const ulong& _drawStart,
                   const ulong& _cursorPosition,
                   const double& _zoomFactor,
                   const vector<uchar>& _old_data)
  {
    undo_data = new vector<uchar>;
    undo_data->assign(_old_data.begin(), _old_data.end());
    frequency      = _frequency;
    channels       = _channels;
    bitRate        = _bitRate;
    markStart      = _markStart;
    markEnd        = _markEnd;
    drawStart      = _drawStart;
    cursorPosition = _cursorPosition;
    zoomFactor     = _zoomFactor;
  }

  bool Undo_operation::perform(Editwindow* ew)
  {
    Data_chunk& dc = ew->wfile->getdata();
    vector<uchar>& data = dc.getwave();
    data.resize(undo_data->size());
    data.assign(undo_data->begin(), undo_data->end());
    if(ew->frequency != this->frequency ||
       ew->channels  != this->channels  ||
       ew->bitRate   != this->bitRate)
    {
      ew->set_format(this->frequency, this->channels, this->bitRate);
    }
    ew->we->setdrawStart(0);
    ew->data_changed();
    ew->we->data_changed();
    ew->we->setzoomFactor(zoomFactor);
    ew->we->setcursorPosition(cursorPosition);
    ew->we->relabel_cursorPosition();
    ew->we->setmarkStart(0);
    ew->we->setmarkEnd(markEnd);
    ew->we->setmarkStart(markStart);
    ew->we->setdrawStart(drawStart);
    ew->adjustScrollbar();
    ew->redraw_needed = ew->REDR_ALL;
    ew->doRedraw();
    return true;
  }

} // end of namespace

// end of file


