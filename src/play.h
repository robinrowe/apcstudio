// $Id: play.h,v 1.17 2002/01/28 23:19:34 martinhenne Exp $

// (C) 2001 Martin Henne
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

/*
 *
 * This class provides playing capabilities using 3rd party libraries.
 * On win32 and linux SDL is the right choice.
 *
 */

#ifndef _play_h
#define _play_h

#include <globals.h>
#include <string>
#include <iostream>
#include <wavefile.h>
#include <fmtchunk.h>
#include <datachunk.h>
#include <configfile.h>
#include <apc_fl_waveedit.h>
#ifndef WIN32
#include <unistd.h>
#endif
  //------ activate ONLY ONE OF THEM! ------//

#ifdef WIN32
  const std::string chooseLib = "USE_SDL";
#endif

  //const string chooseLib = "USE_MIKMOD";
  //const string chooseLib = "USE_MFC";
  //const string chooseLib = "USE_DIRECTX";
  #ifndef WIN32
  const std::string chooseLib = "USE_OSS";
  #endif
  //const string chooseLib = "USE_ALSA";

  //------ end of 3rd party libraries ------//


namespace APC
{
using namespace std;
  /// This class playes a wavefile. It is OS-dependent!!!
  class Play
  {
  public:
    /// This is a handle to 'Apc_fl_waveedit'. You have to cast it from void*.
    void*     myHandle;

    /// constructor
    Play();

    /// indicates, whether we want to play only unselected data
    bool      play_unselected;

    /// indicates, if we are playing (or paused)
    static bool      playing;

    /// indicates, if we are playing and paused
    static bool      paused;

    /// A handle to the wavefile. Pass this value to play()!
    Wavefile* myWave;

    /// buffersize
    int       buf_size;

    /// the file desriptor of the opened audio stream
    int       audio_fd;

    /// How many samples per second?
    int       frequency;

    /// mono, stereo, quadro?
    int       channels;

    /// How many bits per sample (resolution)?
    int       bitrate;

    /// How many bytes has one sample frame?
    int       frameSize;

    /// Start of data to play. Pass this to play()!
    unsigned long playstart;
    /// End of data to play. Pass this to play()!
    unsigned long playend;

    /// The actual playing frame. Needed to update the cursor.
    unsigned long frame;

    /// Call this, to play a file.
    void      play         (Wavefile &wave, void*,
                            unsigned int start,
                            unsigned int end);      // call right play function,
                                                    // depending on 'chooseLib'

    /// This uses SDL Library to play sound. It's called by play().
    void         playSDL       (const Wavefile &wave, void*);

    /// This is called by playSDL();
    static void* playSDLThread (void*);

    /// This uses OSS/Free to play sound on linux/unix. It's called by play().
    void         playOSS       (Wavefile& wave, void*);

    /// This is called by playOSS();
    static void* playOSSThread (void*);


  }; // end of class 'Play'

} // end of namespace

// end of file


#endif

