// $Id: tips.cpp,v 1.4 2002/02/24 16:56:36 martinhenne Exp $

// (C) 2001 Martin Henne
// EMail: Martin.Henne@web.de

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// Visit 'http://www.gnu.org' to get more details.

// This class combines a Fl_Double_Window and a Apc_fl_waveedit widget
// with a Wavefile and provides several editing options. It is the actual
// main window of aPcStudio. Maybe there will never be another main
// windows, wich was planned first, but just another mainframe-class.

#include <tips.h>
#include <string>
#include <ctime>
#include <rand.h>
#include <globals.h>
#include <vector>

namespace APC
{
  using namespace std;

  ulong Tips::count = 0;

  Tips::Tips()
  {
    tips.push_back(string("Press PLAY-Button with SHIFT-Key, to play from cursor\nto the end of file!"));
    tips.push_back(string("Press PLAY+CTRL to hear only UNSELECTED data from the\ncursorposition on!"));
    tips.push_back(string("Move the cursor by using SHIFT and left mousebutton!"));
    tips.push_back(string("ALT+POS1 brings the cursor to the selection start!"));
    tips.push_back(string("ALT+END brings the cursor to the selection end!"));
    tips.push_back(string("aPcStudio is completely free for Linux users! Please support\nLinux."));
    tips.push_back(string("You can change almost all colors in Preferences-Window!"));
    tips.push_back(string("Switch off the raster lines permanently using Preferences\ndialogue! You can switch them temporary using the\nButton in the toolbar!"));
    tips.push_back(string("Check out our website for newer versions of aPcStudio: \nIt is http://apcstudio.sf.net"));
    tips.push_back(string("This time, the aPcStudio is quite unstable and only\nuseful for cutting songs, but more stuff is on the way!"));
    tips.push_back(string("The Noisegate-Function mutes any noise below a given\namplitute (in percent)."));
    tips.push_back(string("To apply a fade-in-effect, use the volume-ramp from the\n'amplitute-based-effects'-button."));
    tips.push_back(string("To apply a fade-out-effect, use the volume-ramp from the\n'amplitute-based-effects'-button."));
    tips.push_back(string("To tune volume up or down, use 'straight amplify'-tab\nfrom the 'amplitude-based-effects'-button!"));
    tips.push_back(string("<add more tips here>"));
    tips.push_back(string("You can select the whole wave by pressing CTRL+'A'"));
    tips.push_back(string("The Zoom-In-Shortcut is CTRL+'+' and you can zoom out\nby pressing CTRL+'-' !"));
    tips.push_back(string("To change only the selection end, use a single click\nwith the RIGHT mousebutton!"));
    tips.push_back(string("To change only the selection start, use a single click\nwith the LEFT mousebutton, but don't drag it!"));
    tips.push_back(string("Please send comments to the author:\nMartin.Henne@@web.de"));
    tips.push_back(string("To select a region, drag the left mousebutton!"));
    tips.push_back(string("Use the middle mousebutton to move the cursor!"));
    tips.push_back(string("Configure the behaviour of the selection after pasting\ndata in 'preferences'-dialog."));
    tips.push_back(string("Press F1 to read the online manual."));
    tips.push_back(string("Doubleclicking selects the whole wave file."));
    tips.push_back(string("aPcStudio suffers a lag of testers. Please send bugs or\ncrashes to apcstudio-bugs@@lists.sf.net !"));
  }

  string Tips::get()
  {
    unsigned long index = Rand::gen(0,tips.size()-1);
    if(index<tips.size())
      return tips[index];
    else
      return string("<your tip here>");
  }

  string Tips::get(const ulong& index)
  {
    if(index<tips.size())
      return tips[index];
    else
      return string("<your tip here>");
  }

  ulong Tips::size()
  {
    return tips.size();
  }



}

// end of file

